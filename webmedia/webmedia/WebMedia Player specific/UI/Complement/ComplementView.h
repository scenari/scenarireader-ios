//
//  ComplementView.h
//  Patrimoine
//
//  Created by soreha on 29/04/10.
//  
//

#import <UIKit/UIKit.h>


@interface ComplementView : UIView {
	UIWebView *content;
}
@property (nonatomic, strong) UIWebView *content;

-(void) urlRequestBlocString:(NSURLRequest *)urlRequestBlocString;
-(void) setContentString:(NSString *)string withModelName:(NSString*)modelName;
- (void) openAction;
- (void) closeAction;
- (void) setTitle:(NSString *)title;
- (void) updateOrientation:(UIDeviceOrientation) orientation duration:(NSTimeInterval)duration;

@end
