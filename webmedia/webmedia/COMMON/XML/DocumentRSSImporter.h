//
//  WebMediaRSSImporter.h
//  WebMediaTest
//
//  Created by soreha on 18/01/11.
//  
//

#import "DocumentRSSImporterDelegate.h"

@class Channel, CoreDataManager, DocumentRSSImporter, Document;

@interface DocumentRSSImporter : NSObject <NSXMLParserDelegate> {
	CoreDataManager *dataManager;
	Channel *channel;
	id <DocumentRssImporterDelegate> __unsafe_unretained delegate;
    NSMutableSet *availableDocumentsGuid;
    int pos;
}

@property (nonatomic, unsafe_unretained) id <DocumentRssImporterDelegate> delegate;
@property (nonatomic, strong) CoreDataManager *dataManager;
@property (nonatomic, strong) Channel *channel;
@property (nonatomic, strong) NSMutableSet *availableDocumentsGuid;
@property (nonatomic) int pos;

@property (nonatomic, strong) NSMutableString *currentContents;
@property (nonatomic, strong) Document *tmpDocument;
@property (nonatomic, strong) NSXMLParser *xmlparser;

-(id) initWithDataManager : (CoreDataManager *) dManager withChannel: (Channel *) ch;
-(void) loadXMLFromURL:(NSURL *)rssURL;
-(void) loadXMLFromData:(NSData *)data;

@end
