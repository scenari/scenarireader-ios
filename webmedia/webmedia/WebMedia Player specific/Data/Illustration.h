//
//  Illutration.h
//  PhoneRadio
//
//  Created by soreha on 27/07/10.
//  
//

#import <Foundation/Foundation.h>
#import "Resource.h"

extern NSString * const TYPE_IMAGE;
extern NSString * const TYPE_VIDEO;
extern NSString * const TYPE_ANIMATION;
extern NSString * const TYPE_TEXT;


@interface Illustration : NSObject {
	NSString *illustrationId;
	NSString *type;
	Resource *resource;
	NSString *text;
	
	float start;
	float end;
	
}

@property (nonatomic, strong) id parent;

@property(nonatomic, strong) NSString *illustrationId;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, strong) Resource *resource;
@property(nonatomic, strong) NSString *text;


@property(nonatomic) float start;
@property(nonatomic) float end;

- (BOOL)isImage;

@end
