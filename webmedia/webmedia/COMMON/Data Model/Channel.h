//
//  Channel.h
//  OptimGenericReader
//
//  Created by soreha on 14/02/11.
//  
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Document, Thumbnail;

@interface Channel : NSManagedObject {
@private
}
@property (nonatomic, strong) NSNumber * updated;
@property (nonatomic, strong) NSString * summary;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * copyright;
@property (nonatomic, strong) NSString * link;
@property (nonatomic, strong) NSDate * pubDate;
@property (nonatomic, strong) NSString * thumbnailURI;
@property (nonatomic, strong) NSDate * lastBuildDate;
@property (nonatomic, strong) NSString * rssUrl;
@property (nonatomic, strong) Thumbnail * thumbnail;
@property (nonatomic, strong) NSSet* documents;
@property (nonatomic, strong) NSNumber * downloadError;

@end

@interface Channel (CoreDataGeneratedAccessors)
- (void)addDocumentsObject:(Document *)value;
- (void)removeDocumentsObject:(Document *)value;
- (void)addDocuments:(NSSet *)value;
- (void)removeDocuments:(NSSet *)value;
@end
