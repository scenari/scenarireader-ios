//
//  ChannelViewController.m
//  OptimGenericReader
//
//  Created by soreha on 15/02/11.
//  
//

#import <QuartzCore/QuartzCore.h>

#import "ChannelsViewController.h"
#import "Channel.h"
#import "CoreDataManager.h"
#import "AddChannelViewController.h"
#import "DocumentsForChannelViewController.h"
#import "ChannelDetailViewController.h"
#import "AvailableDocumentsTableViewController.h"
#import "DownlodedDocumentsTableViewController.h"
#import "AboutViewController.h"
#import "ConfigurationManager.h"
#import "Thumbnail.h"


@interface ChannelsViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (UIButton *) makeDetailDisclosureButtonWithFrame:(CGRect)rect andImage:(UIImage *)img;
@end

@implementation ChannelsViewController

@synthesize fetchedResultsController;
@synthesize addChannelView;
@synthesize documentsForChannelView;
@synthesize defaultImage;

@synthesize mediaHandlerDelegate;

- (void)dealloc
{
    self.fetchedResultsController.delegate = nil;
    self.fetchedResultsController = nil;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad{
    [super viewDidLoad]; 
	
    
    //Test for particular cell views
    self.useChannelCellXib = [[ConfigurationManager sharedInstance] channelListWithDetails];
    if(self.useChannelCellXib){
        self.tableView.separatorColor = [UIColor colorWithWhite:1 alpha:0.9];
    }
    
    
    self.defaultImage = [UIImage imageNamed:@"scenari_small"];
	
    
    if([[ConfigurationManager sharedInstance] canAddSource]){
        UIBarButtonItem *addChannelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(opentAddChannelView)];
#pragma ACCESSIBILITY
        addChannelButton.accessibilityLabel = @"Ajouter une source de données.";
        self.navigationItem.rightBarButtonItem = addChannelButton;
	}
    
	
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    
	UIButton* modalViewButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
	[modalViewButton addTarget:self action:@selector(showAbout) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton];
#pragma ACCESSIBILITY
    modalButton.accessibilityLabel = @"Afficher les informations sur l'application Scenari Reader.";
	self.navigationItem.leftBarButtonItem = modalButton;
    
    
    if(SYSTEM_VERSION_LESS_THAN(@"7.0"))
        [self navigationController].navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
    else
        self.navigationItem.leftBarButtonItem.tintColor = self.navigationItem.rightBarButtonItem.tintColor = [UIColor blueColor];

    self.fetchedResultsController = 
    [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllChannel] 
                                        managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:nil 
                                                   cacheName:@"AllChannel"];
	self.fetchedResultsController.delegate = self;
    
    NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]){
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	   // abort();
	}
    
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView;
    
    if (footerView != nil)
        return footerView;
    
    NSString *footerText ;
    int n = [[self.fetchedResultsController fetchedObjects] count];
     if(n==0)
        footerText = @"Aucune source disponible";
    else if(n==1)
        footerText = @"1 source disponible";
    else footerText = [NSString stringWithFormat:@"%d sources disponibles",n];
    // set the container width to a known value so that we can center a label in it
    // it will get resized by the tableview since we set autoresizeflags
    float footerWidth = 300.0f;
    float padding = 5.0f; // an arbitrary amount to center the label in the container
    
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, footerWidth, 44.0f)];
    footerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // create the label centered in the container, then set the appropriate autoresize mask
    if(!self.footerLabel){
        self.footerLabel = [[UILabel alloc] initWithFrame:CGRectMake(padding, 0, footerWidth - 2.0f * padding, 44.0f)] ;
        self.footerLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        self.footerLabel.textAlignment = UITextAlignmentCenter;
    }
    [footerView addSubview:self.footerLabel];
    self.footerLabel.text = footerText;
    
    
    return footerView;
}

 - (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
     int n = [[self.fetchedResultsController fetchedObjects] count];
     if(n==0)
         return @"Aucune source disponible";
     if(n==1)
         return @"1 source disponible";
     return [NSString stringWithFormat:@"%d sources disponibles",n];
 }


-(void) showAbout{
	
	AboutViewController *aboutViewController = [[AboutViewController alloc] initWithNibName:UI_USER_INTERFACE_IDIOM()?@"AboutViewController_iPad":@"AboutViewController_iPhone" bundle:nil];
	UINavigationController *modalNav = [[UINavigationController alloc] initWithRootViewController:aboutViewController];
	
	//[[self navigationController] presentModalViewController:modalNav animated:YES];
   // aboutViewController.navigationController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];   ;
    if(SYSTEM_VERSION_LESS_THAN(@"7.0)")){
        aboutViewController.navigationController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];   ;
    }
    [[self navigationController] presentViewController:modalNav animated:YES completion:^{
    }];

}
- (void)viewDidUnload
{
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self.fetchedResultsController fetchedObjects] count] ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    float viewHeight = self.parentViewController.view.bounds.size.height;
    return viewHeight>480 ? [[ConfigurationManager sharedInstance] iPhone5ChannelCellHeight] : [[ConfigurationManager sharedInstance] iPhoneChannelCellHeight];//(self.useChannelCellXib?122+(viewHeight>480?30:0):54);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    float viewHeight = self.parentViewController.view.bounds.size.height;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:viewHeight>480 ? [[ConfigurationManager sharedInstance] iPhone5ChannelCellIdentifier] : [[ConfigurationManager sharedInstance] iPhoneChannelCellIdentifier]];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:viewHeight>480 ? [[ConfigurationManager sharedInstance] iPhone5ChannelCellIdentifier] : [[ConfigurationManager sharedInstance] iPhoneChannelCellIdentifier]];
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    Channel *channel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    UIImageView *cellImgView = (UIImageView *)[cell viewWithTag:10 ];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:20 ];
    UILabel *detailsLabel = (UILabel *)[cell viewWithTag:30 ];

    titleLabel.text = channel.title;
    if(self.useChannelCellXib){
        detailsLabel.text = [NSString stringWithFormat:@"%lu document(s) \n\n%@", (unsigned long)[channel.documents count], channel.summary];
	    cell.accessibilityLabel = [NSString stringWithFormat:@"%@ contient %lu documents. %@.",titleLabel.text,(unsigned long)[channel.documents count], channel.summary];

    }else{
        detailsLabel.text = [NSString stringWithFormat:@"%lu document(s)", (unsigned long)[channel.documents count]];
#pragma ACCESSIBILITY
        cell.accessibilityLabel = [NSString stringWithFormat:@"%@ contient %lu documents.",titleLabel.text,(unsigned long)[channel.documents count]];
    }
    if (channel.thumbnail == nil) {
		[cellImgView setImage:defaultImage];
        DLog(@"1 - channel.thumbnailURI : %@",channel.thumbnailURI);

        if (channel.thumbnailURI != nil || [channel.thumbnailURI compare:@""] != NSOrderedSame) {
            [[CoreDataManager sharedInstance] downloadThumbnailForChannel:channel];
            
        }else{
            DLog(@"---- No associated channel image");
            cellImgView.image = self.defaultImage;
        }
	} else {
        DLog(@"2 - channel.thumbnailURI : %@",channel.thumbnailURI);
		cellImgView.image = channel.thumbnail.image;
	}
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        [cell setAccessoryView:[self makeDetailDisclosureButtonWithFrame:CGRectMake(0, 0, 30, 30) andImage:[[ConfigurationManager sharedInstance] getAccessoryImage]]];
    }else{
        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }
#pragma ACCESSIBILITY
    cell.accessoryView.accessibilityLabel = [NSString stringWithFormat:@"Plus d'informations sur la source %@.",titleLabel.text];
    
}




- (UIButton *) makeDetailDisclosureButtonWithFrame:(CGRect)rect andImage:(UIImage *)img{
	UIButton *button = [[UIButton alloc] initWithFrame:rect];
	[button setImage:img forState:UIControlStateNormal];
	
	
	[button addTarget: self
			   action: @selector(accessoryButtonTapped:withEvent:)
	 forControlEvents: UIControlEventTouchUpInside];
	
	return ( button );
}

- (void) accessoryButtonTapped: (UIControl *) button withEvent: (UIEvent *) event	{
	NSIndexPath * indexPath = [self.tableView indexPathForRowAtPoint: [[[event touchesForView: button] anyObject] locationInView: self.tableView]];
	if ( indexPath == nil )
		return;
		
	if([self.tableView.delegate respondsToSelector:@selector(tableView:accessoryButtonTappedForRowWithIndexPath:)])
		[self.tableView.delegate tableView:self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
	// Navigation logic may go here. Create and push another view controller.
	
	ChannelDetailViewController *detailViewController = [[ChannelDetailViewController alloc] initWithNibName:@"DefaultDetailViewController" bundle:nil];
	[detailViewController setItems:[fetchedResultsController fetchedObjects] withCurrent:indexPath.row];
	
	// Pass the selected object to the new view controller.
	[self.navigationController pushViewController:detailViewController animated:YES];
}




// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    // Return NO if you do not want the specified item to be editable.
    return [[ConfigurationManager sharedInstance] canDeleteSource];
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		Channel *channel = [self.fetchedResultsController objectAtIndexPath:indexPath];
		[[CoreDataManager sharedInstance] deleteChannel:channel];
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	Channel *channel = [self.fetchedResultsController objectAtIndexPath:indexPath];
	
	documentsForChannelView = [[DocumentsForChannelViewController alloc] initWithNibName:@"DocumentsForChannel" bundle:nil];
    documentsForChannelView.channel = channel;
    
    [self.navigationController pushViewController:documentsForChannelView animated:YES];
    
    //documentsForChannelView.downlodedDocumentsTableViewController.mediaHandlerDelegate =self.mediaHandlerDelegate;
}


#pragma mark -
#pragma mark - Add Channel methods
-(void) opentAddChannelView {
	
	DLog();
	
	addChannelView = [[AddChannelViewController alloc] init];
    
    UINavigationController *control = [[UINavigationController alloc] initWithRootViewController:addChannelView];
	control.navigationBar.barStyle = self.navigationController.navigationBar.barStyle;
    control.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
    addChannelView.addChannelViewDelegate = self;
	addChannelView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	
	[self presentModalViewController:control animated:YES];
}


-(void) addChannelForUrl : (NSString *) channelUrl {
	DLog(@"Ajout Chaine : %@", channelUrl);
	Channel * channel = nil;
    
    if(!channelUrl || [channelUrl length]<6 || [[channelUrl lowercaseString] isEqualToString:@"http://"]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Adresse non valide." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
		[alert show];
        return;
    }
    
	if ((channel = [[CoreDataManager sharedInstance] createChannelForRssUrl:channelUrl]) != nil) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"%@_deleted",channel.rssUrl]];
		[[CoreDataManager sharedInstance] downloadRssDataForChannel:channel];
	} else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Cette Chaine a déjà été ajoutée." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
		[alert show];
	}
}
- (void)cancelAddChanel:(UIViewController *)source{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - Fetched results controller delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller{
	[self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type{
	switch(type){
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath{
    DLog(@"%d", type);
	UITableView *tableView = self.tableView;
    
    switch(type){
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{

    DLog();
    
	[self.tableView endUpdates];
    
    NSString *footerText ;
    int n = [[self.fetchedResultsController fetchedObjects] count];
    if(n==0)
        footerText = @"Aucune source disponible";
    else if(n==1)
        footerText = @"1 source disponible";
    else footerText = [NSString stringWithFormat:@"%d sources disponibles",n];

    
    self.footerLabel.text =footerText;
}


 // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed. 
 
 /*- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
 {
 // In the simplest, most efficient, case, reload the table view.
 [self.tableView reloadData];
 }*/
 
- (void)awakeFromNib{
	
	self.title = [[ConfigurationManager sharedInstance] sourcesListTitle];
    self.navigationController.tabBarItem.title = [[ConfigurationManager sharedInstance] libraryTabName];
	[super awakeFromNib];
}



@end
