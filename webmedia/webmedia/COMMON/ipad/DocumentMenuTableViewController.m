//
//  DocumentMenuTableViewController.m
//  webmedia
//
//  Created by soreha on 21/06/13.
//  
//

#import "DocumentMenuTableViewController.h"
#define ACTION_OPEN_ONLINE  @"Reprendre la lecture en ligne"
#define ACTION_OPEN_START_ONLINE  @"Ouvrir en ligne"

#define ACTION_OPEN_START  @"Ouvrir"
#define ACTION_OPEN  @"Reprendre la lecture"

#define ACTION_ADD_TO_FAVS  @"Ajouter aux favoris"
#define ACTION_REMOVE_FROM_FAVS  @"Retirer des favoris"

#define ACTION_DOWNLOAD_UPDATE  @"Télécharger la mise à jour"
#define ACTION_DOWNLOAD @"Télécharger"

#define ACTION_DOWNLOAD_CANCEL @"Annuler le téléchargement"
#define ACTION_DOWNLOAD_RESUME @"Reprendre le téléchargement"
#define ACTION_DOWNLOAD_SUSPEND @"Suspendre le téléchargement"

#define ACTION_SHOW_INFORMATION  @"Informations"

#define ACTION_DELETE  @"Supprimer"






@interface DocumentMenuTableViewController ()

@end

@implementation DocumentMenuTableViewController

-(NSString*)getFavAction:(Document *)document{
    return [[document isFavorite] boolValue]?ACTION_REMOVE_FROM_FAVS:ACTION_ADD_TO_FAVS;
}

- (BOOL)isTopazeDocument:(Document *)doc{
    return [[doc.pubType lowercaseString] isEqualToString:@"topaze"];
}
- (BOOL)isWebMediaDocument:(Document *)doc{
    return !doc.pubType || [[doc.pubType lowercaseString] isEqualToString:@"webmedia"];
}

-(void)setDocument:(Document *)document{
    _document=document;
    
    NSArray *tmp;
    
    if(_document){
        if([[_document downloading] boolValue]){
            if([document.link hasSuffix:@".html"]){
                //document accessible en ligne
                if(_document.lastOpenedPage && ![self isTopazeDocument:_document]){
                    if ([_document.downloadProgress floatValue] > 0 ){//Téléchargement en cours
                        tmp=@[ACTION_DOWNLOAD_SUSPEND, ACTION_DOWNLOAD_CANCEL, ACTION_OPEN_ONLINE,ACTION_OPEN_START_ONLINE,[self getFavAction:_document],ACTION_SHOW_INFORMATION];
                    }else if([_document.downloadSuspended boolValue]){//Téléchargement suspendu
                        tmp=@[ACTION_DOWNLOAD_RESUME, ACTION_DOWNLOAD_CANCEL, ACTION_OPEN_ONLINE,ACTION_OPEN_START_ONLINE,[self getFavAction:_document],ACTION_SHOW_INFORMATION];
                    }else{//Téléchargement en attente
                        tmp=@[ACTION_DOWNLOAD_SUSPEND, ACTION_DOWNLOAD_CANCEL, ACTION_OPEN_ONLINE,ACTION_OPEN_START_ONLINE,[self getFavAction:_document],ACTION_SHOW_INFORMATION];
                    }
                }else{
                    if ([_document.downloadProgress floatValue] > 0 ){//Téléchargement en cours
                        tmp=@[ACTION_DOWNLOAD_SUSPEND, ACTION_DOWNLOAD_CANCEL,ACTION_OPEN_START_ONLINE,[self getFavAction:_document],ACTION_SHOW_INFORMATION];
                    }else if([_document.downloadSuspended boolValue]){//Téléchargement suspendu
                        tmp=@[ACTION_DOWNLOAD_RESUME, ACTION_DOWNLOAD_CANCEL,ACTION_OPEN_START_ONLINE,[self getFavAction:_document],ACTION_SHOW_INFORMATION];
                    }else{//Téléchargement en attente
                        tmp=@[ACTION_DOWNLOAD_SUSPEND, ACTION_DOWNLOAD_CANCEL,ACTION_OPEN_START_ONLINE,[self getFavAction:_document],ACTION_SHOW_INFORMATION];
                    }
                }
            }else{
                tmp=@[ACTION_SHOW_INFORMATION];
            }
        }else if ([_document.updateAvailable intValue] == 1) {
            if(_document.lastOpenedPage && ![self isTopazeDocument:_document]){
                tmp=@[ACTION_DOWNLOAD_UPDATE, ACTION_OPEN, ACTION_OPEN_START,[self getFavAction:_document],ACTION_SHOW_INFORMATION,ACTION_DELETE];
            }else{
                tmp=@[ACTION_DOWNLOAD_UPDATE,ACTION_OPEN_START,[self getFavAction:_document], ACTION_SHOW_INFORMATION,ACTION_DELETE];
            }
        }else{
            if([document.downloaded boolValue]){
                if(_document.lastOpenedPage && ![self isTopazeDocument:_document] && ![self isWebMediaDocument:_document]){
                    tmp=@[ACTION_OPEN,ACTION_OPEN_START,[self getFavAction:_document], ACTION_SHOW_INFORMATION,ACTION_DELETE];
                }else{
                    tmp=@[ACTION_OPEN_START,[self getFavAction:_document], ACTION_SHOW_INFORMATION,ACTION_DELETE];
                }
            }else if([_document.link hasSuffix:@".html"]){
                if(_document.lastOpenedPage && ![self isTopazeDocument:_document]){
                    tmp=@[ACTION_DOWNLOAD,ACTION_OPEN_ONLINE,ACTION_OPEN_START_ONLINE,[self getFavAction:_document],ACTION_SHOW_INFORMATION];
                }else{
                    tmp=@[ACTION_DOWNLOAD,ACTION_OPEN_START_ONLINE,[self getFavAction:_document],ACTION_SHOW_INFORMATION];
                }

            }else{
                tmp=@[ACTION_DOWNLOAD,[self getFavAction:_document],ACTION_SHOW_INFORMATION];
            }
        }
        DLog(@"options %@",tmp);
        
        self.options = [NSMutableArray arrayWithArray:tmp];
    }else{
        DLog(@"options %@",self.options);

        self.options = [NSMutableArray arrayWithCapacity:0];

    }
    
    [self.tableView reloadData];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(IS_IPAD()){
        self.navigationItem.rightBarButtonItem = nil;
        self.contentSizeForViewInPopover = CGSizeMake(310, 350);
		//ios 7>
		if([self respondsToSelector:@selector(preferredContentSize:)]){
			self.preferredContentSize = CGSizeMake(310, 350);
		}
		
		[self.tableView reloadData];
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // self.document = nil;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.tableView.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
	NSLog(@"row number: %d",(int)[self.options count]);
    return [self.options count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"menuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSString *string = [self.options objectAtIndex:indexPath.row];
    cell.accessoryType = [string isEqualToString:ACTION_SHOW_INFORMATION] ?
        UITableViewCellAccessoryDisclosureIndicator :
        UITableViewCellAccessoryNone;
    
    cell.textLabel.textColor = [string isEqualToString:ACTION_DELETE] ? [UIColor colorWithRed:0.501 green:0.000 blue:0.000 alpha:1.000] : [UIColor blackColor];
    
    cell.textLabel.text = [self.options objectAtIndex:indexPath.row];
    // Configure the cell...
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *string = [self.options objectAtIndex:indexPath.row];
    DLog(@"%@",string);
    if([string isEqualToString:ACTION_SHOW_INFORMATION]){
        if([self.menuDelegate respondsToSelector:@selector(showInformationsForDocument:sender:)]){
            [self.menuDelegate showInformationsForDocument:self.document sender:self];
        }
    }else if([string isEqualToString:ACTION_OPEN]){
        DLog(@"ACTION_OPEN");
        if([self.menuDelegate respondsToSelector:@selector(openDocument:onLastOpenedPage:sender:)]){
            [self.menuDelegate openDocument:self.document onLastOpenedPage:YES sender:self];
        }
    }else if([string isEqualToString:ACTION_OPEN_START]){
        if([self.menuDelegate respondsToSelector:@selector(openDocument:onLastOpenedPage:sender:)]){
            DLog(@"ACTION_OPEN_START");
            [self.menuDelegate openDocument:self.document onLastOpenedPage:NO sender:self];
        }
    }else if([string isEqualToString:ACTION_OPEN_ONLINE]){
        DLog(@"ACTION_OPEN_ONLINE");
        if([self.menuDelegate respondsToSelector:@selector(openOnlineDocument:onLastOpenedPage:sender:)]){
            [self.menuDelegate openOnlineDocument:self.document onLastOpenedPage:YES sender:self];
        }
    }else if([string isEqualToString:ACTION_OPEN_START_ONLINE]){
        DLog(@"ACTION_OPEN_START_ONLINE");
        if([self.menuDelegate respondsToSelector:@selector(openOnlineDocument:onLastOpenedPage:sender:)]){
            [self.menuDelegate openOnlineDocument:self.document onLastOpenedPage:NO sender:self];
        }
    }else if([string isEqualToString:ACTION_ADD_TO_FAVS]){
        if([self.menuDelegate respondsToSelector:@selector(addToFavs:sender:)]){
            [self.menuDelegate addToFavs:self.document sender:self];
        }
    }else if([string isEqualToString:ACTION_REMOVE_FROM_FAVS]){
        if([self.menuDelegate respondsToSelector:@selector(removeFromFavs:sender:)]){
            [self.menuDelegate removeFromFavs:self.document sender:self];
        }
    }else if([string isEqualToString:ACTION_DOWNLOAD] || [string isEqualToString:ACTION_DOWNLOAD_UPDATE]){
        if([self.menuDelegate respondsToSelector:@selector(download:sender:)]){
            [self.menuDelegate download:self.document sender:self];
        }
    }else if([string isEqualToString:ACTION_DOWNLOAD_RESUME]){
        if([self.menuDelegate respondsToSelector:@selector(resumeDownloadForDocument:sender:)]){
            [self.menuDelegate resumeDownloadForDocument:self.document sender:self];
        }
    }else if([string isEqualToString:ACTION_DOWNLOAD_CANCEL]){
        if([self.menuDelegate respondsToSelector:@selector(cancelDownloadForDocument:sender:)]){
            [self.menuDelegate cancelDownloadForDocument:self.document sender:self];
        }
    }else if([string isEqualToString:ACTION_DOWNLOAD_SUSPEND]){
        if([self.menuDelegate respondsToSelector:@selector(suspendDownloadForDocument:sender:)]){
            [self.menuDelegate suspendDownloadForDocument:self.document sender:self];
        }
    }
    else if([string isEqualToString:ACTION_DELETE]){
        if([self.menuDelegate respondsToSelector:@selector(deleteDocument:sender:)]){
            [self.menuDelegate deleteDocument:self.document sender:self];
        }
    }
}

@end
