//
//  AvailableDocumentsTableViewController.m
//  OptimGenericReader
//
//  Created by soreha on 16/02/11.
//  
//

#import "AvailableDocumentsTableViewController.h"
#import "CoreDataManager.h"
#import "Document.h"
#import "Channel.h"
#import "Thumbnail.h"
#import "ConfigurationManager.h"
#import "MediaHandlerManager.h"
#import "RequestManager.h"
#import "ASINetworkQueue.h"

@implementation AvailableDocumentsTableViewController



-(void) releaseOutlets{
	self.myCell = nil;
}

// @see NSObject
- (void)dealloc {
    self.fetchedResultsController.delegate = nil;
    self.fetchedResultsController = nil;

	[self releaseOutlets];
}

-(void) initWithChannel :(Channel *) ch {
    self.isDownloadedDocumentsView = NO;
    self.channel = ch;
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllDocumentsForChannel:channel sortedBy:0]
										  managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"category" 
													 cacheName:nil];
    self.fetchedResultsController.delegate = self;
	
    NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]){
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    DLog(@"Nb Available Documents : %lu", (unsigned long)[[self.fetchedResultsController fetchedObjects] count]);
}

-(void) reloadFetchedResultControllerSortingBy : (int) criteria {
    DLog();
    self.fetchedResultsController.delegate = nil;
    self.fetchedResultsController = nil;
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllDocumentsForChannel:channel sortedBy:criteria] 
                                                                            managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"category" 
                                                                                       cacheName:nil];
    
    self.fetchedResultsController.delegate = self;
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]){
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        //abort();
    }
    [self.tableView reloadData];
    
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	if([[fetchedResultsController fetchedObjects] count]==0) return @"Aucun nouveau document";
	return @"";
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *MyIdentifier = @"DefaultCell";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
		[[NSBundle mainBundle] loadNibNamed:@"DownloadedDocumentTableViewCell" owner:self options:nil];
        cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
        cell = myCell;
        self.myCell = nil;
    }
	
	// Configure the cell...
	[self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)sort:(int)index{
    [self reloadFetchedResultControllerSortingBy:index];
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (void)contentUpdated{
    [self.contentDelegate updateSegmentTitleNumbers:self];
}

@end

