//
//  BroadcastView.m
//  webmedia
//
//  Created by soreha on 30/11/12.
//  
//

#import "BroadcastView.h"
#import "Thumbnail.h"
#import "MediaHandlerManager.h"
#import "CoreDataManager.h"
#import "RequestManager.h"

@implementation BroadcastView


- (void)alignTop:(UILabel *)label{
    CGSize fontSize = [label.text sizeWithFont:label.font];
    double finalHeight = fontSize.height * label.numberOfLines;
    double finalWidth = label.frame.size.width;    //expected width of label
    CGSize theStringSize = [label.text sizeWithFont:label.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:label.lineBreakMode];
    int newLinesToPad = (finalHeight  - theStringSize.height) / fontSize.height;
    for(int i=0; i<newLinesToPad; i++)
        label.text = [label.text stringByAppendingString:@"\n "];
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 140)];
    _imageView.backgroundColor = [UIColor blackColor];
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    _imageView.clipsToBounds = YES;
    [self addSubview:_imageView];
    
    _favImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"favBook"]];
    _favImageView.frame = CGRectMake(frame.size.width-32, 0, 32, 32);
    _favImageView.backgroundColor = [UIColor clearColor];
    //_favImageView.contentMode = UIViewContentModeScaleAspectFill;
    _favImageView.clipsToBounds = YES;
    [self addSubview:_favImageView];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, 140, frame.size.width-8, 36)];
    //_titleLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    //_titleLabel.shadowColor = [UIColor colorWithWhite:0 alpha:1];
    //_titleLabel.shadowOffset = CGSizeMake(1, 1);
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.font = [UIFont boldSystemFontOfSize:13];
    _titleLabel.adjustsFontSizeToFitWidth = YES;
    _titleLabel.minimumFontSize = 11;
    _titleLabel.numberOfLines = 2;
    

    [self addSubview:_titleLabel];
     _lineLabel= [[UILabel alloc] initWithFrame:CGRectMake(0, 176, frame.size.width, 1)];
    _lineLabel.backgroundColor = [UIColor colorWithRed:0.439 green:0.443 blue:0.451 alpha:1.000];
    [self addSubview:_lineLabel];
    
    _legend1Label = [[UILabel alloc] initWithFrame:CGRectMake(8, 177, frame.size.width-16-20, 20)];
    _legend1Label.backgroundColor = [UIColor clearColor];
    _legend1Label.textColor = [UIColor lightGrayColor];
    _legend1Label.font = [UIFont systemFontOfSize:12];
    [self addSubview:_legend1Label];
    
    self.infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.infoButton setImage:[UIImage imageNamed:@"menuDots"] forState:UIControlStateNormal];
    
    self.infoButton.frame = CGRectMake(frame.size.width-18, 183, 18, 36);
    [self addSubview:self.infoButton];

    self.deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(-12, -12, 32, 32)];
    [self.deleteButton setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
    self.deleteButton.hidden = YES;
    [self addSubview:self.deleteButton];
    
    _legend2Label = [[UILabel alloc] initWithFrame:CGRectMake(8, 197, frame.size.width-16-20, 20)];
    _legend2Label.backgroundColor = [UIColor clearColor];
    _legend2Label.textColor = [UIColor lightGrayColor];
    _legend2Label.font = [UIFont systemFontOfSize:12];

    [self addSubview:_legend2Label];
    
    _downloadView = [[UIView alloc ] initWithFrame:_imageView.frame];
    _downloadView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    [self addSubview: _downloadView];
    
    _buttonDownload = [[UIButton alloc] initWithFrame:CGRectMake(30, 105, frame.size.width-60, 24)];
    [_buttonDownload setTitle:@"Télécharger" forState:UIControlStateNormal];
    [_buttonDownload setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _buttonDownload.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    
    float wb2 =  (frame.size.width-30)/2;

    _buttonOpenOnLineVersion = [[UIButton alloc] initWithFrame:CGRectMake(wb2+20, 105, wb2, 24)];
    [_buttonOpenOnLineVersion setTitle:@"Version en ligne" forState:UIControlStateNormal];
    [_buttonOpenOnLineVersion setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _buttonOpenOnLineVersion.titleLabel.font = [UIFont boldSystemFontOfSize:10];
    
    
    
    _buttonResumeDownload = [[UIButton alloc] initWithFrame:CGRectMake(20, 80, 90, 24)];
    [_buttonResumeDownload setTitle:@"Reprendre" forState:UIControlStateNormal];
    [_buttonResumeDownload setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _buttonResumeDownload.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [_downloadView addSubview: _buttonResumeDownload];
    _buttonResumeDownload.hidden = YES;
    _buttonSuspendDownload = [[UIButton alloc] initWithFrame:CGRectMake(20, 80, 90, 24)];
    [_buttonSuspendDownload setTitle:@"Suspendre" forState:UIControlStateNormal];
    [_buttonSuspendDownload setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _buttonSuspendDownload.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [_downloadView addSubview: _buttonSuspendDownload];
    _buttonSuspendDownload.hidden = YES;
    _buttonCancelDownload = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-110, 80, 90, 24)];
    [_buttonCancelDownload setTitle:@"Annuler" forState:UIControlStateNormal];
    [_buttonCancelDownload setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _buttonCancelDownload.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [_downloadView addSubview: _buttonCancelDownload];
    _buttonCancelDownload.hidden = YES;
    
    _downloadProgressView = [[UIProgressView alloc] initWithFrame:CGRectMake(40, 110, frame.size.width-80, 10)];
    _downloadInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 120, frame.size.width-80, 20)];
    _downloadInfoLabel.backgroundColor = [UIColor clearColor];
    _downloadInfoLabel.textAlignment = UITextAlignmentCenter;
    _downloadInfoLabel.textColor = [UIColor whiteColor];
    _downloadInfoLabel.shadowColor = [UIColor blackColor];
    _downloadInfoLabel.shadowOffset = CGSizeMake(1, 1);
    _downloadInfoLabel.font = [UIFont boldSystemFontOfSize:10];
    
    UIImage *aBackgroundImage = [[UIImage imageNamed:@"bkgLightGrayButton"] stretchableImageWithLeftCapWidth:15 topCapHeight:20];
    [_buttonDownload setBackgroundImage:aBackgroundImage forState:UIControlStateNormal];
    [_buttonOpenOnLineVersion setBackgroundImage:aBackgroundImage forState:UIControlStateNormal];

    [_buttonResumeDownload setBackgroundImage:aBackgroundImage forState:UIControlStateNormal];
    [_buttonSuspendDownload setBackgroundImage:aBackgroundImage forState:UIControlStateNormal];
    [_buttonCancelDownload setBackgroundImage:aBackgroundImage forState:UIControlStateNormal];
    
    [_downloadView addSubview: _buttonDownload];
    [_downloadView addSubview: _buttonOpenOnLineVersion];

    [_downloadView addSubview: _downloadProgressView];
    [_downloadView addSubview: _downloadInfoLabel];

    if (self) {
        // Initialization code
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [self addGestureRecognizer:longPress];
    longPress.minimumPressDuration = 2;
    longPress.delegate = self;
    
    
    return self;
}
- (void)handleLongPress:(id)sender{
    if(![self.document.downloaded boolValue]){
        return;
    }
    DLog();
    self.deleteButton.hidden = NO;
    [NSObject cancelPreviousPerformRequestsWithTarget: self.deleteButton];
    [self.deleteButton performSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES] afterDelay:2.5];
}
- (void)hideSuspendedOptionButtons{
    self.buttonResumeDownload.hidden = YES;
    self.buttonCancelDownload.hidden = YES;
}
- (void)showSuspendedOptionButtons{
    self.buttonResumeDownload.hidden = NO;
    self.buttonCancelDownload.hidden = NO;
}
- (void)showSuspendedStateButtonsForShortTime{
    [NSObject cancelPreviousPerformRequestsWithTarget: self.buttonCancelDownload];
    [NSObject cancelPreviousPerformRequestsWithTarget: self.buttonResumeDownload];
    [self.buttonCancelDownload performSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES] afterDelay:3];
    [self showSuspendedOptionButtons];
    [self.buttonResumeDownload performSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES] afterDelay:3];

}
- (void)hideDownloadingOptionButtons{
    self.buttonSuspendDownload.hidden = YES;
    self.buttonCancelDownload.hidden = YES;
}
- (void)showDownloadingOptionButtons{
    self.buttonSuspendDownload.hidden = NO;
    self.buttonCancelDownload.hidden = NO;
}
- (void)showDownloadingStateButtonsForShortTime{
    
    [NSObject cancelPreviousPerformRequestsWithTarget: self.buttonCancelDownload];
    [NSObject cancelPreviousPerformRequestsWithTarget: self.buttonSuspendDownload];
    [self.buttonCancelDownload performSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES] afterDelay:3];
    
    [self showDownloadingOptionButtons];
    [self.buttonSuspendDownload performSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES] afterDelay:3];

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return !([touch.view isKindOfClass:[UIButton class]] || [touch.view isKindOfClass:[UIPageControl class]]);
}

- (void)handleSingleTap:(id)sender{
    DLog(@"Document : \n%@ ",_document );
    UITapGestureRecognizer *tap = (UITapGestureRecognizer *)sender;
    CGPoint point = [tap locationInView:self];
    if (!CGRectContainsPoint(self.imageView.frame, point) ) {
        return;
    }
    if ([self.document.updateAvailable boolValue] && ![self.document.downloading boolValue] ){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Mise à jour" message:@"Une mise à jour est disponible, souhaitez-vous la télécharger maintenant" delegate:self cancelButtonTitle:@"Oui" otherButtonTitles:@"Non", nil];
        [alert show];
	}else if([self.document.downloading boolValue]){
        
        if([self.document.downloadSuspended boolValue]){
            self.buttonDownload.hidden = YES;
            [self showSuspendedStateButtonsForShortTime];
        }else{
            [self showDownloadingStateButtonsForShortTime];
        }
    }else if([self.document.downloaded boolValue]){
        NSLog(@"je passe ici");
       [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:self.document];

    }
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    DLog(@"buttonindex=%d",buttonIndex);
	if ([actionSheet.title compare:@"Mise à jour"] == NSOrderedSame) {
        if (buttonIndex == 0){
            [[CoreDataManager sharedInstance] downloadDocument:self.document];
		} else{
            [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:self.document];
        }
	}
}
- (void)updateDownloadProgress{

    if(![_document.updateAvailable boolValue] && [_document.downloaded boolValue]){
        _downloadView.hidden = YES;
    }else{
        _downloadView.hidden = NO;
        _downloadInfoLabel.hidden = NO;
        if([_document.downloading boolValue]){
            _buttonDownload.hidden = YES;
            _downloadProgressView.hidden = NO;
            [_downloadProgressView setProgress:[_document.downloadProgress floatValue]];
            if ([_document.downloadProgress floatValue] > 0 ){
                _downloadInfoLabel.text = @"Téléchargement en cours";
            }else if([_document.downloadSuspended boolValue]){
                _downloadInfoLabel.text = @"Téléchargement suspendu";
            }else{
                _downloadInfoLabel.text =@"Téléchargement en attente";
            }
        }else if ([_document.updateAvailable boolValue]){
            _downloadInfoLabel.text = @"Mise à jour disponible";
            _downloadInfoLabel.hidden = NO;
            _buttonDownload.hidden = YES;
            _downloadProgressView.hidden = YES;
        }
        else{
            _downloadInfoLabel.hidden = YES;
            _buttonDownload.hidden = NO;
            _downloadProgressView.hidden = YES;
            [_buttonDownload setTitle:@"Télécharger" forState:UIControlStateNormal];
            _downloadInfoLabel.text = @"";
        }
    }
}

- (void)openOnlineVersion:(UIButton *)sender{
    [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openOnlineDocument:self.document onLastOpenedPage:YES];
}

- (void) setDocument:(Document *)document{
    _document = document;
    [self updateDownloadProgress];
    if(!_document.link || [_document.link hasSuffix:@".zip"] || [_document.downloading boolValue]
       || ![[MediaHandlerManager sharedInstance].mediaHandlerDelegate respondsToSelector:@selector(openOnlineDocument:onLastOpenedPage:)]){
        _buttonOpenOnLineVersion.hidden = YES;
        _buttonDownload.frame = CGRectMake(30, 105, self.frame.size.width-60, 24);
        _downloadView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    }else{
        
        _buttonOpenOnLineVersion.hidden = NO;
        _buttonDownload.frame = CGRectMake(10, 105, (self.frame.size.width-30)/2, 24);
        _downloadView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.10];
        
        
        [_buttonOpenOnLineVersion addTarget:self action:@selector(openOnlineVersion:) forControlEvents:UIControlEventTouchUpInside];

    }
    _titleLabel.text = document.title;
    
    //[self alignTop:_titleLabel];
    
    NSMutableString *str1 = [NSMutableString stringWithString:document.pubDate ? [NSDateFormatter localizedStringFromDate:document.pubDate dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle ]:@""];
    if(document.author){
        if(document.pubDate){
            [str1 appendFormat:@" - %@",document.author];
        }else{
            [str1 setString:document.author];
        }
    }
    
    _legend1Label.text = str1;
    NSString *cat = (![document.category isEqualToString:@"Aucune"]) ? document.category : @"";
    NSString *type = @"WebMedia";
    if([[document.pubType lowercaseString] isEqualToString:@"opale"]){
            type = @"Opale";
    }else if([[document.pubType lowercaseString] isEqualToString:@"topaze"]){
        type = @"Topaze";
    }else if([[document.pubType lowercaseString] isEqualToString:@"optim"]){
        type = @"Optim";
    }
    if([cat isEqualToString:@""]){
        _legend2Label.text = type;
    }else{
        _legend2Label.text =[NSString stringWithFormat:@"%@ | %@",type,cat];
    }
    
    if(document.thumbnail_large_URI){
        _imageView.image = document.thumbnail_large.image;
    }
    _favImageView.hidden = ![document.isFavorite boolValue];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
