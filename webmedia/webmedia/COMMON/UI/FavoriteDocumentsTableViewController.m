//
//  FavoriteDocumentsTableViewController.m
//  OptimGenericReader
//
//  Created by soreha on 18/03/11.
//  
//

#import "FavoriteDocumentsTableViewController.h"
#import "CoreDataManager.h"
#import "Document.h"
#import "Thumbnail.h"
#import "ConfigurationManager.h"

@implementation FavoriteDocumentsTableViewController

-(void) releaseOutlets{
	self.myCell = nil;
}

// @see NSObject
- (void)dealloc {
   
        self.fetchedResultsController.delegate = nil;
        self.fetchedResultsController = nil;
   
	[self releaseOutlets];
}

-(void) viewDidLoad{

    self.title = @"Favoris";
    [self navigationController].navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];   ;   

    self._navigationController = self.navigationController;
    self.isDownloadedDocumentsView = YES;
	
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestFavoriteDocuments] 
                                                                        managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"channel.title" 
                                                                                   cacheName:nil];
    self.fetchedResultsController.delegate = self;
	
    NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error])
    {
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
	
	DLog(@"Nb FavoriteDocuments : %d", [[self.fetchedResultsController fetchedObjects] count]);
    if(SYSTEM_VERSION_LESS_THAN(@"7.0"))
        [self navigationController].navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
    else{
        self.navigationController.navigationBar.tintColor = [UIColor blueColor];
    }
    

}


@end
