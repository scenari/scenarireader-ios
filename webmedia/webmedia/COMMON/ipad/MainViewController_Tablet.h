//
//  MainViewController_Tablet.h
//  webmedia
//
//  Created by soreha on 27/11/12.
//  
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ChannelView.h"
#import "Channel.h"
#import "DocumentDetailViewController.h"
#import "CoreDataManager.h"

#import "AddChannelViewDelegate.h"
#import "DocumentMenuTableViewController.h"

@class AddChannelViewController;
@interface MainViewController_Tablet : UIViewController <DocumentMenuTableViewControllerDelegate,RssDelegate, UIAlertViewDelegate, NSFetchedResultsControllerDelegate, ChannelViewSelectionDelegate, FavoriteListenerDelegate, AddChannelViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollChannels;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollEmissions;
@property (weak, nonatomic) IBOutlet UIView *controlsView;
- (IBAction)showInfoAction:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentType;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentOrder;
@property (weak, nonatomic) IBOutlet UIView *errorView;
@property (weak, nonatomic) IBOutlet UILabel *noSourceLabel;
@property (weak, nonatomic) IBOutlet UILabel *noDocumentLabel;

@property (strong, nonatomic) DocumentMenuTableViewController *menuContent;

- (IBAction)segmentOrderValueChange:(id)sender;
- (IBAction)segmentTypeValueChange:(id)sender;

@property (nonatomic, strong) NSFetchedResultsController *channelFetchedResultsController;
@property (nonatomic, strong) NSFetchedResultsController *broadcastFetchedResultsController;

@property (nonatomic, strong) NSArray *channelViews;
@property (nonatomic, strong) NSArray *broadcastViews;


@property (nonatomic, strong) Channel *currentChannel;

@property (nonatomic, strong) UIPopoverController *menu;
@property (nonatomic, strong) AddChannelViewController* addChannelView;

@end
