//
//  RequestManager.m
//  WebMediaTest
//
//  Created by soreha on 31/01/11.
//  
//

#import "RequestManager.h"
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "Document.h"
#import "ZipArchive.h"
#import "Channel.h"
#import "ConfigurationManager.h"


@interface RequestManager (PrivateMethods)
- (NSString *) getFinalPath;
- (void) downloadCompleted;
@end

@implementation RequestManager

@synthesize rssAndThumbnailDownloadQueue;
@synthesize documentDownloadQueue;
@synthesize coreDataDelegate;


BOOL messageShown = NO;


- (void) initAsiQueue{
	self.documentDownloadQueue = [[ASINetworkQueue alloc] init];
	self.documentDownloadQueue.maxConcurrentOperationCount = 1;
	
	[self.documentDownloadQueue setDelegate:self];
	[self.documentDownloadQueue setShowAccurateProgress:YES];
	[self.documentDownloadQueue setShouldCancelAllRequestsOnFailure:NO];
}

- (id)init {
	self = [super init];
	self.rssAndThumbnailDownloadQueue = [[NSOperationQueue alloc] init];
	self.rssAndThumbnailDownloadQueue.maxConcurrentOperationCount = 4;
	return self;    
}


#pragma mark -
#pragma mark Rss request methods

- (void)addRssRequestForNewChannel:(Channel *) channel {
    DLog();
    
	NSURL *url = [NSURL URLWithString:channel.rssUrl];
	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
	request.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys : channel, @"Channel",nil ];
	[request setDelegate:self];
	[request setDidFinishSelector:@selector(rssRequestDone:)];
	[request setDidFailSelector:@selector(rssCreationRequestWentWrong:)];
	[[self rssAndThumbnailDownloadQueue] addOperation:request];
}


- (void)addRssRequestForChannel:(Channel *) channel {
    DLog();
    
	NSURL *url = [NSURL URLWithString:channel.rssUrl];
	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
	request.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys : channel, @"Channel",nil ];
	[request setDelegate:self];
	[request setDidFinishSelector:@selector(rssRequestDone:)];
	[request setDidFailSelector:@selector(rssRequestWentWrong:)];
	[[self rssAndThumbnailDownloadQueue] addOperation:request];
}

- (void)rssRequestDone:(ASIHTTPRequest *)request{
    DLog();
    
	if (coreDataDelegate != nil) {
		[coreDataDelegate rssRequestDone:request];
	}
}

- (void)rssRequestWentWrong:(ASIHTTPRequest *)request{
    ALog();
    
	if (coreDataDelegate != nil) {  
		[coreDataDelegate rssRequestError:request];
	}	
}
- (void)rssCreationRequestWentWrong:(ASIHTTPRequest *)request{
    ALog();
    
	if (coreDataDelegate != nil) {
		[coreDataDelegate rssCreationRequestError:request];
	}
}

#pragma mark -
#pragma mark Thumbnail request methods
- (void) addThumbnailRequestForDocument:(Document *) document {
	NSURL *url = [NSURL URLWithString:document.thumbnail_small_URI];
    DLog(@"url : %@",url);
	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
	request.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys : document, @"Document",[[NSNumber alloc] initWithBool:YES], @"isSmall",nil ];
	[request setDelegate:self];
	[request setDidFinishSelector:@selector(thumbnailRequestDone:)];
	[request setDidFailSelector:@selector(thumbnailRequestWentWrong:)];
	[[self rssAndThumbnailDownloadQueue] addOperation:request];
	
	if ([document.thumbnail_large_URI compare:@""] != NSOrderedSame) {
		NSURL *url = [NSURL URLWithString:document.thumbnail_large_URI];
		ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
		request.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys : document, @"Document",[[NSNumber alloc] initWithBool:NO], @"isSmall",nil ];
		[request setDelegate:self];
		[request setDidFinishSelector:@selector(thumbnailRequestDone:)];
		[request setDidFailSelector:@selector(thumbnailRequestWentWrong:)];
		[[self rssAndThumbnailDownloadQueue] addOperation:request];
	}
}

- (void)thumbnailRequestDone:(ASIHTTPRequest *)request{
    DLog();
    
	
	Document *document = (Document *)[request.userInfo objectForKey:@"Document"];
	NSNumber *number = (NSNumber *) [request.userInfo objectForKey:@"isSmall"];
	BOOL isSmall;
	if ([number intValue] == 1) {
		isSmall = YES;
	} else {
		isSmall = NO;
	}
	
	UIImage *image = [[UIImage alloc] initWithData:[request responseData]];
	
	if(coreDataDelegate){
		[self.coreDataDelegate document:document didLoadThumbnail:image isSmall:isSmall];
	}
	
}

- (void)thumbnailRequestWentWrong:(ASIHTTPRequest *)request
{
	Document *document = (Document *)[request.userInfo objectForKey:@"Document"];
	ALog(@"Error downloading thumbnail for document : %@\n %@", document.title , request.url );
}


#pragma mark -
#pragma mark channel thumbnail
- (void) addThumbnailRequestForChannel:(Channel *) channel {
	NSURL *url = [NSURL URLWithString:channel.thumbnailURI];
    DLog(@"url : %@",url);
	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
	request.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys : channel, @"Channel",nil ];
	[request setDelegate:self];
	[request setDidFinishSelector:@selector(thumbnailChannelRequestDone:)];
	[request setDidFailSelector:@selector(thumbnailChannelRequestWentWrong:)];
	[[self rssAndThumbnailDownloadQueue] addOperation:request];
}

- (void)thumbnailChannelRequestDone:(ASIHTTPRequest *)request{
    DLog();
    
	Channel *channel = (Channel *)[request.userInfo objectForKey:@"Channel"];	
	UIImage *image = [[UIImage alloc] initWithData:[request responseData]];
	
	if(coreDataDelegate){
		[self.coreDataDelegate channel:channel didLoadThumbnail:image];
	}
	
}

- (void)thumbnailChannelRequestWentWrong:(ASIHTTPRequest *)request{
    Channel *channel = (Channel *)[request.userInfo objectForKey:@"Channel"];
	ALog(@"Error downloading thumbnail for document : %@\n %@", channel.title, request.url );
}


#pragma mark - 
#pragma mark Document request methods
- (void) addDocumentRequest:(Document *) document {
	//Initialisation du dossier temporaire de téléchargement
	srandomdev();
	long my_rand = random();

	
	NSString *tmpFolder = [self getTmpDownloadFolder];
	NSString *tmpDownloadingFile;
	
	if (document.tmpDownloadFilePath != nil) {
		tmpDownloadingFile = document.tmpDownloadFilePath;
	} else {
		tmpDownloadingFile = [tmpFolder stringByAppendingPathComponent:[NSString stringWithFormat : @"%ld.zip.download",my_rand]];
	}

	NSString *zipName = [NSTemporaryDirectory() stringByAppendingPathComponent: [NSString stringWithFormat:@"%ld.zip", my_rand]];
	
	document.tmpDownloadFilePath = tmpDownloadingFile;
	document.downloading = [NSNumber numberWithBool:YES];
	document.isSelected = [NSNumber numberWithBool:NO];
    document.downloadSuspended = [NSNumber numberWithBool:NO];
	[self.coreDataDelegate saveData];
	
		
	NSURL *url = [NSURL URLWithString:document.smartPhoneLink];
	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
	request.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys : document, @"Document", [NSString stringWithFormat:@"%ld/", my_rand], @"tmpFolder", nil ];
	[request setDownloadDestinationPath:zipName];
	[request setTemporaryFileDownloadPath:tmpDownloadingFile];
	[request setAllowResumeForFileDownloads:YES];
	[request setDelegate:self];
	[request setDidFinishSelector:@selector(documentRequestDone:)];
	[request setDidFailSelector:@selector(documentRequestWentWrong:)];
	[request setDownloadProgressDelegate:document];

    
    
    
    /*BACKGROUND DOWNLOADING*/
    if([[ConfigurationManager sharedInstance] downloadInBackground]){
        [request setShouldContinueWhenAppEntersBackground:YES];
    }

    
    
    document.request = request;
    
    
    [[self documentDownloadQueue] addOperation:request];

}


- (void)documentRequestDone:(ASIHTTPRequest *)request{
    DLog();
    
    NSString *my_rand = (NSString *) [request.userInfo objectForKey:@"tmpFolder"];
	NSString *downloadFolder = [NSTemporaryDirectory() stringByAppendingPathComponent:my_rand];
	NSString *zipName = [downloadFolder stringByAppendingString:@".zip"];
	NSString *finalFolder = [[self getFinalPath] stringByAppendingPathComponent:my_rand];
	
	
	Document *document = (Document *)[request.userInfo objectForKey:@"Document"];
	document.downloading = [NSNumber numberWithBool:NO];
    document.downloadProgress = 0;
    document.updateAvailable = [NSNumber numberWithBool:NO];
    
    
    document.request = nil;
    
	DLog(@"Document downloaded : %@\n", document.title);
	
	NSString *currentPath = document.localMediaURI;
	
	ZipArchive* za = [[ZipArchive alloc] init];
	if([za UnzipOpenFile:zipName]){
		[za UnzipFileTo:downloadFolder overWrite:YES];
		[za UnzipCloseFile];
        DLog(@"Document unzipped");
        [[NSFileManager defaultManager] removeItemAtPath:zipName error:nil];
	}
	
	
	NSString *descriptorPath = nil;
	DLog(@"ROOT_DOCUMENT_NAME=%@",ROOT_DOCUMENT_NAME);
	if ([[NSFileManager defaultManager] fileExistsAtPath:[downloadFolder stringByAppendingPathComponent:ROOT_DOCUMENT_NAME]]){
		descriptorPath = finalFolder;
        DLog(@"fileExistsAtPath %@ : YES",descriptorPath);
	} else if ([[NSFileManager defaultManager] fileExistsAtPath:[downloadFolder stringByAppendingPathComponent:@"index.html"]]){
		descriptorPath = finalFolder;
        DLog(@"fileExistsAtPath %@ : YES",descriptorPath);
	}else {
        DLog(@"fileExistsAtPath %@ : NO",descriptorPath);

		NSArray *content = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:downloadFolder error:nil];
        DLog(@"content : %@",content);
		if ([content count] == 1){			
			if ([[NSFileManager defaultManager] fileExistsAtPath:
                 [downloadFolder stringByAppendingPathComponent:[[content objectAtIndex:0] stringByAppendingString:[NSString stringWithFormat:@"/%@",ROOT_DOCUMENT_NAME]]]]){
				descriptorPath = [finalFolder stringByAppendingPathComponent:[content objectAtIndex:0]];
			}
		}
	}
	
	if (descriptorPath != nil) {
		[[NSFileManager defaultManager] moveItemAtPath:downloadFolder toPath:finalFolder error:nil];
		if (currentPath != nil) {
			NSArray *pathComponent  = [currentPath pathComponents];
			NSArray *mediaRootPathComponents = [NSArray arrayWithArray:[pathComponent subarrayWithRange: NSMakeRange(0, [pathComponent indexOfObject:@"medias"]+2)]];  
			NSString *mediaRootPath = [NSString pathWithComponents:mediaRootPathComponents];
			[[NSFileManager defaultManager] removeItemAtPath:mediaRootPath error:nil];
		}
		
		if(coreDataDelegate){
			[coreDataDelegate document:document didLoadMedia:descriptorPath];
		}
	} else{
        DLog(@"descriptorPath is nil");
    }
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL{
   // assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        DLog(@"Error excluding \"%@\" from backup %@", [URL lastPathComponent], error);
    }else{
        DLog(@"Excluding \"%@\" from backup", [URL lastPathComponent]);
    }
    return success;
}

- (NSString *) getFinalPath {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"medias"];
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory]){
		[[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:nil];
	}
	[self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:documentsDirectory isDirectory:YES]];
	return documentsDirectory;
}

- (NSString *) getTmpDownloadFolder {
	NSString *tmpFolder = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat : @"tmp"]];
	if (![[NSFileManager defaultManager] fileExistsAtPath:tmpFolder]){
		[[NSFileManager defaultManager] createDirectoryAtPath:tmpFolder withIntermediateDirectories:YES attributes:nil error:nil];
	}
    [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:tmpFolder isDirectory:YES]];

	return tmpFolder;
}

- (void) supendAllDocumentDownload {
	for (ASIHTTPRequest * request in [documentDownloadQueue operations]) {
        Document *document = (Document *)[request.userInfo objectForKey:@"Document"];
        [document.request clearDelegatesAndCancel];
        document.downloadProgress = 0;
        document.downloadSuspended = [NSNumber numberWithBool:YES];
        document.isSelected = [NSNumber numberWithBool:NO];
        document.request = nil;
    }
    [documentDownloadQueue reset];    
}

- (void) cancelAllDocumentDownload {
	[documentDownloadQueue cancelAllOperations];
	[documentDownloadQueue reset];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    messageShown = NO;
}
- (void)documentRequestWentWrong:(ASIHTTPRequest *)request
{
	Document *document = (Document *)[request.userInfo objectForKey:@"Document"];

    ALog(@"Error downloading document : %@", document.title);
    ALog(@"Error message : %@", [request.error localizedDescription]  );
    
    
    
    if(!messageShown){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Téléchargement impossible. Vérifiez votre connexion Internet." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        messageShown = YES;
    }
    
	document.downloading = [NSNumber numberWithBool:YES];
    document.isSelected = [NSNumber numberWithBool:NO];
    document.downloadSuspended = [NSNumber numberWithBool:YES];
    document.downloadProgress = 0;
    document.request = nil;
}


@end
