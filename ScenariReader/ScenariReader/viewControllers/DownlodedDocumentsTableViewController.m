//
//  DownlodedDocumentTableViewController.m
//  OptimGenericReader
//
//  Created by soreha on 16/02/11.
//  
//

#import "DownlodedDocumentsTableViewController.h"
#import "CoreDataManager.h"
#import "Document.h"
#import "Thumbnail.h"
#import "ConfigurationManager.h"
#import "MediaHandlerManager.h"
#import "RequestManager.h"
#import "ASINetworkQueue.h"
@implementation DownlodedDocumentsTableViewController


// @see NSObject

-(void) initWithChannel :(Channel *) ch {
	self.isDownloadedDocumentsView = YES;
	self.channel = ch;
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestDownloadedDocumentsForChannel:channel withCategory:nil sortedBy:0] 
										  managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"category" 
													 cacheName:nil];
	
    self.fetchedResultsController.delegate = self;
	
    NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error])
    {
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
	DLog(@"Nb Downloaded Documents : %d", [[self.fetchedResultsController fetchedObjects] count]);

}

-(void) reloadFetchedResultControllerSortingBy : (int) criteria {
    self.fetchedResultsController.delegate = nil;
    self.fetchedResultsController = nil;
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestDownloadedDocumentsForChannel:channel withCategory:nil sortedBy:criteria] 
                                                                        managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"category" 
                                                                                   cacheName:nil];
    self.fetchedResultsController.delegate = self;
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error])
    {
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        //abort();
    }
    [self.tableView reloadData];
}


- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	//if([[fetchedResultsController fetchedObjects] count]==0) return @"Aucun média téléchargé";
	return @"";
}

- (void)sort:(int)index{
    [self reloadFetchedResultControllerSortingBy:index];
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64;
}
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (void)contentUpdated{
    [self.contentDelegate updateSegmentTitleNumbers:self];
}

@end

