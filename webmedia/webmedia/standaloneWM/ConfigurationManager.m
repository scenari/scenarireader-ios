//
//  ConfigurationManager.m
//  webmedia
//
//  Created by soreha on 06/06/12.
//  
//

#import "ConfigurationManager.h"

#define KEY_MULTI_CHANNEL_CONFIGURATION                 @"multiChannelConfiguration"
#define KEY_SOURCES_LIST_ARRAY                          @"sourcesList"

#define KEY_CAN_ADD_SOURCE                              @"canAddSource"
#define KEY_CAN_DELETE_SOURCE                           @"canDeleteSource"

#define KEY_CHANNELLIST_WITH_DETAILS                    @"channelListWithDetails"
#define KEY_SHOW_TUTORIAL_FROM_CREDITS_PAGE              @"showTutorialFromCreditPage"

#define KEY_USE_USER_LOCATION                           @"useUserLocation"
#define KEY_SHOW_EMPTY_IMAGE_IN_TOC_TABLE               @"showEmptyImageInTocTable"
#define KEY_ADD_AUTO_NUMBERING_TO_CHAPTER_AND_SEGMENT_NAME  @"addAutoNumbering"
#define KEY_PLAYER_DESCRIPTION_FIRST                    @"playerDescriptionFirst"
#define KEY_DOWNLOAD_IN_BACKGROUND                      @"downloadInBackground"

#define KEY_ARGB_COLOR_NAVIGATION_BAR                   @"argbNavigationBarTintColor"
#define KEY_ACCESSORY_BUTTON_IMAGE_NAME                 @"accessoryButtonImageName"
#define KEY_ARGB_COLOR_CHAPTER_VIEW_BORDER              @"argbChapterViewBorderColor"

#define KEY_ARGB_COLOR_SELECTED_CHANNEL_BORDER          @"argbSelectedChannelBorderColor"
#define KEY_ARGB_COLOR_UNSELECTED_CHANNEL_BORDER        @"argbUnselectedChannelBorderColor"
#define KEY_ARGB_COLOR_MAIN_VIEW_IPAD_CHANNEL_SCROLL_BACKGROUND    @"argbMainViewiPadChannelScrollBackground"

#define KEY_ARGB_COLOR_TABLE_VIEW_SECTION_BACKGROUND    @"argbTableViewSectionBackgroundColor"
#define KEY_ARGB_COLOR_TABLE_VIEW_SECTION_FOREGROUND    @"argbTableViewSectionForegroundColor"
#define KEY_ARGB_COLOR_SELECTED                         @"argbSelectedColor"
#define KEY_ARGB_COLOR_UNSELECTED                       @"argbUnSelectedColor"

#define KEY_ARGB_COLOR_TABLE_VIEW_DARK_BACKGROUND       @"argbTableViewDarkCellBackgroundColor"
#define KEY_ARGB_COLOR_TABLE_VIEW_LIGHT_BACKGROUND      @"argbTableViewLightCellBackgroundColor"
#define KEY_ARGB_COLOR_EXTENDED_PLAYER_BACKGROUND       @"argbExtendedPlayerBackgroundColor"
#define KEY_ARGB_COLOR_PLAYER_MAIN_VIEW_BACKGROUND      @"argbPlayerMainViewBackgroundColor"

#define KEY_ARGB_COLOR_CHAPTER_VIEW_BACKGROUND          @"argbChapterViewBackgroundColor"
#define KEY_ARGB_COLOR_CHAPTER_VIEW_DEFAULT_PAIR        @"argbChapterViewDefaultPairColor"
#define KEY_ARGB_COLOR_CHAPTER_VIEW_DEFAULT_IMPAIR      @"argbChapterViewDefaultImpairColor"
#define KEY_ARGB_COLOR_CHAPTER_VIEW_TEXT_COLOR          @"argbChapterViewTextColor"
#define KEY_ARGB_COLOR_CHAPTER_VIEW_ADVANCE_COLOR       @"argbChapterViewAdvanceColor"

#define KEY_ARGB_COLOR_COMPLEMENT_BAR_FOREGROUND        @"argbComplementBarForegroundColor"
#define KEY_ARGB_COLOR_COMPLEMENT_BAR_BACKGROUND        @"argbComplementBarBackgroundColor"
#define KEY_ARGB_COLOR_COMPLEMENT_BORDER                @"argbComplementBorderColor"

#define KEY_ARGB_COLOR_PLAYER_TIMELABEL_BACKGROUND      @"argbPlayerTimeLabelBackground"
#define KEY_ARGB_COLOR_PLAYER_TIMELABEL_FOREGROUND      @"argbPlayerTimeLabelForeground"
#define KEY_ARGB_COLOR_PLAYER_TIMELABEL_SHADOW          @"argbPlayerTimeLabelShadow"
#define KEY_ARGB_COLOR_PLAYER_TIMELABEL_BORDER          @"argbPlayerTimeLabelBorder"

#define KEY_ARGB_DOWNLOAD_PROGRESS_INFO                 @"argbDownloadProgressLabel"
#define KEY_ARGB_DOWLOAD_ERROR                          @"argbDownloadErrorLabel"

#define KEY_CHAPTER_VIEW_SMALL_HORIZONTAL_BORDER        @"chapterViewSmallHorizontalBorder"
#define KEY_CHAPTER_VIEW_SMALL_VERTICAL_BORDER          @"chapterViewSmallVerticalBorder"
#define KEY_CHAPTER_VIEW_BIG_HORIZONTAL_BORDER          @"chapterViewBigHorizontalBorder"
#define KEY_CHAPTER_VIEW_BIG_VERTICAL_BORDER            @"chapterViewBigVerticalBorder"

#define KEY_NO_NAMED_CATEGORY_LABEL                     @"noNamedCategoryLabel"
#define KEY_NO_NAMED_CATEGORY_CELL_LABEL                @"noNamedCategoryCellLabel"

#define KEY_SOURCES_LIST_TITLE                          @"sourcesListTitle"
#define KEY_LIBRARY_TAB_NAME                            @"libraryTabName"
#define KEY_DOCUMENTS_LIST_TITLE                        @"documentsListTitle"
#define KEY_AVAILABLE_DOCUMENTS_BUTTON_TITLE            @"availableDocumentsButtonTitle"
#define KEY_DOWNLOADED_DOCUMENTS_BUTTON_TITLE           @"downloadedDocumentsButtonTitle"

#define KEY_ADVANCED_TOC_MODE                           @"advancedTocMode"
#define KEY_SINGLE_COMPLEMENT_STRING                    @"singleComplementString"
#define KEY_MULTIPLE_COMPLEMENTS_STRING                 @"multipleComplementsString"
#define KEY_SHORT_COMPLEMENT_STRING                     @"shortComplementString"


#define KEY_IPHONE_CHANNEL_CELL_IDENTIFIER              @"iPhoneChannelCellIdentifier"
#define KEY_IPHONE_CHANNEL_CELL_HEIGHT                  @"iPhoneChannelCellHeight"
#define KEY_IPHONE5_CHANNEL_CELL_IDENTIFIER              @"iPhone5ChannelCellIdentifier"
#define KEY_IPHONE5_CHANNEL_CELL_HEIGHT                  @"iPhone5ChannelCellHeight"

#define KEY_IPAD_MAINVIEWCONTROLLER_TITLE               @"iPadMainViewTitle"

@implementation ConfigurationManager

@synthesize properties;

+ (ConfigurationManager *) sharedInstance{
    static dispatch_once_t onceToken;
    static ConfigurationManager * __sharedInstance = nil;
    
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[self alloc] init];
        
        //read configuration file
        NSString *xmlFile;
        NSString *path = [[NSBundle mainBundle] bundlePath];
        
        xmlFile = [path stringByAppendingPathComponent:@"configuration.plist"];
        __sharedInstance.properties = [NSDictionary dictionaryWithContentsOfFile:xmlFile] ;
    
    
    });
    return __sharedInstance;
}
+ (UIColor *) colorWithHexString: (NSString *) hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];          
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];                      
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];                      
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}


- (NSString *)iPadMainViewTitle{
    if([self.properties objectForKey:KEY_IPAD_MAINVIEWCONTROLLER_TITLE])
        return [self.properties objectForKey:KEY_IPAD_MAINVIEWCONTROLLER_TITLE];
    return @"Web Media";
}


- (BOOL)multiChannelConfiguration{
    if(![self.properties objectForKey:KEY_MULTI_CHANNEL_CONFIGURATION])return YES;
    return [[self.properties objectForKey:KEY_MULTI_CHANNEL_CONFIGURATION] boolValue];
}

- (NSArray *)sourcesList{
    return [self.properties objectForKey:KEY_SOURCES_LIST_ARRAY];
}


- (BOOL)canAddSource{
    return [[self.properties objectForKey:KEY_CAN_ADD_SOURCE] boolValue];
}
- (BOOL)canDeleteSource{
    return [[self.properties objectForKey:KEY_CAN_DELETE_SOURCE] boolValue];
}
//Channel list configuration
- (NSString *)iPhoneChannelCellIdentifier{
    return [self.properties objectForKey:KEY_IPHONE_CHANNEL_CELL_IDENTIFIER] ? : @"classicCell";
}
- (float)iPhoneChannelCellHeight{
    return [[self.properties objectForKey:KEY_IPHONE_CHANNEL_CELL_HEIGHT] floatValue]? : 54;
}
- (NSString *)iPhone5ChannelCellIdentifier{
    return [self.properties objectForKey:KEY_IPHONE5_CHANNEL_CELL_IDENTIFIER] ? : [self iPhoneChannelCellIdentifier] ;
}
- (float)iPhone5ChannelCellHeight{
    return [[self.properties objectForKey:KEY_IPHONE5_CHANNEL_CELL_HEIGHT] floatValue]? : [self iPhoneChannelCellHeight];
}

- (BOOL)useUserLocation{
    return [[self.properties objectForKey:KEY_USE_USER_LOCATION] boolValue];
}
- (BOOL)showEmptyImageInTocTable{
    if(![self.properties objectForKey:KEY_SHOW_EMPTY_IMAGE_IN_TOC_TABLE])return YES;
    return [[self.properties objectForKey:KEY_SHOW_EMPTY_IMAGE_IN_TOC_TABLE] boolValue];
}
- (BOOL)addAutoNumberingToChapitreAndSegmentName{
    if(![self.properties objectForKey:KEY_ADD_AUTO_NUMBERING_TO_CHAPTER_AND_SEGMENT_NAME])return YES;
    return [[self.properties objectForKey:KEY_ADD_AUTO_NUMBERING_TO_CHAPTER_AND_SEGMENT_NAME] boolValue];
}
- (BOOL)channelListWithDetails{
    if(![self.properties objectForKey:KEY_CHANNELLIST_WITH_DETAILS])return NO;
    return [[self.properties objectForKey:KEY_CHANNELLIST_WITH_DETAILS] boolValue];
}
- (BOOL)showTutorialFromCreditPage{
    if(![self.properties objectForKey:KEY_SHOW_TUTORIAL_FROM_CREDITS_PAGE])return NO;
    return [[self.properties objectForKey:KEY_SHOW_TUTORIAL_FROM_CREDITS_PAGE] boolValue];

}
- (BOOL)playerDescriptionFirst{
    if([self.properties objectForKey:KEY_PLAYER_DESCRIPTION_FIRST]){
        return [[self.properties objectForKey:KEY_PLAYER_DESCRIPTION_FIRST] boolValue];
    }
    return NO;
}
- (BOOL)downloadInBackground{
    if([self.properties objectForKey:KEY_DOWNLOAD_IN_BACKGROUND]){
        return [[self.properties objectForKey:KEY_DOWNLOAD_IN_BACKGROUND] boolValue];
    }
    return NO;
}
              

- (BOOL)advancedTocMode{
    if(![self.properties objectForKey:KEY_ADVANCED_TOC_MODE])return NO;
    return [[self.properties objectForKey:KEY_ADVANCED_TOC_MODE] boolValue];
}

- (NSString *)singleComplementString{
    if([self.properties objectForKey:KEY_SINGLE_COMPLEMENT_STRING])
        return [self.properties objectForKey:KEY_SINGLE_COMPLEMENT_STRING];
    return @"Complément";
}
- (NSString *)multipleComplementsString{
    if([self.properties objectForKey:KEY_MULTIPLE_COMPLEMENTS_STRING])
        return [self.properties objectForKey:KEY_MULTIPLE_COMPLEMENTS_STRING];
    return @"Compléments";
}
- (NSString *)shortComplementString{
    if([self.properties objectForKey:KEY_SHORT_COMPLEMENT_STRING])
        return [self.properties objectForKey:KEY_SHORT_COMPLEMENT_STRING];
    return @"Comp.";
}
- (UIColor *)navigationBarTintColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_NAVIGATION_BAR] ];
}

- (UIColor *)selectedChannelBorderColor{
    if([self.properties objectForKey:KEY_ARGB_COLOR_SELECTED_CHANNEL_BORDER])
        return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_SELECTED_CHANNEL_BORDER] ];
    return [self navigationBarTintColor];
}
- (UIColor *)unselectedChannelBorderColor{
    if([self.properties objectForKey:KEY_ARGB_COLOR_UNSELECTED_CHANNEL_BORDER])
        return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_UNSELECTED_CHANNEL_BORDER] ];
    return [UIColor lightGrayColor];
}

- (UIColor *)mainViewTabletteChannelScrollBackgroundColor{
    if([self.properties objectForKey:KEY_ARGB_COLOR_MAIN_VIEW_IPAD_CHANNEL_SCROLL_BACKGROUND])
        return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_MAIN_VIEW_IPAD_CHANNEL_SCROLL_BACKGROUND] ];
    return [UIColor blackColor];
}




- (UIColor *)chapterViewBackgroundColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_CHAPTER_VIEW_BACKGROUND] ];
}

- (UIColor *)chapterViewDefaultPairColor{
return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_CHAPTER_VIEW_DEFAULT_PAIR] ];
}

- (UIColor *)chapterViewDefaultImpairColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_CHAPTER_VIEW_DEFAULT_IMPAIR] ];
}

- (UIColor *)chapterViewTextColor{
return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_CHAPTER_VIEW_TEXT_COLOR] ];
}

- (UIColor *)chapterViewAdvanceColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_CHAPTER_VIEW_ADVANCE_COLOR] ];
}
- (UIColor *)chapterViewBorderColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_CHAPTER_VIEW_BORDER] ];
}
- (UIColor *)tableViewSectionBackgroundColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_TABLE_VIEW_SECTION_BACKGROUND] ];
}
- (UIColor *)tableViewSectionForegroundColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_TABLE_VIEW_SECTION_FOREGROUND] ];
}
- (UIColor *)getSelectedColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_SELECTED] ];
}
- (UIColor *)getUnSelectedColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_UNSELECTED] ];
}
- (UIColor *)tableViewDarckCellBackgroundColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_TABLE_VIEW_DARK_BACKGROUND] ];
}
- (UIColor *)tableViewLightCellBackgroundColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_TABLE_VIEW_LIGHT_BACKGROUND] ];
}

- (UIColor *)getExtendedPlayerBackgroundColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_EXTENDED_PLAYER_BACKGROUND] ];
}
- (UIColor *)getPlayerMainViewBackgroundColor{
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_PLAYER_MAIN_VIEW_BACKGROUND] ];
}


- (UIColor *)complementBarForegroundColor{
    if (![self.properties objectForKey:KEY_ARGB_COLOR_COMPLEMENT_BAR_FOREGROUND]) {
        return [UIColor whiteColor];
    }
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_COMPLEMENT_BAR_FOREGROUND] ];
}
- (UIColor *)complementBarBackgroundColor{
    if (![self.properties objectForKey:KEY_ARGB_COLOR_COMPLEMENT_BAR_BACKGROUND]) {
        return [UIColor grayColor];
    }
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_COMPLEMENT_BAR_BACKGROUND] ];
}

- (UIColor *)complementBorderColor{
    if (![self.properties objectForKey:KEY_ARGB_COLOR_COMPLEMENT_BORDER]) {
        return [UIColor darkGrayColor];
    }
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_COMPLEMENT_BORDER] ];
}
- (UIColor *)playerTimeLabelBackground{
    if (![self.properties objectForKey:KEY_ARGB_COLOR_PLAYER_TIMELABEL_BACKGROUND]) {
        return [UIColor darkGrayColor];
    }
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_PLAYER_TIMELABEL_BACKGROUND] ];
}

- (UIColor *)playerTimeLabelForeground{
    if (![self.properties objectForKey:KEY_ARGB_COLOR_PLAYER_TIMELABEL_FOREGROUND]) {
        return [UIColor whiteColor];
    }
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_PLAYER_TIMELABEL_FOREGROUND] ];
}

- (UIColor *)playerTimeLabelShadow{
    if (![self.properties objectForKey:KEY_ARGB_COLOR_PLAYER_TIMELABEL_SHADOW]) {
        return [UIColor blackColor];
    }
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_PLAYER_TIMELABEL_SHADOW] ];
}

- (UIColor *)playerTimeLabelBorder{
    if (![self.properties objectForKey:KEY_ARGB_COLOR_PLAYER_TIMELABEL_BORDER]) {
        return [UIColor blackColor];
    }
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_COLOR_PLAYER_TIMELABEL_BORDER] ];
}

- (UIColor *)downloadProgressInfoColor{
    if (![self.properties objectForKey:KEY_ARGB_DOWNLOAD_PROGRESS_INFO]) {
        return [UIColor blueColor];
    }
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_DOWNLOAD_PROGRESS_INFO] ];

}
- (UIColor *)downloadErrorInfoColor{
    if (![self.properties objectForKey:KEY_ARGB_DOWLOAD_ERROR]) {
        return [UIColor redColor];
    }
    return [ConfigurationManager colorWithHexString:[self.properties objectForKey:KEY_ARGB_DOWLOAD_ERROR] ];
}

- (UIImage *)getAccessoryImage{
    if (![self.properties objectForKey:KEY_ACCESSORY_BUTTON_IMAGE_NAME]) {
        return nil;
    }
    return [UIImage imageNamed:[self.properties objectForKey:KEY_ACCESSORY_BUTTON_IMAGE_NAME]];
}


- (float)getChapterViewSmallHorizontalBorder{
    return [[self.properties objectForKey:KEY_CHAPTER_VIEW_SMALL_HORIZONTAL_BORDER] floatValue];
}
- (float)getChapterViewSmallVerticalBorder{
    return [[self.properties objectForKey:KEY_CHAPTER_VIEW_SMALL_VERTICAL_BORDER] floatValue];
}
- (float)getChapterViewBigHorizontalBorder{
    return [[self.properties objectForKey:KEY_CHAPTER_VIEW_BIG_HORIZONTAL_BORDER] floatValue];
}
- (float)getChapterViewBigVerticalBorder{
    return [[self.properties objectForKey:KEY_CHAPTER_VIEW_BIG_VERTICAL_BORDER] floatValue];
}


- (NSString *)getNoNamedCategoryHeaderLabel{
    if([self.properties objectForKey:KEY_NO_NAMED_CATEGORY_LABEL])
        return [self.properties objectForKey:KEY_NO_NAMED_CATEGORY_LABEL];
    return @"Aucune";
}

- (NSString *)getNoNamedCategorySearchCellLabel{
    if([self.properties objectForKey:KEY_NO_NAMED_CATEGORY_CELL_LABEL])
        return [self.properties objectForKey:KEY_NO_NAMED_CATEGORY_CELL_LABEL];
    return @"Aucune";
}



//Tab items and titles
- (NSString *)sourcesListTitle{
    if([self.properties objectForKey:KEY_SOURCES_LIST_TITLE])
        return [self.properties objectForKey:KEY_SOURCES_LIST_TITLE];
    return @"Sources";
}

- (NSString *)libraryTabName{
    if([self.properties objectForKey:KEY_LIBRARY_TAB_NAME])
        return [self.properties objectForKey:KEY_LIBRARY_TAB_NAME];
    return @"Bibliothèque";
}

- (NSString *)documentsListTitle{
    if([self.properties objectForKey:KEY_DOCUMENTS_LIST_TITLE])
        return [self.properties objectForKey:KEY_DOCUMENTS_LIST_TITLE];
    return @"Bibliothèque";
    
}
- (NSString *)availableDocumentsButtonTitle{
    if([self.properties objectForKey:KEY_AVAILABLE_DOCUMENTS_BUTTON_TITLE])
        return [self.properties objectForKey:KEY_AVAILABLE_DOCUMENTS_BUTTON_TITLE];
    return @"Disponibles";
}

- (NSString *)downloadedDocumentsButtonTitle{
    if([self.properties objectForKey:KEY_DOWNLOADED_DOCUMENTS_BUTTON_TITLE])
        return [self.properties objectForKey:KEY_DOWNLOADED_DOCUMENTS_BUTTON_TITLE];
    return @"Téléchargés";
}

@end
