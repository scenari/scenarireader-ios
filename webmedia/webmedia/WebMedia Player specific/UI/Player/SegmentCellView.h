//
//  SegmentCellView.h
//  PhoneRadio
//
//  Created by soreha on 19/07/10.
//  
//

#import <UIKit/UIKit.h>


@interface SegmentCellView : UITableViewCell {
	UILabel *readLabel;
	UIImageView *complementIndicator;
}
@property(nonatomic, strong) UILabel *readLabel;
@property(nonatomic, strong) UIImageView *complementIndicator;

@end
