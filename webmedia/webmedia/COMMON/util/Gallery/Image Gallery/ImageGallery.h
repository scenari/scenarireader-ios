//
//  ImageGallery.h
//  WebMedia
//
//  Created by soreha on 22/02/11.
//  
//

#import <Foundation/Foundation.h>

@class ImageUnit;

@interface ImageGallery : NSObject {
	NSString        *name;
	NSString        *identifiant;
	NSMutableArray  *images;
	NSString        *urlRelativeTo;
}

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *identifiant;
@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, strong) NSString *urlRelativeTo;

-(id) initWithSingleImage:(ImageUnit*)img;

-(ImageUnit*) imageAt:(int)index;
-(int) imagesCount;


@end
