//
//  MoviePlayerView.h
//  webmedia
//
//  Created by soreha on 25/09/12.
//  
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface MoviePlayerView : UIView
- (void)setPlayer:(AVPlayer *)setPlayer;
- (AVPlayer*)getPlayer;

@end
