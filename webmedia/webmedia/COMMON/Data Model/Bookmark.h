//
//  Bookmark.h
//  webmedia
//
//  Created by soreha on 28/05/13.
//  
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Document;

@interface Bookmark : NSManagedObject

@property (nonatomic, retain) NSString * pageUri;
@property (nonatomic, retain) NSString * pageTitle;
@property (nonatomic, retain) Document *document;

@end
