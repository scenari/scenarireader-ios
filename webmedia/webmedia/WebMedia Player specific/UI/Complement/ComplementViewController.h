//
//  ComplementViewController.h
//  WebMedia
//
//  Created by soreha on 20/12/10.
//  
//
#import <MessageUI/MessageUI.h>
#import <UIKit/UIKit.h>
#import "FGalleryViewController.h"
#import "ImageGallery.h"

@class RichMedia, ComplementView;

@interface ComplementViewController : UIViewController <UIWebViewDelegate, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate, FGalleryViewControllerDelegate>{
	IBOutlet UIWebView *content;
	//IBOutlet UILabel *complementTitleLabel;
		
	RichMedia* richMedia;
}
@property (nonatomic, retain)ComplementView *complementView;
@property (weak, nonatomic) IBOutlet UIImageView *topShadowView;

@property (nonatomic, strong) ImageGallery *gallery;
@property (nonatomic, strong) FGalleryViewController *localGallery;
@property(nonatomic, strong) IBOutlet UIWebView *content;
//@property(nonatomic, retain) IBOutlet UILabel *complementTitleLabel;

@property(nonatomic, strong) RichMedia* richMedia;

-(id) initWithRichMedia:(RichMedia*)media;
-(id) initForExtrasWithRichMedia:(RichMedia*)media;

-(id) initWithRichMedia:(RichMedia*)media chapterIndex:(int)cindex andSegmentIndex:(int)sindex;

-(void) showComplement:(int)index;
-(void) showExtra:(int)index;

@end
