//
//  Chapter.h
//  PhoneRadio
//
//  Created by soreha on 27/07/10.
//  
//

#import <Foundation/Foundation.h>
#import "Segment.h"

@interface Chapter : NSObject {
	NSString *chapterId;
	NSString *title;
	NSMutableArray *segments;
	Boolean root;
	float start;
	float end;
}

@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *chapterId;
@property(nonatomic, strong) NSMutableArray *segments;
@property(nonatomic) Boolean root;

@property(nonatomic) float start;
@property(nonatomic) float end;

-(Segment*) firstSegment;
-(Segment*) lastSegment;

@end
