//
//  ImageViewController.h
//  WebMedia
//
//  Created by soreha on 29/11/10.
//  
//

#import <UIKit/UIKit.h>


@interface FullScreenViewController : UIViewController <UIGestureRecognizerDelegate>{
	IBOutlet UIImageView *image;
	IBOutlet UITextView *textView;
	IBOutlet UIView *viewBar;
	IBOutlet UILabel *messageLabel;
	IBOutlet UIButton *playPauseButton;
	IBOutlet UIButton *keepUpToDateButton;
	
    IBOutlet UIView *topBar;
	IBOutlet UILabel *chapterLabel;
	
    
    id __unsafe_unretained mediaDelegate;
	BOOL playing;
}
@property(nonatomic, strong) IBOutlet UIImageView *image;
@property(nonatomic, strong) IBOutlet UIView *topBar;
@property(nonatomic, strong) IBOutlet UITextView *textView;
@property(nonatomic, strong) IBOutlet UIView *viewBar;
@property(nonatomic, strong) IBOutlet UIButton *playPauseButton;
@property(nonatomic, strong) IBOutlet UIButton *keepUpToDateButton;

@property(nonatomic, strong) IBOutlet UILabel *chapterLabel;
@property(nonatomic, strong) IBOutlet UILabel *messageLabel;


@property(nonatomic, unsafe_unretained) id mediaDelegate;
@property(nonatomic) BOOL playing;


-(IBAction) close:(id)sender;
-(IBAction) next:(id)sender;
-(IBAction) previous:(id)sender;
-(IBAction) switchPlayPause:(id)sender;
-(IBAction) switchKeepUpToDate:(id)sender;

-(void) setTextChapter:(NSString*) textChapter andSegment:(NSString *) textSegment;

-(void) showEndMessage;

@end
