//
//  EmissionDefaultViewController.h
//  PhoneRadio
//
//  Created by soreha on 18/07/10.
//  
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>


#import "RichMedia.h"
#import "TocView.h"
#import "ExtrasTableViewController.h"
#import "ComplementView.h"

@class TimeMarkerView, MoviePlayerView;

@interface EmissionDefaultViewController : UIViewController <UIPopoverControllerDelegate, UIWebViewDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate>{
	IBOutlet UIView                     *topView;
	IBOutlet UIWebView                  *abstractView;
	IBOutlet UIButton                   *playPauseButton;
	IBOutlet UIButton                   *nextButton;
	IBOutlet UIButton                   *previousButton;
	IBOutlet UIButton                   *nextButtonFP;
	IBOutlet UIButton                   *previousButtonFP;
	IBOutlet UIView                     *playerControlView;
	IBOutlet UIImageView                *expandIndicatorImageView;
	IBOutlet UIImageView                *bulletImageView;
	IBOutlet UILabel                    *bulletLabel;
	IBOutlet UIImageView                *bulletBackgroundImageView;
	IBOutlet UILabel                    *advancingTimeLabel;
	IBOutlet UILabel                    *remainingTimeLabel;
	IBOutlet UIImageView                *timeLineBkgImageView;
	IBOutlet UIScrollView               *scrollView;
	IBOutlet UIView                     *actionInfoView;	
	IBOutlet UILabel                    *currentTimeFullPlayerLabel;
	IBOutlet UIView                     *loadingView;
	IBOutlet UILabel                    *messageLabel;
	IBOutlet UIActivityIndicatorView    *activityIndicator;
    
    
    RichMedia *richMedia;
	AVPlayer *audioPlayer;
	NSString *htmlContentModel;
	NSURL *baseURL;
	TocView *tocView;
	
	UITapGestureRecognizer *singleTapOnControlsPlayerView;
	UISwipeGestureRecognizer *slideToLeftOnControlsPlayerView;
	UISwipeGestureRecognizer *slideToRightOnControlsPlayerView;
	UISwipeGestureRecognizer *slideToTopOnControlsPlayerView;
	UISwipeGestureRecognizer *slideToBottomOnControlsPlayerView;
	UIPinchGestureRecognizer *zoomGestureRecognizerOnTimeLine;
	UISwipeGestureRecognizer *slideToLeftOnVideoView;
	UISwipeGestureRecognizer *slideToRightOnVideoView;
	TimeMarkerView *timeMarkerView;
	NSMutableDictionary *allillustrations;
	float zoomFactor;
}


@property (weak, nonatomic)     IBOutlet UIView                     *contentView;
@property (nonatomic, strong)   IBOutlet UIView                     *topView;
@property (nonatomic, strong)   IBOutlet UIWebView                  *abstractView;
@property (weak, nonatomic)     IBOutlet UIImageView                *scrollingIndicatorView;
@property (weak, nonatomic)     IBOutlet UIView                     *illustrationView;
@property (weak, nonatomic)     IBOutlet UIImageView                *illustrationImageView;
@property (weak, nonatomic)     IBOutlet MoviePlayerView            *videoView;
@property (weak, nonatomic)     IBOutlet UIView                     *fullscreenInfoView;
@property (weak, nonatomic)     IBOutlet UILabel                    *fullscreenTitleLabel;
@property (weak, nonatomic)     IBOutlet UILabel                    *fullScreenSubtitleLabel;
@property (weak, nonatomic)     IBOutlet UILabel                    *fullscreenComplementInfoLabel;
@property (nonatomic, strong)   IBOutlet UIView                     *actionInfoView;
@property (nonatomic, strong)   IBOutlet UIButton                   *previousButton;
@property (nonatomic, strong)   IBOutlet UIButton                   *nextButton;
@property (weak, nonatomic)     IBOutlet UIButton                   *toggleFullscreenButton;
@property (weak, nonatomic)     IBOutlet UILabel                    *legendIllustrationLabel;

@property (nonatomic, strong)   IBOutlet UIView                     *playerControlView;
@property (weak, nonatomic)     IBOutlet UIImageView                *playerControlTopBarBackgroundImageView;
@property (nonatomic, strong)   IBOutlet UIImageView                *expandIndicatorImageView;
@property (nonatomic, strong)   IBOutlet UIScrollView               *scrollView;
@property (nonatomic, strong)   IBOutlet UIImageView                *timeLineBkgImageView;
@property (nonatomic, strong)   IBOutlet UIButton                   *playPauseButton;
@property (nonatomic, strong)   IBOutlet UILabel                    *advancingTimeLabel;
@property (nonatomic, strong)   IBOutlet UILabel                    *remainingTimeLabel;
@property (nonatomic, strong)   IBOutlet UIImageView                *bulletBackgroundImageView;
@property (nonatomic, strong)   IBOutlet UIImageView                *bulletImageView;
@property (nonatomic, strong)   IBOutlet UILabel                    *bulletLabel;
@property (nonatomic, strong)   IBOutlet UILabel                    *currentTimeFullPlayerLabel;

@property (nonatomic, strong)   IBOutlet UIButton                   *previousButtonFP;
@property (nonatomic, strong)   IBOutlet UIButton                   *nextButtonFP;

@property (nonatomic, strong)   IBOutlet UIView                     *loadingView;
@property (nonatomic, strong)   IBOutlet UIActivityIndicatorView    *activityIndicator;

@property (nonatomic, strong)   IBOutlet UILabel                    *messageLabel;

//iPad
@property (weak, nonatomic) IBOutlet UIView *webViewsContainer;
@property (weak, nonatomic) IBOutlet UIWebView *leftWebView;
@property (weak, nonatomic) IBOutlet UIWebView *centerWebView;
@property (weak, nonatomic) IBOutlet UIWebView *rightWebView;


//GRs
@property (nonatomic, strong) UITapGestureRecognizer    *singleTapOnControlsPlayerView;
@property (nonatomic, strong) UISwipeGestureRecognizer  *slideToLeftOnControlsPlayerView;
@property (nonatomic, strong) UISwipeGestureRecognizer  *slideToRightOnControlsPlayerView;
@property (nonatomic, strong) UISwipeGestureRecognizer  *slideToTopOnControlsPlayerView;
@property (nonatomic, strong) UISwipeGestureRecognizer  *slideToBottomOnControlsPlayerView;
@property (nonatomic, strong) UISwipeGestureRecognizer  *slideToLeftOnVideoView;
@property (nonatomic, strong) UISwipeGestureRecognizer  *slideToRightOnVideoView;
@property (nonatomic, strong) UIPinchGestureRecognizer  *zoomGestureRecognizerOnTimeLine;



@property (nonatomic, strong) RichMedia *richMedia;
@property (strong, nonatomic) Segment *playerCurrentSegment;
@property (nonatomic, strong) AVPlayer *audioPlayer;
@property (strong) id playerSecondObserver;
@property (strong) id playerIllustrationTimeObserver;
@property (strong) id playerSegmentChangeObserver;

@property (nonatomic, retain)ComplementView *complementView;
@property (nonatomic, strong) TocView *tocView;
@property (nonatomic, strong) ExtrasTableViewController *extrasView;

@property (nonatomic, strong) UIPopoverController *menu;
@property (strong, nonatomic) UIImageView *helpImageView;
@property (nonatomic, strong) NSMutableDictionary *allIllustrationImageThumbsDictionnary;
@property (nonatomic, strong) TimeMarkerView *timeMarkerView;
@property (nonatomic) float zoomFactor;

@property (nonatomic, strong) NSURL *baseURL;
@property (nonatomic, strong) NSString *htmlContentModel;
@property (nonatomic, strong) NSString *htmlReferenceModel;
//iPad
@property (nonatomic, strong) NSString *htmliPadSegmentModel;
@property (nonatomic, strong) NSString *htmliPadComplementsModel;
@property (nonatomic, strong) NSString *htmliPadReferencesModel;
@property (nonatomic, strong) NSString *htmliPadSegmentDescriptionModel;


@property (nonatomic) BOOL fullscreen;
@property (nonatomic) BOOL fullscreenAnchorMode;


- (IBAction)toggleFullscreenAction:(id)sender;
- (IBAction)showHelpAction:(id)sender;
- (IBAction)playPauseAction;
- (IBAction)nextAction;
- (IBAction)previousAction;
//- (IBAction)showComplement:(id)sender;


+ (UIImage*)imageFromResource:(NSString*)name ;
-(id) initWithMedia:(RichMedia *)aMedia;
-(void) goToSegment:(int)segmentIndex inChapter:(int)chapterIndex;
-(void) backToRootView;
-(void) checkForContinueReading;
-(void)forceUpdate;


-(void) setFullPlayerView;

@end
