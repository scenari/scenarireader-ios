//
//  TocView.h
//  PhoneRadio
//
//  Created by soreha on 30/11/10.
//  
//

#import <UIKit/UIKit.h>
#import "CustomTableViewController.h"
#import "ComplementView.h"
#import "FullScreenViewController.h"


@class RichMedia;
@class EmissionDefaultViewController;

@interface TocView : CustomTableViewController <UIWebViewDelegate>{
	RichMedia *__unsafe_unretained richMedia;
	EmissionDefaultViewController *__unsafe_unretained emissionDefaultViewController;
}
@property (nonatomic, strong) NSString *htmlReferenceModel;

@property (nonatomic, retain)ComplementView *complementView;
@property (nonatomic, strong) FullScreenViewController *fullScreenImageView;


@property(nonatomic, unsafe_unretained) RichMedia *richMedia;
@property(nonatomic, unsafe_unretained) EmissionDefaultViewController *emissionDefaultViewController;

@property(nonatomic,strong) UISegmentedControl *segmentedControl;

@property(nonatomic, strong) UILabel *footerLabel;
@property(nonatomic, strong) UIPopoverController *popover;


-(id) initWithContent:(RichMedia*)media;

@end
