//
//  Document.m
//  webmedia
//
//  Created by soreha on 12/06/13.
//  
//

#import "Document.h"
#import "Bookmark.h"
#import "Channel.h"
#import "Thumbnail.h"
#import "ASIHTTPRequest.h"


@implementation Document

@dynamic downloadError;
@dynamic author;
@dynamic summary;
@dynamic category;
@dynamic lastBuildDate;
@dynamic comments;
@dynamic location;
@dynamic pubDate;
@dynamic smartPhoneLink;
@dynamic duration;
@dynamic isFavorite;
@dynamic version;
@dynamic updateAvailable;
@dynamic downloadSuspended;
@dynamic tmpDownloadFilePath;
@dynamic lastOpenedPage;
@dynamic tabletLink;
@dynamic downloaded;
@dynamic thumbnail_large_URI;
@dynamic link;
@dynamic isSelected;
@dynamic distanceFromLocation;
@dynamic downloadProgress;
@dynamic thumbnail_small_URI;
@dynamic legal;
@dynamic keywords;
@dynamic rssPos;
@dynamic title;
@dynamic guid;
@dynamic downloading;
@dynamic localMediaURI;
@dynamic pubType;
@dynamic thumbnail_large;
@dynamic highlights;
@dynamic channel;
@dynamic bookmarks;
@dynamic thumbnail_small;
@dynamic notes;


@synthesize request;

- (void)copyAttributesFromObject:(Document *)object{
    NSArray *attributeKeys = [[[self entity] attributesByName] allKeys];
	NSDictionary  *values = [object dictionaryWithValuesForKeys:attributeKeys];
	NSMutableDictionary *newValues = [[NSMutableDictionary alloc] initWithDictionary:values];
	
	[newValues removeObjectForKey:@"localMediaURI"];
	[newValues removeObjectForKey:@"tmpDownloadFilePath"];
	[newValues removeObjectForKey:@"downloaded"];
	[newValues removeObjectForKey:@"downloading"];
	[newValues removeObjectForKey:@"updateAvailable"];
    [newValues removeObjectForKey:@"downloadProgress"];
    [newValues removeObjectForKey:@"downloadSuspended"];
    [newValues removeObjectForKey:@"isFavorite"];
    [newValues removeObjectForKey:@"lastOpenedPage"];

	
	[self setValuesForKeysWithDictionary: newValues];
}

- (void)setProgress:(float) value {
	NSNumber *number = [[NSNumber alloc] initWithFloat:value];
	self.downloadProgress = number;
}

@end
