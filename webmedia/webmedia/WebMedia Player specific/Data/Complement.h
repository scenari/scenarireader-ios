//
//  Complement.h
//  PhoneRadio
//
//  Created by soreha on 08/07/10.
//  
//

#import <Foundation/Foundation.h>
#import "HtmlContentType.h"

@interface Complement : HtmlContentType

@property (nonatomic, strong) id parent;

@end
