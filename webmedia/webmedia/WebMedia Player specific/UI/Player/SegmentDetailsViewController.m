//
//  SegmentDetailsViewController.m
//  PhoneRadio
//
//  Created by soreha on 19/01/11.
//  
//

#import "SegmentDetailsViewController.h"
#import "Chapter.h"
#import "Segment.h"
#import "Illustration.h"
#import "Reference.h"

#import "ComplementViewController.h"
#import "Complement.h"
#import "FullScreenViewController.h"
#import "RichMedia.h"
#import "FullScreenViewController.h"
#import "ConfigurationManager.h"


#define kCustomButtonHeight		30.0

@interface SegmentDetailsViewController (PrivateMethods)
	-(void) updateData;
	-(void) segmentAction;
@end


@implementation SegmentDetailsViewController


int chapIndex;
int segIndex;


@synthesize abstractView;
@synthesize richMedia;
@synthesize htmlContentModel;
@synthesize baseURL;


-(id) initWithMedia:(RichMedia *)aMedia chapterIndex:(int)cIndex andSegmentIndex:(int)sIndex{
	if ((self = [super initWithNibName:@"SegmentDetailsViewController" bundle:nil])) {
   		self.richMedia = aMedia;
		chapIndex = cIndex;
		segIndex = sIndex;
	}
	
    return self;	
}

- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = @"Segment";
	
	[self.abstractView setBackgroundColor:[UIColor clearColor]];
		
	
	NSString *path = [[NSBundle mainBundle] pathForResource :@"SegmentModel" ofType:@"html"];
	self.htmlContentModel = [[NSString alloc] initWithContentsOfFile:path 
													   encoding:NSUTF8StringEncoding 
														  error:nil];
	self.baseURL = [[NSURL alloc] initFileURLWithPath:path];
	
    UIImage *down = [UIImage imageNamed:@"down"];
    UIImage *up = [UIImage imageNamed:@"up"];
#pragma ACCESSIBILITY
    down.accessibilityLabel = @"Afficher les informations de l'élément suivant.";
#pragma ACCESSIBILITY
    up.accessibilityLabel = @"Afficher les informations de l'élément précédent.";
    
	UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:
											[NSArray arrayWithObjects:
											 up,
											 down,
											 nil]];
	
    
    
	[segmentedControl addTarget:self action:@selector(segmentAction) forControlEvents:UIControlEventValueChanged];
	segmentedControl.frame = CGRectMake(0, 0, 80, kCustomButtonHeight);
	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;

	segmentedControl.momentary = YES;
	
	
	
	UIBarButtonItem *segmentBarItem = [[UIBarButtonItem alloc] initWithCustomView:segmentedControl];
    
	self.navigationItem.rightBarButtonItem = segmentBarItem;
   // self.contentSizeForViewInPopover = CGSizeMake(350, 410);
    
    
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        [self navigationController].navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
        segmentedControl.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];

    }
    /*else{
        self.navigationController.navigationBar.tintColor = [UIColor blueColor];
        self.navigationItem.rightBarButtonItem.tintColor =  segmentedControl.tintColor =[UIColor blueColor];
    }
    */

	
}
-(void) updateData{
	NSMutableString *htmlContent= [NSMutableString stringWithString:self.htmlContentModel];
	Chapter *chapter = [[self.richMedia chapters] objectAtIndex:chapIndex];
	Segment *segment = [chapter.segments objectAtIndex:segIndex];
	
	Illustration *illustration = [segment firstIllustration];
	
    BOOL addNumbers = [[ConfigurationManager sharedInstance] addAutoNumberingToChapitreAndSegmentName];
    NSString *info ;
    if(addNumbers)
        info = [NSString stringWithFormat:@"%d - %@",self.richMedia.currentChapter+1, [chapter title]?[chapter title]:@""];
    else
        info = [chapter title]?[chapter title]:@"";
    
	[htmlContent replaceOccurrencesOfString:@"CHAPTER_TITLE"
								 withString:info
									options:NSCaseInsensitiveSearch
									  range:NSMakeRange(0, [htmlContent length]) ];
	//self.segmentTitleView.text = [segment title];
	[htmlContent replaceOccurrencesOfString:@"SEGMENT_DESCRIPTION" 
								 withString:([segment short_description]?[segment short_description]:@"") 
									options:NSCaseInsensitiveSearch 
									  range:NSMakeRange(0, [htmlContent length]) ];
	
	[htmlContent replaceOccurrencesOfString:@"CHAPTER_TITLE" 
								 withString:([chapter title]?[chapter title]:@"") 
									options:NSCaseInsensitiveSearch 
									  range:NSMakeRange(0, [htmlContent length]) ];
		
	[htmlContent replaceOccurrencesOfString:@"SEGMENT_TITLE" 
								 withString:([segment title] && ([segment.title compare:[chapter title]]!=NSOrderedSame)?[segment title]:@"")
									options:NSCaseInsensitiveSearch 
									  range:NSMakeRange(0, [htmlContent length]) ];
	
	if([illustration.resource refUri] && [[illustration.resource refUri] hasPrefix:@"/"]){
		[htmlContent replaceOccurrencesOfString:@"IMAGE_SRC" 
									 withString:([NSString stringWithFormat:@"%@%@",self.richMedia.base,[[illustration.resource refUri] substringFromIndex:1]]) 
										options:NSCaseInsensitiveSearch 
										  range:NSMakeRange(0, [htmlContent length]) ];		
	}else {
		[htmlContent replaceOccurrencesOfString:@"IMAGE_SRC" 
									 withString:([illustration.resource refUri]?[NSString stringWithFormat:@"%@%@",self.richMedia.base,[illustration.resource refUri]]:@"-") 
										options:NSCaseInsensitiveSearch 
										  range:NSMakeRange(0, [htmlContent length]) ];
	}
	
	[htmlContent replaceOccurrencesOfString:@"IMAGE_VISIBLE" 
								 withString:([illustration.resource refUri]&&!IS_IPAD()?@"block":@"none")
									options:NSCaseInsensitiveSearch 
									  range:NSMakeRange(0, [htmlContent length]) ];
	
	
	
	//complement
	
	[htmlContent replaceOccurrencesOfString:@"DISPLAY_COMPLEMENT_BLOCK" 
								 withString:(([[segment complements] count]>=1 || [segment.long_description length]>=1)?@"block":@"none") 
									options:NSCaseInsensitiveSearch 
									  range:NSMakeRange(0, [htmlContent length]) ];
	
	if([[segment complements] count]>=1){
		if([[segment complements] count]==1){
			[htmlContent replaceOccurrencesOfString:@"COMPLEMENT_NUMBER" 
										 withString:[NSString stringWithFormat:@"1 %@" , [[ConfigurationManager sharedInstance] singleComplementString] ]
											options:NSCaseInsensitiveSearch 
											  range:NSMakeRange(0, [htmlContent length]) ];
		}
		else{
			[htmlContent replaceOccurrencesOfString:@"COMPLEMENT_NUMBER" 
										 withString:[NSString stringWithFormat:@"%d %@",[[segment complements] count], [[ConfigurationManager sharedInstance] multipleComplementsString]]
											options:NSCaseInsensitiveSearch 
											  range:NSMakeRange(0, [htmlContent length]) ];
		}
		
		Complement *firstComplement = [[segment complements] objectAtIndex:0];
		NSMutableString *listComplements = [NSMutableString stringWithFormat:@"<li class=\"first\"><a href=\"showComplement:0\">%@</a></li>",
											[firstComplement title]];
		for (int i=1, n=[[segment complements] count];i<n; i++) {
			Complement *comp = [[segment complements] objectAtIndex:i];
			[listComplements appendString:[NSMutableString stringWithFormat:@"<li><a href=\"showComplement:%d\">%@</a></li>",
										   i,
										   [comp title]]];
		}
		[htmlContent replaceOccurrencesOfString:@"COMPLEMENTS_LIST" 
									 withString:listComplements 
										options:NSCaseInsensitiveSearch 
										  range:NSMakeRange(0, [htmlContent length]) ];
		
	}
    //References
    if([[segment references] count]==0){
        [htmlContent replaceOccurrencesOfString:@"DISPLAY_REFERENCE_BLOCK"
									 withString:@"none"
										options:NSCaseInsensitiveSearch
										  range:NSMakeRange(0, [htmlContent length]) ];
    }else{
        [htmlContent replaceOccurrencesOfString:@"DISPLAY_REFERENCE_BLOCK"
									 withString:@"block"
										options:NSCaseInsensitiveSearch
										  range:NSMakeRange(0, [htmlContent length]) ];
        Reference *firstReference = [[segment references] objectAtIndex:0];
        
        DLog(@"firstReference.type:%@",firstReference.type);
        
		NSMutableString *listRefs = [NSMutableString stringWithFormat:@"<li class=\"first\"><a class=\"%@\" href=\"showReference:0\">%@ </a></li>",
                                     [firstReference type],
                                     [firstReference title]];
        
		for (int i=1, n=[[segment references] count];i<n; i++) {
			Reference *ref = [[segment references] objectAtIndex:i];
            DLog(@"ref:%@",ref.type);
            
            [listRefs appendString:[NSMutableString stringWithFormat:@"<li><a class=\"%@\" href=\"showReference:%d\">%@ </a></li>",
                                    [ref type],
                                    i,
                                    [ref title]]];
		}
		[htmlContent replaceOccurrencesOfString:@"REFERENCES_LIST"
									 withString:listRefs
										options:NSCaseInsensitiveSearch
										  range:NSMakeRange(0, [htmlContent length]) ];
    }
	
	
	
	[self.abstractView loadHTMLString:htmlContent baseURL:baseURL ];
	
	[self.view setNeedsDisplay];
}
- (void)viewWillAppear:(BOOL)animated{
	UISegmentedControl *segmentedControl = (UISegmentedControl *)self.navigationItem.rightBarButtonItem.customView;
	
    
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
	// Before we show this view make sure the segmentedControl matches the nav bar style
	if (self.navigationController.navigationBar.barStyle == UIBarStyleBlackTranslucent ||
		self.navigationController.navigationBar.barStyle == UIBarStyleBlackOpaque)
		segmentedControl.tintColor = [UIColor darkGrayColor];
	else
		segmentedControl.tintColor = self.navigationController.navigationBar.tintColor;
	}
	
    
    
    
	[self updateData];
	
	
	[segmentedControl setEnabled:chapIndex>0 forSegmentAtIndex:0];
	
	Chapter *chapter = [[self.richMedia chapters] objectAtIndex:chapIndex];
	BOOL next = (chapIndex != [[self.richMedia chapters] count]-1) || (segIndex !=[[chapter segments] count]-1);
	
	[segmentedControl setEnabled:next forSegmentAtIndex:1];
	if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        segmentedControl.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
	}
	[super viewWillAppear:animated];
    
    BOOL oneSegOneChap =[self.richMedia isOneSegOneChapMedia];
    if(oneSegOneChap){
       self.title = [NSString stringWithFormat:@"Chapitre %d",chapIndex+1];
    }else{
       self.title = [NSString stringWithFormat:@"Segment %d-%d",chapIndex+1,segIndex+1];
    }
}

-(void) segmentAction{
	UISegmentedControl *segmentedControl = (UISegmentedControl *)self.navigationItem.rightBarButtonItem.customView;
	if(segmentedControl.selectedSegmentIndex == 0){
		//--
		if(segIndex>0){
			segIndex--;
		}else {
			chapIndex--;
			segIndex = [[[[self.richMedia chapters] objectAtIndex:chapIndex] segments] count]-1;
		}

	}
	else {
		//++
		if(segIndex < [[[[self.richMedia chapters] objectAtIndex:chapIndex] segments] count]-1){
			segIndex ++;
		}else{
			segIndex = 0;
			chapIndex ++;
		}
	}
	[self updateData];
	[self viewWillAppear:NO];
	
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {	
	if ([touch.view isKindOfClass:[UIButton class]]) {
		// we touched our control surface
        return NO; // ignore the touch
    }
    return YES; // handle the touch
}


- (void) webViewDidFinishLoad:(UIWebView *)webView {
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	
		
	NSString *requestString = [[request URL] absoluteString];
	
	if( requestString  && [requestString hasPrefix:@"open"]){
		
		Chapter *chap = [[self.richMedia chapters] objectAtIndex:chapIndex];
		Segment *seg = [[chap segments] objectAtIndex:segIndex];
		NSArray *components = [requestString componentsSeparatedByString:@":"];
		
		
		
		DLog(@"Ouverture de l'image fullScreen : %@\n",[components objectAtIndex:1]);
		
		
		
		NSMutableString *urlImg = [NSMutableString stringWithFormat:@"/%@",[components objectAtIndex:1] ];
		[urlImg replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [urlImg length])];
		
		FullScreenViewController *fsvc = [[FullScreenViewController alloc] initWithNibName:@"FullScreen" bundle:nil];
		fsvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
		
		//fsvc.title=[chap title] ;
		//[self presentModalViewController:fsvc animated:YES];
        //fsvc.image.image =   [UIImage imageWithContentsOfFile:urlImg] ;
        //[fsvc setTextChapter:chap.title andSegment:[seg title]];
		[self presentViewController:fsvc animated:YES completion:^{
            fsvc.image.image =   [UIImage imageWithContentsOfFile:urlImg] ;
            [fsvc setTextChapter:chap.title andSegment:[seg title]];
        }];
		
		return NO;
		
	}else if(requestString  && [[requestString lowercaseString] hasPrefix:@"showcomplement"]){
		NSArray *components = [requestString componentsSeparatedByString:@":"];
		
		int index = [[components objectAtIndex:1] intValue];
		
		ComplementViewController *cv = [[ComplementViewController alloc] initWithRichMedia:self.richMedia chapterIndex:chapIndex andSegmentIndex:segIndex] ;
		
		UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:self action:nil];
		self.navigationItem.backBarButtonItem = backButton;
		
		[self.navigationController pushViewController:cv animated:YES];
		[cv showComplement:index];
        
        
		return NO;
		
	}else if(requestString  && [[requestString lowercaseString] hasPrefix:@"showreference"]){
        Chapter *chap = [[self.richMedia chapters] objectAtIndex:chapIndex];
		Segment *seg = [[chap segments] objectAtIndex:segIndex];
        
        NSArray *components = [requestString componentsSeparatedByString:@":"];
		int index = [[components objectAtIndex:1] intValue];
        
		Reference *ref = [[seg references] objectAtIndex:index];
        
		self.complementView = nil;
		if(self.complementView ==nil){
			
            
			self.complementView = [[ComplementView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
			UIWindow *tmpWindow = [[UIApplication sharedApplication].windows objectAtIndex:0];
			
			[tmpWindow addSubview:self.complementView];
			//[self.view addSubview:complementView];
			
			self.complementView.content.delegate = self;
            
		}
		
		[self.complementView setTitle:ref.title];
		
		/*
         NSURLRequest *rq = [NSURLRequest requestWithURL:
         [NSURL URLWithString:uriBlocString relativeToURL:[self.content.request URL]]];
         
         */
		
        DLog(@"ref.content : %@",ref.content);
        
		[self.complementView setContentString:ref.content withModelName:nil];
        
		[self.complementView openAction];
        
        return NO;
    }
	
	
	return YES;
}

- (void)viewDidUnload {
	// Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
	// For example: self.myOutlet = nil;
}


 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
 }

- (void)viewDidLayoutSubviews{
    [self updateData];
    [super viewDidLayoutSubviews];
}



#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
	// Relinquish ownership of any cached data, images, etc that aren't in use.
}




@end


