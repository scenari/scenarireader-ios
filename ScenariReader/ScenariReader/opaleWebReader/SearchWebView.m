//
//  SearchWebView.m
//  OpaleReader
//
//  Created by soreha on 04/07/11.
//  
//

#import "SearchWebView.h"


@implementation UIWebView (SearchWebView)

- (NSInteger)highlightAllOccurencesOfString:(NSString*)str{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"SearchWebView" ofType:@"js"];
    if(!path) printf("le fichier n'a pas été trouvé!\n");
    DLog(@"path for js code : \n%@",path);
    
    NSString *jsCode = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        
    [self stringByEvaluatingJavaScriptFromString:jsCode];
    
    NSString *startSearch = [NSString stringWithFormat:@"OpaleReader_HighlightAllOccurencesOfString('%@')",str];
    [self stringByEvaluatingJavaScriptFromString:startSearch];
    
    NSString *result = [self stringByEvaluatingJavaScriptFromString:@"OpaleReader_SearchResultCount"];
    return [result integerValue];
}

- (void)removeAllHighlights{
    [self stringByEvaluatingJavaScriptFromString:@"OpaleReader_RemoveAllHighlights()"];
}

@end
