//
//  MoviePlayerView.m
//  webmedia
//
//  Created by soreha on 25/09/12.
//  
//

#import "MoviePlayerView.h"

@implementation MoviePlayerView
//@synthesize player;
- (id)init{
    self = [super init];
    if (self) {
    }
    return self;
}

+ (Class)layerClass {
    return [AVPlayerLayer class];
}
- (AVPlayer*)getPlayer {
    DLog();
    
    return [(AVPlayerLayer *)[self layer] player];
}
- (void)setPlayer:(AVPlayer *)player {
    DLog();
    
    [(AVPlayerLayer *)[self layer] setPlayer:player];
}
- (CGRect)getVideoContentFrame {
    AVPlayerLayer *avLayer = (AVPlayerLayer *)[self layer];
    // AVPlayerLayerContentLayer
    CALayer *layer = (CALayer *)[[avLayer sublayers] objectAtIndex:0];
    CGRect transformedBounds = CGRectApplyAffineTransform(layer.bounds, CATransform3DGetAffineTransform(layer.sublayerTransform));
    return transformedBounds;
}

@end
