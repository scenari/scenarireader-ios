//
//  AddChannelDelegate.h
//  OptimGenericReader
//
//  Created by soreha on 15/02/11.
//  
//

#import <Foundation/Foundation.h>


@protocol AddChannelViewDelegate <NSObject> 

@required
//Méthodes obligatoires
- (void)addChannelForUrl : (NSString *) channelUrl;
- (void)cancelAddChanel:(UIViewController *)source; 
@end

