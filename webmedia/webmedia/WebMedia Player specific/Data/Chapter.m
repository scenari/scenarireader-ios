//
//  Chapter.m
//  PhoneRadio
//
//  Created by soreha on 27/07/10.
//  
//

#import "Chapter.h"


@implementation Chapter

@synthesize title;
@synthesize segments;
@synthesize root;
@synthesize chapterId;
@synthesize start;
@synthesize end;

- (id) init{
	self = [super init];
	if (self != nil) {
		self.segments= [[NSMutableArray alloc] initWithCapacity:0];
		root = NO;
	}
	return self;
}

-(Segment*) firstSegment{
	if([self.segments count]==0) return nil;
	return [self.segments objectAtIndex:0];
}
-(Segment*) lastSegment{
	return [self.segments lastObject];
}



@end
