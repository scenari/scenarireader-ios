//
//  TimeMarkerView.m
//  PhoneRadio
//
//  Created by soreha on 19/12/10.
//  
//

#import "TimeMarkerView.h"


@implementation TimeMarkerView
@synthesize duration;

- (id)initWithFrame:(CGRect)frame andDuration:(float) timeInSeconds{
    
    self = [super initWithFrame:frame];
	self.duration = timeInSeconds;
    if (self) {
        // Initialization code.
    }
	self.backgroundColor = [UIColor clearColor];
    return self;
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
	
    // Drawing code.
	CGContextRef context = UIGraphicsGetCurrentContext(); 
   	//CGContextClearRect (context, rect); 	
	if(self.duration==0) return;
	
	float secondStep = rect.size.width / self.duration;
	
	CGContextSetLineWidth(context, 1);
	CGContextBeginPath(context);
	CGContextSetRGBStrokeColor(context, 0.0f, 0.0f, 0.0f, 1.0f); // black line
	
	CGContextMoveToPoint(context, 0, rect.size.height-2);
	CGContextAddLineToPoint(context, rect.size.width-1, rect.size.height-2);
	for(int i=0,j=0;i<rect.size.width;i++,j++){
		CGContextMoveToPoint(context, (float)i*secondStep, rect.size.height-2);
		
		CGContextAddLineToPoint(context, (float)i*secondStep, (j%5==0)?rect.size.height-8:rect.size.height-5);
	}
	CGContextStrokePath(context);
}





@end
