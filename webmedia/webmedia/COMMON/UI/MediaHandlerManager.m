//
//  MediaHandlerManager.m
//  webmedia
//
//  Created by soreha on 13/06/12.
//  
//

#import "MediaHandlerManager.h"

@implementation MediaHandlerManager

@synthesize mediaHandlerDelegate;

+ (MediaHandlerManager *) sharedInstance{
    static dispatch_once_t onceToken;
    static MediaHandlerManager * __sharedInstance = nil;
    
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[self alloc] init];
    });
    return __sharedInstance;

}

@end
