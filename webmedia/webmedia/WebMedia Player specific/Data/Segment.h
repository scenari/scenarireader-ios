//
//  Segment.h
//  PhoneRadio
//
//  Created by soreha on 08/07/10.
//  
//

#import <Foundation/Foundation.h>
@class Illustration;

@interface Segment : NSObject 

@property (nonatomic, strong) NSString *segmentId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *short_description;
@property (nonatomic, strong) NSString *long_description;

@property (nonatomic) float start;
@property (nonatomic) float end;
@property (nonatomic) float endUnrounded;


@property (nonatomic, strong) NSMutableArray *illustrations;
@property (nonatomic, strong) NSMutableArray *complements;

@property (nonatomic, strong) NSMutableArray *references;



@property (nonatomic, strong) NSString *mediaUri;
@property (nonatomic, strong) NSString *mediaType;
@property (nonatomic) float clipBegin;
@property (nonatomic) float clipEnd;
@property (nonatomic) float clipEndUnrounded;


@property (nonatomic, strong) id parent;

- (BOOL)hasComplement;
- (BOOL)hasIllustration;
- (BOOL)hasIllustrationImage;

- (BOOL)hasReferences;
- (BOOL)hasVideo;

- (Illustration* )firstIllustration;
- (Illustration *)illustrationAtTime:(float)second;
- (int)illustrationIndexAtTime:(float)second;
- (void)printSegment;

- (Illustration *)nextIllustration:(int)currentIllustrationIndex;
- (Illustration *)previousIllustration:(int)currentIllustrationIndex;
- (Illustration*)lastIllustration;

- (NSArray *)illustrationBeginTimeValues;

@end
