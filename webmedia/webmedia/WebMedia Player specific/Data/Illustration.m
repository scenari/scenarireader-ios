//
//  Illutration.m
//  PhoneRadio
//
//  Created by soreha on 27/07/10.
//  
//

#import "Illustration.h"

NSString * const TYPE_IMAGE		= @"image";
NSString * const TYPE_VIDEO		= @"video";
NSString * const TYPE_ANIMATION = @"animation";
NSString * const TYPE_TEXT		= @"text";

@implementation Illustration
@synthesize illustrationId;
@synthesize resource;
@synthesize text;
@synthesize type;
@synthesize start;
@synthesize end;


- (BOOL)isImage{
    return self.type == TYPE_IMAGE;
}



@end
