//
//  BookmarksList.h
//  OpaleReader
//
//  Created by soreha on 20/06/11.
//  
//

#import <UIKit/UIKit.h>

@class Bookmark;

@protocol BookMarkListManagement <NSObject>

@required
-(void) selectBookMark:(Bookmark *) aBookmark;
-(void) hasRemoveBookmark:(Bookmark *) aBookmark;

@end

@interface BookmarksList : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    
    NSMutableArray *bookmarks;
    
    UITableView *theTableView;

    id<BookMarkListManagement> __weak bookMarkListManager;

}
@property (nonatomic, weak) id<BookMarkListManagement> bookMarkListManager;
@property (nonatomic, strong) IBOutlet UITableView *theTableView;
@property (nonatomic, strong) NSMutableArray *bookmarks;

- (IBAction)close:(id)sender;

@end
