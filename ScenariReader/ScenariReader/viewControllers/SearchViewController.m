//
//  SearchViewController.m
//  OptimGenericReader
//
//  Created by soreha on 26/02/11.
//  
//

#import "SearchViewController.h"
#import "CoreDataManager.h"
#import "RequestManager.h"

#import "Document.h"
#import "Thumbnail.h"
#import "DocumentDetailViewController.h"
#import "ConfigurationManager.h"
#import "MediaHandlerManager.h"
#import "CoreDataManager.h"
#import "SearchViewControllerCell.h"
#import "RequestManager.h"
#import "ASINetworkQueue.h"

int sliderPreviousValue;

@implementation SearchViewController

@synthesize aChannel;
@synthesize searchView;
@synthesize defaultFetchedResultsController;
@synthesize textFilteredFetchedResultsController;
@synthesize distanceFilteredFetchedResultsController;
@synthesize searchModeInfo;
@synthesize distanceLabel;
@synthesize distanceSlider;
@synthesize searchModeSegment;
@synthesize locationManager;
@synthesize searchIsActive;
@synthesize myCell;
@synthesize searchDisplayController;

-(void) releaseOutlets{
	searchDisplayController = nil;
	//aTableView = nil;
	searchModeInfo = nil;
	distanceLabel = nil;
	distanceSlider = nil;
	searchModeSegment = nil;
}

- (void)dealloc{
    [self releaseOutlets];
    self.defaultFetchedResultsController.delegate = nil;
    self.defaultFetchedResultsController = nil;
    self.textFilteredFetchedResultsController.delegate = nil;
    self.textFilteredFetchedResultsController = nil;
    self.distanceFilteredFetchedResultsController.delegate = nil;
    self.distanceFilteredFetchedResultsController = nil;
}



// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	self.title = @"Recherche";
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationItem.leftBarButtonItem.tintColor = [UIColor blueColor];
        //self.searchDisplayController.displaysSearchBarInNavigationBar = YES;
    }else{
        [self navigationController].navigationBar.tintColor = self.searchDisplayController.searchBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];   ;
    }
    
    /*
    if(![[ConfigurationManager sharedInstance] useUserLocation]){
        //hide options search view
        CGRect placeToFill=self.searchView.bounds;
        [searchView removeFromSuperview];
        self.searchBar.frame = CGRectMake(0, 0, self.searchBar.bounds.size.width, self.searchBar.bounds.size.height);
       // [self.searchBar setNeedsLayout];
         self.aTableView.frame = CGRectMake(self.aTableView.frame.origin.x, self.searchBar.frame.size.height, self.aTableView.frame.size.width, self.aTableView.frame.size.height+placeToFill.size.height);
    }
    else {
       [[self locationManager] startUpdatingLocation];
    }
     */
    [self updateSearchMode];
    
    self.aTableView.backgroundColor = [UIColor whiteColor];
    self.searchDisplayController.searchResultsTableView.backgroundColor = [UIColor whiteColor];
  }



- (void)closeView {
    
	//[[self locationManager] setDelegate:nil];
    //self.locationManager = nil;
	[self.navigationController dismissModalViewControllerAnimated:YES];
}


 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
 }



/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

-(void) initDataManager : (id) dManager {
    
    
    searchIsActive = NO;
    [self updateDistances];
	[self initDefaultFetchedResultController];
}

#pragma mark -
#pragma mark fetchedResultsController management

- (NSFetchedResultsController *) getCurrentFetchedResultController {
    
    NSFetchedResultsController * fetchedResultsController;
    if (searchModeSegment.selectedSegmentIndex==1) {
        fetchedResultsController = self.distanceFilteredFetchedResultsController;
    } else {
        if (self.searchIsActive) {
            fetchedResultsController = textFilteredFetchedResultsController;
        } else {
            fetchedResultsController = defaultFetchedResultsController;
        }
    }
    return fetchedResultsController;
}

-(void) initDefaultFetchedResultController {
    self.defaultFetchedResultsController.delegate = nil;
	self.defaultFetchedResultsController = nil;
	self.defaultFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllDocuments]
																		managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"channel.title" 
																				   cacheName:nil];
	self.defaultFetchedResultsController.delegate = self;
	
    NSError *error = nil;
	if (![self.defaultFetchedResultsController performFetch:&error]){
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
}

-(void) initDistanceFetchedResultController {
	NSNumber * d ;
	
	if (((int)self.distanceSlider.value) == 1){
		d = [[NSNumber alloc] initWithInt : 500];
	}else if (((int)self.distanceSlider.value) == 2){
		d = [[NSNumber alloc] initWithInt : 1000];
	}else if (((int)self.distanceSlider.value) == 3){
		d =  [[NSNumber alloc] initWithInt : 5000];
	}else if (((int)self.distanceSlider.value) == 4){
		d = [[NSNumber alloc] initWithInt : 10000];
	}else if (((int)self.distanceSlider.value) == 5){
		d = [[NSNumber alloc] initWithInt : 50000];
	}else if (((int)self.distanceSlider.value) == 6){
		d = [[NSNumber alloc] initWithInt : 50000000];
	}else{
       d = [[NSNumber alloc] initWithInt : 100]; 
    }
	
	
	DLog(@"distance min : %f m", [d floatValue]);
	self.distanceFilteredFetchedResultsController.delegate = nil;
	self.distanceFilteredFetchedResultsController = nil;
    
	self.distanceFilteredFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllDocumentsWithDistanceLessThan:d] 
																		managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:nil 
																				   cacheName:nil];
	
    self.distanceFilteredFetchedResultsController.delegate = self;
	
	
	NSError *error = nil;
	if (![self.distanceFilteredFetchedResultsController performFetch:&error]){
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    
	DLog(@"Nb doc filtrés : %d\n",[[self.distanceFilteredFetchedResultsController fetchedObjects] count]);
}

- (IBAction) updateSearchMode{
	distanceLabel.hidden = distanceSlider.hidden = (searchModeSegment.selectedSegmentIndex==0);
	searchModeInfo.hidden = /*searchBar.hidden =*/ (searchModeSegment.selectedSegmentIndex==1);
	
	if(searchModeSegment.selectedSegmentIndex==1){
		// If it's not possible to get a location, then return.
		//CLLocation *location = [self.locationManager location];
		[self initDistanceFetchedResultController];
	} else {
       // [self.view bringSubviewToFront:searchBar];
        [self initDefaultFetchedResultController];
    }
    [self.aTableView reloadData];
}

- (IBAction) sliderValueChanged:(id)sender{
	
	int step = (int)[distanceSlider value];
	
	if(sliderPreviousValue == step) return;
	
	sliderPreviousValue = step;
	
    if(!self.distanceValues){
        self.distanceValues = [[NSArray alloc] initWithObjects:@"Distance inférieure à 100 m", @"Distance inférieure à 500 m", @"Distance inférieure à 1 km", @"Distance inférieure à 5 km", @"Distance inférieure à 10 km", @"Distance inférieure à 50 km", @"Distance illimitée", nil];
    }
    
	distanceLabel.text = [self.distanceValues objectAtIndex:step];
    distanceLabel.hidden=NO;
	
	[self initDistanceFetchedResultController];
    
	[self.aTableView reloadData];
}


#pragma mark -
#pragma mark Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (searchModeSegment.selectedSegmentIndex==0) {
        NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
        DLog(@"Nb Section : %d ", [fetchedResultsController.sections count]);
        return [fetchedResultsController.sections count];
    } else {
        return 1;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
    
    if (searchModeSegment.selectedSegmentIndex==0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else {
        return [[fetchedResultsController fetchedObjects] count];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    //if (searchModeSegment.selectedSegmentIndex==0) {
        //NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
        id <NSFetchedResultsSectionInfo> sectionInfo = [[[self getCurrentFetchedResultController] sections] objectAtIndex:section];
        
        NSString *unnamedLabel = [[ConfigurationManager sharedInstance] getNoNamedCategoryHeaderLabel];
        
        return [[[sectionInfo name] lowercaseString] isEqualToString:@"aucune"]?(unnamedLabel?unnamedLabel:@"Aucune"):[sectionInfo name];
        
   // } else {
   //     return @"Documents géo-localisés";
   // }
}
//Custom title view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	
	
	// create the parent view that will hold header Label
	UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 24)];
	customView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	customView.backgroundColor = [[ConfigurationManager sharedInstance] tableViewSectionBackgroundColor];
	
	// create the button object
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
	headerLabel.textColor = [[ConfigurationManager sharedInstance] tableViewSectionForegroundColor];
	headerLabel.highlightedTextColor = [UIColor whiteColor];
	headerLabel.font = [UIFont boldSystemFontOfSize:14];
	headerLabel.frame = CGRectMake(10.0, 0.0, 310.0, 24);
	headerLabel.adjustsFontSizeToFitWidth = YES;
	headerLabel.minimumFontSize = 12.0;
	
	headerLabel.text = [self tableView:tableView titleForHeaderInSection:section];
	
	[customView addSubview:headerLabel];
	
	return customView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return 24.0;
}




// Customize the appearance of table view cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
	static NSString *MyIdentifier = @"MySearchViewCell";

    UITableViewCell *cell = (UITableViewCell *)[self.aTableView dequeueReusableCellWithIdentifier:MyIdentifier];

    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        DLog(@"cell was nil");
    }
    DLog(@"cell : %@",[cell description]);
	// Configure the cell...
	[self configureCell:cell atIndexPath:indexPath];
    
    return cell;

}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    DLog();
    
    //NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	Document  *document = [[self getCurrentFetchedResultController] objectAtIndexPath:indexPath];
   	
    DLog(@"document : %@",[document.title description]);
	UILabel *titleLabel = (UILabel *)[cell viewWithTag:1];
    UILabel *subTitleLabel = (UILabel *)[cell viewWithTag:2];
    UILabel *categoryLabel = (UILabel *)[cell viewWithTag:5];

    UIImageView *imageView = (UIImageView *)[cell viewWithTag:4];

    NSMutableString *accessibilityText = [NSMutableString stringWithString:@""];
    
    UILabel *type = (UILabel *)[cell viewWithTag:10];
    if([[document.pubType lowercaseString] isEqualToString:@"opale"]){
        type.text = @"opale";
#pragma ACCESSIBILITY
        [accessibilityText appendString:@"Document Opale : "];
    }else if([[document.pubType lowercaseString] isEqualToString:@"topaze"]){
        type.text = @"topaze";
#pragma ACCESSIBILITY
        [accessibilityText appendString:@"Document Topaze : "];
    }else if([[document.pubType lowercaseString] isEqualToString:@"optim"]){
        type.text = @"optim";
#pragma ACCESSIBILITY
        [accessibilityText appendString:@"Document Optim : "];
    }else{
        type.text = @"webmedia";
#pragma ACCESSIBILITY
        [accessibilityText appendString:@"Document WebMedia : "];
    }
    
	titleLabel.text = [NSString stringWithFormat:@"%@", document.title];
    
#pragma ACCESSIBILITY
	[accessibilityText appendFormat:@"%@.",titleLabel.text];
    
	int duration = [document.duration intValue];
    if(duration==0) titleLabel.numberOfLines = 2;
    else titleLabel.numberOfLines = 1;
    
	
	if (document.thumbnail_small == nil) {
		imageView.image = [UIImage imageNamed:@"blank"];
        if (document.thumbnail_small_URI != nil || [document.thumbnail_small_URI compare:@""] != NSOrderedSame) {
            [[CoreDataManager sharedInstance] downloadThumbnailForDocument:document];
        }
	} else {
		imageView.image = document.thumbnail_small.image;
	}
	
    
    NSString *cat = [[ConfigurationManager sharedInstance] getNoNamedCategorySearchCellLabel];
    

	categoryLabel.text = [NSString stringWithFormat:@"Catégorie : %@",[[document.category lowercaseString] isEqualToString:@"aucune"]?cat:document.category];

#pragma ACCESSIBILITY
	[accessibilityText appendFormat:@"%@.",categoryLabel.text];
    
    if(duration==0){
        subTitleLabel.text = document.author?:@"";
        
    }else{
        
        subTitleLabel.text = [NSString stringWithFormat:@"(%@:%@) %@",
				  [self number2digit:(int)(duration/60.0)], 
				  [self number2digit:((int)(duration))%60],
                              document.author?:@""];
#pragma ACCESSIBILITY
        [accessibilityText appendFormat:@"Durée : %@ minutes et %@ secondes.",[self number2digit:(int)(duration/60.0)],[self number2digit:((int)(duration))%60] ];
	}
#pragma ACCESSIBILITY
    if(document.author)
        [accessibilityText appendFormat:@"L'auteur du document est : %@.",document.author];
    
    
    
    UILabel *label = (UILabel *)[cell viewWithTag:3];

    
    if ([document.downloading intValue] == 1) {
		if ([document.downloadProgress floatValue] > 0 ) {
			label.text = [NSString stringWithFormat:@"Téléchargement : %d %%",(int)([document.downloadProgress floatValue]*100)];
            label.textColor = [[ConfigurationManager sharedInstance] downloadProgressInfoColor];
		} else if ([document.downloadSuspended intValue]== 0){
			label.text = @"Téléchargement en attente";
            label.textColor = [[ConfigurationManager sharedInstance] downloadProgressInfoColor];
		} else {
            label.text = @"Téléchargement suspendu";
            label.textColor = [[ConfigurationManager sharedInstance] downloadErrorInfoColor];
        }
    } else if ([document.updateAvailable intValue] == 1) {
		label.text = @"Mise à jour disponible";
		label.textColor = [[ConfigurationManager sharedInstance] downloadErrorInfoColor];
	}  else {
        if([document.downloaded boolValue]){
            label.text = @"Téléchargé";
            label.textColor = [UIColor colorWithRed:0.000 green:0.503 blue:0.000 alpha:1.000];
        }else if([document.link hasSuffix:@".html"]){
            DLog(@"document.link:[%@]",document.link);
            label.text = @"Version en ligne ou à télécharger";
            label.textColor = [UIColor colorWithRed:0.000 green:0.000 blue:0.503 alpha:1.000];
        }else{
            label.text = @"À télécharger";
            label.textColor = [UIColor colorWithRed:0.503 green:0.000 blue:0.000 alpha:1.000];
        }
    }
#pragma ACCESSIBILITY
	[accessibilityText appendFormat:@"%@.",label.text];
	
    UIImageView *imFav = (UIImageView *)[cell viewWithTag:20];
	[imFav setHidden:![document.isFavorite boolValue]];
    
    if([document.isFavorite boolValue]){
#pragma ACCESSIBILITY
        [accessibilityText appendString:@"Ce document fait partie des documents favoris."];
    }else{
#pragma ACCESSIBILITY
        [accessibilityText appendString:@"Ce document ne fait pas partie des documents favoris."];
    }
    
    cell.accessibilityLabel = [NSString stringWithString:accessibilityText];
    
    
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        [cell setAccessoryView:[self makeDetailDisclosureButtonWithFrame:CGRectMake(0, 0, 30, 30) andImage:[[ConfigurationManager sharedInstance] getAccessoryImage]]];
    }
   
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	//NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	//Document  *document = [[self getCurrentFetchedResultController] objectAtIndexPath:indexPath];
	
	//if (([document.downloaded intValue] == 1 || [document.downloading intValue] == 0) && [document.updateAvailable intValue] == 0) {
    //    return 66.0;
	//} else {
		return 86.0;
	//}
}


- (NSString*) number2digit:(int) number{
	if (number<10) {
		return [NSString stringWithFormat:@"0%d",number];
	}
	return [NSString stringWithFormat:@"%d",number];
}

- (UIButton *) makeDetailDisclosureButtonWithFrame:(CGRect)rect andImage:(UIImage *)img{
	UIButton *button = [[UIButton alloc] initWithFrame:rect];
	[button setImage:img forState:UIControlStateNormal];
	
	
	[button addTarget: self
			   action: @selector(accessoryButtonTapped:withEvent:)
	 forControlEvents: UIControlEventTouchUpInside];
	
	return ( button );
}

- (void) accessoryButtonTapped: (UIControl *) button withEvent: (UIEvent *) event	{
    
    UITableView * tView;
    
    if (!self.searchIsActive ){//|| searchModeSegment.selectedSegmentIndex == 1 ) {
        tView = self.aTableView;
    } else {
        tView = self.searchDisplayController.searchResultsTableView;
    }
        
    
    NSIndexPath * indexPath = [tView indexPathForRowAtPoint: [[[event touchesForView: button] anyObject] locationInView: tView]];
    
    if ( indexPath == nil )
		return;
		
	if([tView.delegate respondsToSelector:@selector(tableView:accessoryButtonTappedForRowWithIndexPath:)])
		[tView.delegate tableView:tView accessoryButtonTappedForRowWithIndexPath: indexPath];
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    DLog();
    
	// Navigation logic may go here. Create and push another view controller.
	
	DocumentDetailViewController *detailViewController = [[DocumentDetailViewController alloc] initWithNibName:@"DefaultDetailViewController" bundle:nil];
	
    NSFetchedResultsController * fResultsController = [self getCurrentFetchedResultController];
		
	Document * document = [fResultsController objectAtIndexPath:indexPath];
    
    
    //DLog(@"fResultsController=%@",[fResultsController fetchedObjects]);
	
    
    
    
    // Pass the selected object to the new view controller.
    
	[detailViewController setItems:[fResultsController fetchedObjects] withCurrent:[[fResultsController fetchedObjects] indexOfObject:document]];
	[self.navigationController pushViewController:detailViewController animated:YES];
    
    
    
}

#pragma mark -
#pragma mark Table view delegate

-(void) downloadDocument:(Document *)document{
    NSString *tmpDirectory = [[[CoreDataManager sharedInstance] requestManager] getTmpDownloadFolder];
    
    if(document.tmpDownloadFilePath && ![document.tmpDownloadFilePath hasPrefix:tmpDirectory]){
        //Cancel previous tmp download file
        DLog(@"New app version number -> previous tmp file canceled document:%@",document.title);
        document.tmpDownloadFilePath = nil;
    }
    if ([CoreDataManager sharedInstance].requestManager.documentDownloadQueue == nil) {
        [[CoreDataManager sharedInstance].requestManager initAsiQueue];
    }
    [[CoreDataManager sharedInstance] downloadDocument:document];
}


-(void)suspendDocumentDownload:(Document*)document{
    [document.request clearDelegatesAndCancel];
    document.downloadProgress = 0;
    document.downloadSuspended = [NSNumber numberWithBool:YES];
    document.isSelected = [NSNumber numberWithBool:NO];
    document.request = nil;
    [[CoreDataManager sharedInstance] saveData];
}

- (void)resumeDocumentDownload:(Document *)document{
    if ([CoreDataManager sharedInstance].requestManager.documentDownloadQueue == nil) {
        [[CoreDataManager sharedInstance].requestManager initAsiQueue];
    }
    
    
    NSString *tmpDirectory = [[[CoreDataManager sharedInstance] requestManager] getTmpDownloadFolder];
    if(document.tmpDownloadFilePath && ![document.tmpDownloadFilePath hasPrefix:tmpDirectory]){
        //Cancel previous version app tmp download file (another folder)
        ALog(@"new app version number -> previous tmp file canceled document:%@",document.title);
        document.tmpDownloadFilePath = nil;
    }
    document.isSelected = [NSNumber numberWithBool:NO];
    if (document.request == nil) {
        [[CoreDataManager sharedInstance].requestManager addDocumentRequest:document];
    }
    
    [[CoreDataManager sharedInstance].requestManager.documentDownloadQueue go];
    [[CoreDataManager sharedInstance] saveData];
}

- (BOOL)isTopazeDocument:(Document *)doc{
    return [[doc.pubType lowercaseString] isEqualToString:@"topaze"];
}
- (BOOL)isWebMediaDocument:(Document *)doc{
    return !doc.pubType || [[doc.pubType lowercaseString] isEqualToString:@"webmedia"];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    self.lastSelectedPath = indexPath;
    NSFetchedResultsController * fResultsController = [self getCurrentFetchedResultController];
    
    Document  *document = [fResultsController objectAtIndexPath:indexPath];
	/*
     if ([document.updateAvailable intValue] == 1 && [document.downloading intValue] == 0){
     UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Mise à jour" message:@"Une mise à jour est disponible, souhaitez-vous la télécharger maintenant ?" delegate:self cancelButtonTitle:@"Oui" otherButtonTitles:@"Non", nil];
     [alert show];
     } else if ([document.downloading intValue] == 1){
     UIAlertView *alert;
     if ([document.downloadSuspended intValue] == 0) {
     alert = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Document en cours de téléchargement, merci de patienter" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
     }
     else {
     alert = [[UIAlertView alloc] initWithTitle:@"Téléchargement" message:@"Téléchargement suspendu, souhaitez-vous le reprendre ?" delegate:self cancelButtonTitle:@"Oui" otherButtonTitles:@"Non", nil];
     }
     
     [alert show];
     } else if ([document.downloaded intValue] == 0 ){
     UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Téléchargement" message:@"Le document n'a pas été téléchargé, souhaitez-vous le télécharger maintenant ?" delegate:self cancelButtonTitle:@"Oui" otherButtonTitles:@"Non", nil];
     [alert show];
     } else {
     [self openMedia:document];
     }
     */
    
    
    if ([document.downloading intValue] == 1) {
		if ([document.downloadProgress floatValue] > 0 ) {
			//Téléchargement en cours->suspendre
            [self suspendDocumentDownload:document];
            return;
        } else if ([document.downloadSuspended intValue]== 0){
			//Téléchargement en attente->suspendre
            [self suspendDocumentDownload:document];
            
		} else {
            //Téléchargement suspendu"->reprendre
            [self resumeDocumentDownload:document];
        }
    } else if ([document.updateAvailable intValue] == 1) {
		//Mise à jour disponible
        UIActionSheet *actionSheet ;
        if(document.lastOpenedPage && ![self isTopazeDocument:document]){
            actionSheet = [[UIActionSheet alloc] initWithTitle:@"Mise à jour disponible" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Télécharger", @"Ignorer, ouvrir au début",@"Ignorer, reprendre la lecture", nil];
        }else{
            actionSheet = [[UIActionSheet alloc] initWithTitle:@"Mise à jour disponible" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Télécharger", @"Ignorer et ouvrir", nil];
        }
        actionSheet.actionSheetStyle = UIBarStyleBlackOpaque;
        if (IS_IPAD()) {
            DLog(@"iPad actionshetFrom cellrect");
            CGRect rectInTableView = [tableView rectForRowAtIndexPath:indexPath];
            CGRect rectInSuperview = [tableView convertRect:rectInTableView toView:nil];
            [actionSheet showFromRect:rectInSuperview inView:[[[UIApplication sharedApplication] delegate] window]  animated:YES];
        }else{
            [actionSheet showInView:[[[UIApplication sharedApplication] delegate] window]];
        }
        
	} else {
        if([document.downloaded boolValue]){
            //Téléchargé->ouverture en local
            UIActionSheet *actionSheet ;
            if(document.lastOpenedPage && ![self isTopazeDocument:document] && ![self isWebMediaDocument:document] ){
                actionSheet = [[UIActionSheet alloc] initWithTitle:@"Ouvrir" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Ouvrir au début",@"Reprendre la lecture", nil];
                actionSheet.actionSheetStyle = UIBarStyleBlackOpaque;
                if (IS_IPAD()) {
                    DLog(@"iPad actionshetFrom cellrect");
                    CGRect rectInTableView = [tableView rectForRowAtIndexPath:indexPath];
                    CGRect rectInSuperview = [tableView convertRect:rectInTableView toView:nil];
                    [actionSheet showFromRect:rectInSuperview inView:[[[UIApplication sharedApplication] delegate] window]  animated:YES];
                }else{
                    [actionSheet showInView:[[[UIApplication sharedApplication] delegate] window]];
                }
            }
            else{
                
                if(IS_IPAD()){
                    DLog();
                    
                    [self.popover dismissPopoverAnimated:YES];
                }
                [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document];
                
            }
            
            DLog(@"document : %@",document);
            
        }else if([document.link hasSuffix:@".html"]){
            DLog(@"document.link=%@",document.link);
            DLog(@"document.lastOpenedPage=%@",document.lastOpenedPage);
            
            
            //Version en ligne ou à télécharger
            UIActionSheet *actionSheet ;
            if(document.lastOpenedPage && ![document.lastOpenedPage isEqualToString:document.link] && ![self isTopazeDocument:document]){
                actionSheet = [[UIActionSheet alloc] initWithTitle:@"Options" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Ouvrir en ligne",@"Reprendre en ligne",@"Télécharger", nil];
            }
            else{
                actionSheet = [[UIActionSheet alloc] initWithTitle:@"Options" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Ouvrir en ligne",@"Télécharger", nil];
            }
            actionSheet.actionSheetStyle = UIBarStyleBlackOpaque;
            if (IS_IPAD()) {
                DLog(@"iPad actionshetFrom cellrect");
                CGRect rectInTableView = [tableView rectForRowAtIndexPath:indexPath];
                CGRect rectInSuperview = [tableView convertRect:rectInTableView toView:nil];
                [actionSheet showFromRect:rectInSuperview inView:[[[UIApplication sharedApplication] delegate] window]  animated:YES];
            }else{
                [actionSheet showInView:[[[UIApplication sharedApplication] delegate] window]];
            }
            
        }else{
            [self downloadDocument:document];
        }
	}
    
}
- (void)actionSheetCancel:(UIActionSheet *)actionSheet {
    //if([actionSheet.title isEqualToString:@"Méthode de tri"]){
    [actionSheet dismissWithClickedButtonIndex:(actionSheet.numberOfButtons-1) animated:YES];
    //}
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(IS_IPAD()){
        DLog();
        
        [self.popover dismissPopoverAnimated:YES];
    }
    
    if(actionSheet.cancelButtonIndex == buttonIndex) return;
    
    DLog(@"Button index : %d",buttonIndex);
    NSFetchedResultsController * fResultsController = [self getCurrentFetchedResultController];
    
    if([actionSheet.title isEqualToString:@"Options"]){
        Document  *document = [fResultsController objectAtIndexPath:self.lastSelectedPath];
        if(buttonIndex==0){//Ouvrir en ligne
            [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openOnlineDocument:document onLastOpenedPage:NO];
        }else if((document.lastOpenedPage) && buttonIndex==1 && ![self isTopazeDocument:document]){
            [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openOnlineDocument:document  onLastOpenedPage:YES];
        }else {//if(buttonIndex==actionSheet.numberOfButtons-2){//Télécharger
            [self downloadDocument:document];
        }
    }else if([actionSheet.title isEqualToString:@"Ouvrir"]){
        
        Document  *document = [fResultsController objectAtIndexPath:self.lastSelectedPath];
        if(buttonIndex==0){//Ouvrir au début
            [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document onLastOpenedPage:NO];
        }else if(buttonIndex==1){//reprendre
            [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document  onLastOpenedPage:YES];
        }
    }else if([actionSheet.title isEqualToString:@"Mise à jour disponible"]){
        Document  *document = [fResultsController objectAtIndexPath:self.lastSelectedPath];
        if(buttonIndex==0){//Télécharger
            [self downloadDocument:document];
        }else if(buttonIndex==1){//ouvrir au début
            [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document  onLastOpenedPage:NO];
        }else if(buttonIndex==2 && actionSheet.numberOfButtons==4){//reprendre
            [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document  onLastOpenedPage:YES];
        }
        
    }
}



-(void) openMedia:(Document *) document {
    
    [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document];
    if(IS_IPAD()){
        DLog();
        
        [self.popover dismissPopoverAnimated:YES];
    }
}

#pragma mark -
#pragma mark ToolBar buttons actions

-(IBAction) downloadAll {
	//[self.dataManager prepareDownloadWithView:self];
	[[CoreDataManager sharedInstance] downloadAllDocumentsForChannel:self.aChannel withCategory:nil];
}

-(IBAction) downloadSelected {
	//[self.dataManager prepareDownloadWithView:self];
	if (![[CoreDataManager sharedInstance] downloadSelectedDocumentsForChannel:self.aChannel withCategory:nil]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Vous n'avez sélectionné aucun élément à télécharger" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
		[alert show];
	}
}




#pragma mark -
#pragma mark Location manager

/**
 Return a location manager -- create one if necessary.
 */

- (CLLocationManager *)locationManager {
/*    DLog();

    if (locationManager != nil) {
		return locationManager;
	}
	
	locationManager = [[CLLocationManager alloc] init];
	[locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
	[locationManager setDelegate:self];
	
	return locationManager;
 */
    return nil;
}


/**
 Conditionally enable the Add button:
 If the location manager is generating updates, then enable the button;
 If the location manager is failing, then disable the button.
 */
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
	DLog(@"didUpdateToLocation\n");
	[self updateDistances];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
	DLog(@"Update location didFailWithError\n");
}


- (void) updateDistances {
	
	/*
	CLLocation *location = [self.locationManager location];
	
	if (location) {
        DLog();
        	
		NSFetchedResultsController * allDocumentsFetchedResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllDocuments] 
																						  managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:nil 
																									 cacheName:nil];
	
		NSError *error = nil;
		if (![allDocumentsFetchedResultController performFetch:&error]){
			ALog(@"Unresolved error %@, %@", error, [error userInfo]);
			//abort();
		}
	
		for (Document * document in [allDocumentsFetchedResultController fetchedObjects]) {
			NSNumber *d;
			if (document.location != nil &&  [document.location compare:@""] != NSOrderedSame) {
				
				NSArray * locationComponent = [document.location componentsSeparatedByString:@","]; 
				
				DLog(@"docLocation lat : %f long : %f",[[locationComponent objectAtIndex:0] floatValue],[[locationComponent objectAtIndex:1] floatValue]);
			
				CLLocation *docLoc = [[CLLocation alloc] initWithLatitude:[[locationComponent objectAtIndex:0] floatValue] longitude: [[locationComponent objectAtIndex:1] floatValue]];
			
				CLLocationDistance dist =  [location distanceFromLocation:docLoc];//meters
						
				d = [[NSNumber alloc] initWithDouble:dist];
				document.distanceFromLocation = d;
			
			}
			else {
				d = [[NSNumber alloc] initWithInt:-1];
				document.distanceFromLocation = d;
			}
		}
		
	}
     */
}


#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    self.textFilteredFetchedResultsController.delegate = nil;
	self.textFilteredFetchedResultsController = nil;
    self.textFilteredFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllDocumentsForKeyword:searchText]
																		managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"channel.title" 
																				   cacheName:nil];
	

    self.textFilteredFetchedResultsController.delegate = self;
	
    NSError *error = nil;
	if (![self.textFilteredFetchedResultsController performFetch:&error]){
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    searchIsActive = YES;
}



#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    DLog();

    [self filterContentForSearchText:searchString scope:
    [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:
	  [self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
	
    return YES;
}


- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    DLog();

    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
	 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
	
    return YES;
}

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller {
    DLog();

	[self.searchModeSegment setEnabled:NO];
   
    //self.searchView.hidden = YES;
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller{
    self.searchView.hidden = NO;
    searchIsActive = NO;
    [self.searchModeSegment setEnabled:YES];

}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller {
    DLog();
    
	[self.aTableView reloadData];
}


#pragma mark -
#pragma mark - Fetched results controller delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    //NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	if (controller == [self getCurrentFetchedResultController] && !searchIsActive) {
        DLog();
        
		[self.aTableView beginUpdates];
	} 
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    
	//NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	UITableView *tView;
	if (controller == [self getCurrentFetchedResultController] && !searchIsActive) {
		tView = self.aTableView;
        DLog();
        
		switch(type)
		{
			case NSFetchedResultsChangeInsert:
				[tView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
				break;
            
			case NSFetchedResultsChangeDelete:
				[tView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
				break;
		}
	}
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    
	//NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	UITableView *tView;
	if (controller == [self getCurrentFetchedResultController] && !searchIsActive) {
		tView = self.aTableView;
		switch(type)
		{
            
			case NSFetchedResultsChangeInsert:
				[tView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
				break;
				
			case NSFetchedResultsChangeDelete:
				[tView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
				break;
            
			case NSFetchedResultsChangeUpdate:
				[self configureCell:(SearchViewControllerCell *)[tView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
				break;
            
			case NSFetchedResultsChangeMove:
				[tView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
				[tView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
				break;
		}
	}
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    //NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	UITableView *tView;
	if (controller == [self getCurrentFetchedResultController] && !searchIsActive) {
		tView = self.aTableView;
        DLog();
        
		[tView endUpdates];
	} else if (searchIsActive) {
		[self.searchDisplayController.searchResultsTableView reloadData];
        //[self.aTableView reloadData];
	}
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    searchView = nil;
    [self setSearchView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end
