//
//  DocumentDetailViewController.h
//  PhoneRadio
//
//  Created by soreha on 22/07/10.
//  
//

#import <UIKit/UIKit.h>
#import "DefaultDetailViewController.h"

@class Document;


@protocol FavoriteListenerDelegate <NSObject>

- (void)favoriteAttributeForDocumentHasChanged:(Document *)document;

@end

@interface DocumentDetailViewController : DefaultDetailViewController {
    Document *currentDocument;
}
@property (nonatomic, strong)  id<FavoriteListenerDelegate> favChangeDelegate;
@property (nonatomic, strong)  Document *currentDocument;


@end
