//
//  RequestManagerCoreDataDelegate.h
//  OptimGenericReader
//
//  Created by soreha on 16/02/11.
//  
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "Document.h"
#import "Channel.h"

@protocol RequestManagerCoreDataDelegate <NSObject>

@required
//Méthodes obligatoires
-(void) rssRequestDone :(ASIHTTPRequest *)request;
-(void) rssRequestError :(ASIHTTPRequest *)request;
-(void) rssCreationRequestError :(ASIHTTPRequest *)request;
-(void) document:(Document *)document didLoadMedia:(NSString *)path;
-(void) document:(Document *)document didLoadThumbnail:(UIImage *)image isSmall : (BOOL) isSmall;
-(void) channel:(Channel *)channel didLoadThumbnail:(UIImage *)image;
-(void) saveData;
@end
