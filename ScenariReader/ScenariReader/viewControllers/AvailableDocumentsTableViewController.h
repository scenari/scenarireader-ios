//
//  AvailableEmissionsTableViewController.h
//  OptimGenericReader
//
//  Created by soreha on 16/02/11.
//  
//

#import <UIKit/UIKit.h>
#import "DefaultDocumentsTableViewController.h"

@class CoreDataManager, Channel;

@interface AvailableDocumentsTableViewController : DefaultDocumentsTableViewController 

@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonDownloadAll;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonDownloadSelected;

@property (strong,nonatomic) NSIndexPath *lastSelectedPath;

-(void) initWithChannel :(Channel *) ch;
-(void) reloadFetchedResultControllerSortingBy : (int) criteria;


@end
