//
//  DefaultEmissionsTableViewController.h
//  OptimGenericReader
//
//  Created by Mory Doukouré on 17/02/11.
//  Copyright 2011 Soreha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaHandlerDelegate.h"


@protocol ContentDelegate <NSObject>

- (void)updateSegmentTitleNumbers:(id)sender;

@end


@class DocumentsForChannelViewController, Channel, Document;

@interface DefaultDocumentsTableViewController : UITableViewController <NSFetchedResultsControllerDelegate> {
	IBOutlet DocumentsForChannelViewController	*documentsForChannelViewController;
	UINavigationController * _navigationController;
	NSFetchedResultsController *fetchedResultsController;
	UIImage * defaultImage;
	IBOutlet UITableViewCell * myCell;
	BOOL isDownloadedDocumentsView;
    Channel *channel;
}

//properties
@property(nonatomic, strong) NSIndexPath *lastSelectedPath;

@property(nonatomic, strong) IBOutlet DocumentsForChannelViewController	*documentsForChannelViewController;

@property (nonatomic, strong) Channel *channel;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) UIImage * defaultImage;
@property (nonatomic, strong) UITableViewCell * myCell;
@property (nonatomic, strong) UINavigationController * _navigationController;
@property (nonatomic) BOOL isDownloadedDocumentsView;


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void) openMedia:(Document *) document;
- (NSString*) number2digit:(int) number;
- (UIButton *) makeDetailDisclosureButtonWithFrame:(CGRect)rect andImage:(UIImage *)img;

@property (nonatomic,strong) id<ContentDelegate> contentDelegate;

- (void)contentUpdated;

@end