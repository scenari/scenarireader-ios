//
//  DownlodedEmissionTableViewController.h
//  OptimGenericReader
//
//  Created by Mory Doukouré on 16/02/11.
//  Copyright 2011 Soreha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefaultDocumentsTableViewController.h"

@class Channel, Document;

@interface DownlodedDocumentsTableViewController : DefaultDocumentsTableViewController {

}

//properties
-(void) initWithChannel :(Channel *) ch; 
-(void) reloadFetchedResultControllerSortingBy : (int) criteria;

@property (nonatomic,strong) id contentDelegate;


@end
