//
//  ComplementView.m
//  Patrimoine
//
//  Created by soreha on 29/04/10.
//  
//

#import <QuartzCore/QuartzCore.h>

#import "ComplementView.h"
#import "ConfigurationManager.h"

@interface ComplementView (PrivateMethods)
- (void) closeAction;
@end

@implementation ComplementView

@synthesize content;

CGPoint from;
CGPoint screenCenter;
CGPoint toPoint;
CGRect toFrame;
CGRect screenRect;
UIView *aView;
UILabel *titleLabel;
UIView *backView;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
	
	self.contentMode=UIViewContentModeRedraw;
	
	backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 
														 MAX(frame.size.width, frame.size.height), MAX(frame.size.width, frame.size.height))];
	backView.backgroundColor = [UIColor blackColor];
		
	backView.alpha = 0.75;
	
	screenCenter = CGPointMake(frame.origin.x + frame.size.width/2, frame.origin.y + frame.size.height/2);
	screenRect = frame;
	
    float minW = MIN(frame.size.width - (IS_IPAD()?400:40), frame.size.height - (IS_IPAD()?400:200));
    
	toFrame = CGRectMake(frame.origin.x/*+5*/, frame.origin.y+10/*+80*/, minW, minW);
	
	aView = [[UIView alloc] initWithFrame:toFrame];
	
	
	aView.backgroundColor = [UIColor whiteColor];
	//aView.alpha = 0.8;
	aView.layer.cornerRadius = 10;
	
	aView.layer.borderWidth = 2;
	aView.layer.borderColor = [[[ConfigurationManager sharedInstance] complementBorderColor] CGColor];
	aView.clipsToBounds = YES; 
	
	toPoint = aView.center;
	
	UIView *titleView =  [[UILabel alloc] initWithFrame:CGRectMake(aView.bounds.origin.x, 
																   aView.bounds.origin.y, 
																   aView.bounds.size.width,
																   44)];
	titleView.alpha = 1.0;
	titleView.backgroundColor = [[ConfigurationManager sharedInstance] complementBarBackgroundColor];

	
	titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(aView.bounds.origin.x+5, 
																	aView.bounds.origin.y, 
																	aView.bounds.size.width-45,
																	44)];
	titleLabel.text = @"Complément";
	//titleLabel.alpha = 1.0;
	titleLabel.backgroundColor = [UIColor clearColor];
	titleLabel.textColor = [[ConfigurationManager sharedInstance] complementBarForegroundColor];
	titleLabel.textAlignment = UITextAlignmentCenter;
	titleLabel.adjustsFontSizeToFitWidth=YES; 
	titleLabel.minimumFontSize=12.0;
	titleLabel.numberOfLines = 2;
		
	
	UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[closeButton setImage:[UIImage imageNamed:@"CloseComplement32"] forState:UIControlStateNormal];
	[closeButton addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
	
	closeButton.frame = CGRectMake(aView.bounds.size.width-40, (titleLabel.bounds.size.height-40)/2, 40, 40);
	
	
	
	content = [[UIWebView alloc] initWithFrame:CGRectMake(aView.bounds.origin.x+2, 
														  aView.bounds.origin.y+44,
														  aView.frame.size.width -4, aView.frame.size.height - 46)];
	
    content.backgroundColor = [UIColor clearColor];
    
    UIImageView *shadow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"smallShadow"]];
    shadow.frame = CGRectMake(aView.bounds.origin.x+1,
                              aView.bounds.origin.y+42,
                              aView.bounds.size.width-2,
                              10);
    
    
	[aView addSubview:titleView];
	[aView addSubview:titleLabel];
	[aView addSubview:closeButton];
	[aView addSubview:content];
	[aView addSubview:shadow];
    
	[self addSubview:backView];
	[self addSubview:aView];
	
	from = self.center;
	CGAffineTransform transform = CGAffineTransformMakeScale(0.01,0.01);
	aView.transform = transform;
	aView.autoresizesSubviews = YES;
	aView.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
    return self;
}

- (void)layoutSubviews{
	CGRect frame = [[UIScreen mainScreen] bounds];
	aView.center = CGPointMake(frame.origin.x + frame.size.width/2, frame.origin.y + frame.size.height/2);
	[aView setNeedsLayout];
    
	[super layoutSubviews];
}

- (void) updateOrientation:(UIDeviceOrientation) orientation duration:(NSTimeInterval)duration{
	    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
    UIInterfaceOrientation sorientation = [UIApplication sharedApplication].statusBarOrientation;
	
    if(sorientation == UIInterfaceOrientationLandscapeLeft){
		aView.transform = CGAffineTransformRotate(CGAffineTransformIdentity, -3.14/2);
	}else if(sorientation == UIInterfaceOrientationPortraitUpsideDown){
		aView.transform = CGAffineTransformRotate(CGAffineTransformIdentity, -3.14);
	}else if(sorientation == UIInterfaceOrientationLandscapeRight){
		aView.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 3.14/2);
	}else {
		aView.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 0);
	}
	[UIView commitAnimations];
    
}

- (void) openAction{
	[self performSelector:@selector(open) withObject:nil afterDelay:0.3];
	
	[self setHidden:NO];
	//backView.alpha = 0.0;
	
	UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	CGAffineTransform transform = CGAffineTransformMakeScale(1, 1);
	
	transform = CGAffineTransformRotate(transform, 0);
	
	if(orientation == UIInterfaceOrientationPortraitUpsideDown){
		transform = CGAffineTransformRotate(transform, -3.14);
	}else if(orientation == UIInterfaceOrientationLandscapeRight){
		transform = CGAffineTransformRotate(transform, 3.14/2);
	}else if(orientation == UIInterfaceOrientationLandscapeLeft){
		transform = CGAffineTransformRotate(transform, -3.14/2);
	}else{
        //transform = CGAffineTransformRotate(transform, 3.14);
    }
	
	aView.transform = transform;
	
	[UIView commitAnimations];
	
	
}

- (void) setTitle:(NSString *)title{
	titleLabel.text = [title stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

- (void) hide{
	[self setHidden:YES];
	[content loadHTMLString:@"" baseURL:nil];
	titleLabel.text = @"";
}
- (void) open{
	//backView.alpha = 0.75;
}

- (void) closeAction{
	
	[self performSelector:@selector(hide) withObject:nil afterDelay:0.3];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	CGAffineTransform transform = CGAffineTransformMakeScale(0.01, 0.01);
	aView.transform = transform;
	[UIView commitAnimations];

}

-(void) urlRequestBlocString:(NSURLRequest *)urlRequestBlocString{
	[content loadRequest:urlRequestBlocString];	
}

-(void) setContentString:(NSString *)string withModelName:(NSString*)modelName{
	if(modelName){
        DLog(@"content : %@",content);
        DLog(@"modelName : %@",modelName);
        NSMutableString *path = [NSMutableString stringWithFormat:@"%@/.",modelName];
        
        [path replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [path length])];
        
        //DLog(@"%@",[path description]);
        
        DLog(@"baseURL : %@",[NSURL fileURLWithPath:path]);
        [content loadHTMLString:string baseURL:[NSURL fileURLWithPath:path]];
        DLog(@"string : %@",string);


    }else{
        DLog(@"string : %@",string);

        [content loadHTMLString:string baseURL:nil];

    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



@end
