//
//  CustomTableViewController.h
//  WebMedia
//
//  Created by soreha on 01/12/10.
//  
//

#import <UIKit/UIKit.h>


@interface CustomTableViewController : UITableViewController

- (UIButton *) makeDetailDisclosureButtonWithFrame:(CGRect)rect andImage:(UIImage *)img;

@end
