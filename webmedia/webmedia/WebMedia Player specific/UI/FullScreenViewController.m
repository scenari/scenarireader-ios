//
//  ImageViewController.m
//  WebMedia
//
//  Created by soreha on 29/11/10.
//  
//

#import "FullScreenViewController.h"



@implementation FullScreenViewController

@synthesize image;
@synthesize topBar;
@synthesize textView;
@synthesize viewBar;
@synthesize playing;
@synthesize mediaDelegate;
@synthesize playPauseButton;
@synthesize keepUpToDateButton;

@synthesize chapterLabel;
@synthesize messageLabel;


Boolean statusIsHidden;


-(IBAction) close:(id)sender{
	[self dismissModalViewControllerAnimated:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
	if ([touch.view isKindOfClass:[UIButton class]] ) {
		// we touched our control surface
        return NO; // ignore the touch
    }
    return YES; // handle the touch
}

-(void) handleSingleTap{
	if(self.topBar.hidden && self.viewBar.hidden){
		self.topBar.hidden = NO;
		self.textView.hidden = NO;
		self.chapterLabel.hidden = NO;
	}else if (self.viewBar.hidden && self.mediaDelegate) {
		self.viewBar.hidden = NO;
	}else {
		self.topBar.hidden = YES;
		self.textView.hidden = YES;
		self.viewBar.hidden = YES;
		self.chapterLabel.hidden = YES;
	}
}

-(void) showEndMessage{
	if(!self.messageLabel) return;
	self.messageLabel.hidden = NO;
	
	NSInvocation* invoc = [NSInvocation invocationWithMethodSignature:[self.messageLabel methodSignatureForSelector:@selector(setHidden:)]];
	[invoc setTarget:self.messageLabel];
	[invoc setSelector:@selector(setHidden:)];
	BOOL yes = YES;
	[invoc setArgument:&yes atIndex:2];
	[invoc performSelector:@selector(invoke) withObject:nil afterDelay:3];
	
}

-(IBAction) switchKeepUpToDate:(id)sender{
	
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	//self.topBar.tintColor = [UIColor colorWithRed:0.373 green:0.333 blue:0.263 alpha:1.0f];
	self.textView.font = [UIFont systemFontOfSize:12.0f];
	UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap)];
	[singleTap setNumberOfTapsRequired:1];
	[singleTap setDelegate:self];
	[self.view addGestureRecognizer:singleTap];
	
	
	UISwipeGestureRecognizer *slideToLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(previous:)];
	[slideToLeft setDirection:UISwipeGestureRecognizerDirectionRight];
	[self.view addGestureRecognizer:slideToLeft];
	
	UISwipeGestureRecognizer *slideToRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(next:)];
	[slideToRight setDirection:UISwipeGestureRecognizerDirectionLeft];
	[self.view addGestureRecognizer:slideToRight];
		
	self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
		
}

-(void) setTextChapter:(NSString*) textChapter andSegment:(NSString *) textSegment{
	self.chapterLabel.text = textChapter ? textChapter : @"";
	self.textView.text = (textSegment&& [textSegment compare:textChapter]!=NSOrderedSame) ? textSegment : @"";
}

-(IBAction) next:(id)sender{
	if([mediaDelegate respondsToSelector:@selector(nextAction)]){
		[mediaDelegate performSelector:@selector(nextAction)];
	}
}

-(IBAction) previous:(id)sender{
	if([mediaDelegate respondsToSelector:@selector(previousAction)]){
		[mediaDelegate performSelector:@selector(previousAction)];
	}
}

-(IBAction) switchPlayPause:(id)sender{
	if([mediaDelegate respondsToSelector:@selector(playPauseAction)]){
		[mediaDelegate performSelector:@selector(playPauseAction)];
		[playPauseButton setImage:[UIImage imageNamed:(playing?@"pause32":@"play32")] forState:UIControlStateNormal];
	}
}


 - (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	//[self.navBar.topItem setTitle:self.title];
	
    topBar.hidden = YES;
	statusIsHidden = [[UIApplication sharedApplication] isStatusBarHidden];
	[self.textView setHidden:YES];
    [self.chapterLabel setHidden:YES];
    self.viewBar.hidden = YES;
	[playPauseButton setImage:[UIImage imageNamed:(playing?@"pause32":@"play32")] forState:UIControlStateNormal];
	//[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
} 
- (void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
	topBar.hidden = NO;
	[self.textView setHidden:NO];
	[self.chapterLabel setHidden:NO];
} 
- (void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
	//[[UIApplication sharedApplication] setStatusBarHidden:statusIsHidden withAnimation:UIStatusBarAnimationNone];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end
