//
//  SearchViewControllerCell.h
//  webmedia
//
//  Created by soreha on 21/09/12.
//  
//

#import <UIKit/UIKit.h>

@interface SearchViewControllerCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *categoryLabel;
@property (nonatomic, weak) IBOutlet UILabel *downloadLabel;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;



@end
