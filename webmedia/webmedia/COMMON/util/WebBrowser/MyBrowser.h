//
//  MyBrowser.h
//  Patrimoine
//
//  Created by soreha on 26/04/10.
//  
//
#import <MessageUI/MessageUI.h>
#import <UIKit/UIKit.h>


@interface MyBrowser : UIViewController <UIWebViewDelegate, MFMailComposeViewControllerDelegate>{
	IBOutlet UIWebView *content;
	IBOutlet UIActivityIndicatorView *indicatorView;
}


@property (nonatomic, strong) NSURLRequest *currentRequest;

@property (nonatomic, strong) IBOutlet UIWebView *content;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong) UIBarButtonItem *forward;
@property (nonatomic, strong) UIBarButtonItem *back ;
@property (nonatomic, strong) UIBarButtonItem *refresh ;
@property (nonatomic, strong) UIBarButtonItem *stop ;


-(void) showURL:(NSURLRequest *)request;
-(void) openContentInBrowser;

@end
