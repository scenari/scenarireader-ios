//
//  ImageUnit.m
//  WebMedia
//
//  Created by soreha on 22/02/11.
//  
//

#import "ImageUnit.h"


@implementation ImageUnit

@synthesize identifiant;
@synthesize name;
@synthesize description;
@synthesize imageTitle;
@synthesize refUri;

- (id) initWithUri:(NSString*)aRefUri title:(NSString*)aTitle{
	self=[super init];
    if(self){
		
	}
	self.imageTitle = aTitle;
	self.refUri = aRefUri;
	return self;
}


@end
