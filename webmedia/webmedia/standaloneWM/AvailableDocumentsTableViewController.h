//
//  AvailableEmissionsTableViewController.h
//  OptimGenericReader
//
//  Created by Mory Doukouré on 16/02/11.
//  Copyright 2011 Soreha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefaultDocumentsTableViewController.h"

@class CoreDataManager, Channel;

@interface AvailableDocumentsTableViewController : DefaultDocumentsTableViewController 

@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonDownloadAll;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonDownloadSelected;

-(void) initWithChannel :(Channel *) ch;
-(void) reloadFetchedResultControllerSortingBy : (int) criteria;


@end
