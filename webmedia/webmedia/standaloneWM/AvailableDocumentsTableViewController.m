//
//  AvailableDocumentsTableViewController.m
//  OptimGenericReader
//
//  Created by Mory Doukouré on 16/02/11.
//  Copyright 2011 Soreha. All rights reserved.
//

#import "AvailableDocumentsTableViewController.h"
#import "CoreDataManager.h"
#import "Document.h"
#import "Channel.h"
#import "Thumbnail.h"


@implementation AvailableDocumentsTableViewController



-(void) releaseOutlets{
	self.myCell = nil;
}

// @see NSObject
- (void)dealloc {
    self.fetchedResultsController.delegate = nil;
    self.fetchedResultsController = nil;

	[self releaseOutlets];
}

-(void) manageBarButtonItemAccessibility{
    
}

-(void) initWithChannel :(Channel *) ch {
    self.isDownloadedDocumentsView = NO;
    self.channel = ch;
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAvailableDocumentsForChannel:channel withCategory:nil sortedBy:0] 
										  managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"category" 
													 cacheName:nil];
    self.fetchedResultsController.delegate = self;
	
    NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]){
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
	[self manageBarButtonItemAccessibility];
    DLog(@"Nb Available Documents : %d", [[self.fetchedResultsController fetchedObjects] count]);
}

-(void) reloadFetchedResultControllerSortingBy : (int) criteria {
    DLog();
    self.fetchedResultsController.delegate = nil;
    self.fetchedResultsController = nil;
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAvailableDocumentsForChannel:channel withCategory:nil sortedBy:criteria] 
                                                                            managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"category" 
                                                                                       cacheName:nil];
    
    self.fetchedResultsController.delegate = self;
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]){
        ALog(@"Unresolved error %@, %@", error, [error userInfo]);
        //abort();
    }
    [self manageBarButtonItemAccessibility];
    [self.tableView reloadData];
    
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	if([[fetchedResultsController fetchedObjects] count]==0) return @"Aucun nouveau document";
	return @"";
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    DLog();
	Document  *document = [self.fetchedResultsController objectAtIndexPath:indexPath];
	if ([document.isSelected intValue] == 0){
		document.isSelected = [NSNumber numberWithBool:YES];
	} else {
		document.isSelected = [NSNumber numberWithBool:NO];
	}
    [self manageBarButtonItemAccessibility];

}
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (void)contentUpdated{
    [self.contentDelegate updateSegmentTitleNumbers:self];
}

@end

