//
//  WebMediaRSSImporter.m
//  WebMediaTest
//
//  Created by soreha on 18/01/11.
//  
//

#import "DocumentRSSImporter.h"
#import "Channel.h"
#import "CoreDataManager.h"
#import "Document.h"

@interface DocumentRSSImporter (PrivateMethods)
-(NSDate *)dateFromRFC822:(NSString *)date;
@end

@implementation DocumentRSSImporter

@synthesize dataManager;
@synthesize channel;
@synthesize delegate;
@synthesize pos;
@synthesize availableDocumentsGuid, currentContents, tmpDocument, xmlparser;




Boolean in_item	= NO;
Boolean in_image = NO;


- (id) initWithDataManager : (CoreDataManager *) dManager withChannel: (Channel *) ch {
	self = [super init];
	self.dataManager = dManager;
	self.channel = ch;
	return self;
}

#pragma mark xml
-(void) loadXMLFromURL:(NSURL *)richMediaURL{
	self.xmlparser = [[NSXMLParser alloc] initWithContentsOfURL:richMediaURL];
	[xmlparser setDelegate:self];
	[xmlparser parse];	
}

-(void) loadXMLFromData:(NSData *)data {
	self.xmlparser = [[NSXMLParser alloc] initWithData:data];
	[xmlparser setDelegate:self];
	[xmlparser parse];	
}

//Document parsing start
- (void)parserDidStartDocument:(NSXMLParser *)parser {	
	//initialyze var here
	self.currentContents = [[NSMutableString alloc] initWithCapacity:0];
    availableDocumentsGuid  = [[NSMutableSet alloc] initWithCapacity:0];
    pos = 0;
}

//Document parsing end
- (void)parserDidEndDocument:(NSXMLParser *)parser {
	DLog(@"Fin de lecture du fichier\n");
	if (channel.title == nil || [channel.title compare:@""] == NSOrderedSame) {
		channel.title = @"Sans titre";
	}
 
	[dataManager saveData];
    
    DLog(@"nbDocs : %d",[availableDocumentsGuid count]);

    [dataManager manageDeletedDocuments:availableDocumentsGuid forChannel:channel];
    
	[delegate didFinishParsing];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
	if(currentContents && string){
		[currentContents appendString:string];
	}
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName 
	attributes:(NSDictionary *)attributeDict {
	
	if([elementName compare:@"item"] == NSOrderedSame){
		in_item = YES;
		tmpDocument = [dataManager newDocument];
        tmpDocument.pubType = @"webmedia";
        [tmpDocument setRssPos:[NSNumber numberWithInt:pos]];
        pos ++;
	} else if([elementName compare:@"image"] == NSOrderedSame){ 
		in_image = YES;
	} else if([elementName compare:@"mobile:smallThumbnail"] == NSOrderedSame){
		tmpDocument.thumbnail_small_URI = [attributeDict objectForKey:@"url"];
	} else if([elementName compare:@"mobile:largeThumbnail"] == NSOrderedSame){
		tmpDocument.thumbnail_large_URI = [attributeDict objectForKey:@"url"];
	} else if([elementName compare:@"mobile:mobileInf"] == NSOrderedSame){
		tmpDocument.author = [attributeDict objectForKey:@"author"];
		tmpDocument.version = [attributeDict objectForKey:@"version"];
		tmpDocument.legal = [attributeDict objectForKey:@"legal"];
		tmpDocument.duration = [NSNumber numberWithInt:[[attributeDict objectForKey:@"duration"] intValue]];
		tmpDocument.keywords = [attributeDict objectForKey:@"keywords"];
		tmpDocument.location = [attributeDict objectForKey:@"location"];
	} else if([elementName compare:@"mobile:largeThumbnail"] == NSOrderedSame){
	} else [currentContents setString:@""];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qName {
	
	if ([elementName compare:@"title"] == NSOrderedSame) {
		if (in_item && !in_image) { 
			tmpDocument.title = [NSString stringWithString:currentContents];
		} else if (!in_image) {  
			self.channel.title = [NSString stringWithString:currentContents];
		}
	} else if ([elementName compare:@"link"] == NSOrderedSame) {
		if (in_item) {
			tmpDocument.link = [NSString stringWithString:currentContents];
		} else if (!in_image) { 
			self.channel.link = [NSString stringWithString:currentContents];
		}
	} else if ([elementName compare:@"description"] == NSOrderedSame) {
		if (in_item) {
			tmpDocument.summary = [NSString stringWithString:currentContents];
		} else if (!in_image) { 
			self.channel.summary = [NSString stringWithString:currentContents];
		}
	} else if ([elementName compare:@"pubDate"] == NSOrderedSame) {
		NSDate *date = [self dateFromRFC822:[NSString stringWithString:currentContents]];
		if (!in_item) {
			self.channel.pubDate = date;
		}
	}  else if ([elementName compare:@"mobile:pubDate"] == NSOrderedSame) {
		NSDate *date = [self dateFromRFC822:[NSString stringWithString:currentContents]];
		tmpDocument.pubDate = date;
	} else if ([elementName compare:@"copyright"] == NSOrderedSame) {
		self.channel.copyright = [NSString stringWithString:currentContents];
	} else if ([elementName compare:@"mobile:lastBuildDate"] == NSOrderedSame) {
		NSDate *date = [self dateFromRFC822:[NSString stringWithString:currentContents]];
		tmpDocument.lastBuildDate = date;
	} else if ([elementName compare:@"lastBuildDate"] == NSOrderedSame) {
		NSDate *date = [self dateFromRFC822:[NSString stringWithString:currentContents]];
		self.channel.lastBuildDate = date;
	} else if ([elementName compare:@"image"] == NSOrderedSame) {
		in_image = NO;
	} else if ([elementName compare:@"url"] == NSOrderedSame) {
        if(in_image){
            self.channel.thumbnailURI = [NSString stringWithString:currentContents];
        }
		//
	} else if ([elementName compare:@"width"] == NSOrderedSame) {
		//tmpImg.width = [[NSString stringWithString:currentContents] integerValue];
		//DLog(@"Image - largeur : %d\n",tmpImg.width);
	} else if ([elementName compare:@"height"] == NSOrderedSame) {
		//tmpImg.height = [[NSString stringWithString:currentContents] integerValue];
		//DLog(@"Image - hauteur : %d\n",tmpImg.height);
	} else if ([elementName compare:@"category"] == NSOrderedSame) {
		if (in_item)
			tmpDocument.category = [NSString stringWithString:currentContents?:@""];
	} else if ([elementName compare:@"comments"] == NSOrderedSame) {
		tmpDocument.comments = [NSString stringWithString:currentContents];
	} else if ([elementName compare:@"mobile:smartphoneLink"] == NSOrderedSame) {
		tmpDocument.smartPhoneLink = [NSString stringWithString:currentContents];
	} else if ([elementName compare:@"mobile:tabletLink"] == NSOrderedSame) {
		tmpDocument.tabletLink = [NSString stringWithString:currentContents];
	} else if([elementName compare:@"mobile:pubType"] == NSOrderedSame){
		tmpDocument.pubType = [NSString stringWithString:currentContents];
	}else if ([elementName compare:@"guid"] == NSOrderedSame) {
		tmpDocument.guid = [NSString stringWithString:currentContents];
	} else if ([elementName compare:@"item"] == NSOrderedSame) {
		in_item = NO;
		
		if (tmpDocument.guid == nil) {
			tmpDocument.guid = tmpDocument.smartPhoneLink;
		}
		
        [availableDocumentsGuid addObject:tmpDocument.guid];
        
		[dataManager addOrModifyDocument:tmpDocument forChannel:channel];
        
    }
}


- (NSDate *)dateFromRFC822:(NSString *)date {
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	NSLocale *us = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
	[formatter setLocale:us];
	
	
	if ([date length] == 31 || [date length] == 30) {
		[formatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss z"];
	} else if ([date length] == 28 || [date length] == 27) {
		[formatter setDateFormat:@"EEE, dd MMM yyyy HH:mm z"];
	} else if ([date length] == 26 || [date length] == 25) {
		[formatter setDateFormat:@"dd MMM yyyy HH:mm:ss z"];
	} else if ([date length] == 23 || [date length] == 22) {
		[formatter setDateFormat:@"dd MMM yyyy HH:mm z"];
	}
	
    return [formatter dateFromString:date];
}



@end
