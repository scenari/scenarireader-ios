//
//  AboutViewController.h
//  
//
//  Created by soreha on 13/11/09.
//   
//


#import <MessageUI/MessageUI.h>

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController <UIWebViewDelegate, MFMailComposeViewControllerDelegate>{
	IBOutlet UIWebView *content;

}
@property (nonatomic, strong) IBOutlet UIWebView *content;

- (void) closeView;


@end
