//
//  MediaHandlerManager.h
//  webmedia
//
//  Created by soreha on 13/06/12.
//  
//

#import <Foundation/Foundation.h>
#import "MediaHandlerDelegate.h"


@interface MediaHandlerManager : NSObject


@property (nonatomic) id<MediaHandlerDelegate> mediaHandlerDelegate;


+ (MediaHandlerManager *) sharedInstance;
@end
