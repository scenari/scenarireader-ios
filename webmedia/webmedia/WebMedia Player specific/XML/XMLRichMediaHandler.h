//
//  XMLRichMediaHandler.h
//  PhoneRadio
//
//  Created by soreha on 26/07/10.
//  
//

#import <Foundation/Foundation.h>
#import "Segment.h"
#import "Illustration.h"
#import "Extras.h"
#import "Credits.h"
#import "RichMedia.h"
#import "Chapter.h"
#import "Complement.h"
#import "Resource.h"
#import "ImageGallery.h"
#import "Reference.h"


@interface XMLRichMediaHandler : NSObject <NSXMLParserDelegate>{
	RichMedia *richMedia;
	
	NSMutableString *currentContents;
}

@property(nonatomic, strong) RichMedia *richMedia;
@property(nonatomic, strong) NSMutableString *currentContents;
@property(nonatomic, strong) Segment *tmpSegment;
@property(nonatomic, strong) Chapter *tmpChapter;
@property(nonatomic, strong) Resource *tmpResource;
@property(nonatomic, strong) Resource *tmpImage;
@property(nonatomic, strong) ImageGallery *tmpGallery;
@property(nonatomic, strong) Complement *tmpComplement;
@property(nonatomic, strong) Reference *tmpReference;

@property(nonatomic, strong) Extras *tmpExtra;
@property(nonatomic, strong) Credits *tmpCredit;
@property(nonatomic, strong) Illustration *tmpIllustration;
@property(nonatomic, strong) NSXMLParser *mediaParser;


-(void) loadXMLFromURL:(NSURL *)richMediaURL;

@end
