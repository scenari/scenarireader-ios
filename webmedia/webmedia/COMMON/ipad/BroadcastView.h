//
//  BroadcastView.h
//  webmedia
//
//  Created by soreha on 30/11/12.
//  
//

#import <UIKit/UIKit.h>
#import "Document.h"

@interface BroadcastView : UIView <UIGestureRecognizerDelegate>
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImageView *favImageView;

@property (strong, nonatomic) UILabel *titleLabel;

@property (strong, nonatomic) UILabel *legend1Label;
@property (strong, nonatomic) UILabel *legend2Label;
@property (strong, nonatomic) UILabel *lineLabel;
@property (strong, nonatomic) UIButton *infoButton;
@property (strong, nonatomic) UIButton *deleteButton;
@property (strong, nonatomic) UIButton *actionButton;


@property (strong, nonatomic) UIView *downloadView;
@property (strong, nonatomic) UIButton *buttonDownload;
@property (strong, nonatomic) UIButton *buttonOpenOnLineVersion;


@property (strong, nonatomic) UIButton *buttonResumeDownload;
@property (strong, nonatomic) UIButton *buttonSuspendDownload;
@property (strong, nonatomic) UIButton *buttonCancelDownload;

@property (strong, nonatomic) UIProgressView *downloadProgressView;
@property (strong, nonatomic) UILabel *downloadInfoLabel;


@property (strong, nonatomic) Document *document;


- (void)updateDownloadProgress;
- (void)showDownloadingOptionButtons;
- (void)hideDownloadingOptionButtons;
- (void)showSuspendedOptionButtons;
- (void)hideSuspendedOptionButtons;


@end
