//
//  Bookmark.m
//  webmedia
//
//  Created by soreha on 28/05/13.
//  
//

#import "Bookmark.h"
#import "Document.h"


@implementation Bookmark

@dynamic pageUri;
@dynamic pageTitle;
@dynamic document;

@end
