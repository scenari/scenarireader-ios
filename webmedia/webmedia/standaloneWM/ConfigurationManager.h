//
//  ConfigurationManager.h
//  webmedia
//
//  Created by soreha on 06/06/12.
//  
//

#import <Foundation/Foundation.h>

@interface ConfigurationManager : NSObject

@property (strong, nonatomic) NSDictionary *properties;


+ (ConfigurationManager *)sharedInstance;

- (BOOL)multiChannelConfiguration;
- (BOOL)canAddSource;
- (BOOL)canDeleteSource;
- (BOOL)useUserLocation;
- (BOOL)showEmptyImageInTocTable;
- (BOOL)addAutoNumberingToChapitreAndSegmentName;
- (BOOL)channelListWithDetails;
- (BOOL)showTutorialFromCreditPage;
- (BOOL)advancedTocMode;
- (BOOL)playerDescriptionFirst;
- (BOOL)downloadInBackground;

- (NSString *)singleComplementString;
- (NSString *)multipleComplementsString;
- (NSString *)shortComplementString;

- (UIColor *)navigationBarTintColor;
- (UIColor *)selectedChannelBorderColor;
- (UIColor *)unselectedChannelBorderColor;
- (UIColor *)mainViewTabletteChannelScrollBackgroundColor;

- (UIColor *)tableViewSectionBackgroundColor;
- (UIColor *)tableViewSectionForegroundColor;
- (UIColor *)tableViewDarckCellBackgroundColor;
- (UIColor *)tableViewLightCellBackgroundColor;

- (UIColor *)getSelectedColor;
- (UIColor *)getUnSelectedColor;
- (UIColor *)getExtendedPlayerBackgroundColor;
- (UIColor *)getPlayerMainViewBackgroundColor;

- (UIColor *)chapterViewBackgroundColor;
- (UIColor *)chapterViewDefaultPairColor;
- (UIColor *)chapterViewDefaultImpairColor;
- (UIColor *)chapterViewTextColor;
- (UIColor *)chapterViewAdvanceColor;
- (UIColor *)chapterViewBorderColor;


- (UIColor *)complementBarForegroundColor;
- (UIColor *)complementBarBackgroundColor;
- (UIColor *)complementBorderColor;


- (UIColor *)playerTimeLabelBackground;
- (UIColor *)playerTimeLabelForeground;
- (UIColor *)playerTimeLabelShadow;
- (UIColor *)playerTimeLabelBorder;


- (UIColor *)downloadProgressInfoColor;
- (UIColor *)downloadErrorInfoColor;

- (float)getChapterViewSmallHorizontalBorder;
- (float)getChapterViewSmallVerticalBorder;
- (float)getChapterViewBigHorizontalBorder;
- (float)getChapterViewBigVerticalBorder;

- (UIImage *)getAccessoryImage;


- (NSString *)getNoNamedCategoryHeaderLabel;
- (NSString *)getNoNamedCategorySearchCellLabel;


- (NSArray *)sourcesList;

//Tab items and titles
- (NSString *)sourcesListTitle;
- (NSString *)libraryTabName;
- (NSString *)documentsListTitle;
- (NSString *)availableDocumentsButtonTitle;
- (NSString *)downloadedDocumentsButtonTitle;

//Channel list configuration
- (NSString *)iPhoneChannelCellIdentifier;
- (float)iPhoneChannelCellHeight;
- (NSString *)iPhone5ChannelCellIdentifier;
- (float)iPhone5ChannelCellHeight;


- (NSString *)iPadMainViewTitle;




@end
