//
//  DocumentsForChannelViewController.h
//  OptimGenericReader
//
//  Created by Mory Doukouré on 16/02/11.
//  Copyright 2011 Soreha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefaultDocumentsTableViewController.h"
#import "CoreDataManager.h"

@class DownlodedDocumentsTableViewController,AvailableDocumentsTableViewController, Channel, SearchViewController;

@interface DocumentsForChannelViewController : UIViewController <RssDelegate, UIActionSheetDelegate, ContentDelegate> {
	/* controller pour la vue 'agences' */
	IBOutlet DownlodedDocumentsTableViewController	*downlodedDocumentsTableViewController;
	/* controller pour la vue 'devis' */
	IBOutlet AvailableDocumentsTableViewController	*availableDocumentsTableViewController;
	
	IBOutlet UISegmentedControl						*segmentData;
	
	IBOutlet UIToolbar								*toolbar;
	
	Channel											*channel;
}


//properties
@property(nonatomic, strong) IBOutlet  DownlodedDocumentsTableViewController	*downlodedDocumentsTableViewController;
@property(nonatomic, strong) IBOutlet  AvailableDocumentsTableViewController	*availableDocumentsTableViewController;
@property(nonatomic, strong) IBOutlet  UISegmentedControl						*segmentData;
@property(nonatomic, strong) IBOutlet  IBOutlet UIToolbar						*toolbar;
@property (weak, nonatomic) IBOutlet UIView *errorView;

@property(nonatomic, strong) Channel											*channel;

/* permet de passer d'une table à l'autre à l'aide du segment */
- (IBAction) switchView;
- (IBAction) downloadAll;
- (IBAction) downloadSelected;
- (void) openSortActionSheet;


- (void)showAboutLeftButtonOnNavigationBar;


@end
