//
//  Channel.m
//  OptimGenericReader
//
//  Created by soreha on 14/02/11.
//  
//

#import "Channel.h"
#import "Document.h"
#import "Thumbnail.h"

@implementation Channel
@dynamic updated;
@dynamic summary;
@dynamic title;
@dynamic copyright;
@dynamic link;
@dynamic pubDate;
@dynamic thumbnailURI;
@dynamic lastBuildDate;
@dynamic rssUrl;
@dynamic thumbnail;
@dynamic documents;
@dynamic downloadError;



- (void)addDocumentsObject:(Document *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"documents" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"documents"] addObject:value];
    [self didChangeValueForKey:@"documents" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
}

- (void)removeDocumentsObject:(Document *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"documents" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"documents"] removeObject:value];
    [self didChangeValueForKey:@"documents" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
}

@end
