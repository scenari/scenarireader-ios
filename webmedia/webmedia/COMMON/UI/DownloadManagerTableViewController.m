//
//  DownloadManagerTableViewController.m
//  OptimGenericReader
//
//  Created by soreha on 18/03/11.
//  
//

#import "DownloadManagerTableViewController.h"
#import "CoreDataManager.h"
#import "Document.h"
#import "Thumbnail.h"
#import "RequestManager.h"
#import "ASINetworkQueue.h"

@implementation DownloadManagerTableViewController

@synthesize nbSelectedDocument;
@synthesize labelSelection;

-(void) releaseOutlets{
	self.myCell = nil;
}

// @see NSObject
- (void)dealloc {

        self.fetchedResultsController.delegate = nil;
        self.fetchedResultsController = nil;
	[self releaseOutlets];
}
- (void)update{
    self.isDownloadedDocumentsView = NO;
    self.nbSelectedDocument = 0;
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestDownloadingDocuments:NO]
                                                                        managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"updateAvailable"
                                                                                   cacheName:nil];
    self.fetchedResultsController.delegate = self;
	
    NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]){
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
	
	DLog(@"Nb Downloading or Updated Documents : %d", [[self.fetchedResultsController fetchedObjects] count]);
    if([[self.fetchedResultsController fetchedObjects] count]==0){
        self.labelSelection.text = @"Aucun téléchargement en cours";
        
    }
    self.startButtonItem.enabled = self.cancelButtonItem.enabled = self.suspendButtonItem.enabled = ([[self.fetchedResultsController fetchedObjects] count]!=0);
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self update];
}



- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	if([[fetchedResultsController fetchedObjects] count]==0) return @"Aucun téléchargement en cours";
	return @"";
}


#pragma mark -
#pragma mark Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    if ([[sectionInfo name] compare:@"1"] == NSOrderedSame) {
        return @"Mises à jour";
    } else {
        return @"Téléchargements";
    }
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    Document  *document = [self.fetchedResultsController objectAtIndexPath:indexPath];
	if ([document.isSelected intValue] == 0){
		document.isSelected = [NSNumber numberWithBool:YES];
	} else {
		document.isSelected = [NSNumber numberWithBool:NO];
	}
    
    [[CoreDataManager sharedInstance] saveData];
}

#pragma mark -
#pragma mark download action methods

-(IBAction) resumeDownload{
    NSFetchedResultsController *resultController;
    if (self.nbSelectedDocument == 0) {
        resultController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestDownloadingDocuments:NO] 
                                                               managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:nil 
                                                                          cacheName:nil];
    } else {
        resultController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestDownloadingDocuments:YES] 
                                                               managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:nil 
                                                                          cacheName:nil];
    }
    
    NSError *error = nil;
    [resultController performFetch:&error];

    
    if ([CoreDataManager sharedInstance].requestManager.documentDownloadQueue == nil) {
        [[CoreDataManager sharedInstance].requestManager initAsiQueue];
    }
    
    NSArray *documents = [resultController fetchedObjects];
    
    NSString *tmpDirectory = [[[CoreDataManager sharedInstance] requestManager] getTmpDownloadFolder];
    for (Document * document in documents){
        if(document.tmpDownloadFilePath && ![document.tmpDownloadFilePath hasPrefix:tmpDirectory]){
            //Cancel previous version app tmp download file (another folder)
            ALog(@"new app version number -> previous tmp file canceled document:%@",document.title);
            document.tmpDownloadFilePath = nil;
        }
        document.isSelected = [NSNumber numberWithBool:NO];
        if (document.request == nil) {
            [[CoreDataManager sharedInstance].requestManager addDocumentRequest:document];
        }
    }
    
    [[CoreDataManager sharedInstance].requestManager.documentDownloadQueue go];
    [[CoreDataManager sharedInstance] saveData];
}


-(IBAction) suspendDownload{
    NSFetchedResultsController *resultController;
    if (self.nbSelectedDocument == 0) {
        resultController = fetchedResultsController;
    } else {
        resultController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestDownloadingDocuments:YES] 
                                                               managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:nil 
                                                                          cacheName:nil];
        NSError *error = nil;
        [resultController performFetch:&error];
    }
    
    for (Document * document in [resultController fetchedObjects]){
        [document.request clearDelegatesAndCancel];
        document.downloadProgress = 0;
        document.downloadSuspended = [NSNumber numberWithBool:YES];
        document.isSelected = [NSNumber numberWithBool:NO];
        document.request = nil;
    }

    [[CoreDataManager sharedInstance] saveData];
}

-(IBAction) cancelDownload{
    
    NSFetchedResultsController *resultController;
    if (self.nbSelectedDocument == 0) {
        resultController = fetchedResultsController;
    } else {
        resultController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestDownloadingDocuments:YES] 
                                                               managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:nil 
                                                                          cacheName:nil];
        NSError *error = nil;
        [resultController performFetch:&error];
    }
    
    for (Document * document in [resultController fetchedObjects]){
        [document.request clearDelegatesAndCancel];
        document.downloadProgress = 0;
        document.downloading = [NSNumber numberWithBool:NO];
        document.downloadSuspended = [NSNumber numberWithBool:NO];
        document.isSelected = [NSNumber numberWithBool:NO];
        document.request = nil;
    }
    
    [[CoreDataManager sharedInstance] saveData];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath{
	UITableView *tableView = self.tableView;
    
    switch(type){
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
			[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
    
    NSFetchedResultsController *resultController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestDownloadingDocuments:YES] 
                                                                    managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:nil 
                                                                               cacheName:nil];
     NSError *error = nil;
    [resultController performFetch:&error];
    
    
    self.nbSelectedDocument = [[resultController fetchedObjects] count];
    
    if (self.nbSelectedDocument > 0) {
        self.labelSelection.text = @"Sélection";
#pragma ACCESSIBILITY
        self.startButtonItem.accessibilityLabel = @"Démarrer le téléchargement des documents sélectionnés.";
#pragma ACCESSIBILITY
        self.cancelButtonItem.accessibilityLabel = @"Annuler le téléchargement des documents sélectionnés.";
#pragma ACCESSIBILITY
        self.suspendButtonItem.accessibilityLabel = @"Suspendre le téléchargement des documents sélectionnés.";
    } else {
        self.labelSelection.text = @"Tout";
#pragma ACCESSIBILITY
        self.startButtonItem.accessibilityLabel = @"Démarrer tous les téléchargements.";
#pragma ACCESSIBILITY
        self.cancelButtonItem.accessibilityLabel = @"Annuler tous les téléchargements.";
#pragma ACCESSIBILITY
        self.suspendButtonItem.accessibilityLabel = @"Suspendre tous les téléchargements.";
    }
    
    if([[self.fetchedResultsController fetchedObjects] count]==0){
        self.labelSelection.text = @"Aucun téléchargement en cours";
    }
    self.startButtonItem.enabled = self.cancelButtonItem.enabled = self.suspendButtonItem.enabled = ([[self.fetchedResultsController fetchedObjects] count]!=0);

    
    
    
}

@end
