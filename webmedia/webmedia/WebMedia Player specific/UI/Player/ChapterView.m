//
//  ChapterView.m
//  WebMedia
//
//  Created by soreha on 02/12/10.
//  
//

#include <math.h>

#import "ChapterView.h"
#import "ConfigurationManager.h"


@implementation ChapterView

@synthesize backgroundLabel;
@synthesize progressLabel;
@synthesize textLabel;

@synthesize index;



- (id)initWithFrame:(CGRect)frame andIndex:(int) anIndex{
    if ((self = [super initWithFrame:frame])) {
        self.index = anIndex;
		// Initialization code
		//self.opaque = YES;
		self.backgroundColor = [[ConfigurationManager sharedInstance] chapterViewBackgroundColor];
				
		//self.clipsToBounds = YES;
		
		float x = 0;
		float y = [[ConfigurationManager sharedInstance] getChapterViewSmallVerticalBorder];
		float w = frame.size.width;
		float h = frame.size.height -2*[[ConfigurationManager sharedInstance] getChapterViewSmallVerticalBorder] ;
				
		backgroundLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, w, h)];
		[backgroundLabel.layer setMasksToBounds:YES];
		[backgroundLabel.layer setBorderColor:[[[ConfigurationManager sharedInstance] chapterViewBorderColor] CGColor]];
		[backgroundLabel.layer setBorderWidth:1.0];
		
		self.autoresizesSubviews = YES;		
		
		[self addSubview:backgroundLabel];
				
		self.contentMode=UIViewContentModeRedraw;
				
		self.progressLabel = [[UILabel alloc] 
						 initWithFrame:CGRectMake(x, 
												  y,
												  0, 
												  0)];
		//self.progressLabel.opaque = YES;
		[self addSubview:progressLabel];
		
		self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(3, y, w-6, h)];
		[self addSubview:textLabel];
		self.textLabel.opaque = NO;
		self.textLabel.font = [UIFont boldSystemFontOfSize:9];
		
		//colors
		self.backgroundLabel.backgroundColor = ((self.index%2==0)?[[ConfigurationManager sharedInstance] chapterViewDefaultPairColor]:[[ConfigurationManager sharedInstance] chapterViewDefaultImpairColor]);
		self.progressLabel.backgroundColor = [[ConfigurationManager sharedInstance] chapterViewAdvanceColor];
		self.textLabel.textColor = [[ConfigurationManager sharedInstance] chapterViewTextColor];
		self.textLabel.backgroundColor = [UIColor clearColor];
		
		self.textLabel.hidden = YES;
		
		self.clipsToBounds = NO;
    }
    return self;
}

-(void) setProgression:(float) progress{
	if(progress == INFINITY) progress = 1.0f;
	else if(isnan(progress)) progress = 0.0f;
	float x = 0;
	float y = 0;
	float w = self.frame.size.width ;
	float w2 = w*progress;
	if(isnan(w2)) w2=w;
	float h = self.frame.size.height ;
	self.progressLabel.frame = CGRectMake(x, y, MIN(w, w2), h);
}


-(void) adapteToFrame:(CGRect)frame showText:(Boolean)showText{
	float scale = (frame.size.width!=0)?self.frame.size.width/frame.size.width:1.0;
	[self setFrame:frame];
	
	float x = 0;
	float y = [[ConfigurationManager sharedInstance] getChapterViewSmallVerticalBorder];
	float w = frame.size.width;
	float h = frame.size.height -2*[[ConfigurationManager sharedInstance] getChapterViewSmallVerticalBorder] ;
	
	self.backgroundLabel.frame = CGRectMake(x, y, w, h);
	
	self.progressLabel.frame = CGRectMake(x, y, (self.progressLabel.frame.size.width * scale), h);
	
	self.textLabel.frame = CGRectMake(3, y, w-6.0, h);
	
	self.textLabel.hidden = !showText;
}


@end
