//
//  DownloadManagerTableViewController.h
//  OptimGenericReader
//
//  Created by soreha on 18/03/11.
//  
//

#import <UIKit/UIKit.h>
#import "DefaultDocumentsTableViewController.h"

@class CoreDataManager;

@interface DownloadManagerTableViewController : DefaultDocumentsTableViewController {
    int nbSelectedDocument;
    UILabel *labelSelection;
}

@property(nonatomic)  int nbSelectedDocument;
@property (nonatomic, strong) IBOutlet UILabel *labelSelection;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *startButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *suspendButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButtonItem;


-(IBAction) resumeDownload;
-(IBAction) suspendDownload;
-(IBAction) cancelDownload;

- (void)update;

@end
