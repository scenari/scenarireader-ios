//
//  AddChannelViewController.h
//  OptimGenericReader
//
//  Created by soreha on 15/02/11.
//
//

#import <UIKit/UIKit.h>
#import "AddChannelViewDelegate.h"
#import "ZBarReaderViewController.h"


@interface AddChannelViewController : UIViewController <UITextFieldDelegate > {
	NSObject <AddChannelViewDelegate> *addChannelViewDelegate;
    UINavigationBar *navigationBar;
}

@property (nonatomic, strong) IBOutlet UITextField * rssAddress;
@property (nonatomic, strong) NSObject <AddChannelViewDelegate> *addChannelViewDelegate;
@property (weak, nonatomic) IBOutlet UIButton *qrbutton;

@property (nonatomic, strong) ZBarReaderViewController *zReader;

- (IBAction)addChannel:(id) sender;
- (IBAction)cancel:(id) sender;
- (IBAction)QrcodeAction:(id)sender;
//test de modification


@end
