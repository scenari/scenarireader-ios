//
//  HtmlContentType.h
//  PhoneRadio
//
//  Created by soreha on 28/11/10.
//  
//

#import <Foundation/Foundation.h>


@interface HtmlContentType : NSObject {
	NSString *refUri;
	NSString *title;
	NSMutableArray *galleries;
}

@property(nonatomic, strong) NSString *refUri;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSMutableArray *galleries;

@end
