//
//  AppDelegate.h
//  webmedia
//
//  Created by Emmanuel Beaufort on 01/06/12.
//  Copyright (c) 2012 Soreha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaHandlerDelegate.h"

@interface WebMediaAppDelegate : UIResponder <UIApplicationDelegate, MediaHandlerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIImageView *splashView;

@end
