//
//  Resource.h
//  PhoneRadio
//
//  Created by soreha on 28/11/10.
//  
//

#import <Foundation/Foundation.h>


@interface Resource : NSObject {
	NSString *title;
	NSString *refUri;
}


@property(nonatomic, strong) NSString *refUri;
@property(nonatomic, strong) NSString *title;

@end
