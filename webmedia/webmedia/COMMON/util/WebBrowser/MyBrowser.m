//
//  MyBrowser.m
//  Patrimoine
//
//  Created by soreha on 26/04/10.
//  
//

#import "MyBrowser.h"



@interface MyBrowser (PrivateMethods)
- (void) showEmailModalView:(NSString *)to subject:(NSString *)subject body:(NSString *)body;
@end

@implementation MyBrowser

@synthesize content;
@synthesize indicatorView;

@synthesize forward;
@synthesize back ;
@synthesize refresh ;
@synthesize stop ;


 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    DLog();
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    self.title = @"Lien externe";
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Dans Safari" style:UIBarButtonItemStyleBordered target:self action:@selector(openContentInBrowser)];
    self.contentSizeForViewInPopover = CGSizeMake(350, 410);
	
    
	//Bottom toolBar initialization
	forward = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"NavForward"] style:UIBarButtonItemStylePlain target:self.content action:@selector(goForward)];
	back = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"NavBack"] style:UIBarButtonItemStylePlain target:self.content action:@selector(goBack)];
	refresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self.content action:@selector(reload)];
	stop = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self.content action:@selector(stopLoading)];
	
	forward.enabled = NO;
	back.enabled = NO;
	stop.enabled = NO;
	refresh.enabled = YES;
	
	UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	[self setToolbarItems:[NSArray arrayWithObjects:back,space,forward,space,refresh,space,stop, nil]];
	
	
    
	if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        self.navigationController.navigationBar.translucent = self.navigationController.toolbar.translucent =NO;
        self.navigationController.toolbar.tintColor = self.navigationController.navigationBar.tintColor ;

        
    }else{
        self.navigationController.toolbar.tintColor =  [UIColor blueColor];
    }

	return self;
}






// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
   [super viewDidLoad];
}

-(void) openContentInBrowser{
	[[UIApplication sharedApplication] openURL:[[self.content request] URL]];
}

- (void)viewWillDisappear:(BOOL)animated {
	
	[self.content stopLoading];
	[self.navigationController setToolbarHidden:YES];
}

/*
- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	
 DLog();
	if(self.navigationController.toolbar.hidden){
		//show bars
		[self.navigationController setToolbarHidden:NO];
		[self.navigationController setNavigationBarHidden:NO];
		//[self.content setNeedsDisplay];
	}
	
}*/
- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	
		
    DLog();
	//if(self.navigationController.toolbar.hidden){
		//show bars
		[self.navigationController setToolbarHidden:NO];
		[self.navigationController setNavigationBarHidden:NO];
		//[self.content setNeedsDisplay];
	//}
    if(self.currentRequest){
        [self.content loadRequest:self.currentRequest];
    }
    
}



- (void)webViewDidFinishLoad:(UIWebView *)webView{
    DLog();
	if (webView.isLoading)
		return;
	
	[self.indicatorView stopAnimating];

	[forward setEnabled:[content canGoForward]];
	[back setEnabled:[content canGoBack] ];
	
	[stop setEnabled:NO];
	[refresh setEnabled:YES];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    DLog();
	[indicatorView startAnimating];
	[stop setEnabled:YES];
	[refresh setEnabled:NO];
	
}
 
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    DLog();
	[indicatorView stopAnimating];
	// report the error inside the webview
    //NSString* errorString = [NSString stringWithFormat:
    //                         @"<html><center><font size=+5 color='red'>An error occurred:<br>%@</font></center></html>",
    //                         error.localizedDescription];
	//[self.content loadHTMLString:errorString baseURL:nil];
	[stop setEnabled:NO];
	[refresh setEnabled:YES];
}


-(void) showURL:(NSURLRequest *)request{
	DLog();
    self.content.delegate = self;
    //if(self.content.loading){
	//	[self.content stopLoading];
	//}
    self.currentRequest = request;
	[self.content loadRequest:request];
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
   self.content = nil;
    DLog();
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    DLog();
	self.currentRequest = request;
    NSString *requestString = [[request URL] absoluteString];

    if ([[requestString lowercaseString] hasPrefix:@"itms-appss://"]
        || [[requestString lowercaseString] hasSuffix:@".zip"]
        || [[requestString lowercaseString] hasSuffix:@".rar"]
        || [[requestString lowercaseString] hasSuffix:@".odg"]) {
        
        [[UIApplication sharedApplication] openURL:[request URL]];
        
        return NO;
    }
    
	if( requestString  && [requestString hasPrefix:@"mailto:"]){
		
		NSArray *components = [requestString componentsSeparatedByString:@":"];
		
		[self showEmailModalView:[components objectAtIndex:1] subject:kEmailSubject body:kEmailBody];		
		return NO;
		
	}
	
	return YES;
}	
-(void) showEmailModalView:(NSString *)to subject:(NSString *)subject body:(NSString *)body {
	
	if (![MFMailComposeViewController canSendMail]) {
		return;
	}
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;// &lt;- very important step if you want feedbacks on what the user did with your email sheet
	
	[picker setSubject:subject];
	[picker setToRecipients:[NSArray arrayWithObjects:to,nil]];
	
	// Fill out the email body text
	
	
	[picker setMessageBody:body isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
	
	//picker.navigationBar.barStyle = UIBarStyleBlack; // choose your style, unfortunately, Translucent colors behave quirky.
	
	//[self presentModalViewController:picker animated:YES];
	[self presentViewController:picker animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
	// Notifies users about errors associated with the interface
	switch (result){
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
		default:{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Erreur lors de l'envoi du mail"
														   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
		}
			
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return YES;
}


- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
}


@end
