//
//  ChannelViewController.h
//  OptimGenericReader
//
//  Created by soreha on 15/02/11.
//  
//


#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AddChannelViewDelegate.h"
#import "MediaHandlerDelegate.h"

/*#import "CoreDataManager.h"
#import "AddChannelViewController.h"*/

@class AddChannelViewController, DocumentsForChannelViewController;


@interface ChannelsViewController : UITableViewController <NSFetchedResultsControllerDelegate, AddChannelViewDelegate> {
    NSFetchedResultsController *fetchedResultsController;
	AddChannelViewController *addChannelView;
	DocumentsForChannelViewController *documentsForChannelView;
    
	UIImage * defaultImage;
    
    IBOutlet id<MediaHandlerDelegate> __unsafe_unretained mediaHandlerDelegate;
}


@property (nonatomic, strong) UILabel *footerLabel;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) AddChannelViewController *addChannelView;
@property (nonatomic, strong) DocumentsForChannelViewController *documentsForChannelView;
@property (nonatomic, strong) UIImage * defaultImage;
@property (nonatomic) BOOL useChannelCellXib;


@property (nonatomic, unsafe_unretained) IBOutlet id<MediaHandlerDelegate> mediaHandlerDelegate;

@end
