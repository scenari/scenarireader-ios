//
//  Reference.h
//  webmedia
//
//  Created by soreha on 11/10/12.
//  
//

#import <Foundation/Foundation.h>

@interface Reference : NSObject

@property(nonatomic,strong) NSString *type;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *content;

@property(nonatomic,strong) id parent;


@end
