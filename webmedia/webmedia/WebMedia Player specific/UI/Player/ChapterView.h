//
//  ChapterView.h
//  WebMedia
//
//  Created by soreha on 02/12/10.
//  
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>


@interface ChapterView : UIView 

@property(nonatomic,strong) UILabel *backgroundLabel;
@property(nonatomic,strong) UILabel *progressLabel;
@property(nonatomic,strong) UILabel *textLabel;


@property(nonatomic) int index;

- (id)initWithFrame:(CGRect)frame andIndex:(int)anIndex;

-(void) adapteToFrame:(CGRect)frame showText:(Boolean)showText;

-(void) setProgression:(float) progress;

@end
