//
//  EmissionDefaultViewController_iPad.m
//  webmedia
//
//  Created by soreha on 20/12/12.
//  
//

#import "EmissionDefaultViewController_iPad.h"
#import "ChapterView.h"



@interface EmissionDefaultViewController_iPad ()

@end

@implementation EmissionDefaultViewController_iPad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setWebViewsContainer:nil];
    [self setLeftWebView:nil];
    [self setCenterWebView:nil];
    [self setRightWebView:nil];
    [super viewDidUnload];
}






@end
