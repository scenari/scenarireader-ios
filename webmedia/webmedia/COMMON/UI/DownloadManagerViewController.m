//
//  DownloadManagerViewController.m
//  OptimGenericReader
//
//  Created by soreha on 18/03/11.
//  
//

#import "DownloadManagerViewController.h"
#import "CoreDataManager.h"
#import "DownloadManagerTableViewController.h"
#import "ConfigurationManager.h"

@implementation DownloadManagerViewController
@synthesize tableViewController;
@synthesize dataManager;


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad{
    [super viewDidLoad];
    if(IS_IPAD()){
        self.title = @"Téléchargements";
    }
    [self.tableViewController update];
    self.tableViewController._navigationController = self.navigationController;

    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        [self navigationController].navigationBar.tintColor = self.toolBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
        self.toolBar.barStyle = UIBarStyleBlack;
        self.toolBar.translucent = NO;
    }
    else{
        self.navigationController.navigationBar.tintColor = [UIColor blueColor];
        
    }
    
}


- (void)viewDidUnload
{
    [self setTableViewController:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)awakeFromNib{
	
	self.title = @"Téléchargements";
    self.navigationController.tabBarItem.title = @"Téléchargements";
	[super awakeFromNib];
}


@end
