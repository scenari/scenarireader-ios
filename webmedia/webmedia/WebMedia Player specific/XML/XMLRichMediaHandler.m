//
//  XMLRichMediaHandler.m
//  PhoneRadio
//
//  Created by soreha on 26/07/10.
//  
//

#import "XMLRichMediaHandler.h"


@implementation XMLRichMediaHandler

@synthesize richMedia, currentContents;


@synthesize tmpSegment;
@synthesize tmpChapter;
@synthesize tmpResource;
@synthesize tmpImage;
@synthesize tmpGallery;
@synthesize tmpComplement;
@synthesize tmpReference;

@synthesize tmpExtra;
@synthesize tmpCredit;
@synthesize tmpIllustration;

@synthesize mediaParser;


float start ;



Boolean in_webRadio			= NO;
Boolean in_extras			= NO;
Boolean in_credits			= NO;
Boolean in_chapter			= NO;
Boolean in_segment			= NO;
Boolean in_complement		= NO;
Boolean in_illustration		= NO;
Boolean in_gallery			= NO;
Boolean in_notes            = NO;
Boolean in_description		= NO;
Boolean in_reference		= NO;


- (id) init{
	self = [super init];
	if (self != nil) {
		self.richMedia = [[RichMedia alloc] init];
		start = 0;
	}
	currentContents = nil;
	tmpSegment = nil;
	tmpChapter = nil;
	tmpResource = nil;
	tmpImage = nil;
	tmpGallery = nil;
	tmpComplement = nil;
	tmpExtra = nil;
	tmpCredit = nil;
	tmpIllustration = nil;
	start =0;
	
	return self;
}

#pragma mark xml
-(void) loadXMLFromURL:(NSURL *)richMediaURL{
    DLog(@"richMediaURL:%@", [richMediaURL description]);
	mediaParser = [[NSXMLParser alloc] initWithContentsOfURL:richMediaURL];
    NSString *content = [NSString stringWithContentsOfURL:richMediaURL encoding:NSUTF8StringEncoding error:nil];
    DLog(@"content : \n%@",[content description]);
    [mediaParser setDelegate:self];
	[mediaParser parse];	
}

//Document parsing start
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	//initialyze var here
	currentContents = [[NSMutableString alloc] initWithCapacity:0];
}

//Document parsing end
- (void)parserDidEndDocument:(NSXMLParser *)parser {
    DLog();

	//release tmp var here
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
	if(currentContents && string){
		[currentContents appendString:string];
	}
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName 
	attributes:(NSDictionary *)attributeDict {
		
	if([elementName compare:@"webRadio"] == NSOrderedSame){
		in_webRadio = YES;
		self.richMedia.title = [attributeDict objectForKey:@"title"];
		self.richMedia.author = [attributeDict objectForKey:@"author"];
		self.richMedia.durationInSeconds = [[attributeDict objectForKey:@"duration"] integerValue];
		self.richMedia.description = [attributeDict objectForKey:@"description"];
		self.richMedia.legal = [attributeDict objectForKey:@"legal"];
		self.richMedia.modificationDate = [attributeDict objectForKey:@"modificationDate"];
        
        /*specific webmedia 1*/
        self.richMedia.mediaUri = [attributeDict objectForKey:@"mediaUri"];
        if([attributeDict objectForKey:@"mediaUri"]){
            self.richMedia.webMediaVersion = WEB_MEDIA_1;
        }else{
            self.richMedia.webMediaVersion = WEB_MEDIA_2;
        }
        /*specific webMedia2*/
        if([attributeDict objectForKey:@"autoStop"]){
            
            self.richMedia.autoStop = [[attributeDict objectForKey:@"autoStop"] isEqualToString:@"true"];
        }else{
            self.richMedia.autoStop = NO;
        }
        
	}else if([elementName compare:@"reference"] == NSOrderedSame){
        in_reference = YES;
        tmpReference = [[Reference alloc] init];
        tmpReference.type = [attributeDict objectForKey:@"type"];
        tmpReference.parent = tmpSegment;
        
    }else if([elementName compare:@"credits"] == NSOrderedSame){
			in_credits = YES;
			tmpCredit = [[Credits alloc] init];
			tmpCredit.title = [attributeDict objectForKey:@"title"];
			tmpCredit.refUri = [attributeDict objectForKey:@"refUri"];
	}else if([elementName compare:@"extra"] == NSOrderedSame){
			in_extras = YES;
			tmpExtra = [[Extras alloc] init];
			tmpExtra.title = [attributeDict objectForKey:@"title"];
			tmpExtra.refUri = [attributeDict objectForKey:@"refUri"];
	}else if([elementName compare:@"chapter"] == NSOrderedSame){
			in_chapter = YES;
			tmpChapter = [[Chapter alloc] init];
			tmpChapter.chapterId = [attributeDict objectForKey:@"id"];
			tmpChapter.title = [attributeDict objectForKey:@"title"];
            tmpChapter.start = [[attributeDict objectForKey:@"start"] floatValue];//roundf([[attributeDict objectForKey:@"start"] floatValue]);
            tmpChapter.end = [[attributeDict objectForKey:@"end"] floatValue];//roundf([[attributeDict objectForKey:@"end"] floatValue]);
	}else if([elementName compare:@"segment"] == NSOrderedSame){
			in_segment = YES;
			tmpSegment = [[Segment alloc] init];
			tmpSegment.segmentId = [attributeDict objectForKey:@"id"];
			tmpSegment.title = [attributeDict objectForKey:@"title"];
			
        tmpSegment.start = [[attributeDict objectForKey:@"start"] floatValue];//roundf([[attributeDict objectForKey:@"start"] floatValue]);
        tmpSegment.end = [[attributeDict objectForKey:@"end"] floatValue];//roundf([[attributeDict objectForKey:@"end"] floatValue]);
            tmpSegment.endUnrounded = [[attributeDict objectForKey:@"end"] floatValue];
        
            //webmedia2
            if([attributeDict objectForKey:@"clipBegin"]){
                tmpSegment.clipBegin = [[attributeDict objectForKey:@"clipBegin"] floatValue];//roundf([[attributeDict objectForKey:@"clipBegin"] floatValue]);
            }else{
                tmpSegment.clipBegin = tmpSegment.start;
            }
            if([attributeDict objectForKey:@"clipEnd"]){
                tmpSegment.clipEnd = [[attributeDict objectForKey:@"clipEnd"] floatValue];//roundf([[attributeDict objectForKey:@"clipEnd"] floatValue]);
                tmpSegment.clipEndUnrounded = [[attributeDict objectForKey:@"clipEnd"] floatValue];
            }else{
                tmpSegment.clipEnd = tmpSegment.end;
                tmpSegment.clipEndUnrounded = tmpSegment.endUnrounded;
            }
            if([attributeDict objectForKey:@"mediaUri"]){      
                tmpSegment.mediaUri = [attributeDict objectForKey:@"mediaUri"] ;
            }else{
                tmpSegment.mediaUri = self.richMedia.mediaUri;
            }
            tmpSegment.mediaType = [attributeDict objectForKey:@"mediaType"]?[attributeDict objectForKey:@"mediaType"]:@"audio" ;
             
        
        
			if(!in_chapter){
				tmpChapter = [[Chapter alloc] init];
				tmpChapter.chapterId = tmpSegment.segmentId;
				tmpChapter.title = tmpSegment.title;
				tmpChapter.start = tmpSegment.start;
				tmpChapter.end = tmpSegment.end;
				tmpChapter.root = YES;
			}
                
            tmpSegment.parent = tmpChapter;
		
        } else if([elementName compare:@"description"] == NSOrderedSame){
			
		} else if ([elementName compare:@"illustration"] == NSOrderedSame) {
			in_illustration = YES;
			tmpIllustration = [[Illustration alloc] init];
			tmpIllustration.illustrationId = [attributeDict objectForKey:@"id"];
            tmpIllustration.start = [[attributeDict objectForKey:@"start"] floatValue];//roundf([[attributeDict objectForKey:@"start"] floatValue]);
			tmpIllustration.end = [[attributeDict objectForKey:@"end"] floatValue];//roundf([[attributeDict objectForKey:@"end"] floatValue]);
                        
		} else if ([elementName compare:@"image"] == NSOrderedSame) {
			tmpImage = [[Resource alloc] init];
			tmpImage.title = [attributeDict objectForKey:@"title"];
			tmpImage.refUri = [attributeDict objectForKey:@"refUri"];
		} else if ([elementName compare:@"video"] == NSOrderedSame) {
			tmpIllustration.type = TYPE_VIDEO;
			tmpIllustration.resource.title = [attributeDict objectForKey:@"title"];
			tmpIllustration.resource.refUri = [attributeDict objectForKey:@"refUri"];
		} else if ([elementName compare:@"animation"] == NSOrderedSame) {
			tmpIllustration.type = TYPE_ANIMATION;
			tmpIllustration.resource.title = [attributeDict objectForKey:@"title"];
			tmpIllustration.resource.refUri = [attributeDict objectForKey:@"refUri"];
		} else if ([elementName compare:@"text"] == NSOrderedSame) {
			tmpIllustration.type = TYPE_TEXT;
		} else if ([elementName compare:@"complement"] == NSOrderedSame) {
			in_complement = YES;
			tmpComplement = [[Complement alloc] init];
			tmpComplement.title = [attributeDict objectForKey:@"title"];
			tmpComplement.refUri = [attributeDict objectForKey:@"refUri"];
            tmpComplement.parent = tmpSegment;
            
		} else if ([elementName compare:@"gallery"] == NSOrderedSame) {
			in_gallery = YES;
			tmpGallery = [[ImageGallery alloc] init];
			tmpGallery.identifiant = [attributeDict objectForKey:@"id"];
		}
		else {
			[currentContents setString:@""];
		}
	 
}



- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qName {
	if([elementName compare:@"webRadio"] == NSOrderedSame){
		in_webRadio = NO;
	}else if([elementName compare:@"credits"] == NSOrderedSame){
		in_credits = NO;
		self.richMedia.credits = tmpCredit;
		
	}else if([elementName compare:@"notes"] == NSOrderedSame){
        in_notes = YES;
    }else if([elementName compare:@"description"] == NSOrderedSame){
        in_description = YES;
    }else if([elementName compare:@"extra"] == NSOrderedSame){
		in_extras = NO;
		[[self.richMedia extras] addObject:tmpExtra];
		
	}else if([elementName compare:@"chapter"] == NSOrderedSame){
		in_chapter = NO;
		[[self.richMedia chapters] addObject:tmpChapter];
	}else if([elementName compare:@"segment"] == NSOrderedSame){		
		in_segment = NO;
		[tmpChapter.segments addObject:tmpSegment];
		
		if(!in_chapter){
			[[self.richMedia chapters] addObject:tmpChapter];
		}    
		
	}else if([elementName compare:@"description"] == NSOrderedSame){
		tmpSegment.short_description = [NSString stringWithString:currentContents];
	}else if ([elementName compare:@"illustration"] == NSOrderedSame) {
		in_illustration = NO;
        tmpIllustration.parent = tmpSegment;
		[[tmpSegment illustrations] addObject:tmpIllustration];
		
	}else if ([elementName compare:@"image"] == NSOrderedSame) {
		
		if (in_illustration) {
			tmpIllustration.resource = tmpImage;
            tmpIllustration.type = TYPE_IMAGE;
		}else if(in_notes){
            
        }else if(in_description){
            
        }else if(in_gallery){
			[[tmpGallery images] addObject:tmpImage];
		}
	} else if ([elementName compare:@"text"] == NSOrderedSame) {
		tmpIllustration.text = [NSString stringWithString:currentContents];
	} else if ([elementName compare:@"complement"] == NSOrderedSame) {
		in_complement = NO;
		[[tmpSegment complements] addObject:tmpComplement];
	} else if ([elementName compare:@"gallery"] == NSOrderedSame) {
		in_gallery = NO;
		if (in_extras) {
			[[tmpExtra galleries] addObject:tmpGallery];
		} else if (in_credits){
			[[tmpCredit galleries] addObject:tmpGallery];
		} else {
			[[tmpComplement galleries] addObject:tmpGallery];
		}
	}else if([elementName compare:@"notes"] == NSOrderedSame){
        in_notes = NO;
    }else if([elementName compare:@"reference"] == NSOrderedSame){
        in_reference = NO;
        [[tmpSegment references] addObject:tmpReference];
        
    }else if([elementName compare:@"title"] == NSOrderedSame && in_reference){
        tmpReference.title = [NSString stringWithString:currentContents];
        
    }else if([elementName compare:@"desc"] == NSOrderedSame && in_reference){
        tmpReference.content = [NSString stringWithString:currentContents];
        
    }else if([elementName compare:@"description"] == NSOrderedSame){
        in_description = NO;
    }
}



@end
