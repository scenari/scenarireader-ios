//
//  DownlodedEmissionTableViewController.h
//  OptimGenericReader
//
//  Created by soreha on 16/02/11.
//  
//

#import <UIKit/UIKit.h>
#import "DefaultDocumentsTableViewController.h"

@class Channel, Document;

@interface DownlodedDocumentsTableViewController : DefaultDocumentsTableViewController <UIActionSheetDelegate>{

}

//properties
-(void) initWithChannel :(Channel *) ch; 
-(void) reloadFetchedResultControllerSortingBy : (int) criteria;

@property (nonatomic,strong) id contentDelegate;


@end
