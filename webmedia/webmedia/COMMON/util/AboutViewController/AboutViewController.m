//
//  AboutViewController.m
//  DavidElia
//
//  Created by soreha on 13/11/09.
//  
//

#import "AboutViewController.h"

#import "MyBrowser.h"
#import "ConfigurationManager.h"


@interface AboutViewController (PrivateMethods)
	- (void) showEmailModalView:(NSString *)to subject:(NSString *)subject body:(NSString *)body;
@end

@implementation AboutViewController

@synthesize content;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	[self.navigationController setToolbarHidden:YES animated:YES];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.content.delegate = self;
	self.title=@"Informations";
	NSString *path = [[NSBundle mainBundle] pathForResource:IS_IPAD()?@"about-iPad":@"about" ofType:@"html"];
	NSURLRequest *rq = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]];
	[content loadRequest:rq];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Fermer" style:UIBarButtonItemStylePlain target:self action:@selector(closeView)];
#pragma ACCESSIBILITY
    self.navigationItem.leftBarButtonItem.accessibilityLabel = @"Fermer la vue informations.";
    if([[ConfigurationManager sharedInstance] showTutorialFromCreditPage]){
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Aide" style:UIBarButtonItemStylePlain target:self action:@selector(showHelp)];
    }

    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        self.navigationController.navigationBar.tintColor =  [UIColor blueColor];
    }else{
        self.navigationController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
        self.navigationController.toolbar.tintColor = self.navigationController.navigationBar.tintColor;
    }
    
    //self.navigationController.navigationBar.translucent = NO;
	
}

- (void) closeView{
	[self.navigationController dismissModalViewControllerAnimated:YES];
}
- (void) showHelp{
    UIAlertView *showTutorial = [[UIAlertView alloc] initWithTitle:@"Question" message:@"Montrer un tutorial ?" delegate:nil cancelButtonTitle:@"Fermer" otherButtonTitles:nil];
    [showTutorial show];
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self.content reload];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
    self.content=nil;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

	NSString *requestString = [[request URL] absoluteString];
	DLog(@"requestString : %@",requestString);
	
	if ( requestString  && [requestString hasPrefix:@"http://"]){
				
        MyBrowser *myBrowser = [[MyBrowser alloc] initWithNibName:(IS_IPAD()?@"MyBrowser_iPad":@"MyBrowser_iPhone") bundle:nil];
		
		UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:nil action:nil];
		self.navigationItem.backBarButtonItem = backButton;
		[self.navigationController pushViewController:myBrowser animated:YES];
		[myBrowser showURL:request];
		
		
		return NO;
	}else if( requestString  && [requestString hasPrefix:@"mailto:"]){
		
		NSArray *components = [requestString componentsSeparatedByString:@":"];
		
		[self showEmailModalView:[components objectAtIndex:1] subject:kEmailSubject body:kEmailBody];		
		return NO;
		
	}
	
	//[(NSString *)[components objectAtIndex:0] isEqualToString:@"myapp"]) {
	
	return YES; // Return YES to make sure regular navigation works as expected.
}

-(void) showEmailModalView:(NSString *)to subject:(NSString *)subject body:(NSString *)body {
	if (![MFMailComposeViewController canSendMail]) {
		return;
	}
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;// &lt;- very important step if you want feedbacks on what the user did with your email sheet
	
	[picker setSubject:subject];
	[picker setToRecipients:[NSArray arrayWithObjects:to,nil]];
	
	// Fill out the email body text
	
	
	[picker setMessageBody:body isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
	
	picker.navigationBar.barStyle = self.navigationController.navigationBar.barStyle; // choose your style, unfortunately, Translucent colors behave quirky.
	picker.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
	//[self presentModalViewController:picker animated:YES];
    [self presentViewController:picker animated:YES completion:NULL];

}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
	// Notifies users about errors associated with the interface
	switch (result){
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
		default:{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Erreur lors de l'envoi du mail"
														   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
		}
			
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}




@end
