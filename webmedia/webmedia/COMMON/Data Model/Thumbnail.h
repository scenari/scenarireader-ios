//
//  Thumbnail.h
//  OptimGenericReader
//
//  Created by soreha on 14/02/11.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Thumbnail : NSManagedObject {
@private
}
@property (nonatomic, strong) id image;
@property (nonatomic, strong) NSString * url;
@property (nonatomic, strong) NSString * format;
@end
