//
//  DefaultEmissionsTableViewController.m
//  OptimGenericReader
//
//  Created by soreha on 17/02/11.
//  
//

#import "DefaultDocumentsTableViewController.h"
#import "Document.h"
#import "Thumbnail.h"
#import "CoreDataManager.h"
#import "RequestManager.h"
#import "DocumentDetailViewController.h"
#import "DocumentsForChannelViewController.h"
#import "ConfigurationManager.h"
#import "MediaHandlerManager.h"
#import "RequestManager.h"
#import "ASINetworkQueue.h"



@implementation DefaultDocumentsTableViewController

@synthesize documentsForChannelViewController;
@synthesize fetchedResultsController;
@synthesize defaultImage;
@synthesize myCell;
@synthesize isDownloadedDocumentsView;
@synthesize channel;
@synthesize _navigationController;

-(void) releaseOutlets{
	self.documentsForChannelViewController = nil;
	self.myCell =  nil;
}

#pragma mark -
#pragma mark Initialization

/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
	[super viewDidLoad];
   
	self.defaultImage = [UIImage imageNamed:@"blank"];
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        [self navigationController].navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
    }else{
        self.navigationController.navigationBar.tintColor = [UIColor blueColor];
    }
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [fetchedResultsController.sections count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSString *unnamedLabel = [[ConfigurationManager sharedInstance] getNoNamedCategoryHeaderLabel];
    return [[sectionInfo name] isEqualToString:@"Aucune"]?(unnamedLabel?unnamedLabel:@"Aucune"):[sectionInfo name];
}


//Custom title view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{	
	// create the parent view that will hold header Label
	UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 24)];
	customView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	customView.backgroundColor = [[ConfigurationManager sharedInstance] tableViewSectionBackgroundColor];
	
	// create the button object
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
	headerLabel.textColor = [[ConfigurationManager sharedInstance] tableViewSectionForegroundColor];
	headerLabel.highlightedTextColor = [UIColor whiteColor];
	headerLabel.font = [UIFont boldSystemFontOfSize:14];
	headerLabel.frame = CGRectMake(10.0, 0.0, 310.0, 24);
	headerLabel.adjustsFontSizeToFitWidth = YES;
	headerLabel.minimumFontSize = 12.0;
	
	headerLabel.text = [self tableView:tableView titleForHeaderInSection:section];
		
	[customView addSubview:headerLabel];
	
	return customView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return 24.0;
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *MyIdentifier = @"DefaultCell";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
		if (self.isDownloadedDocumentsView) {
			[[NSBundle mainBundle] loadNibNamed:@"DownloadedDocumentTableViewCell" owner:self options:nil];
		} else {
			[[NSBundle mainBundle] loadNibNamed:@"AvailableDocumentTableViewCell" owner:self options:nil];
		}
        cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
        cell = myCell;
        self.myCell = nil;
    }
	
	// Configure the cell...
	[self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Document  *document = [self.fetchedResultsController objectAtIndexPath:indexPath];
   	
    NSMutableString *accessibilityText = [NSMutableString stringWithString:@""];
    
    UILabel *type = (UILabel *)[cell viewWithTag:10];
    if([[document.pubType lowercaseString] isEqualToString:@"opale"]){
        type.text = @"opale";
#pragma ACCESSIBILITY
        [accessibilityText appendString:@"Document Opale : "];
    }else if([[document.pubType lowercaseString] isEqualToString:@"topaze"]){
        type.text = @"topaze";
#pragma ACCESSIBILITY
        [accessibilityText appendString:@"Document Topaze : "];
    }else if([[document.pubType lowercaseString] isEqualToString:@"optim"]){
        type.text = @"optim";
#pragma ACCESSIBILITY
       [accessibilityText appendString:@"Document Optim : "];
    }else{
        type.text = @"webmedia";
#pragma ACCESSIBILITY
       [accessibilityText appendString:@"Document WebMedia : "];
    }
    
    UILabel *label;
    
    label = (UILabel *)[cell viewWithTag:1];
	label.text = [NSString stringWithFormat:@"%@", document.title];
#pragma ACCESSIBILITY
	[accessibilityText appendFormat:@"%@.",label.text];
	
	UIImageView *im = (UIImageView *)[cell viewWithTag:4];
	if (document.thumbnail_small == nil) {
		im.image = defaultImage;
        
        if (document.thumbnail_small_URI != nil || [document.thumbnail_small_URI compare:@""] != NSOrderedSame) {
            [[CoreDataManager sharedInstance] downloadThumbnailForDocument:document];
        }
	} else {
		im.image = document.thumbnail_small.image;
	}
	
	if (self.isDownloadedDocumentsView == NO) {
		im = (UIImageView *)[cell viewWithTag:5];
        if ([document.isSelected intValue] == 0) {
			im.image = [UIImage imageNamed:@"NotSelected"];
#pragma ACCESSIBILITY
            if(self.isDownloadedDocumentsView)[accessibilityText appendString:@"Non sélectionné."];
        } else {
            im.image = [UIImage imageNamed:@"IsSelected"];
#pragma ACCESSIBILITY
            if(self.isDownloadedDocumentsView)[accessibilityText appendString:@"Sélectionné."];
        }
        
	}
	
	label = (UILabel *)[cell viewWithTag:2];
	label.textColor = [UIColor grayColor];
	
	label.text = document.author;
#pragma ACCESSIBILITY
    [accessibilityText appendFormat:@"L'auteur du document est : %@.",label.text];
	
	label = (UILabel *)[cell viewWithTag:3];
	
    if ([document.downloading intValue] == 1) {
		if ([document.downloadProgress floatValue] > 0 ) {
			label.text = [NSString stringWithFormat:@"Téléchargement : %d %%",(int)([document.downloadProgress floatValue]*100)];
               label.textColor = [[ConfigurationManager sharedInstance] downloadProgressInfoColor];
		} else if ([document.downloadSuspended intValue]== 0){
			label.text = @"Téléchargement en attente";
            label.textColor = [[ConfigurationManager sharedInstance] downloadProgressInfoColor];
		} else {
            label.text = @"Téléchargement suspendu";
            label.textColor = [[ConfigurationManager sharedInstance] downloadErrorInfoColor];
        }
        
    } else if ([document.updateAvailable intValue] == 1) {
		label.text = @"Mise à jour disponible";
		label.textColor = [[ConfigurationManager sharedInstance] downloadErrorInfoColor];
	}  else {
        if([document.downloaded boolValue]){
            label.text = @"Téléchargé";
            label.textColor = [UIColor colorWithRed:0.000 green:0.503 blue:0.000 alpha:1.000];
        }else if([document.link hasSuffix:@".html"]){
            DLog(@"document.link:[%@]",document.link);
            label.text = @"Version en ligne ou à télécharger";
            label.textColor = [UIColor colorWithRed:0.000 green:0.000 blue:0.503 alpha:1.000];
        }else{
            label.text = @"À télécharger";
            label.textColor = [UIColor colorWithRed:0.503 green:0.000 blue:0.000 alpha:1.000];
        }
    }
    
#pragma ACCESSIBILITY
	[accessibilityText appendFormat:@"%@.",label.text];
	
    UIImageView *imFav = (UIImageView *)[cell viewWithTag:20];
	[imFav setHidden:![document.isFavorite boolValue]];
	
    if([document.isFavorite boolValue]){
#pragma ACCESSIBILITY
        [accessibilityText appendString:@"Ce document fait partie des documents favoris."];
    }else{
#pragma ACCESSIBILITY
        [accessibilityText appendString:@"Ce document ne fait pas partie des documents favoris."];
    }
    
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        [cell setAccessoryView:[self makeDetailDisclosureButtonWithFrame:CGRectMake(0, 0, 30, 30) andImage:[[ConfigurationManager sharedInstance] getAccessoryImage]]];
    }else{
        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }
    
    
    cell.accessibilityLabel = [NSString stringWithString:accessibilityText];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
		return 64;
}


- (NSString*) number2digit:(int) number{
	if (number<10) {
		return [NSString stringWithFormat:@"0%d",number];
	}
	return [NSString stringWithFormat:@"%d",number];
}

- (UIButton *) makeDetailDisclosureButtonWithFrame:(CGRect)rect andImage:(UIImage *)img{
	UIButton *button = [[UIButton alloc] initWithFrame:rect];
	[button setImage:img forState:UIControlStateNormal];
	
	
	[button addTarget: self
			   action: @selector(accessoryButtonTapped:withEvent:)
	 forControlEvents: UIControlEventTouchUpInside];
	
	return ( button );
}

- (void) accessoryButtonTapped: (UIControl *) button withEvent: (UIEvent *) event	{
	NSIndexPath * indexPath = [self.tableView indexPathForRowAtPoint: [[[event touchesForView: button] anyObject] locationInView: self.tableView]];
	if ( indexPath == nil )
		return;
		
	if([self.tableView.delegate respondsToSelector:@selector(tableView:accessoryButtonTappedForRowWithIndexPath:)])
		[self.tableView.delegate tableView:self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
	// Navigation logic may go here. Create and push another view controller.
	
	DocumentDetailViewController *detailViewController = [[DocumentDetailViewController alloc] initWithNibName:@"DefaultDetailViewController" bundle:nil];
	
	Document * document = [fetchedResultsController objectAtIndexPath:indexPath];
	
	[detailViewController setItems:[fetchedResultsController fetchedObjects] withCurrent:[[fetchedResultsController fetchedObjects] indexOfObject:document]];
	
    
    [self._navigationController pushViewController:detailViewController animated:YES];
            
}

-(void) downloadDocument:(Document *)document{
    NSString *tmpDirectory = [[[CoreDataManager sharedInstance] requestManager] getTmpDownloadFolder];
    
    if(document.tmpDownloadFilePath && ![document.tmpDownloadFilePath hasPrefix:tmpDirectory]){
        //Cancel previous tmp download file
        DLog(@"New app version number -> previous tmp file canceled document:%@",document.title);
        document.tmpDownloadFilePath = nil;
    }
    if ([CoreDataManager sharedInstance].requestManager.documentDownloadQueue == nil) {
        [[CoreDataManager sharedInstance].requestManager initAsiQueue];
    }
    [[CoreDataManager sharedInstance] downloadDocument:document];
}


-(void)suspendDocumentDownload:(Document*)document{
    [document.request clearDelegatesAndCancel];
    document.downloadProgress = 0;
    document.downloadSuspended = [NSNumber numberWithBool:YES];
    document.isSelected = [NSNumber numberWithBool:NO];
    document.request = nil;
    [[CoreDataManager sharedInstance] saveData];
}

- (void)resumeDocumentDownload:(Document *)document{
    if ([CoreDataManager sharedInstance].requestManager.documentDownloadQueue == nil) {
        [[CoreDataManager sharedInstance].requestManager initAsiQueue];
    }
    
    
    NSString *tmpDirectory = [[[CoreDataManager sharedInstance] requestManager] getTmpDownloadFolder];
    if(document.tmpDownloadFilePath && ![document.tmpDownloadFilePath hasPrefix:tmpDirectory]){
        //Cancel previous version app tmp download file (another folder)
        ALog(@"new app version number -> previous tmp file canceled document:%@",document.title);
        document.tmpDownloadFilePath = nil;
    }
    document.isSelected = [NSNumber numberWithBool:NO];
    if (document.request == nil) {
        [[CoreDataManager sharedInstance].requestManager addDocumentRequest:document];
    }
    
    [[CoreDataManager sharedInstance].requestManager.documentDownloadQueue go];
    [[CoreDataManager sharedInstance] saveData];
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.lastSelectedPath = indexPath;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

	Document  *document = [self.fetchedResultsController objectAtIndexPath:indexPath];
	
    /*
    if ([document.updateAvailable boolValue] && ![document.downloading boolValue] ){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Mise à jour" message:@"Une mise à jour est disponible, souhaitez-vous la télécharger maintenant" delegate:self cancelButtonTitle:@"Oui" otherButtonTitles:@"Non", nil];
        [alert show];
	} else if ([document.downloading boolValue]){
        UIAlertView *alert;
		if (![document.downloadSuspended boolValue]) {
            alert = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Document en cours de téléchargement, merci de patienter" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        }else{
            alert = [[UIAlertView alloc] initWithTitle:@"Téléchargement" message:@"Téléchargement suspendu, souhaitez-vous le reprendre" delegate:self cancelButtonTitle:@"Oui" otherButtonTitles:@"Non", nil];
        }
        
		[alert show];
	} else if (![document.downloaded boolValue]  ){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Téléchargement" message:@"Le document n'a pas été téléchargé, souhaitez-vous le télécharger maintenant ?" delegate:self cancelButtonTitle:@"Oui" otherButtonTitles:@"Non", nil];
        [alert show];
	} else  {
        [self openMedia:document];
    }
     */
    /*
     plusieurs cas possibbles
     1 - Document téléchargé : on l'ouvre
     2 - Document non téléchargé et indisponible online : on le télécharge
     3 - Document non téléchargé et disponible online : choix télécharger ou ouvrir online
     */
    
    
    
    if ([document.downloading intValue] == 1) {
		if ([document.downloadProgress floatValue] > 0 ) {
			//Téléchargement en cours->suspendre
            [self suspendDocumentDownload:document];
            return;
        } else if ([document.downloadSuspended intValue]== 0){
			//Téléchargement en attente->suspendre
            [self suspendDocumentDownload:document];
            
		} else {
            //Téléchargement suspendu"->reprendre
            [self resumeDocumentDownload:document];
        }
    } else if ([document.updateAvailable intValue] == 1) {
		//Mise à jour disponible
        UIActionSheet *actionSheet ;
        if(document.lastOpenedPage && ![self isTopazeDocument:document]){
            actionSheet = [[UIActionSheet alloc] initWithTitle:@"Mise à jour disponible" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Télécharger", @"Ignorer, ouvrir",@"Ignorer, reprendre la lecture", nil];
        }else{
            actionSheet = [[UIActionSheet alloc] initWithTitle:@"Mise à jour disponible" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Télécharger", @"Ignorer et ouvrir", nil];
        }
        actionSheet.actionSheetStyle = UIBarStyleBlackOpaque;
        [actionSheet showInView:[[[UIApplication sharedApplication] delegate] window]];
        
        
	} else {
        if([document.downloaded boolValue]){
            //Téléchargé->ouverture en local
            UIActionSheet *actionSheet ;
            if(document.lastOpenedPage && ![self isTopazeDocument:document]){
                actionSheet = [[UIActionSheet alloc] initWithTitle:@"Ouvrir" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Ouvrir",@"Reprendre la lecture", nil];
                actionSheet.actionSheetStyle = UIBarStyleBlackOpaque;
                [actionSheet showInView:[[[UIApplication sharedApplication] delegate] window]];
            }
            else{
                [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document];
                
            }
            
            DLog(@"document : %@",document);
            
        }else if([document.link hasSuffix:@".html"]){
            DLog(@"document.link=%@",document.link);
            DLog(@"document.lastOpenedPage=%@",document.lastOpenedPage);
            
            
            //Version en ligne ou à télécharger
            UIActionSheet *actionSheet ;
            if(document.lastOpenedPage && ![document.lastOpenedPage isEqualToString:document.link] && ![self isTopazeDocument:document]){
                actionSheet = [[UIActionSheet alloc] initWithTitle:@"Options" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Ouvrir en ligne",@"Reprendre en ligne",@"Télécharger", nil];
            }
            else{
                actionSheet = [[UIActionSheet alloc] initWithTitle:@"Options" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Ouvrir en ligne",@"Télécharger", nil];
            }
            actionSheet.actionSheetStyle = UIBarStyleBlackOpaque;
          
                [actionSheet showInView:[[[UIApplication sharedApplication] delegate] window]];
            
        }else{
            [self downloadDocument:document];
        }
	}

}
- (void) openSortActionSheet {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Méthode de tri" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Ordre initial",@"Trier par date", @"Trier par titre", nil];
    [actionSheet showInView:[[[UIApplication sharedApplication] delegate] window]];
}

- (void)actionSheetCancel:(UIActionSheet *)actionSheet {
    //if([actionSheet.title isEqualToString:@"Méthode de tri"]){
    [actionSheet dismissWithClickedButtonIndex:(actionSheet.numberOfButtons-1) animated:YES];
    //}
}
/*
- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	
    if ([actionSheet.title compare:@"Mise à jour"] == NSOrderedSame || [actionSheet.title compare:@"Téléchargement"] == NSOrderedSame) {
		Document  *document = [self.fetchedResultsController objectAtIndexPath:self.lastSelectedPath];
        
        if (buttonIndex == 0){
            NSString *tmpDirectory = [[[CoreDataManager sharedInstance] requestManager] getTmpDownloadFolder];
            
            if(document.tmpDownloadFilePath && ![document.tmpDownloadFilePath hasPrefix:tmpDirectory]){
                //Cancel previous tmp download file
                DLog(@"New app version number -> previous tmp file canceled document:%@",document.title);
                document.tmpDownloadFilePath = nil;
            }
            [[CoreDataManager sharedInstance] downloadDocument:document];
		} else if ([actionSheet.title compare:@"Mise à jour"] == NSOrderedSame) {
            [self openMedia:document];
        }
	} 
 }
     */
- (BOOL)isTopazeDocument:(Document *)doc{
    return [[doc.pubType lowercaseString] isEqualToString:@"topaze"];
}

    - (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
       
        //DLog(@"Button index : %d",buttonIndex);
        
        if(actionSheet.cancelButtonIndex == buttonIndex)return;
        
        if([actionSheet.title isEqualToString:@"Méthode de tri"]){
            [self sort:buttonIndex];
        }else if([actionSheet.title isEqualToString:@"Options"]){
            Document  *document = [self.fetchedResultsController objectAtIndexPath:self.lastSelectedPath];
            if(buttonIndex==0){//Ouvrir en ligne
                [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openOnlineDocument:document onLastOpenedPage:NO];
            }else if((document.lastOpenedPage) && buttonIndex==1 && ![self isTopazeDocument:document]){
                [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openOnlineDocument:document  onLastOpenedPage:YES];
            }else{// if(buttonIndex==actionSheet.numberOfButtons-2){//Télécharger
                [self downloadDocument:document];
            }
        }else if([actionSheet.title isEqualToString:@"Ouvrir"]){
            Document  *document = [self.fetchedResultsController objectAtIndexPath:self.lastSelectedPath];
            if(buttonIndex==0){//Ouvrir au début
                [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document onLastOpenedPage:NO];
            }else if(buttonIndex==1){//reprendre
                [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document  onLastOpenedPage:YES];
            }
        }else if([actionSheet.title isEqualToString:@"Mise à jour disponible"]){
            Document  *document = [self.fetchedResultsController objectAtIndexPath:self.lastSelectedPath];
            if(buttonIndex==0){//Télécharger
                [self downloadDocument:document];
            }else if(buttonIndex==1){//ouvrir au début
                [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document  onLastOpenedPage:NO];
            }else if(buttonIndex==2 && actionSheet.numberOfButtons==4){//reprendre
                [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document  onLastOpenedPage:YES];
            }
            
        }
    }


- (void) openMedia:(Document *) document {
   
    DLog(@"openMedia : mediaHandlerDelegate%@",[[MediaHandlerManager sharedInstance].mediaHandlerDelegate description]);
    [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document];
}




/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    Document  *document = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    return [document.downloaded boolValue];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
    
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		// Delete the row from the data source.
		//[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
		Document  *document = [self.fetchedResultsController objectAtIndexPath:indexPath];
		[[CoreDataManager sharedInstance] removeDownloadedDocument:document];
	}
}

/*
 // Override to support editing the table view.
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source.
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


- (void)dealloc{
    self.fetchedResultsController.delegate = nil;
    self.fetchedResultsController = nil;
}

#pragma mark - Fetched results controller delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller{
	[self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type{
	switch(type){
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath{

	UITableView *tableView = self.tableView;
    	
    switch(type){
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
	[self.tableView endUpdates];
    [self contentUpdated];
    
}
- (void)contentUpdated{
}



// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed. 

/*- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
 {
 // In the simplest, most efficient, case, reload the table view.
 [self.tableView reloadData];
 }*/



#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}



@end


