//
//  main.m
//  webmedia
//
//  Created by Emmanuel Beaufort on 01/06/12.
//  Copyright (c) 2012 Soreha. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WebMediaAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WebMediaAppDelegate class]));
    }
}
