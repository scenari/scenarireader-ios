//
//  DefaultDetailViewController.m
//  OptimGenericReader
//
//  Created by soreha on 24/02/11.
//  
//

#import "DefaultDetailViewController.h"
#import "MyBrowser.h"
#import "ConfigurationManager.h"

#define kCustomButtonHeight		30.0


@interface DefaultDetailViewController (PrivateMethods)
-(void) segmentAction;
@end

@implementation DefaultDetailViewController


@synthesize actionButton;
@synthesize content;
@synthesize itemsList;
@synthesize currentItem;



// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	self.title = @"Informations";
	/*
    for (id subview in content.subviews){
		if ([[subview class] isSubclassOfClass: [UIScrollView class]])
			((UIScrollView *)subview).bounces = NO;
	}
	*/
    
    // "Segmented" control to the right
    
    UIImage *down = [UIImage imageNamed:@"down"];
    UIImage *up = [UIImage imageNamed:@"up"];
#pragma ACCESSIBILITY
    down.accessibilityLabel = @"Afficher les informations de l'élément suivant.";
#pragma ACCESSIBILITY
    up.accessibilityLabel = @"Afficher les informations de l'élément précédent.";
	UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:
											[NSArray arrayWithObjects:
											 up,
											 down,
											 nil]];
	

    
    [segmentedControl addTarget:self action:@selector(segmentAction) forControlEvents:UIControlEventValueChanged];
	segmentedControl.frame = CGRectMake(0, 0, 90, kCustomButtonHeight);
	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
	segmentedControl.momentary = YES;
	
	defaultTintColor = segmentedControl.tintColor;	// keep track of this for later
	
	UIBarButtonItem *segmentBarItem = [[UIBarButtonItem alloc] initWithCustomView:segmentedControl];
    
	self.navigationItem.rightBarButtonItem = segmentBarItem;
    
    content.delegate = self;
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (NSString*) number2digit:(int) number{
	if (number<10) {
		return [NSString stringWithFormat:@"0%d",number];
	}
	return [NSString stringWithFormat:@"%d",number];
}

-(IBAction) deleteDocument{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"TODO" message:@"Confirmation de la suppression + suppression locale" 
												   delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
}

-(void) segmentAction{
	UISegmentedControl *segmentedControl = (UISegmentedControl *)self.navigationItem.rightBarButtonItem.customView;
	if(segmentedControl.selectedSegmentIndex == 0){
		if(self.currentItem>0) self.currentItem--;
		[segmentedControl setEnabled:(self.currentItem!=0) forSegmentAtIndex:0];
	}
	else {
		if(self.currentItem<([itemsList count]-1)) self.currentItem++;
		[segmentedControl setEnabled:(self.currentItem<([itemsList count]-1)) forSegmentAtIndex:1];
	}
	[self viewWillAppear:NO];
}


-(void) setItems:(NSArray *) items withCurrent:(int) current{
	self.currentItem = current;
	self.itemsList = [NSArray arrayWithArray:items];
    
    DLog(@"Nb elts in self.itemsList = %d",[self.itemsList count]);
	
	UISegmentedControl *segmentedControl = (UISegmentedControl *)self.navigationItem.rightBarButtonItem.customView;
	[segmentedControl setImage:[UIImage imageNamed:((current!=0)?@"up":@"noAction")] forSegmentAtIndex:0];
	[segmentedControl setImage:[UIImage imageNamed:@"noAction"] forSegmentAtIndex:1];
	segmentedControl.tintColor = self.navigationController.navigationBar.tintColor;
	
}


static const char _base64EncodingTable[64] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const short _base64DecodingTable[256] = {
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -1, -1, -2, -1, -1, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -1, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, 62, -2, -2, -2, 63,
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -2, -2, -2, -2, -2, -2,
    -2,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -2, -2, -2, -2, -2,
    -2, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2
};

- (NSString *)encodeBase64WithData:(NSData *)objData {
    const unsigned char * objRawData = [objData bytes];
    char * objPointer;
    char * strResult;
	
    // Get the Raw Data length and ensure we actually have data
    int intLength = [objData length];
    if (intLength == 0) return nil;
	
    // Setup the String-based Result placeholder and pointer within that placeholder
    strResult = (char *)calloc(((intLength + 2) / 3) * 4, sizeof(char));
    objPointer = strResult;
	
    // Iterate through everything
    while (intLength > 2) { // keep going until we have less than 24 bits
        *objPointer++ = _base64EncodingTable[objRawData[0] >> 2];
        *objPointer++ = _base64EncodingTable[((objRawData[0] & 0x03) << 4) + (objRawData[1] >> 4)];
        *objPointer++ = _base64EncodingTable[((objRawData[1] & 0x0f) << 2) + (objRawData[2] >> 6)];
        *objPointer++ = _base64EncodingTable[objRawData[2] & 0x3f];
		
        // we just handled 3 octets (24 bits) of data
        objRawData += 3;
        intLength -= 3; 
    }
	
    // now deal with the tail end of things
    if (intLength != 0) {
        *objPointer++ = _base64EncodingTable[objRawData[0] >> 2];
        if (intLength > 1) {
            *objPointer++ = _base64EncodingTable[((objRawData[0] & 0x03) << 4) + (objRawData[1] >> 4)];
            *objPointer++ = _base64EncodingTable[(objRawData[1] & 0x0f) << 2];
            *objPointer++ = '=';
        } else {
            *objPointer++ = _base64EncodingTable[(objRawData[0] & 0x03) << 4];
            *objPointer++ = '=';
            *objPointer++ = '=';
        }
    }
	
    // Terminate the string-based result
    *objPointer = '\0';
	
    // Return the results as an NSString object
    return [NSString stringWithCString:strResult encoding:NSASCIIStringEncoding];
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{ 
	// Notifies users about errors associated with the interface
	switch (result){
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
		default:{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Erreur lors de l'envoi du mail"
														   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
		}
			
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}
-(void) showEmailModalView:(NSString *)to subject:(NSString *)subject body:(NSString *)body {
	if (![MFMailComposeViewController canSendMail]) {
		return;
	}
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;// &lt;- very important step if you want feedbacks on what the user did with your email sheet
	
	[picker setSubject:subject];
	[picker setToRecipients:[NSArray arrayWithObjects:to,nil]];
	
	// Fill out the email body text
	
	
	[picker setMessageBody:body isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
	
	picker.navigationBar.barStyle = self.navigationController.navigationBar.barStyle; // choose your style, unfortunately, Translucent colors behave quirky.
	picker.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
	//[self presentModalViewController:picker animated:YES];
    [self presentViewController:picker animated:YES completion:NULL];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	NSString *requestString = [[request URL] absoluteString];
	
	
	if ( requestString  && [requestString hasPrefix:@"http://"]){
        
        MyBrowser *myBrowser = [[MyBrowser alloc] initWithNibName:(IS_IPAD()?@"MyBrowser_iPad":@"MyBrowser_iPhone") bundle:nil];
		
		UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:nil action:nil];
		self.navigationItem.backBarButtonItem = backButton;
		
        [self.navigationController pushViewController:myBrowser animated:YES];
		[myBrowser showURL:request];
		
		
		return NO;
	}else if( requestString  && [requestString hasPrefix:@"mailto:"]){
		
		NSArray *components = [requestString componentsSeparatedByString:@":"];
		
		[self showEmailModalView:[components objectAtIndex:1] subject:kEmailSubject body:kEmailBody];		
		return NO;
		
	}
	
	//[(NSString *)[components objectAtIndex:0] isEqualToString:@"myapp"]) {
	
	return YES; // Return YES to make sure regular navigation works as expected.
}





 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
 }


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end
