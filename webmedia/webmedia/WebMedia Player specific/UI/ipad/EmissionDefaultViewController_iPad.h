//
//  EmissionDefaultViewController_iPad.h
//  webmedia
//
//  Created by soreha on 20/12/12.
//  
//

#import <UIKit/UIKit.h>
#import "EmissionDefaultViewController.h"

@interface EmissionDefaultViewController_iPad : EmissionDefaultViewController

@property (weak, nonatomic) IBOutlet UIView *webViewsContainer;
@property (weak, nonatomic) IBOutlet UIWebView *leftWebView;
@property (weak, nonatomic) IBOutlet UIWebView *centerWebView;
@property (weak, nonatomic) IBOutlet UIWebView *rightWebView;


@end
