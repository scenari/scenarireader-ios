//
//  SegmentCellView.m
//  PhoneRadio
//
//  Created by soreha on 19/07/10.
//  
//

#import "SegmentCellView.h"

#define MARGIN 10

@implementation SegmentCellView

@synthesize readLabel;
@synthesize complementIndicator;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
		self.readLabel = [[UILabel alloc] init];
		self.readLabel.opaque = YES;
		[self addSubview:readLabel];
		//self.readLabel.backgroundColor=[UIColor greenColor];
		complementIndicator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hasComplement"]];
		complementIndicator.hidden = NO;
		[self addSubview:complementIndicator];
		
		self.imageView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.1];		
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) layoutSubviews {
    DLog();
    
    [super layoutSubviews];
    CGRect cvf = self.contentView.frame;
	self.readLabel.frame = CGRectMake(2.0,
                                      1.0,
                                      5,
                                      cvf.size.height-2);
    self.imageView.frame = CGRectMake(12.0,
                                      2.0,
                                      cvf.size.height-4,
                                      cvf.size.height-4);
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
	
    
    if(self.imageView.image !=nil){
        self.imageView.hidden = NO;
        CGRect frame = CGRectMake(cvf.size.height + MARGIN,
                              self.textLabel.frame.origin.y,
                              cvf.size.width - cvf.size.height - 2*MARGIN,
                              self.textLabel.frame.size.height);
        self.textLabel.frame = frame;
	
        frame = CGRectMake(cvf.size.height + MARGIN,
                       self.detailTextLabel.frame.origin.y,
                       cvf.size.width - cvf.size.height - 2*MARGIN,
                       self.detailTextLabel.frame.size.height);   
        self.detailTextLabel.frame = frame;
	
	
        frame = CGRectMake(cvf.size.width+7,
                       cvf.size.height-12-MARGIN,
                       12,
                       12);
    	complementIndicator.frame = frame;

    }else{
        self.imageView.hidden = YES;
        CGRect frame = CGRectMake(10,
                                  5,
                                  cvf.size.width - 20 - 2*MARGIN,
                                  40);
        self.textLabel.frame = frame;
        
        frame = CGRectMake(10,
                           50,
                           cvf.size.width - 20 - 2*MARGIN,
                           self.detailTextLabel.frame.size.height);
        self.detailTextLabel.frame = frame;
        
        
        frame = CGRectMake(cvf.size.width+7,
                           cvf.size.height-12-MARGIN,
                           12,
                           12);
    	complementIndicator.frame = frame;
    }
    
	//self.textLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin;
}




@end
