//
//  RichMedia.h
//  PhoneRadio
//
//  Created by soreha on 26/07/10.
//  
//

#import <Foundation/Foundation.h>
#import "Credits.h"
#import "Extras.h"


#define WEB_MEDIA_1 0
#define WEB_MEDIA_2 1

@class Segment, Chapter, Illustration;

@interface RichMedia : NSObject 



@property(nonatomic) int webMediaVersion;

@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *author;

@property(nonatomic, strong) NSString *mediaUri;

@property(nonatomic, strong) NSString *legal;
@property(nonatomic, strong) NSString *version;
@property(nonatomic, strong) NSString *description;
@property(nonatomic, strong) NSString *modificationDate;

@property(nonatomic, strong) NSMutableArray *chapters;
@property(nonatomic, strong) Credits *credits;
@property(nonatomic, strong) NSMutableArray *extras;

@property(nonatomic) int currentSegment;
@property(nonatomic) int currentChapter;
@property(nonatomic) int currentSegmentInChapter;
@property(nonatomic) int durationInSeconds;


@property(nonatomic, strong) NSString *base;


@property(nonatomic, strong) NSArray *allComplements;
@property(nonatomic, strong) NSArray *allReferences;
@property(nonatomic, strong) NSArray *allIllustrations;



@property (nonatomic) BOOL autoStop;

+(NSString *) getIllustrationFullPath:(Illustration *)illustration forMedia:(RichMedia *)media;
+(NSString *) getIllustrationFullURLString:(Illustration *)illustration forMedia:(RichMedia *)media;

-(void) computeCurrentSegment;
-(BOOL) isTimeInCurrentSegment:(float)time;

-(Segment*) getSegmentAtTime:(float)time;
-(Segment*) getCurrentSegmentObject;

-(Chapter*) getCurrentChapterObject;


-(int) getChapterIndexAtTime:(float)time;
-(int) getSegmentIndexInChapter:(int)chapterIndex atTime:(float)time;

-(void) printData;
-(void) updateIndexWithTime:(float)time;
-(void) resetIndexes;

-(Segment*) nextSegment;

- (BOOL)isWebMediaVersion1;
- (BOOL)isWebMediaVersion2;

- (BOOL)isOneSegOneChapMedia;
- (BOOL)hasIllustrationImage;


@end
