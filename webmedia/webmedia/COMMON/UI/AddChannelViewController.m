//
//  AddChannelViewController.m
//  OptimGenericReader
//
//  Created by soreha on 15/02/11.
//  
//

#import "AddChannelViewController.h"
#import "ConfigurationManager.h"
//#import "AudioToolbox/AudioServices.h"



@implementation AddChannelViewController

@synthesize rssAddress;
@synthesize addChannelViewDelegate;


- (void) releaseOutlets {
	self.rssAddress = nil;
}


- (void)dealloc {
	[self releaseOutlets];
}


#pragma mark textField delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if([textField.text compare:@""] == NSOrderedSame){
        textField.text = @"http://";
    }
}

- (void)viewDidLoad{
    [self.rssAddress becomeFirstResponder];
    self.title = @"Nouvelle source";
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithTitle:@"Annuler" style:UIBarButtonItemStyleDone target:self action:@selector(cancel:)];
#pragma ACCESSIBILITY
    cancel.accessibilityLabel = @"Annuler la création d'une source.";
    self.navigationItem.leftBarButtonItem = cancel;
    
    UIDevice* device = [UIDevice currentDevice];
    if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]
       || [device.name hasPrefix:@"iPhone1,"]
       || [device.name hasPrefix:@"iPad1,"]
       || [device.name hasPrefix:@"iPod3,"]){
        self.qrbutton.enabled = NO;
    }
#pragma ACCESSIBILITY
    self.qrbutton.accessibilityLabel = @"Scanner un Q R Code contenant l'addresse d'une source.";
    
    //self.contentSizeForViewInPopover = CGSizeMake(420, 320);

    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationItem.leftBarButtonItem.tintColor = [UIColor blueColor];
    }
	
	self.rssAddress.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"scenariReaderLastRssAddressUsed"];

	
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	[textField resignFirstResponder];
    [self addChannel:self];
	return YES;
}

- (IBAction) addChannel:(id) sender {
	
    [[NSUserDefaults standardUserDefaults] setValue:self.rssAddress.text forKey:@"scenariReaderLastRssAddressUsed"];
    if (addChannelViewDelegate != nil) [addChannelViewDelegate addChannelForUrl:rssAddress.text];
   
	[self dismissModalViewControllerAnimated:YES];
}

- (IBAction) cancel:(id) sender {
    DLog();
    
	[self.addChannelViewDelegate cancelAddChanel:self];
}

- (IBAction)QrcodeAction:(id)sender{
    self.zReader = [ZBarReaderViewController new];
    self.zReader.readerDelegate = self;
    self.zReader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = self.zReader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
    
    self.zReader.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
    self.zReader.showsCameraControls = NO;
    self.zReader.showsZBarControls=NO;
    self.zReader.cameraOverlayView=[self setOverlayPickerView];
    
    //[self.navigationController presentModalViewController: reader animated: YES];
    
    [self presentViewController:self.zReader animated:YES completion:NULL];
    
}



- (UIView *)setOverlayPickerView{
    UIView *v=[[UIView alloc] initWithFrame:self.zReader.view.bounds];
    //[v setBackgroundColor:[UIColor yellowColor]];
    UIToolbar *myToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, v.frame.size.height-44, v.frame.size.width, 44)];
    
    UIButton *bCancel = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    [bCancel setTitle:@"Annuler" forState:UIControlStateNormal];
    [bCancel setTitleColor:[UIColor colorWithRed:0 green:0.478 blue:1 alpha:1] forState:UIControlStateNormal];
    [bCancel addTarget:self action:@selector(dismissOverlayView:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc] initWithCustomView:bCancel];
    
     //UISwitch *sw=[[UISwitch alloc] init];
    //[sw setOn:NO];
    //UIBarButtonItem *switchButton=[[UIBarButtonItem alloc] initWithCustomView:sw];
    //UIBarButtonItem *fixed=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //[sw addTarget:self action:@selector(handleSwitchFlash:) forControlEvents:UIControlEventValueChanged];
    [myToolBar setItems:[NSArray arrayWithObjects:backButton,nil]];
    [myToolBar setBarStyle:UIBarStyleDefault];
    [v addSubview:myToolBar];
    
    v.autoresizesSubviews = YES;
    v.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    myToolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    return  v;
}

- (void)dismissOverlayView:(id)sender{
    [self dismissModalViewControllerAnimated: YES];
    
    self.zReader = nil;
}


- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
   // AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    // EXAMPLE: do something useful with the barcode data
    
    
    DLog(@"self.rssAddress=%@",self.rssAddress);
    
    DLog(@"symbol.data=%@",symbol.data);
    
    // EXAMPLE: do something useful with the barcode image
    //resultImage.image =
    //[info objectForKey: UIImagePickerControllerOriginalImage];
    
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    [reader dismissModalViewControllerAnimated: YES];
	[self.rssAddress setText:symbol.data];
    
    DLog(@"-> self.rssAddress.text=%@",self.rssAddress.text);
    
}




// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/





// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [self setQrbutton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.superview.bounds = CGRectMake(0, 0, 420, 320);

}

@end
