//
//  TocView.m
//  PhoneRadio
//
//  Created by soreha on 30/11/10.
//  
//

#import "TocView.h"
#import "RichMedia.h"
#import "Chapter.h"
#import "Segment.h"
#import "Complement.h"
#import "Reference.h"

#import "Illustration.h"
#import "SegmentCellView.h"

#import "EmissionDefaultViewController.h"
#import "ComplementViewController.h"
#import "SegmentDetailsViewController.h"
#import "ConfigurationManager.h"
#import "MyBrowser.h"

@implementation TocView

@synthesize richMedia;
@synthesize emissionDefaultViewController;

#pragma mark -
#pragma mark View lifecycle


-(id) initWithContent:(RichMedia*)media{
	self = [[TocView alloc] initWithNibName:@"TocView" bundle:nil];
	
	self.richMedia = media;
	
	self.title = self.richMedia.title;
	self.tableView.backgroundColor = [UIColor whiteColor];
    
    
    
    if([[ConfigurationManager sharedInstance] advancedTocMode]){
        NSArray *segmentTextContent = [NSArray arrayWithObjects:@"Chapitres",
                                       [[ConfigurationManager sharedInstance] shortComplementString],
                                       @"Références",
                                       nil];
        self.segmentedControl = [[UISegmentedControl alloc] initWithItems:segmentTextContent];
        self.segmentedControl.selectedSegmentIndex = 0;
        //self.segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
        //self.segmentedControl.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
        [self.segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
        self.segmentedControl.frame = CGRectMake(0, 0, self.view.bounds.size.width, 28);
        
        self.navigationItem.titleView = self.segmentedControl;
        
        [self.segmentedControl setEnabled:([[self.richMedia allComplements] count]>0) forSegmentAtIndex:1];
        [self.segmentedControl setEnabled:([[self.richMedia allReferences] count]>0) forSegmentAtIndex:2];
        
    }
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40)];
    footer.backgroundColor = [UIColor clearColor];
    
    self.footerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.view.bounds.size.width-20, 40)];
    self.footerLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.footerLabel.textAlignment = UITextAlignmentCenter;
    self.footerLabel.textColor = [UIColor darkGrayColor];
    self.footerLabel.font = [UIFont systemFontOfSize:12];
    [footer addSubview:self.footerLabel];
    [self.tableView setTableFooterView:footer];
    [self segmentAction:nil];
    
    self.htmlReferenceModel = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource :@"referenceModel" ofType:@"html"]
                                                              encoding:NSUTF8StringEncoding
                                                                 error:nil];
    
    return self;
}

- (void)segmentAction:(id)sender{
    
    if(!self.segmentedControl || self.segmentedControl.selectedSegmentIndex==0){
        if([[self.richMedia chapters] count] == 0){
            self.footerLabel.text = @"0 chapitre";
        }else if([[self.richMedia chapters] count] == 1){
            self.footerLabel.text = @"1 chapitre";
        }else{
            self.footerLabel.text = [NSString stringWithFormat:@"%d  chapitres",[self.richMedia.chapters count]];
        }
    }else if(self.segmentedControl.selectedSegmentIndex==1){
         if([[self.richMedia allComplements] count] == 0){
             self.footerLabel.text = [NSString stringWithFormat:@"0 %@",[[ConfigurationManager sharedInstance] singleComplementString]];
         }else if([[self.richMedia allComplements] count] == 1){
             self.footerLabel.text = [NSString stringWithFormat:@"1 %@",[[ConfigurationManager sharedInstance] singleComplementString]];
         }else{
             self.footerLabel.text = [NSString stringWithFormat:@"%d  %@",[[self.richMedia allComplements] count],[[ConfigurationManager sharedInstance] multipleComplementsString]];
         }
    }else if(self.segmentedControl.selectedSegmentIndex==2){
        if([[self.richMedia allReferences] count] == 0){
            self.footerLabel.text = @"0 référence";
        }else if([[self.richMedia allReferences] count] == 1){
            self.footerLabel.text = @"1 référence";
        }else{
            self.footerLabel.text = [NSString stringWithFormat:@"%d références",[[self.richMedia allReferences] count]];
        }
    }else{
        self.footerLabel.text = @"Aucun élément";
    }
    [self.tableView reloadData];
}


- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}



#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(!self.segmentedControl || self.segmentedControl.selectedSegmentIndex==0)
        return [self.richMedia.chapters count];
    //if(self.segmentedControl.selectedSegmentIndex==1){
    //return [[self.richMedia allComplements] count];
    //}
    return 1;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if(!self.segmentedControl || self.segmentedControl.selectedSegmentIndex==0){
        Chapter *chapter = [[self.richMedia chapters] objectAtIndex:section];
	    return [chapter.segments count];
    }
    if(self.segmentedControl.selectedSegmentIndex==1){
        return [[self.richMedia allComplements] count];
    }
    if(self.segmentedControl.selectedSegmentIndex==2){
        return [[self.richMedia allReferences] count];
    }
    return 0;
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
	if([self.richMedia isOneSegOneChapMedia] || (self.segmentedControl && self.segmentedControl.selectedSegmentIndex>0)){
        return nil;
    }
    Chapter *chapter = [[self.richMedia chapters] objectAtIndex:section];
    if(chapter.root){
		return @"ROOT - no title";
	}
	return [chapter title];
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
	cell.backgroundColor = ((indexPath.row+indexPath.section)%2)?[[ConfigurationManager sharedInstance] tableViewLightCellBackgroundColor]:[[ConfigurationManager sharedInstance] tableViewDarckCellBackgroundColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if([self.richMedia isOneSegOneChapMedia]|| (self.segmentedControl && self.segmentedControl.selectedSegmentIndex>0)){
        //DLog(@"isOneSegOneChapMedia");
        return 0;
    }
    return 24;
}

//Custom title view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	
	Chapter *chapter = [[self.richMedia chapters] objectAtIndex:section];
	
	// create the parent view that will hold header Label
	UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 24)];
	customView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	customView.backgroundColor = [[ConfigurationManager sharedInstance] tableViewSectionBackgroundColor];
	
	// create the button object
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
	headerLabel.textColor = [[ConfigurationManager sharedInstance] tableViewSectionForegroundColor];
	headerLabel.highlightedTextColor = [UIColor whiteColor];
	headerLabel.font = [UIFont boldSystemFontOfSize:14];
	headerLabel.frame = CGRectMake(10.0, 0.0, 310.0, 24);
	headerLabel.adjustsFontSizeToFitWidth = YES;
	headerLabel.minimumFontSize = 12.0;
		
	if(![self.richMedia isOneSegOneChapMedia]) headerLabel.text = [NSString stringWithFormat:@"%d - %@",section+1,[chapter title]];
		
	[customView addSubview:headerLabel];
	
	return customView;
}




- (NSString*) number2digit:(int) number{
	if (number<10) {
		return [NSString stringWithFormat:@"0%d",number];
	}
	return [NSString stringWithFormat:@"%d",number];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL addNumbers = [[ConfigurationManager sharedInstance] addAutoNumberingToChapitreAndSegmentName];

    if([self.segmentedControl selectedSegmentIndex]==1){//Affichages des compléments
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"complementCell"];
        if(!cell){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"complementCell"];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.font = [cell.textLabel.font fontWithSize:12];
            cell.textLabel.textColor = [UIColor darkGrayColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.adjustsFontSizeToFitWidth = YES;
            cell.textLabel.minimumFontSize = 8.0;
            cell.detailTextLabel.font = [cell.textLabel.font fontWithSize:11];
            cell.detailTextLabel.numberOfLines = 2;
        }
        Complement *complement = [[self.richMedia allComplements] objectAtIndex:indexPath.row];
        cell.textLabel.text = complement.title;
        Segment *segment = complement.parent;
        Chapter *chapter = [segment parent];
        
        
        
        
        if([self.richMedia isOneSegOneChapMedia]){
            if(addNumbers){
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%d - %@",indexPath.section+1,[segment title]];
            }else{
                cell.detailTextLabel.text = [segment title];
            }
        }else{
            //Plusieurs segments par chapitre
            NSString *line1 ;
            if([chapter.title isEqualToString:segment.title]){
                if(addNumbers){
                    line1 = [NSString stringWithFormat:@"%d - %@",indexPath.section+1,[chapter title]];
                }else{
                    line1 = [chapter title];
                }
            }else{
                if(addNumbers){
                    line1 = [NSString stringWithFormat:@"%d - %@\n%d.%d - %@",
                             indexPath.section+1,[chapter title],indexPath.section+1,indexPath.row+1,[segment title] ];
                }else{
                    line1 = [NSString stringWithFormat:@"%@\n%@",[chapter title],[segment title] ];
                }
            }
            cell.detailTextLabel.text = line1;
            
        }
        
        return cell;
    }
    if([self.segmentedControl selectedSegmentIndex]==2){//Affichages des références
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"refCell"];
        if(!cell){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"refCell"];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.textLabel.font = [cell.textLabel.font fontWithSize:12];
            cell.textLabel.textColor = [UIColor darkGrayColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.adjustsFontSizeToFitWidth = YES;
            cell.textLabel.minimumFontSize = 8.0;
            cell.detailTextLabel.font = [cell.textLabel.font fontWithSize:11];
            cell.detailTextLabel.numberOfLines = 2;
        }
        Reference *ref = [[self.richMedia allReferences] objectAtIndex:indexPath.row];
        if([[ref.type lowercaseString] isEqualToString:@"glossary"]){
            cell.textLabel.text = [NSString stringWithFormat:@"Glossaire : %@",ref.title];
        }else if ([[ref.type lowercaseString] isEqualToString:@"bibliography"]){
            cell.textLabel.text = [NSString stringWithFormat:@"Bibliographie : %@",ref.title];
        }else if ([[ref.type lowercaseString] isEqualToString:@"index"]){
            cell.textLabel.text = [NSString stringWithFormat:@"Index : %@",ref.title];
        }
        
        Segment *segment = ref.parent;
        Chapter *chapter = [segment parent];
        if([self.richMedia isOneSegOneChapMedia]){
            if(addNumbers){
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%d - %@",indexPath.section+1,[segment title]];
            }else{
                cell.detailTextLabel.text = segment.title;
            }
        }else{
            //Plusieurs segments par chapitre
            NSString *line1 ;
            if([chapter.title isEqualToString:segment.title]){
                if(addNumbers){
                    line1 = [NSString stringWithFormat:@"%d - %@",indexPath.section+1,[chapter title]];
                }else{
                    line1 = [chapter title];
                }
            }else{
                if(addNumbers){
                    line1 = [NSString stringWithFormat:@"%d - %@\n%d.%d - %@",
                             indexPath.section+1,[chapter title],indexPath.section+1,indexPath.row+1,[segment title] ];
                }else{
                    line1 = [NSString stringWithFormat:@"%@\n%@",[chapter title],[segment title] ];
                }
            }

            
            
            
            cell.detailTextLabel.text = line1;
        }
        
        return cell;
    }
    
	SegmentCellView *cell = (SegmentCellView *)[tableView dequeueReusableCellWithIdentifier:@"segment"];
	if (cell == nil) {
        cell = [[SegmentCellView alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"segment"];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.textLabel.textColor = [UIColor darkGrayColor];
		cell.textLabel.font = [cell.textLabel.font fontWithSize:12];
		//cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
		cell.textLabel.minimumFontSize = 8.0;
		cell.detailTextLabel.font = [cell.textLabel.font fontWithSize:11];
		
		/*[cell setAccessoryView:[super makeDetailDisclosureButtonWithFrame:CGRectMake(0, 0, 30, 30)
																 andImage:[[ConfigurationManager sharedInstance] getAccessoryImage]]];
	
        */
        
        if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
            [cell setAccessoryView:[self makeDetailDisclosureButtonWithFrame:CGRectMake(0, 0, 30, 30) andImage:[[ConfigurationManager sharedInstance] getAccessoryImage]]];
        }else{
            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        }
        
    }
	
	cell.textLabel.numberOfLines = 2;
    cell.detailTextLabel.numberOfLines = 1;
	
	Chapter *chapter = [[self.richMedia chapters] objectAtIndex:indexPath.section];
	Segment *segment = [chapter.segments objectAtIndex:indexPath.row];
		
	//cell.accessoryView.hidden = [segment.complements count]==0;
	//cell.textLabel.textColor = [segment.complements count]==0?[UIColor darkGrayColor]:[UIColor blackColor];
	
    
	if([self.richMedia isOneSegOneChapMedia]){
        if(addNumbers){      
            cell.textLabel.text = [NSString stringWithFormat:@"%d - %@",indexPath.section+1,[segment title]];
        }else{
            cell.textLabel.text = [segment title];
        }
    }else{
        if(addNumbers){
            cell.textLabel.text = [NSString stringWithFormat:@"%d.%d - %@",indexPath.section+1,indexPath.row+1,[segment title]];
        }else{
            cell.textLabel.text = [segment title];
        }
    }
	
	float s_start = [segment start];
	float s_end =  [segment end];
    
	
    
    
	NSString *compIndicatorString ;
	if ([segment hasComplement]) {
		//cell.textLabel.textColor = [UIColor blackColor];
		if([[segment complements] count]==1){
			compIndicatorString = [NSString stringWithFormat:@"(1 %@)", [[ConfigurationManager sharedInstance] shortComplementString]];
		}else {
			compIndicatorString = [NSString stringWithFormat:@"(%d %@)",[[segment complements] count], [[ConfigurationManager sharedInstance] multipleComplementsString]];
		}
		
	}else{
		//cell.textLabel.textColor = [UIColor darkGrayColor];
		compIndicatorString = @"";
	}
	
	cell.detailTextLabel.text = [NSString stringWithFormat: @" %@:%@ ┃ %@:%@  %@",
								 [self number2digit:(int)(s_start/60.0)], 
								 [self number2digit:((int)(s_start))%60], 
								 [self number2digit:(int)(s_end/60.0)], 
								 [self number2digit:((int)(s_end))%60],
								 compIndicatorString ];
	
	cell.imageView.image = [emissionDefaultViewController.allIllustrationImageThumbsDictionnary valueForKey:[NSString stringWithFormat:@"%d-%d-0",indexPath.section,indexPath.row]];
	// DLog(@"cell.imageView.image : %@",[cell.imageView.image description]);
    cell.complementIndicator.hidden = YES;//![segment hasComplement];
	
	
	if (((indexPath.section==[self.richMedia currentChapter]) && (indexPath.row <= [self.richMedia currentSegmentInChapter])) 
		|| indexPath.section<[self.richMedia currentChapter]){
		cell.readLabel.backgroundColor = [[ConfigurationManager sharedInstance] getSelectedColor];
	}else {
		cell.readLabel.backgroundColor = [[ConfigurationManager sharedInstance] getUnSelectedColor];
	}
	return cell;
	
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
	// Navigation logic may go here. Create and push another view controller.
	
    SegmentDetailsViewController *sdvc = [[SegmentDetailsViewController alloc] initWithMedia:self.richMedia chapterIndex:indexPath.section andSegmentIndex:indexPath.row];
	UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:self action:nil];
	self.navigationItem.backBarButtonItem = backButton;
	sdvc.contentSizeForViewInPopover = CGSizeMake(350, 410);
	[self.navigationController pushViewController:sdvc animated:YES];
		
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([self.segmentedControl selectedSegmentIndex]==1){
        
        Complement *complement = [[self.richMedia allComplements] objectAtIndex:indexPath.row];
        Segment *sParent= complement.parent;
        Chapter *cParent = sParent.parent;
        int sIndex = 0;
        int cIndex = 0;
        int n=0;
        for (Chapter *chap in self.richMedia.chapters) {
            if(chap == cParent){
                cIndex = n;
                int m= 0;
                for (Segment *seg in chap.segments) {
                    if(seg == sParent){
                        sIndex = m;
                        break;

                    }
                    m++;
                }
                
            }
            n++;
        }
        
        ComplementViewController *cv = [[ComplementViewController alloc] initWithRichMedia:self.richMedia chapterIndex:cIndex andSegmentIndex:sIndex];
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:self action:nil];
        self.navigationItem.backBarButtonItem = backButton;
        
        
        int complementIndex = 0;
        for (Complement *comp  in sParent.complements) {
            if(comp == complement){
                break;
            }
            complementIndex++;
        }
        
        [self.navigationController pushViewController:cv animated:YES];
        [cv showComplement:complementIndex];
        
        
        return;
    }
    if(self.segmentedControl.selectedSegmentIndex == 2){
        
        Reference *ref = [[self.richMedia allReferences]  objectAtIndex:indexPath.row];
        
		self.complementView = nil;
		if(self.complementView ==nil){
			
            
			self.complementView = [[ComplementView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
			UIWindow *tmpWindow = [[UIApplication sharedApplication].windows objectAtIndex:0];
			
			[tmpWindow addSubview:self.complementView];
			//[self.view addSubview:complementView];
			
			self.complementView.content.delegate = self;
            
		}
		
		[self.complementView setTitle:ref.title];
		
		/*
         NSURLRequest *rq = [NSURLRequest requestWithURL:
         [NSURL URLWithString:uriBlocString relativeToURL:[self.content.request URL]]];
         
         */
		
        NSMutableString *htmlContent= [NSMutableString stringWithString:self.htmlReferenceModel];
        
        [htmlContent replaceOccurrencesOfString:@"REFERENCE_CONTENT"
                                     withString:ref.content
                                        options:NSCaseInsensitiveSearch
                                          range:NSMakeRange(0, [htmlContent length]) ];
        
        [self.complementView setContentString:htmlContent withModelName:self.richMedia.base];
		[self.complementView openAction];
        
        return;
        
    }
    
	[emissionDefaultViewController goToSegment:indexPath.row inChapter:indexPath.section];
    if(IS_IPAD()){
        [self.popover dismissPopoverAnimated:YES];
        
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    [emissionDefaultViewController forceUpdate];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	
	DLog();
	
	NSString *requestString = [[request URL] absoluteString];
	
	if( requestString  && [requestString hasPrefix:@"open"]){
		
		NSArray *components = [requestString componentsSeparatedByString:@":"];
		Chapter *chapter = [[self.richMedia chapters] objectAtIndex:self.richMedia.currentChapter];
		Segment *segment =[chapter.segments objectAtIndex:self.richMedia.currentSegmentInChapter] ;
		
        
		NSMutableString *urlImg = [NSMutableString stringWithFormat:@"/%@",[components objectAtIndex:1] ];
		[urlImg replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [urlImg length])];
		self.fullScreenImageView.mediaDelegate = self;
        
		//[self presentModalViewController:self.fullScreenImageView animated:YES];
		//self.fullScreenImageView.image.image =  [UIImage imageWithContentsOfFile:urlImg] ;
		//[self.fullScreenImageView setTextChapter:chapter.title andSegment:[segment title]];

        [self presentViewController:self.fullScreenImageView animated:YES completion:^{
            self.fullScreenImageView.image.image =  [UIImage imageWithContentsOfFile:urlImg] ;
            [self.fullScreenImageView setTextChapter:chapter.title andSegment:[segment title]];
        }];
		
        
        return NO;
		
	}else if(requestString  && [[requestString lowercaseString] hasPrefix:@"showcomplement"]){
		NSArray *components = [requestString componentsSeparatedByString:@":"];
		
		int index = [[components objectAtIndex:1] intValue];
		
		ComplementViewController *cv = [[ComplementViewController alloc] initWithRichMedia:self.richMedia] ;
		
		UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:self action:nil];
		self.navigationItem.backBarButtonItem = backButton;
		
		[self.navigationController pushViewController:cv animated:YES];
		
		[cv showComplement:index];
		
        
		return NO;
		
	}else if(requestString  && [[requestString lowercaseString] hasPrefix:@"showreference"]){
        NSArray *components = [requestString componentsSeparatedByString:@":"];
		int index = [[components objectAtIndex:1] intValue];
        
		Reference *ref = [[[self.richMedia getCurrentSegmentObject] references] objectAtIndex:index];
        
		self.complementView = nil;
		if(self.complementView ==nil){
			
            
			self.complementView = [[ComplementView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
			UIWindow *tmpWindow = [[UIApplication sharedApplication].windows objectAtIndex:0];
			
			[tmpWindow addSubview:self.complementView];
			//[self.view addSubview:complementView];
			
			self.complementView.content.delegate = self;
            
		}
		
		[self.complementView setTitle:ref.title];
		
		/*
         NSURLRequest *rq = [NSURLRequest requestWithURL:
         [NSURL URLWithString:uriBlocString relativeToURL:[self.content.request URL]]];
         
         */
		
        DLog(@"ref.content : %@",ref.content);
        
		[self.complementView setContentString:ref.content withModelName:nil];
        
		[self.complementView openAction];
        
        return NO;
    }else if ( requestString  && [requestString hasPrefix:@"http://"]){
		if(self.complementView && self.complementView.content==webView){
			[self.complementView closeAction];
		}
        MyBrowser *myBrowser = [[MyBrowser alloc] initWithNibName:(IS_IPAD()?@"MyBrowser_iPad":@"MyBrowser_iPhone") bundle:nil];
		
		UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:nil action:nil];
		self.navigationItem.backBarButtonItem = backButton;
		
		[self.navigationController pushViewController:myBrowser animated:YES];
		[myBrowser showURL:request];
		
		
		return NO;
	}
	
	
	
	return YES;
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}



@end

