//
//  ChannelView.h
//  webmedia
//
//  Created by soreha on 27/11/12.
//  
//

#import <UIKit/UIKit.h>

@class ChannelView;
@protocol ChannelViewSelectionDelegate <NSObject>
- (void)didSelectChannelView:(ChannelView *)channelView;
@end

@interface ChannelView : UIView <UIGestureRecognizerDelegate>


@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UILabel *labelDocsInfo;
- (IBAction)infoAction:(id)sender;
@property (strong, nonatomic) UITextView *descriptionInfoTextView;
@property (strong, nonatomic) UIImageView *selectedImage;
@property (strong, nonatomic) UIView *backgroundFooterView;
@property (strong, nonatomic) UILabel *titleLabel;

@property (strong, nonatomic) UIButton *infoButton;
@property (strong, nonatomic) UIButton *deleteButton;


@property (nonatomic) BOOL selected;

@property (strong, nonatomic)id<ChannelViewSelectionDelegate> delegate;
@property (strong, nonatomic)id channelObject;



@property (strong, nonatomic) NSMutableArray *accessibleElements;

@end
