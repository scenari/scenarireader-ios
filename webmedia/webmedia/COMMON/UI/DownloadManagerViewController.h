//
//  DownloadManagerViewController.h
//  OptimGenericReader
//
//  Created by soreha on 18/03/11.
//  
//

#import <UIKit/UIKit.h>

@class DownloadManagerTableViewController, CoreDataManager;

@interface DownloadManagerViewController : UIViewController {
    CoreDataManager *dataManager;
    DownloadManagerTableViewController *tableViewController;
}

@property (nonatomic, strong) IBOutlet DownloadManagerTableViewController *tableViewController;
@property (nonatomic, strong) CoreDataManager *dataManager;;

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;


@end
