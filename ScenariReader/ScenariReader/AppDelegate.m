//
//  AppDelegate.m
//  ScenariReader
//
//  Created by soreha on 02/04/13.
//  
//


#import "Appirater.h"

#import "AppDelegate.h"
#import "ConfigurationManager.h"
#import "CoreDataManager.h"
#import "RequestManager.h"
#import "XMLRichMediaHandler.h"
#import "EmissionDefaultViewController.h"
#import "EmissionDefaultViewController_iPad.h"
#import "MediaHandlerManager.h"
#import "ZipArchive.h"
#import "Channel.h"
#import "DocumentsForChannelViewController.h"

#import "OpaleWebReader.h"


@implementation AppDelegate
@synthesize window = _window;
@synthesize splashView;


#pragma mark -
#pragma mark CSS handling
-(void) checkLocalCSS{
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *cssPath = [documentsDirectory stringByAppendingPathComponent:@"medias/css/styles.css"];
    NSString *css2Path = [documentsDirectory stringByAppendingPathComponent:@"medias/css/img/clbClsSml"];
    
	NSString *destDirectory = [documentsDirectory stringByAppendingPathComponent:@"medias/"];
	
	DLog(@"css path = %@",cssPath);
	
	//On teste si le css a déjà été copié dans le dossier Documents
	if([[NSFileManager defaultManager] fileExistsAtPath:cssPath] && [[NSFileManager defaultManager] fileExistsAtPath:css2Path]){
		return;
	}
    
	//unzipping downloaded data
	ZipArchive* za = [[ZipArchive alloc] init];
	
	NSString *zipPath = [[NSBundle mainBundle] pathForResource:@"css" ofType:@"zip"];
	
	if([za UnzipOpenFile:zipPath]){
		[za UnzipFileTo:destDirectory overWrite:YES];
		[za UnzipCloseFile];
	}
    NSURL *cssDirectory = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"medias/css/"]  isDirectory:YES ];
    [[[CoreDataManager sharedInstance] requestManager] addSkipBackupAttributeToItemAtURL:cssDirectory];
    
}

#pragma mark -
#pragma mark Channel(s) configuration methods
- (void)configureForSingleChannelConfiguration{
    
    NSArray *sources = [[ConfigurationManager sharedInstance] sourcesList] ;
    
    if([sources count]<1){
        DLog(@"ERROR MISSING SOURCE URL IN CONFIGURATION FILE");
        return;
    }
    
    NSString *channelURL = [sources objectAtIndex:0];
    Channel *channel = [[CoreDataManager sharedInstance] getChannelForRssUrl:channelURL];
    if(!channel){
        channel =[[CoreDataManager sharedInstance] createChannelForRssUrl:channelURL];
        [[CoreDataManager sharedInstance] saveData];
    }
    
    if (!IS_IPAD()) {
        UITabBarController *tabBarController = (UITabBarController *)[self.window rootViewController];
        UINavigationController *navController = [tabBarController.viewControllers objectAtIndex:0];
        DocumentsForChannelViewController *documentsForChannelView = [[DocumentsForChannelViewController alloc] initWithNibName:@"DocumentsForChannel" bundle:nil];
        documentsForChannelView.channel = channel;
        navController.viewControllers=[NSArray arrayWithObject:documentsForChannelView];
        [documentsForChannelView showAboutLeftButtonOnNavigationBar];
    }
    
    
}
- (void)configureForMultiChannelConfiguration{
    
    
    NSArray *sources = [[ConfigurationManager sharedInstance] sourcesList] ;
    int n=[sources count];
    if(n<1){
        DLog(@"NO SOURCE URL IN CONFIGURATION FILE (sourcesList dans le configuration.plist) <=> NO Default Channel");
        return;
    }
    
    BOOL changed = NO;
    
    for(int i=0;i<n;i++){
        NSString *channelURL = [sources objectAtIndex:i] ;
        
        DLog(@"channelURL : %@",channelURL);
        
        //UITabBarController *tabBarController = (UITabBarController *)[self.window rootViewController];
        
        //UINavigationController *navController = [tabBarController.viewControllers objectAtIndex:0];
        
        //DocumentsForChannelViewController *documentsForChannelView = [[DocumentsForChannelViewController alloc] initWithNibName:@"DocumentsForChannel" bundle:nil];
        
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@_deleted",channelURL]]){
            DLog(@"Channel was deleted");
            break;
        }
        Channel *channel = [[CoreDataManager sharedInstance] getChannelForRssUrl:channelURL];
        if(!channel){
            DLog(@"Channel didn't exist");
            channel =[[CoreDataManager sharedInstance] createChannelForRssUrl:channelURL];
            changed = YES;
            
        }
        else{
            DLog(@"Channel already exixts");
        }
        
    }
    
    if(changed)[[CoreDataManager sharedInstance] saveData];
    
    //documentsForChannelView.channel = channel;
    
    //navController.viewControllers=[NSArray arrayWithObject:documentsForChannelView];
    //[documentsForChannelView showAboutLeftButtonOnNavigationBar];
    
}

- (void)customizeAppearence{
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        [[UIBarButtonItem appearanceWhenContainedIn:[UIPopoverController class], nil]
         setTintColor:[[ConfigurationManager sharedInstance] navigationBarTintColor]];
        // Load resources for iOS 6.1 or earlier
        //Fixe 5.1 default light grey segment color on iPad
        [[UISegmentedControl appearanceWhenContainedIn:nil] setTintColor:[UIColor darkGrayColor]];
        
        [[UISegmentedControl appearanceWhenContainedIn:[UIPopoverController class], nil]
         setTintColor:[[ConfigurationManager sharedInstance] navigationBarTintColor]];
        
        [[UISegmentedControl appearanceWhenContainedIn:[UINavigationBar class], nil]
         setTintColor:[[ConfigurationManager sharedInstance] navigationBarTintColor]];
        
        
        [[UIToolbar appearanceWhenContainedIn:nil]  setTintColor:[[ConfigurationManager sharedInstance] navigationBarTintColor]];
        
    } else {
        NSLog(@"ios 7+");
        // Load resources for iOS 7 or later
        //[[UISegmentedControl appearanceWhenContainedIn:nil] setTintColor:[UIColor whiteColor]];
        [[UIBarButtonItem appearanceWhenContainedIn:[UIPopoverController class], nil]
         setTintColor:[UIColor blueColor]];
        [[UIButton appearanceWhenContainedIn:[UINavigationBar class], nil]
         setTintColor:[UIColor blueColor]];
        [[UISegmentedControl appearanceWhenContainedIn:[UINavigationBar class], nil]
         setTintColor:[UIColor blueColor]];
        //[[UISegmentedControl appearanceWhenContainedIn:nil] setTintColor:[UIColor blueColor]];
        [[UIBarButtonItem appearanceWhenContainedIn:nil] setTintColor:[UIColor blueColor]];
        
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil]
         setTintColor:[UIColor blueColor]];
        
        [[UIButton appearanceWhenContainedIn:nil] setTintColor:[UIColor blueColor]];
        
        //[[UINavigationBar appearanceWhenContainedIn:nil] setTintColor:[UIColor whiteColor]];

    }
    
    
//    [[UITabBar appearance] setSelectedImageTintColor:[[ConfigurationManager sharedInstance] navigationBarTintColor]];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
   
    [Appirater setAppId:@"664566546"];
    
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Add code here to do background processing
        //
        //
        [self checkLocalCSS];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            // Add code here to update the UI/send notifications based on the
            // results of the background processing
        });
    });
    
    
    [self customizeAppearence];
    // Override point for customization after application launch.
    [self.window makeKeyAndVisible];
    
    // if(!IS_IPAD()){
    //    if(![[ConfigurationManager sharedInstance] multiChannelConfiguration]){
    //        //Une chaine unique non configurable
    //        [self configureForSingleChannelConfiguration];
    //    }else{
    //        [self configureForMultiChannelConfiguration];
    //    }
    //}
	
	/*
    if(!self.splashView){
        float height = MAX(self.window.bounds.size.width, self.window.bounds.size.height);
        
        
        if (IS_IPAD()) {
            
            UIDeviceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
            
            CGAffineTransform transform = CGAffineTransformMakeScale(1, 1);
            
            
            
            if(UIDeviceOrientationIsPortrait(orientation)){
                DLog(@"device is in PORTRAIT");
                self.splashView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default-Portrait"]];
            }else{
                DLog(@"device is in LANDSCAPE");
                self.splashView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default-Landscape"]];
            }
            transform = CGAffineTransformRotate(transform, 0);
            
            if(orientation == UIDeviceOrientationPortraitUpsideDown){
                transform = CGAffineTransformRotate(transform, 3.14);
            }else if(orientation == UIDeviceOrientationLandscapeLeft){
                transform = CGAffineTransformRotate(transform, 3.14/2);
            }else if(orientation == UIDeviceOrientationLandscapeRight){
                transform = CGAffineTransformRotate(transform, -3.14/2);
            }
            
            self.splashView.transform = transform;
            
            DLog(@"[[UIScreen mainScreen] applicationFrame : %@", NSStringFromCGRect([[UIScreen mainScreen] applicationFrame]));
            self.splashView.contentMode = UIViewContentModeScaleAspectFill;
            
            self.splashView.frame = [[UIScreen mainScreen] applicationFrame];
            
        }else{
            self.splashView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:(height==568)?@"Default-568":@"Default"]];
            [self.window addSubview:self.splashView];
        }
        //[self.window addSubview:splashView];
        
    }
	*/
    
    [MediaHandlerManager sharedInstance].mediaHandlerDelegate = self;
    
    //Après une seconde appel de la méthode 'start'
	[self performSelector:@selector(start) withObject:nil afterDelay:0.5];
    [[ UIApplication sharedApplication ] setIdleTimerDisabled: YES ];
    
    
    //pushNotification
    //[application registerForRemoteNotificationTypes: UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
    
    
    [Appirater appLaunched:YES];
    
    
    return YES;
}

/*
 * Masque le splashscreen. L'animation dure 1.5 secondes.
 */
-(void) start{
    DLog();
	self.splashView.alpha = 1.0;
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0];
	[UIView setAnimationDuration:1.5];
	self.splashView.alpha = 0.0;
	[UIView commitAnimations];
}


- (void)applicationWillResignActive:(UIApplication *)application{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    if(![[ConfigurationManager sharedInstance] downloadInBackground]){
        [[CoreDataManager sharedInstance].requestManager supendAllDocumentDownload];
    }
    
    [[CoreDataManager sharedInstance] saveData];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [Appirater appEnteredForeground:YES];
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    if(![[ConfigurationManager sharedInstance] multiChannelConfiguration]){
        //Une chaine unique non configurable
        [self configureForSingleChannelConfiguration];
    }else{
        [self configureForMultiChannelConfiguration];
    }
    
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [[CoreDataManager sharedInstance] downloadAllChannelRssData];
    NSFetchedResultsController *resultController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestDownloadingDocuments:NO]
                                                                                       managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:nil
                                                                                                  cacheName:nil];
    
    NSError *error = nil;
	if (![resultController performFetch:&error]){
	    DLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
        return;
	}
    if ([[resultController fetchedObjects] count] >0) {
        BOOL showAlert = YES;
        
        if([[ConfigurationManager sharedInstance] downloadInBackground]){
            showAlert = NO;
            for (Document *doc in [resultController fetchedObjects]) {
                if([doc.downloading isEqualToNumber:[NSNumber numberWithInt:0]]){
                    showAlert = YES;
                    break;
                }
            }
        }
        if(showAlert){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Des téléchargements ont été interrompus ou des mises à jour sont disponibles.\n\nVous pouvez les télécharger à partir de l'onglet téléchargement." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


-(void)openOnlineDocument:(Document *) document onLastOpenedPage:(BOOL) lastOpenPage{
    NSLog(@"%s", __func__);

    
    NSLog(@"trying to open online document : %@",document.link);
    NSLog(@"Ouverture document %@ url:%@",document.title, document.localMediaURI);
    OpaleWebReader *reader = [[OpaleWebReader alloc] initWithNibName:@"OpaleWebReader" bundle:nil];
    
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	//NSString *documentsDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"medias"];
    //document.localMediaURI = [NSString stringWithFormat:@"%@/%@",documentsDirectory,[[document localMediaURI] lastPathComponent]];
    //[[CoreDataManager sharedInstance] saveData];
    
    //[self.window.rootViewController presentModalViewController:reader animated:YES];
    //[reader openOnlineDocument:document onLastOpenedPage:lastOpenPage];
    
    [self.window.rootViewController presentViewController:reader animated:YES completion:^{
        [reader openOnlineDocument:document onLastOpenedPage:(![[document.pubType lowercaseString] isEqualToString:@"topaze"] && lastOpenPage)];
    }];
}

- (void)openDocument:(Document *)document onLastOpenedPage:(BOOL)lastOpenPage{
    NSLog(@"%s", __func__);

    //Don't REMOVE USED to keep trace of old DOCUMENT WHEN APP UPDATED (DIRECTORY APP NAME CHANGED)
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"medias"];
    document.localMediaURI = [NSString stringWithFormat:@"%@/%@",documentsDirectory,[[document localMediaURI] lastPathComponent]];
    
    [[CoreDataManager sharedInstance] saveData];
    NSLog(@"lastOpenPage=%d",lastOpenPage);
    if(!lastOpenPage){
        
        if(![[document.pubType lowercaseString] isEqualToString:@"webmedia"]){
            OpaleWebReader *reader = [[OpaleWebReader alloc] initWithNibName:@"OpaleWebReader" bundle:nil];
            
            //[self.window.rootViewController presentModalViewController:reader animated:YES];
            //[reader openDocument:document onLastOpenedPage:NO];
        
            [self.window.rootViewController presentViewController:reader animated:YES completion:^{
                [reader openDocument:document onLastOpenedPage:NO];
            }];
            
        }else{
            [self openDocument:document];
        }
        return;
    }
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *documentsDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"medias"];
    //document.localMediaURI = [NSString stringWithFormat:@"%@/%@",documentsDirectory,[[document localMediaURI] lastPathComponent]];
    //[[CoreDataManager sharedInstance] saveData];
    
    OpaleWebReader *reader = [[OpaleWebReader alloc] initWithNibName:@"OpaleWebReader" bundle:nil];
    //[self.window.rootViewController presentModalViewController:reader animated:YES];
    //[reader openDocument:document onLastOpenedPage:lastOpenPage];
    
    [self.window.rootViewController presentViewController:reader animated:YES completion:^{
        [reader openDocument:document onLastOpenedPage:lastOpenPage];
    }];
    
    return;
    
}

-(void) openDocument:(Document *) document {
    NSLog(@"%s", __func__);
    //Don't REMOVE USED to keep trace of old DOCUMENT WHEN APP UPDATED (DIRECTORY APP NAME CHANGED)
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"medias"];
    document.localMediaURI = [NSString stringWithFormat:@"%@/%@",documentsDirectory,[[document localMediaURI] lastPathComponent]];
    
    [[CoreDataManager sharedInstance] saveData];
    
    
    NSLog(@"document.pubType=%@",document.pubType);
    
    if(![[document.pubType lowercaseString] isEqualToString:@"webmedia"]){
        OpaleWebReader *reader = [[OpaleWebReader alloc] initWithNibName:@"OpaleWebReader" bundle:nil];
        
        //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //NSString *documentsDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"medias"];
        //document.localMediaURI = [NSString stringWithFormat:@"%@/%@",documentsDirectory,[[document localMediaURI] lastPathComponent]];
        //[[CoreDataManager sharedInstance] saveData];
        
        
       // [self.window.rootViewController presentModalViewController:reader animated:YES];
       // [reader openDocument:document onLastOpenedPage:YES];
        
        [self.window.rootViewController presentViewController:reader animated:YES completion:^{
            [reader openDocument:document onLastOpenedPage:![[document.pubType lowercaseString] isEqualToString:@"topaze"]];
        }];
        
        return;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/webRadio.xml",[document localMediaURI]];
	XMLRichMediaHandler *handler = [[XMLRichMediaHandler alloc] init];
	NSURL *url=[NSURL fileURLWithPath:urlString];
	
	[handler loadXMLFromURL:url];
	handler.richMedia.base = [NSString stringWithFormat:@"%@/",[document localMediaURI]];
    //DLog(@"handler.richMedia.base:%@\n",handler.richMedia.base);
    
	EmissionDefaultViewController *emissionDefaultViewController ;
    
    if(IS_IPAD()){
        emissionDefaultViewController = [[EmissionDefaultViewController_iPad alloc] initWithMedia:handler.richMedia];
    }else{
        emissionDefaultViewController = [[EmissionDefaultViewController alloc] initWithMedia:handler.richMedia];
    }
    
    //DLog(@"color:%@",((UINavigationController*)(self.parentViewController)).navigationBar.tintColor);
    UINavigationController *rootController = [[UINavigationController alloc] initWithRootViewController:emissionDefaultViewController];
    
    //rrootController.navigationBar.barStyle = self.window.rootViewController.navigationController.navigationBar.barStyle;
    rootController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
    
    DLog(@"self.window.rootViewController : %@",[rootController description]);
   
    //[self.window.rootViewController presentModalViewController:rootController animated:YES];
    //[emissionDefaultViewController performSelector:@selector(checkForContinueReading) withObject:nil afterDelay:0.5];

    [self.window.rootViewController presentViewController:rootController animated:YES completion:^{
        [emissionDefaultViewController performSelector:@selector(checkForContinueReading) withObject:nil afterDelay:0.5];
    }];

}




- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    return UIInterfaceOrientationMaskAll;
}


@end
