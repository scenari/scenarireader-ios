//
//  SamplePopoverBackgroundView.m
//  smalto
//
//  Created by soreha on 21/04/12.
//  
//

#import "SamplePopoverBackgroundView.h"

@implementation SamplePopoverBackgroundView

@synthesize arrowOffset, arrowDirection;



-(id)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        _imageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"popover_background"] resizableImageWithCapInsets: UIEdgeInsetsMake(40.0, 10.0,30.0, 10.0)]] ;
        
        _arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popover_arrow_top"]] ;
   
        
        self.backgroundColor =  _arrowView.backgroundColor =  _imageView.backgroundColor = [UIColor clearColor];
        
        
        [self addSubview:_imageView];
        
        [self addSubview:_arrowView];
        
    }
    
    return self;
    
}



- (void)drawRect:(CGRect)rect {
    
    
    
}



-(void)layoutSubviews{
    
    
    
    if (arrowDirection == UIPopoverArrowDirectionUp) {  
        
        _imageView.frame = CGRectMake(0, [SamplePopoverBackgroundView arrowHeight], self.superview.frame.size.width, self.superview.frame.size.height - [SamplePopoverBackgroundView arrowHeight]);
        
        
        float x = MIN(self.superview.frame.size.width / 2 + arrowOffset - [SamplePopoverBackgroundView arrowBase] / 2, self.superview.frame.size.width - [SamplePopoverBackgroundView arrowBase] - 3 );
        
        _arrowView.frame = CGRectMake(x, 2, [SamplePopoverBackgroundView arrowBase], [SamplePopoverBackgroundView arrowHeight]);
        
    }
    else if (arrowDirection == UIPopoverArrowDirectionDown) {  
        
        _imageView.frame = CGRectMake(0, 0, self.superview.frame.size.width, self.superview.frame.size.height-[SamplePopoverBackgroundView arrowHeight]  );
        
        
        _arrowView.image = [[UIImage alloc] initWithCGImage: [UIImage imageNamed:@"popover_arrow_left"].CGImage scale: 1.0 orientation: UIImageOrientationLeft];
                
        _arrowView.frame = CGRectMake(self.superview.frame.size.width / 2 + arrowOffset - [SamplePopoverBackgroundView arrowBase] / 2, _imageView.frame.size.height-2, [SamplePopoverBackgroundView arrowBase], [SamplePopoverBackgroundView arrowHeight]);
        
    }
    
    
    else if (arrowDirection == UIPopoverArrowDirectionRight) {  
        _imageView.frame = CGRectMake(0, 0, self.superview.frame.size.width - [SamplePopoverBackgroundView arrowHeight], self.superview.frame.size.height);
        
        
        
        _arrowView.image = [[UIImage alloc] initWithCGImage: [UIImage imageNamed:@"popover_arrow_left"].CGImage scale: 1.0 orientation: UIImageOrientationUpMirrored];
        
        
        
        _arrowView.frame = CGRectMake(self.superview.frame.size.width - [SamplePopoverBackgroundView arrowHeight] - 1, self.superview.frame.size.height / 2 + arrowOffset - [SamplePopoverBackgroundView arrowBase] / 2, [SamplePopoverBackgroundView arrowHeight], [SamplePopoverBackgroundView arrowBase]);
        
    }
    else if (arrowDirection == UIPopoverArrowDirectionLeft) {  
              
        _imageView.frame = CGRectMake([SamplePopoverBackgroundView arrowHeight], 0, self.superview.frame.size.width - [SamplePopoverBackgroundView arrowHeight], self.superview.frame.size.height);
        
        _arrowView.image =  [UIImage imageNamed:@"popover_arrow_left"];
        
        _arrowView.frame = CGRectMake( 2, self.superview.frame.size.height / 2 + arrowOffset - [SamplePopoverBackgroundView arrowBase] / 2, [SamplePopoverBackgroundView arrowHeight], [SamplePopoverBackgroundView arrowBase]);
        
    }
}



+(UIEdgeInsets)contentViewInsets{
    
    return UIEdgeInsetsMake(5, 5, 5, 5);
    
}



+(CGFloat)arrowHeight{
    
    return 18.0;
    
}



+(CGFloat)arrowBase{
    
    return 32.0;
    
}



@end