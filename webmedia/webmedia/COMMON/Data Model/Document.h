//
//  Document.h
//  webmedia
//
//  Created by soreha on 12/06/13.
//  
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Bookmark, Channel, Thumbnail, ASIHTTPRequest;

@interface Document : NSManagedObject {
    ASIHTTPRequest *request;
}

@property (nonatomic, retain) NSNumber * downloadError;
@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSDate * lastBuildDate;
@property (nonatomic, retain) NSString * comments;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSDate * pubDate;
@property (nonatomic, retain) NSString * smartPhoneLink;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSNumber * isFavorite;
@property (nonatomic, retain) NSString * version;
@property (nonatomic, retain) NSNumber * updateAvailable;
@property (nonatomic, retain) NSNumber * downloadSuspended;
@property (nonatomic, retain) NSString * tmpDownloadFilePath;
@property (nonatomic, retain) NSString * lastOpenedPage;
@property (nonatomic, retain) NSString * tabletLink;
@property (nonatomic, retain) NSNumber * downloaded;
@property (nonatomic, retain) NSString * thumbnail_large_URI;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSNumber * isSelected;
@property (nonatomic, retain) NSNumber * distanceFromLocation;
@property (nonatomic, retain) NSNumber * downloadProgress;
@property (nonatomic, retain) NSString * thumbnail_small_URI;
@property (nonatomic, retain) NSString * legal;
@property (nonatomic, retain) NSString * keywords;
@property (nonatomic, retain) NSNumber * rssPos;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * guid;
@property (nonatomic, retain) NSNumber * downloading;
@property (nonatomic, retain) NSString * localMediaURI;
@property (nonatomic, retain) NSString * pubType;
@property (nonatomic, retain) Thumbnail *thumbnail_large;
@property (nonatomic, retain) NSSet *highlights;
@property (nonatomic, retain) Channel *channel;
@property (nonatomic, retain) NSSet *bookmarks;
@property (nonatomic, retain) Thumbnail *thumbnail_small;
@property (nonatomic, retain) NSSet *notes;
@property (nonatomic, retain) ASIHTTPRequest *request;


- (void)copyAttributesFromObject:(Document *)object;
- (void)setProgress:(float) value;

@end

@interface Document (CoreDataGeneratedAccessors)

- (void)addHighlightsObject:(NSManagedObject *)value;
- (void)removeHighlightsObject:(NSManagedObject *)value;
- (void)addHighlights:(NSSet *)values;
- (void)removeHighlights:(NSSet *)values;

- (void)addBookmarksObject:(Bookmark *)value;
- (void)removeBookmarksObject:(Bookmark *)value;
- (void)addBookmarks:(NSSet *)values;
- (void)removeBookmarks:(NSSet *)values;

- (void)addNotesObject:(NSManagedObject *)value;
- (void)removeNotesObject:(NSManagedObject *)value;
- (void)addNotes:(NSSet *)values;
- (void)removeNotes:(NSSet *)values;

@end
