//
//  DocumentMenuTableViewController.h
//  webmedia
//
//  Created by soreha on 21/06/13.
//  
//

#import <UIKit/UIKit.h>
#import "Document.h"


@protocol DocumentMenuTableViewControllerDelegate <NSObject>

@optional
- (void)addToFavs:(Document *)document sender:(id)sender;
- (void)removeFromFavs:(Document *)document sender:(id)sender;
- (void)download:(Document *)document sender:(id)sender;
- (void)openDocument:(Document *)document onLastOpenedPage:(BOOL)lastOpened sender:(id)sender;
- (void)openOnlineDocument:(Document *)document onLastOpenedPage:(BOOL)lastOpened sender:(id)sender;
- (void)showInformationsForDocument:(Document *)document sender:(id)sender;
- (void)resumeDownloadForDocument:(Document *)document sender:(id)sender;
- (void)cancelDownloadForDocument:(Document *)document sender:(id)sender;
- (void)suspendDownloadForDocument:(Document *)document sender:(id)sender;
- (void)deleteDocument:(Document *)document sender:(id)sender;
@end


@interface DocumentMenuTableViewController : UITableViewController

@property (nonatomic, retain) Document *document;
@property (nonatomic, retain) NSMutableArray *options;
@property (nonatomic, weak) id<DocumentMenuTableViewControllerDelegate> menuDelegate;

@property (nonatomic, retain) NSObject *userInfo;

@end
