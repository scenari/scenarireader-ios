//
//  TimeMarkerView.h
//  PhoneRadio
//
//  Created by soreha on 19/12/10.
//  
//

#import <UIKit/UIKit.h>


@interface TimeMarkerView : UIView {
	float duration;
}

@property(nonatomic) float duration;

- (id)initWithFrame:(CGRect)frame andDuration:(float) timeInSeconds;


@end
