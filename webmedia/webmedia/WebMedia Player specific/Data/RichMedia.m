//
//  RichMedia.m
//  PhoneRadio
//
//  Created by soreha on 26/07/10.
//  
//

#import "RichMedia.h"
#import "Chapter.h"
#import "Segment.h"
#import "Illustration.h"
#import "Resource.h"
#import "Complement.h"
#import "Reference.h"


@implementation RichMedia

@synthesize title;
@synthesize author;
@synthesize mediaUri;
@synthesize legal;
@synthesize version;
@synthesize description;
@synthesize chapters;
@synthesize credits;
@synthesize extras;
@synthesize modificationDate;

@synthesize currentChapter;
@synthesize currentSegment;
@synthesize currentSegmentInChapter;
@synthesize durationInSeconds;

@synthesize base;
@synthesize autoStop;
@synthesize webMediaVersion;


- (id) init{
	self = [super init];
	if (self != nil) {
		self.chapters = [[NSMutableArray alloc] initWithCapacity:0];
		self.extras = [[NSMutableArray alloc] initWithCapacity:0];
		self.credits = [[Credits alloc] init];
		
		currentChapter = 0;
		currentSegment = 0;
		currentSegmentInChapter = 0;
        autoStop = NO;
	}
	return self;
}

-(void) computeCurrentSegment{
	int n = currentSegmentInChapter;
	for(int i=0;i<currentChapter;i++){
		Chapter *chap = [chapters objectAtIndex:i];
		n+=[chap.segments count];
	}
	currentSegment = n;
}

-(Segment*) getSegmentAtTime:(float)time{
	
	Segment *result=nil;
	
	for (int i=0, n=[chapters count]; i<n; i++) {
		Chapter *chap = [chapters objectAtIndex:i];
		for (int j=0,m=[chap.segments count]; (time<chap.end) && j<m; j++) {
			Segment *seg = [chap.segments objectAtIndex:j];
			if(time<seg.end){
				return seg;
			}
		}
	}
	
	return result;
}
-(Segment*) nextSegment{
    DLog(@"next segment currentChapter:%d currentSegment:%d",currentChapter,currentSegmentInChapter);
    int segNumberInCurrentChapter = [[self getCurrentChapterObject].segments count];
    DLog(@"Nombre de segment dans le chapitre en cours : %d",segNumberInCurrentChapter);
    if((currentSegmentInChapter+1)<segNumberInCurrentChapter){
        DLog(@"Cas A");
        currentSegmentInChapter++;
        return [self getCurrentSegmentObject];
    }
    if((currentChapter+1)<[chapters count]){
        DLog(@"Cas B");
        currentChapter++;
        currentSegmentInChapter=0;
        return [self getCurrentSegmentObject];
    }
    return nil;
}
-(Segment*) getCurrentSegmentObject{
    if(currentChapter<0 || currentChapter >= [chapters count]){
        currentChapter=0;
    }
    if([chapters count]==0)return nil;
    if(currentChapter>=[chapters count]) return nil;
	Chapter *chap = [chapters objectAtIndex:currentChapter];
    
    if(currentSegmentInChapter<0 || currentSegmentInChapter>=[chap.segments count]){
        currentSegmentInChapter = 0;
        [self computeCurrentSegment];
    }
    @try {
        return [chap.segments objectAtIndex:currentSegmentInChapter];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    return nil;
}
-(Chapter*) getCurrentChapterObject{
    if(currentChapter<0 || currentChapter>= [chapters count]){
        currentChapter=0;
    }
    if([chapters count]==0)return nil;
    return [chapters objectAtIndex:currentChapter];
}

-(void) resetIndexes{
	currentChapter=0;
	currentSegmentInChapter=0;
	currentSegment=0;
}
-(void) updateIndexWithTime:(float)time{
	if(time==durationInSeconds){
		currentChapter = [chapters count] -1;
		Chapter *chap = [chapters objectAtIndex:currentChapter];
		currentSegmentInChapter = [chap.segments count]-1;
		
		[self computeCurrentSegment];
		return;		
	}
	currentChapter = [self getChapterIndexAtTime:time];
	currentSegmentInChapter = [self getSegmentIndexInChapter:currentChapter atTime:time];
	[self computeCurrentSegment];
}


-(int) getChapterIndexAtTime:(float)time{
	for (int i=0, n=[chapters count]; i<n; i++) {
		Chapter *chap = [chapters objectAtIndex:i];
		if(time<chap.end) return i;
	}
	return -1;
}
-(int) getSegmentIndexInChapter:(int)chapterIndex atTime:(float)time{
	if([chapters count]<=chapterIndex)
        return -1;
    
    Chapter *chap = [chapters objectAtIndex:chapterIndex];
	for (int i=0,n=[chap.segments count]; i<n; i++) {
		Segment *seg = [chap.segments objectAtIndex:i];
		if(time<seg.end) return i;
	}
	
	return -1;
}

-(BOOL) isTimeInCurrentSegment:(float)time{
	Chapter *chap = [self.chapters objectAtIndex:currentChapter];
	Segment *seg = [chap.segments objectAtIndex:currentSegmentInChapter];
	return (time>=seg.start && time<=seg.end);
	
}

-(void) printData{
    
    NSMutableString *res = [[NSMutableString alloc] initWithCapacity:0];
    
	[res appendFormat:@"\n*******************************************************************\nINFORMATIONS RICH MEDIA ********************************************\n"];
	[res appendFormat:@"\t title=%@\n",title ];
    [res appendFormat:@"\t duration=%d\n",self.durationInSeconds];
	[res appendFormat:@"\t author=%@\n",author ];
	[res appendFormat:@"\t mediaUri=%@\n",mediaUri ];
	[res appendFormat:@"\t legal=%@\n",legal ];
	[res appendFormat:@"\t version=%@\n",version ];
	[res appendFormat:@"\t description=%@\n",description ];
	[res appendFormat:@"\t modificationDate=%@\n",modificationDate ];
	[res appendFormat:@"\nChapitres/segments/illustrations\n"];
    [res appendFormat:@"\nNombre de chapitres : %d",[chapters count]];
	for (int i=0, n=[chapters count]; i<n; i++) {
		Chapter *chap = [chapters objectAtIndex:i];
		[res appendFormat:@"\n- CHAPITRE %d, title = %@, start = %.1f, end = %.1f\n",i,chap.title,chap.start,chap.end];
		[res appendFormat:@"\nNombre de segments ds le chapitre n°%d: %d",(i+1),[chap.segments count]];
        for (int j=0,m=[chap.segments count]; j<m; j++) {
			Segment *seg = [chap.segments objectAtIndex:j];
			[res appendFormat:@"-- SEGMENT %d, title = %@, start = %.1f, end = %.1f, clipBegin=%.1f, clipEnd=%.1f, mediaURI=%@\n",j,seg.title ,seg.start,seg.end,seg.clipBegin, seg.clipEnd, seg.mediaUri ];
			[res appendFormat:@"\nNombre d'illustrations : %d",[seg.illustrations count]];
            for (int k=0,l=[seg.illustrations count]; k<l; k++) {
				Illustration *illu = [seg.illustrations objectAtIndex:k];
				[res appendFormat:@"--- ILLUSTRATION %d, refURI = %@, start = %.1f, end = %.1f\n",
					   k,
					   [[illu resource] refUri] ,
					   illu.start, 
					   illu.end];
			}
			[res appendFormat:@"---- NB COMPLEMENTS %d\n", [seg.complements count]];
			for(int m=0,n=[seg.complements count];m<n;m++){
				Complement *comp=[seg.complements objectAtIndex:m];
				[res appendFormat:@"----  COMP %d, GalN=%d",m, [comp.galleries count]];
			}
		}
	}
	[res appendFormat:@"\n**************************************\n"];
         
    DLog(@"%@",res);
}

+(NSString *) getIllustrationFullPath:(Illustration *)illustration forMedia:(RichMedia *)media{
    @try {
        if (!illustration.resource.refUri) {
            return nil;
        }
        NSMutableString *imagePath = [NSMutableString stringWithString:illustration.resource.refUri];
        if (![imagePath hasPrefix:@"/"] && ![media.base hasSuffix:@"/"]) {
            imagePath = [NSMutableString stringWithFormat:@"/%@",illustration.resource.refUri];
        }
        if(media.base)
            imagePath = [NSMutableString stringWithFormat:@"%@%@",media.base, imagePath];
        
        [imagePath replaceOccurrencesOfString:@"%20" withString:@" " options:NSCaseInsensitiveSearch range:NSMakeRange(0,[imagePath length])];
        return imagePath;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    return nil;
    
    
}
+(NSString *) getIllustrationFullURLString:(Illustration *)illustration forMedia:(RichMedia *)media{
    @try{
        NSString *tmp = [RichMedia getIllustrationFullPath:illustration forMedia:media ];
    if(!tmp) return @"-";
    NSMutableString *path;
    if([tmp hasPrefix:@"/"]){
         path = [NSMutableString stringWithString:[tmp substringFromIndex:1]];
    }
    else {
        path = [NSMutableString stringWithString:tmp];
    }
    
    [path replaceOccurrencesOfString:@" " withString:@"%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0,[path length])];

        
    return path;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    return @"-";
}
- (BOOL)isWebMediaVersion1{
    return self.webMediaVersion == WEB_MEDIA_1;
}
- (BOOL)isWebMediaVersion2{
    return self.webMediaVersion == WEB_MEDIA_2;
}
- (BOOL)isOneSegOneChapMedia{
    static BOOL toCheck = YES;
    static BOOL result = NO;
    if(toCheck){
        for (Chapter *chapter in self.chapters) {
            if(!chapter.segments || [chapter.segments count]>1 || [chapter.segments count]==0){
               toCheck = NO;
               DLog(@"result is NO 1");
               return NO;
            }
            Segment *segment = [chapter.segments lastObject];
            if(	chapter.chapterId != segment.segmentId ||
               ![chapter.title isEqualToString:segment.title] ||
               chapter.start != segment.start ||
               chapter.end != segment.end ){
               toCheck = NO;
                DLog(@"result is NO 2");

               return NO;
            }
        }
        DLog(@"result is YES");
        result = YES ;
        toCheck = NO;
    }
    
    return result;
    
}
- (NSArray *)allComplements{
    if(!_allComplements){
        NSMutableArray *tmp = [NSMutableArray arrayWithCapacity:0];
        for (Chapter *chap in self.chapters) {
            for (Segment *seg in chap.segments) {
                for(Complement *comp in seg.complements){
                    [tmp addObject:comp];
                }
            }
        }
        _allComplements = [NSArray arrayWithArray:tmp];
    }
    return _allComplements;
}
- (NSArray *)allReferences{
    if(!_allReferences){
        NSMutableArray *tmp = [NSMutableArray arrayWithCapacity:0];
        for (Chapter *chap in self.chapters) {
            for (Segment *seg in chap.segments) {
                for(Reference *ref in seg.references){
                    [tmp addObject:ref];
                }
            }
        }
        _allReferences = [NSArray arrayWithArray:tmp];
    }
    return _allReferences;
}

- (NSArray *)allIllustrations{
    if(!_allIllustrations){
        NSMutableArray *tmp = [NSMutableArray arrayWithCapacity:0];
        for (Chapter *chap in self.chapters) {
            for (Segment *seg in chap.segments) {
                for(Illustration *illu in seg.illustrations){
                    [tmp addObject:illu];
                }
            }
        }
        _allIllustrations = [NSArray arrayWithArray:tmp];
    }
    return _allIllustrations;
}

- (BOOL)hasIllustrationImage{
    for (Chapter *chap in self.chapters) {
        for (Segment *seg in chap.segments) {
            if([seg hasIllustrationImage])return YES;
        }
    }
    return NO;
}

@end
