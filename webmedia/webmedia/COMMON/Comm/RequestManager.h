//
//  RequestManager.h
//  WebMediaTest
//
//  Created by soreha on 31/01/11.
//  
//

#import <Foundation/Foundation.h>
#import "RequestManagerCoreDataDelegate.h"

@class Channel, Document, ASINetworkQueue;

@interface RequestManager : NSObject {
	NSOperationQueue *rssAndThumbnailDownloadQueue;
	ASINetworkQueue *documentDownloadQueue;
	NSObject <RequestManagerCoreDataDelegate> *coreDataDelegate;
}

@property (nonatomic, strong) NSOperationQueue *rssAndThumbnailDownloadQueue;
@property (nonatomic, strong) ASINetworkQueue *documentDownloadQueue;
@property (nonatomic, strong) NSObject <RequestManagerCoreDataDelegate> *coreDataDelegate;



- (void)addThumbnailRequestForDocument:(Document *) document;
- (void)addThumbnailRequestForChannel:(Channel *) channel;
- (void)addDocumentRequest:(Document *) document;
- (void)addRssRequestForNewChannel:(Channel *) channel;
- (void)addRssRequestForChannel:(Channel *) channel;
- (void)supendAllDocumentDownload;
- (void)cancelAllDocumentDownload;
- (void)initAsiQueue;
- (NSString *)getTmpDownloadFolder;

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

@end
