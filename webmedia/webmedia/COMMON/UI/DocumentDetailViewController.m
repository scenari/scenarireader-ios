//
//  DocumentDetailViewController.m
//  PhoneRadio
//
//  Created by soreha on 22/07/10.
//  
//

#import "DocumentDetailViewController.h"
#import "Document.h"
#import "Thumbnail.h"
#import "CoreDataManager.h"


@implementation DocumentDetailViewController

@synthesize currentDocument;

- (void)viewDidLoad {
    DLog();
    [super viewDidLoad];
    content.delegate = self;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        self.navigationItem.leftBarButtonItem.tintColor = self.navigationItem.rightBarButtonItem.tintColor =[UIColor blueColor];

    }

}

- (void)viewWillAppear:(BOOL)animated{
	UISegmentedControl *segmentedControl = (UISegmentedControl *)self.navigationItem.rightBarButtonItem.customView;
#pragma ACCESSIBILITY
    [segmentedControl imageForSegmentAtIndex:1].accessibilityLabel = @"Afficher les informations du document suivant.";
#pragma ACCESSIBILITY
    [segmentedControl imageForSegmentAtIndex:0].accessibilityLabel = @"Afficher les informations du document précédent.";
	// Before we show this view make sure the segmentedControl matches the nav bar style
	if (self.navigationController.navigationBar.barStyle == UIBarStyleBlackTranslucent ||
		self.navigationController.navigationBar.barStyle == UIBarStyleBlackOpaque)
		segmentedControl.tintColor = [UIColor darkGrayColor];
	else
		segmentedControl.tintColor = defaultTintColor;
	
    
    if(IS_IPAD()){
        self.navigationItem.rightBarButtonItem = nil;
        self.contentSizeForViewInPopover = CGSizeMake(310, 350);
		//ios 7>
		if([self respondsToSelector:@selector(preferredContentSize:)]){
			self.preferredContentSize = CGSizeMake(310, 350);
		}
    }
    
	self.currentDocument = [self.itemsList objectAtIndex:currentItem];
	
	
	NSString *modelForDescriptionPage;
	
	if (currentDocument.location != nil) {
		modelForDescriptionPage = [NSString 
											 stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource :@"MediaDetailTemplateLocation" ofType:@"html"] 
											 encoding:NSUTF8StringEncoding 
											 error:nil];
	} else {
		modelForDescriptionPage = [NSString 
											 stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource :@"MediaDetailTemplate" ofType:@"html"] 
											 encoding:NSUTF8StringEncoding 
											 error:nil];
	}

	
	
	NSMutableString *desc=[NSMutableString stringWithString:modelForDescriptionPage];
	
	[desc replaceOccurrencesOfString:@"RICHMEDIA_TITLE" 
						  withString:([currentDocument title]?[currentDocument title]:@"-") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
	NSString * durationString;
	
	if ([currentDocument duration]) {
		int duration = [currentDocument.duration intValue];
		if(duration!=0){
            durationString = [NSString stringWithFormat:@"-<br/>%@:%@",
										[self number2digit:(int)(duration/60.0)], 
										[self number2digit:((int)(duration))%60]];
        }else durationString=@"";
	}else durationString=@"";
	
	if (!IS_IPAD() && currentDocument.thumbnail_large) {
	
		NSData *imageData = UIImagePNGRepresentation(currentDocument.thumbnail_large.image);
		NSString *encodedString = [self encodeBase64WithData:imageData];
	
		NSString *imageString = [NSString stringWithFormat: @"<img src='data:image/png;base64, %@' style=\"margin-left:auto; margin-right:auto; \
                                 background: url(shadow-1000x1000.gif) no-repeat right bottom;padding: 5px 10px 10px 5px;\
                                 max-width:80%%; height:auto;\" alt='Illustration du document.'/>", encodedString];
		
		
        
        [desc replaceOccurrencesOfString:@"RICHMEDIA_IMAGE" 
							  withString:imageString 
								 options:NSCaseInsensitiveSearch 
								   range:NSMakeRange(0, [desc length]) ];
        
        
	} else {
		[desc replaceOccurrencesOfString:@"RICHMEDIA_IMAGE" 
							  withString:@"" 
								 options:NSCaseInsensitiveSearch 
								   range:NSMakeRange(0, [desc length]) ];
        
	}
    
    
    NSData *imageData;
    NSString *altString;
    if ([currentDocument.isFavorite intValue] == 0) {
        imageData = UIImagePNGRepresentation([UIImage imageNamed:@"favorites16_off"]);
#pragma ACCESSIBILITY
        altString = @"Appuyez pour ajouter aux favoris.";
    } else {
        imageData = UIImagePNGRepresentation([UIImage imageNamed:@"favorites16"]);
#pragma ACCESSIBILITY
        altString = @"Appuyez pour supprimer ce document de vos favoris.";
    }
		
    NSString *encodedString = [self encodeBase64WithData:imageData];
#pragma ACCESSIBILITY
    NSString *imageString = [NSString stringWithFormat: @"<a href='addFav://emission'><img width='20px' heigth='20px' src='data:image/png;base64, %@' alt='%@'/></a>", encodedString, altString];
        
    [desc replaceOccurrencesOfString:@"FAV_IMAGE" 
						  withString:imageString 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
    
	[desc replaceOccurrencesOfString:@"RICHMEDIA_DURATION" 
						  withString:durationString
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
	[desc replaceOccurrencesOfString:@"RICHMEDIA_SUMMARY" 
						  withString:([currentDocument summary]?[currentDocument summary]:@"-") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
	[desc replaceOccurrencesOfString:@"RICHMEDIA_COPYRIGHT" 
						  withString:([currentDocument legal]?[currentDocument legal]:@"-") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
	[desc replaceOccurrencesOfString:@"RICHMEDIA_VERSION" 
						  withString:([currentDocument version]?[currentDocument version]:@"-") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
	[desc replaceOccurrencesOfString:@"RICHMEDIA_AUTHOR" 
						  withString:([currentDocument author]?[currentDocument author]:@"-") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
	
	[desc replaceOccurrencesOfString:@"RICHMEDIA_PUBDATE" 
						  withString:([currentDocument pubDate]?[formatter stringFromDate:[currentDocument pubDate]]:@"-") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
	[desc replaceOccurrencesOfString:@"RICHMEDIA_LASTBUIDDATE" 
						  withString:([currentDocument lastBuildDate]?[formatter stringFromDate:[currentDocument lastBuildDate]]:@"-") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	if (currentDocument.location != nil) {
		[desc replaceOccurrencesOfString:@"DOCUMENT_GPS_LOCATION" 
							  withString:([currentDocument location]?[currentDocument location]:@"-") 
								 options:NSCaseInsensitiveSearch 
								   range:NSMakeRange(0, [desc length]) ];
	}
	
			
	NSString *path = [[NSBundle mainBundle] pathForResource :@"MediaDetailTemplate" ofType:@"html"];
	NSURL *baseURL = [NSURL fileURLWithPath:path];
	
	
	[self.content loadHTMLString:desc baseURL:baseURL ];
	
	[segmentedControl setEnabled:(self.currentItem!=0) forSegmentAtIndex:0];
	[segmentedControl setEnabled:(self.currentItem<([itemsList count]-1)) forSegmentAtIndex:1];
	 segmentedControl.tintColor = self.navigationController.navigationBar.tintColor;
//#pragma ACCESSIBILITY
    //self.navigationController.navigationItem.leftBarButtonItem.accessibilityLabel = @"Retour à la liste des documents.";


}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *requestString = [[request URL] absoluteString];
    
    if ( requestString  && [[requestString lowercaseString ]hasPrefix:@"addfav:"]) {
        
        UIAlertView *alert;
        if ( [currentDocument.isFavorite intValue] == 0 ) {
            currentDocument.isFavorite = [NSNumber numberWithBool:YES];
            alert = [[UIAlertView alloc] initWithTitle:@"Favoris" message:@"Le document a été ajouté à votre liste de favoris" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
        } else {
            currentDocument.isFavorite = [NSNumber numberWithBool:NO];
            alert = [[UIAlertView alloc] initWithTitle:@"Favoris" message:@"Le document a été supprimé de votre liste de favoris" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        }
        
        if([self.favChangeDelegate respondsToSelector:@selector(favoriteAttributeForDocumentHasChanged:)]){
            [self.favChangeDelegate favoriteAttributeForDocumentHasChanged:currentDocument];
        }
        [[CoreDataManager sharedInstance] saveData];

        [alert show];
        [self viewWillAppear:NO];
        return NO;
    }
    return YES;
}




@end
