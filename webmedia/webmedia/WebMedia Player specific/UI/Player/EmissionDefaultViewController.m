//
//  EmissionDefaultViewController.m
//  PhoneRadio
//
//  Created by soreha on 18/07/10.
//  
//


#import "EmissionDefaultViewController.h"
#import "Chapter.h"
#import "Segment.h"
#import "Illustration.h"
#import "Reference.h"
#import "SegmentCellView.h"

#import "TocView.h"
#import "ChapterView.h"
#import "TimeMarkerView.h"
#import "ComplementViewController.h"
#import "Complement.h"
#import "SegmentDetailsViewController.h"
#import "ConfigurationManager.h"
#import "MoviePlayerView.h"
#import "MoviePlayerView.h"
#import "MyBrowser.h"
#import "SamplePopoverBackgroundView.h"
#import "ExtrasTableViewController.h"


#define MAX_ZOOM_FACTOR 100
#define DEFAULT_ZOOM_FACTOR 25
#define MIN_ZOOM_FACTOR 10

#define MAX_TIME_LINE_WIDTH 3600


#define MODE_FULLSCREEN_PIN_OFF 0
#define MODE_FULLSCREEN_PIN_ON  1



CGFloat kMovieViewOffsetX = 0.0;//20.0
CGFloat kMovieViewOffsetY = 0.0;//20.0

@interface EmissionDefaultViewController (PrivateMethods)
//-(void) updateTimeLine;
-(void) updateTimeLabelsWithTime:(CMTime)time;
-(void) resetTimeLine;
-(void) toggleFullScreen;
-(void) showTableView;
-(void) handleSlideToNext;
-(void) handleSlideToPrevious;
-(void) handleSingleTap;
-(void) handleZoomGesture;
-(void) expandPlayerView;
-(void) decreasePlayerView;


-(void) setMiniPlayerView;

-(void) updateTimeLabels;
//-(void) updateFullScreenView;
-(void) updateData:(BOOL) andBullet;
- (void)updateIllustrationImage;

-(void) showActionInfo;
-(void) setCurrentTimeValueForFullPlayer;
-(void) jumpToTimeLineSelection;
-(void) loadIllustrationImages;
-(UIImage*) illustrationImageForTime:(float) time;
//-(UIImage*) illustrationImageForCurrentSegment;

-(float) getPlayerCurrentTime;
-(void) seekPlayerToTime:(float) time;
- (void)pausePlayer;
- (void)handlePlayerForWebMedia1;
- (void)handlePlayerForWebMedia2;

- (void)manageInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;
- (void)handleAudioOrVideoViewsConfiguration;

- (void)manageGestureRecognizers;
- (void)configureActionInfoView;
- (void)updateFullScreenInformationView:(Segment *)segment;

- (BOOL)isInFullscreenMode;

- (float)getVerifiedScrollTimelinePosition;

@end


@implementation EmissionDefaultViewController

@synthesize richMedia;
@synthesize htmlContentModel;
@synthesize baseURL;


@synthesize abstractView;
@synthesize playPauseButton;
@synthesize nextButton;
@synthesize previousButton;
@synthesize nextButtonFP;
@synthesize previousButtonFP;

@synthesize topView;
@synthesize playerControlView;
@synthesize expandIndicatorImageView;
@synthesize bulletImageView;
@synthesize bulletBackgroundImageView;
@synthesize bulletLabel;
@synthesize advancingTimeLabel;
@synthesize remainingTimeLabel;
@synthesize timeLineBkgImageView, scrollView;

@synthesize audioPlayer;

@synthesize tocView;

@synthesize slideToRightOnControlsPlayerView ;
@synthesize singleTapOnControlsPlayerView ;
@synthesize slideToLeftOnControlsPlayerView ;
@synthesize slideToTopOnControlsPlayerView;
@synthesize slideToBottomOnControlsPlayerView;
@synthesize zoomGestureRecognizerOnTimeLine;
@synthesize slideToRightOnVideoView ;
@synthesize slideToLeftOnVideoView ;

@synthesize actionInfoView;
@synthesize currentTimeFullPlayerLabel;
@synthesize timeMarkerView;

@synthesize allIllustrationImageThumbsDictionnary;

@synthesize playerIllustrationTimeObserver;
@synthesize playerSecondObserver;
@synthesize playerSegmentChangeObserver;

@synthesize zoomFactor;
//@synthesize fullScreenImageView;
@synthesize loadingView;
@synthesize activityIndicator;
@synthesize messageLabel;

Boolean playerIsExpanded = NO;
Boolean stopPlayerOnDisapear = NO;

double oldIllustrationBulletStart = -1;

Boolean playing = NO;


-(void)createAndConfigureMoviePlayerWithURL:(NSURL *)movieURL sourceType:(MPMovieSourceType)sourceType{
    DLog(@"movieURL = %@", [movieURL description]);
    AVPlayer *player = [self.videoView getPlayer];
    if(!player){
        DLog(@"player was nil");
        AVPlayerItem *itemToPlay = [AVPlayerItem playerItemWithURL:movieURL];
        [self.videoView setPlayer:[[AVPlayer alloc] initWithPlayerItem:itemToPlay]];
        self.audioPlayer = [self.videoView getPlayer];
        NSArray *metadataList = [itemToPlay.asset commonMetadata];
        for (AVMetadataItem *metaItem in metadataList) {
            DLog(@"..%@ : %@",[metaItem commonKey], [metaItem value]);
        }
    }
}


-(id) initWithMedia:(RichMedia *)aMedia{
    DLog();
    
	if ((self = [super initWithNibName:(IS_IPAD()?@"EmissionDefaultViewController_iPad":@"EmissionDefaultViewController") bundle:nil])) {
   		self.richMedia = aMedia;
		self.zoomFactor = DEFAULT_ZOOM_FACTOR;
	}
	if (self.richMedia.durationInSeconds!=0 && self.richMedia.durationInSeconds*self.zoomFactor > MAX_TIME_LINE_WIDTH) {
		self.zoomFactor = (float) MAX_TIME_LINE_WIDTH / (float) self.richMedia.durationInSeconds;
	}
    
    //TOC navigation bar button
    UIImage *extraImage =[UIImage imageNamed:@"addbookmark"];
    UIImage *tocImage =[UIImage imageNamed:@"icon_list_bullets"];

#pragma ACCESSIBILITY
    extraImage.accessibilityLabel = @"Afficher les extras";
    
#pragma ACCESSIBILITY
    tocImage.accessibilityLabel = @"Afficher les indexs";
    
    
	UIBarButtonItem *showTable = [[UIBarButtonItem alloc] initWithImage:tocImage style:UIBarButtonItemStyleBordered target:self action:@selector(showTableView)];
	UIBarButtonItem *showExtra = [[UIBarButtonItem alloc] initWithImage:extraImage style:UIBarButtonItemStyleBordered target:self action:@selector(showExtrasTableView)];
    showTable.accessibilityLabel = @"Afficher les indexs";
    showExtra.accessibilityLabel = @"Afficher les extras";
    //Segmented control avec extra si le media contient des extras
    if ([[self.richMedia extras] count]>0) {
        self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:showTable,showExtra, nil];       
    }else{
        self.navigationItem.rightBarButtonItem = showTable;
    }
    
    
    [self.richMedia printData];
    
    return self;
}

-(void) hideView{
    if(self.menu){
        [self.menu dismissPopoverAnimated:YES];
    }
    [self dismissModalViewControllerAnimated:YES];
    [self backToRootView];
}

- (void)manageGestureRecognizers{
    /*Player Controller View Gestures*/
    //1 tap On Player Controller View (playerControlView) : Show/hide or go to if tap on Bullet    
   	singleTapOnControlsPlayerView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap)];
	[singleTapOnControlsPlayerView setNumberOfTapsRequired:1];
	[singleTapOnControlsPlayerView setDelegate:self];
	[self.playerControlView addGestureRecognizer:singleTapOnControlsPlayerView];
	//1 swipe to the right -> previous segment
    slideToRightOnControlsPlayerView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSlideToPrevious)];
	[slideToRightOnControlsPlayerView setDirection:UISwipeGestureRecognizerDirectionRight];
	[self.playerControlView addGestureRecognizer:slideToRightOnControlsPlayerView];
	//1 swipe to the left -> next segment
	slideToLeftOnControlsPlayerView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSlideToNext)];
	[slideToLeftOnControlsPlayerView setDirection:UISwipeGestureRecognizerDirectionLeft];
	[self.playerControlView addGestureRecognizer:slideToLeftOnControlsPlayerView];
    //1 swipe up -> show player controls
    slideToTopOnControlsPlayerView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap)];
	[slideToTopOnControlsPlayerView setDirection:UISwipeGestureRecognizerDirectionUp];
	[self.playerControlView addGestureRecognizer:slideToTopOnControlsPlayerView];
	//1 swipe down -> hide player controls
	slideToBottomOnControlsPlayerView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap)];
	[slideToBottomOnControlsPlayerView setDirection:UISwipeGestureRecognizerDirectionDown];
	[self.playerControlView addGestureRecognizer:slideToBottomOnControlsPlayerView];
	//1 pinch -> zoom in/out Timeline
	zoomGestureRecognizerOnTimeLine = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handleZoomGesture)];
	[self.playerControlView addGestureRecognizer:zoomGestureRecognizerOnTimeLine];
    
    
    /*UIWebview*/
    //1 swipe left -> next segment
    UISwipeGestureRecognizer* leftSwipeRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSlideToNext)];
    leftSwipeRecognizer.numberOfTouchesRequired = 1;
    leftSwipeRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    leftSwipeRecognizer.cancelsTouchesInView = YES;
    [self.abstractView addGestureRecognizer:leftSwipeRecognizer];
    //1 swipe right -> previous segment
    UISwipeGestureRecognizer* rightSwipeRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSlideToPrevious)];
    rightSwipeRecognizer.numberOfTouchesRequired = 1;
    rightSwipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    rightSwipeRecognizer.cancelsTouchesInView = YES;
    [self.abstractView addGestureRecognizer:rightSwipeRecognizer];
    
    	
	/*Video view*/
    //1 swipe left -> next segment
	slideToRightOnVideoView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSlideToNext)];
	[slideToRightOnVideoView setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.videoView addGestureRecognizer:slideToRightOnVideoView];
    //1 swipe right -> previous segment
	slideToLeftOnVideoView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSlideToPrevious)];
	[slideToLeftOnVideoView setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.videoView addGestureRecognizer:slideToLeftOnVideoView];
    
    
    //2 Tap on illustration (illustrationView) <=> play/Pause
    UITapGestureRecognizer *doubleTapVideo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playPauseAction)];
	[doubleTapVideo setNumberOfTapsRequired:2];
	[doubleTapVideo setDelegate:self];
	[self.videoView addGestureRecognizer:doubleTapVideo];
    //1 Tap on Movie (videoView) <=> showActionInfo
    UITapGestureRecognizer *singleTapMovie = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleInfoView)];
	[singleTapMovie setNumberOfTapsRequired:1];
	[singleTapMovie setDelegate:self];
    [singleTapMovie requireGestureRecognizerToFail:doubleTapVideo];
	[self.videoView addGestureRecognizer:singleTapMovie];
    
    //Pinch <=> fullscreen
    UIPinchGestureRecognizer *zoomInVideoPinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchDetected:)];
    [self.videoView addGestureRecognizer:zoomInVideoPinch];

    /*Illustration View*/
    //1 swipe left -> next illustration
	UISwipeGestureRecognizer *slideToRightOnIllustrationView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(goToNextIllustration)];
	[slideToRightOnIllustrationView setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.illustrationView addGestureRecognizer:slideToRightOnIllustrationView];
    //1 swipe right -> previous illustration
	UISwipeGestureRecognizer *slideToLeftOnIllustrationView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(goToPreviousIllustration)];
	[slideToLeftOnIllustrationView setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.illustrationView addGestureRecognizer:slideToLeftOnIllustrationView];
    //1 swipe left 2 doigts-> next segment
	UISwipeGestureRecognizer *slide2fToRightOnIllustrationView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(nextAction)];
	[slide2fToRightOnIllustrationView setDirection:UISwipeGestureRecognizerDirectionLeft];
    [slide2fToRightOnIllustrationView setNumberOfTouchesRequired:2];
    [slide2fToRightOnIllustrationView requireGestureRecognizerToFail:slideToRightOnIllustrationView];
    [self.illustrationView addGestureRecognizer:slide2fToRightOnIllustrationView];
    //1 swipe right 2 doigts -> previous segment
	UISwipeGestureRecognizer *slide2fToLeftOnIllustrationView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(previousAction)];
	[slide2fToLeftOnIllustrationView setDirection:UISwipeGestureRecognizerDirectionRight];
    [slide2fToLeftOnIllustrationView setNumberOfTouchesRequired:2];
    [slide2fToLeftOnIllustrationView requireGestureRecognizerToFail:slideToLeftOnIllustrationView];
    [self.illustrationView addGestureRecognizer:slide2fToLeftOnIllustrationView];
    
    //2 Tap on illustration (illustrationView) <=> play/Pause
    UITapGestureRecognizer *doubleTapIllustration = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playPauseAction)];
	[doubleTapIllustration setNumberOfTapsRequired:2];
	[doubleTapIllustration setDelegate:self];
	[self.illustrationView addGestureRecognizer:doubleTapIllustration];
    //1 Tap on Illustration (illustrationView) <=> showActionInfo
    UITapGestureRecognizer *singleTapIllustration = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleInfoView)];
	[singleTapIllustration setNumberOfTapsRequired:1];
	[singleTapIllustration setDelegate:self];
    [singleTapIllustration requireGestureRecognizerToFail:doubleTapIllustration];
	[self.illustrationView addGestureRecognizer:singleTapIllustration];
    
    //Pinch <=> fullscreen
    UIPinchGestureRecognizer *zoomInPinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchDetected:)];
    [self.illustrationView addGestureRecognizer:zoomInPinch];
    
    
    
}
- (void)toggleInfoView{
    //On affiche pendant un bref instant les contrôles qui se masque automatiquement
    //BOOL landscape = self.view.bounds.size.width>self.view.bounds.size.height;
    if(![self isInFullscreenMode]){
        if(!playerIsExpanded || IS_IPAD())[self showActionInfo];
        return;
    }
    
        
    //EN plein ecran
    //on switch la visibilité du titre et des contrôle
       
    BOOL hidden = !self.fullscreenInfoView.hidden;
    
    self.fullscreenInfoView.hidden = hidden;
    self.playerControlView.hidden = hidden;
    if(!playerIsExpanded || IS_IPAD()){
        self.actionInfoView.hidden = hidden;
        [NSObject cancelPreviousPerformRequestsWithTarget: self.actionInfoView];
        [self.actionInfoView performSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES] afterDelay:3];
    }
    
    /*
    //En pause on ne fait rien d'autre
    if(!playing)return;

    if(self.fullscreenInfoView.hidden) return;
    
    //On les masque automatiquement au bout de 3 secondes si visibles
    [NSObject cancelPreviousPerformRequestsWithTarget: self.fullscreenInfoView];
    [self.fullscreenInfoView performSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES] afterDelay:3];

    [NSObject cancelPreviousPerformRequestsWithTarget: self.playerControlView];
    [self.playerControlView performSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES] afterDelay:3];
    
    [NSObject cancelPreviousPerformRequestsWithTarget: self.actionInfoView];
    [self.actionInfoView performSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES] afterDelay:3];
     */

}

-(void)toggleFullScreen{
    if (self.view.bounds.size.width>self.view.bounds.size.height && !IS_IPAD()){
        //LANDSCAPE : do nothing on iPhone
        return;
    }
    self.fullscreen = !self.fullscreen;
    if(self.fullscreen){
        self.playerControlView.hidden = self.actionInfoView.hidden = self.fullscreenInfoView.hidden = YES;
    }
    [self.toggleFullscreenButton setImage:[UIImage imageNamed:self.fullscreen?@"closeFullscreen":@"setFullscreen"] forState:UIControlStateNormal];
    [self viewWillLayoutSubviews];
}
- (void)pinchDetected:(UIPinchGestureRecognizer *)pinchRecognizer{
    if (self.view.bounds.size.width>self.view.bounds.size.height && !IS_IPAD()){
        //LANDSCAPE : do nothing on iPhone
        return;
    }
    CGFloat scale = pinchRecognizer.scale;
    pinchRecognizer.scale = 1.0;
    if(!self.fullscreen){
        if(scale>1.0){
           self.fullscreen = YES;
            self.playerControlView.hidden = self.actionInfoView.hidden = self.fullscreenInfoView.hidden = YES;
           [self viewWillLayoutSubviews];
        }
    }else{
        if(scale<1.0){
            self.fullscreen = NO;
            [self viewWillLayoutSubviews];
        }
    }

}
- (void)goToNextIllustration{
    Segment *seg = [self.richMedia getCurrentSegmentObject];
    int currentIllustrationIndex = [seg illustrationIndexAtTime:[self getPlayerCurrentTime]];
    DLog(@"currentIllustrationIndex:%d", currentIllustrationIndex);
    Illustration *tmp = [seg illustrationAtTime:[self getPlayerCurrentTime]];
    DLog(@"illustration Before : %@ %f to %f",  tmp.resource.refUri, tmp.start, tmp.end);
    Illustration *next = [seg nextIllustration:currentIllustrationIndex];
    
    DLog(@"\n\n %d / %d / %d",self.richMedia.currentChapter, self.richMedia.currentSegmentInChapter, self.richMedia.currentSegment);

    DLog(@"illustration After : %@ %f to %f", next.resource.refUri, next.start, next.end);
    if(next){//next illustration in the same segment
        BOOL wasPlaying = playing;
        if(wasPlaying)[self playPauseAction];
        [self goToSegment:seg atTime:next.start+0.05 continuous:NO];
        
	
        [self updateTimeLine];
        [self updateData:NO];
        [self updateTimeLabels];
        if(!playerIsExpanded)[self showActionInfo];
        else {
            float secStep =(self.timeLineBkgImageView.frame.size.width) / self.richMedia.durationInSeconds;
            self.scrollView.contentOffset = CGPointMake(-self.view.frame.size.width/2 + secStep*[self getPlayerCurrentTime], 0.0);
            [self setCurrentTimeValueForFullPlayer];
        }
        [self handleAudioOrVideoViewsConfiguration];
        if(wasPlaying)[self playPauseAction];
        
        DLog(@"\n\n %d / %d / %d",self.richMedia.currentChapter, self.richMedia.currentSegmentInChapter, self.richMedia.currentSegment);

        
    }
    else{
        DLog(@"Going to next Segment");
        [self nextAction];
    }
        
}
- (void)goToPreviousIllustration{
    Segment *seg = [self.richMedia getCurrentSegmentObject];
    int currentIllustrationIndex = [seg illustrationIndexAtTime:[self getPlayerCurrentTime]];
    Illustration *previous = [seg previousIllustration:currentIllustrationIndex];
    DLog(@"currentIllustrationIndex:%d", currentIllustrationIndex);
    DLog(@"previous Illustration:%@", previous.resource.refUri);
    
    BOOL wasPlaying = playing;
    
    
    if(previous){//There is a previous illustration in the same segment
        if(wasPlaying)[self playPauseAction];
        [self goToSegment:seg atTime:previous.start continuous:NO];
        
        [self updateTimeLine];
        [self updateData:NO];
        [self updateTimeLabels];
        if(!playerIsExpanded)[self showActionInfo];
        else {
            float secStep =(self.timeLineBkgImageView.frame.size.width) / self.richMedia.durationInSeconds;
            self.scrollView.contentOffset = CGPointMake(-self.view.frame.size.width/2 + secStep*[self getPlayerCurrentTime], 0.0);
            [self setCurrentTimeValueForFullPlayer];
        }
        [self handleAudioOrVideoViewsConfiguration];
        if(wasPlaying)[self playPauseAction];
        DLog(@"A - \n\n %d / %d / %d",self.richMedia.currentChapter, self.richMedia.currentSegmentInChapter, self.richMedia.currentSegment);

    }
    else{
        //Get last illustration of previous segment
        
        //1 previous segment :
        if (self.richMedia.currentSegmentInChapter > 0) {//on reste dans le même chapitre
            if(wasPlaying)[self playPauseAction];
            self.richMedia.currentSegmentInChapter --;
            [self.richMedia computeCurrentSegment];
            DLog(@"B \n\n %d / %d / %d",self.richMedia.currentChapter, self.richMedia.currentSegmentInChapter, self.richMedia.currentSegment);

            
        }else if (self.richMedia.currentChapter > 0 ) {//on va dans le dernier segment du chapitre précédent
            if(wasPlaying)[self playPauseAction];
            DLog(@"C BEFORE \n\n %d / %d / %d",self.richMedia.currentChapter, self.richMedia.currentSegmentInChapter, self.richMedia.currentSegment);

            self.richMedia.currentChapter -- ;
            Chapter *prevChapter = [[self.richMedia chapters] objectAtIndex:self.richMedia.currentChapter];
            self.richMedia.currentSegmentInChapter = [prevChapter.segments count] - 1;
            [self.richMedia computeCurrentSegment];
            DLog(@"C AFTER \n\n %d / %d / %d",self.richMedia.currentChapter, self.richMedia.currentSegmentInChapter, self.richMedia.currentSegment);

        }else{
            [self previousAction];
            return;
        }
        
        Segment *segToGoTo = [self.richMedia getCurrentSegmentObject];
        previous = [segToGoTo lastIllustration];
        
        
        if(previous){//There is a previous illustration in the same segment
            [self goToSegment:segToGoTo atTime:previous.start continuous:NO];
            
            [self updateTimeLine];
            [self updateData:NO];
            [self updateTimeLabels];
            if(!playerIsExpanded)[self showActionInfo];
            else {
                float secStep =(self.timeLineBkgImageView.frame.size.width) / self.richMedia.durationInSeconds;
                self.scrollView.contentOffset = CGPointMake(-self.view.frame.size.width/2 + secStep*[self getPlayerCurrentTime], 0.0);
                [self setCurrentTimeValueForFullPlayer];
            }
            [self handleAudioOrVideoViewsConfiguration];
            if(wasPlaying)[self playPauseAction];

        }else{
            if(wasPlaying)[self playPauseAction];

            [self previousAction];
        }
    }
}



- (void)configureActionInfoView{
    self.actionInfoView.alpha = 0.8f;

	//Enable maskstobound so that corner radius would work.
	[self.actionInfoView.layer setMasksToBounds:YES];
	//Set the corner radius
	//[self.actionInfoView.layer setCornerRadius:10.0];
	//Set the border color
	[self.actionInfoView.layer setBorderColor:[[UIColor blackColor] CGColor]];
	//Set the image border
	[self.actionInfoView.layer setBorderWidth:1.0];
	self.actionInfoView.userInteractionEnabled = YES;
}

- (void)initializeHTMLModels{
    NSString *path = [[NSBundle mainBundle] pathForResource :@"SegmentModel" ofType:@"html"];
	self.htmlContentModel = [[NSString alloc] initWithContentsOfFile:path
													   encoding:NSUTF8StringEncoding
														  error:nil];
	baseURL = [[NSURL alloc] initFileURLWithPath:path];
    self.htmlReferenceModel = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource :@"referenceModel" ofType:@"html"]
                                                              encoding:NSUTF8StringEncoding
                                                                 error:nil];
    if(IS_IPAD()){
        self.htmliPadSegmentModel               = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource :@"iPad-SegmentModel" ofType:@"html"] encoding:NSUTF8StringEncoding error:nil];
        self.htmliPadComplementsModel           = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource :@"iPad-ComplementsListModel" ofType:@"html"] encoding:NSUTF8StringEncoding error:nil];
        self.htmliPadReferencesModel            = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource :@"iPad-ReferencesListModel" ofType:@"html"] encoding:NSUTF8StringEncoding error:nil];
        self.htmliPadSegmentDescriptionModel    = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource :@"iPad-SegmentDescriptionModel" ofType:@"html"] encoding:NSUTF8StringEncoding error:nil];
    }
    
    
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    DLog();
    [super viewDidLoad];
	self.title = self.richMedia.title;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Fermer" style:UIBarButtonItemStyleBordered target:self action:@selector(hideView)];
	self.navigationItem.leftBarButtonItem = backButton;
    
	playing = NO;
	 [self.richMedia printData];
    self.abstractView.delegate = self;
	self.scrollView.delegate=self;
    self.scrollView.userInteractionEnabled = NO;

    
	//L&F
    ConfigurationManager *cm = [ConfigurationManager sharedInstance];
    self.currentTimeFullPlayerLabel.backgroundColor = [cm playerTimeLabelBackground];
    self.currentTimeFullPlayerLabel.textColor = [cm playerTimeLabelForeground];
    self.currentTimeFullPlayerLabel.shadowColor = [cm playerTimeLabelShadow];
    self.currentTimeFullPlayerLabel.layer.borderColor = [[cm playerTimeLabelBorder] CGColor];
    self.currentTimeFullPlayerLabel.layer.borderWidth = 2;
	[self.abstractView setBackgroundColor:[UIColor clearColor]];

	playerControlView.backgroundColor = [cm getExtendedPlayerBackgroundColor];
	topView.backgroundColor = [cm getPlayerMainViewBackgroundColor];
	
    [self handleAudioOrVideoViewsConfiguration];
	
    [self initializeHTMLModels];
    
	
    //AUDIO
    //if(self.richMedia.webMediaVersion == WEB_MEDIA_1){
    //    [self handlePlayerForWebMedia1];
    //}else{//AUDIO & VIDEO
    //    [self handlePlayerForWebMedia2];
    //}
	//Used to indicate playback availability
    
    
    if(self.richMedia.durationInSeconds>0){
        [self goToSegment:[self.richMedia getCurrentSegmentObject] atTime:0 continuous:NO];
        [[self.richMedia getCurrentSegmentObject] printSegment];
    }
    AVAudioSession *session = [AVAudioSession sharedInstance];
	[session setActive:TRUE error:nil];
	[session setCategory:AVAudioSessionCategoryPlayback error:nil];
	
	
	//Gesture Recognizer
    [self manageGestureRecognizers];
	    
	if(self.richMedia.durationInSeconds>0){
        //Set initial chapter/segment timeline
        float secStep = (self.timeLineBkgImageView.frame.size.width) / self.richMedia.durationInSeconds;
        float startX = 0;
        float startY = 0;
        for(int i=0, n = [[self.richMedia chapters] count]; i<n; i++){
		
            Chapter *chap = [[self.richMedia chapters] objectAtIndex:i];
            ChapterView *cv = [[ChapterView alloc]
						   initWithFrame:CGRectMake(startX, startY, (secStep*(chap.end-chap.start)),(int)( self.timeLineBkgImageView.frame.size.height))
						   andIndex:i];
            [self.timeLineBkgImageView addSubview:cv];
        
            BOOL addNumbers = [cm addAutoNumberingToChapitreAndSegmentName];
            if(addNumbers){
                [cv.textLabel setText:[NSString stringWithFormat:@"%d. %@",(i+1),chap.title]];
            }else{
                [cv.textLabel setText:chap.title];
            }
            [cv setTag:100+i];
		
            startX+=cv.frame.size.width;
        }
    }
		
	//configure the action information view
	[self configureActionInfoView];
    
    //set up the time marker view
	timeMarkerView = [[TimeMarkerView alloc] initWithFrame:CGRectMake(0, -10, self.richMedia.durationInSeconds*self.zoomFactor , 10) andDuration:self.richMedia.durationInSeconds];
	[self.scrollView addSubview:timeMarkerView];
	timeMarkerView.hidden=YES;
		
	
    //Load illustrations images in background thread
    [NSThread detachNewThreadSelector:@selector(loadIllustrationImages) toTarget:self withObject:nil];
	
    //Set up fullscreen view
	//fullScreenImageView = [[FullScreenViewController alloc] initWithNibName:@"FullScreen" bundle:nil];
	//fullScreenImageView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    
   
    if(IS_IPAD()){        
        self.webViewsContainer.hidden = NO;
        self.webViewsContainer.backgroundColor =
        self.leftWebView.backgroundColor =
        self.centerWebView.backgroundColor =
        self.rightWebView.backgroundColor =[[ConfigurationManager sharedInstance] getPlayerMainViewBackgroundColor];
    
    }
    self.view.backgroundColor =
            
            self.abstractView.backgroundColor =
            self.contentView.backgroundColor =
            self.topView.backgroundColor =
            
            [[ConfigurationManager sharedInstance] getPlayerMainViewBackgroundColor];
    
    
    self.illustrationView.backgroundColor = [UIColor blackColor];
    
    self.navigationController.navigationBar.translucent = NO;
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        [self navigationController].navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
    }else{
        self.navigationController.navigationBar.tintColor = [UIColor blueColor];
    }
}

-(void)test{
    //[self handleWebMedia2SegmentEnd];
}

- (void)setTimeOberversForSegment:(Segment *)segment{
    EmissionDefaultViewController *weakself = self;
    playerIllustrationTimeObserver= [audioPlayer addBoundaryTimeObserverForTimes:[segment illustrationBeginTimeValues]
                                                                           queue:NULL
                                                                      usingBlock:^{
                                                                          [weakself updateIllustrationImage];
                                                                      }];
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(test)
     name:AVPlayerItemDidPlayToEndTimeNotification
     object:audioPlayer.currentItem];
    
    
    DLog(@"segment.clipEndUnrounded:%f", segment.clipEndUnrounded);
    DLog(@"segment.clipEndRounded:%lld", CMTimeMake(segment.clipEndUnrounded, 1.0f).value);

    playerSegmentChangeObserver = [audioPlayer addBoundaryTimeObserverForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:CMTimeMake(segment.clipEndUnrounded*10, 10.0f)]]
                                                                         queue:NULL
                                                                    usingBlock:^{
                                                                        DLog(@"Playing ? : %d",([weakself.audioPlayer rate] != 0.0));
                                                                        [weakself handleWebMedia2SegmentEnd];
                                                                    }];
    
    playerSecondObserver = [audioPlayer addPeriodicTimeObserverForInterval:CMTimeMake(1, 1)
                                                                     queue:NULL
                                                                usingBlock:^(CMTime time){
                                                                    [weakself updateTimeLineWithTime:time];
                                                                    [weakself updateTimeLabelsWithTime:time];
                                                                    
                                                                }];
}


- (void)removeAndCleanTimeObservers{

    [self.audioPlayer removeTimeObserver:self.playerIllustrationTimeObserver];
    [self.audioPlayer removeTimeObserver:self.playerSecondObserver];
    [self.audioPlayer removeTimeObserver:self.playerSegmentChangeObserver];
    self.playerIllustrationTimeObserver =nil;
    self.playerSecondObserver = nil;
    self.playerSegmentChangeObserver = nil;
}

- (void)handleAudioOrVideoViewsConfiguration{   
    if ([self checkIfVideo]) {
        self.videoView.hidden=NO;
        self.illustrationView.hidden=YES;
    }else{
        self.videoView.hidden=YES;
        self.illustrationView.hidden=NO;
    }
}
- (void)handleSingleTapOnIllustration{
    [self playPauseAction];
}
- (void)handleSingleTapOnMovie{
    [self playPauseAction];
}
- (BOOL)checkIfVideo{
    if(self.richMedia.webMediaVersion == WEB_MEDIA_1){
        return NO;
    }
    Segment *cSegment = [self.richMedia getCurrentSegmentObject];
    return ([cSegment hasVideo]);
}


- (void)goToSegment:(Segment*)segment atTime:(float)time continuous:(BOOL)continuous{
    DLog();
    
    NSString *mp3 = [NSString stringWithFormat:@"%@%@",self.richMedia.base,segment.mediaUri];
    NSMutableString *mp3bis = [NSMutableString stringWithString:mp3];
    [mp3bis replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [mp3 length])];
    NSURL *url = [NSURL fileURLWithPath:mp3bis];
    DLog(@"\n\n.mp3:%@\n\nmp3bis:%@\n\n.url:%@\n\n",mp3,mp3bis,url);
    
    [self removeAndCleanTimeObservers];
    
    
    [self handleAudioOrVideoViewsConfiguration];
    if ([self checkIfVideo]) {
        if(![[self.playerCurrentSegment mediaUri] isEqual:[segment mediaUri]]){
            self.audioPlayer = nil;
            [self createAndConfigureMoviePlayerWithURL:url sourceType:MPMovieSourceTypeFile];
        }
    }else{
        [self.videoView setPlayer:nil];
        if(![[self.playerCurrentSegment mediaUri] isEqual:[segment mediaUri]]){
            [self.audioPlayer replaceCurrentItemWithPlayerItem:nil];
            self.audioPlayer = nil;
            self.audioPlayer = [[AVPlayer alloc] initWithPlayerItem:[AVPlayerItem playerItemWithURL:url]];
        }
    }
    self.playerCurrentSegment = segment;
        
    CMTime cmt = CMTimeMake((float)(segment.clipBegin+time-segment.start)*100.0, 100.0);
    
    if(!continuous){
        [audioPlayer seekToTime:cmt toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    }else{
        //nothing
    }
    
    [self setTimeOberversForSegment:segment];
    [self updateData:YES];
    

}

- (void)handleWebMedia2SegmentEnd{
    DLog(@"handleWebMedia2SegmentEnd  FIN SEGMENT, Auto Stop : %d",self.richMedia.autoStop);
    [self.audioPlayer removeTimeObserver:self.playerIllustrationTimeObserver];
    self.playerIllustrationTimeObserver=nil;
    [self.audioPlayer removeTimeObserver:self.playerSecondObserver];
    self.playerSecondObserver=nil;
    [self.audioPlayer removeTimeObserver:self.playerSegmentChangeObserver];
    self.playerSegmentChangeObserver = nil;
    
    if(self.richMedia.autoStop){
        [self pausePlayer];
    }
    
    Segment *previousSegment = [self.richMedia getCurrentSegmentObject];
    Segment *seg = [self.richMedia nextSegment];

    if(seg){
        [seg printSegment];
        BOOL continuous = ([[previousSegment.mediaUri lowercaseString] isEqualToString:[seg.mediaUri lowercaseString]] && previousSegment.clipEnd==seg.clipBegin);
        
        [self goToSegment:seg atTime:seg.start continuous:continuous];
        [audioPlayer play];
        playing = YES;
        //[self updateData:NO];
        return;
    }
    
    
    //SEG IS NULL
    //Fin de la lecture;
    
    DLog(@"Next segment is nil");
    [self pausePlayer];
    
    
    //[self seekPlayerToTime:0];
    self.richMedia.currentChapter = 0;
    self.richMedia.currentSegment = 0;
    self.richMedia.currentSegmentInChapter = 0;
    
    [self updateData:YES];
    self.activityIndicator.hidden = YES;
    self.messageLabel.hidden=NO;
    NSInvocation* invoc = [NSInvocation invocationWithMethodSignature:[self.messageLabel methodSignatureForSelector:@selector(setHidden:)]];
    [invoc setTarget:self.messageLabel];
    [invoc setSelector:@selector(setHidden:)];
    BOOL yes = YES;
    
    [invoc setArgument:&yes atIndex:2];//indices 0 and 1 indicate the hidden arguments self and _cmd, respectively.
    [invoc performSelector:@selector(invoke) withObject:nil afterDelay:3];
    
    [self removeAndCleanTimeObservers];

    [self goToSegment:0 inChapter:0];
    
}


/*
- (void)handlePlayerForWebMedia2{
     EmissionDefaultViewController *weakself = self;
   
    Segment *firstSegment = [[[[self.richMedia chapters] objectAtIndex:0] segments] objectAtIndex:0];
    
    NSString *mp3 = [NSString stringWithFormat:@"%@%@",self.richMedia.base,firstSegment.mediaUri];
    NSMutableString *mp3bis = [NSMutableString stringWithString:mp3];
    [mp3bis replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [mp3 length])];
    NSURL *url = [NSURL fileURLWithPath:mp3bis];
    
 
    if(!audioPlayer){
        audioPlayer = [[AVPlayer alloc] initWithPlayerItem:[AVPlayerItem playerItemWithURL:url]];
        self.playerCurrentSegment = firstSegment;
        if([self checkIfVideo]){
             [self.videoView setPlayer:audioPlayer];
            self.videoView.hidden = NO;
            self.illustrationView.hidden = YES;

        }else{
             self.videoView.hidden = YES;
            self.illustrationView.hidden = NO;

        }
       
        self.playerSecondObserver = [audioPlayer addPeriodicTimeObserverForInterval:CMTimeMake(1, 1)
                                                                         queue:NULL 
                                                                    usingBlock:^(CMTime time){
                                                                        [weakself updateTimeLineWithTime:time];
                                                                        [weakself updateTimeLabelsWithTime:time];
                                                                    }];

        self.playerIllustrationTimeObserver= [audioPlayer addBoundaryTimeObserverForTimes:[firstSegment illustrationBeginTimeValues]
                                                                               queue:NULL
                                                                          usingBlock:^{
                                                                              [weakself updateIllustrationImage];
                                                                          }];
        
        
        self.playerSegmentChangeObserver = [audioPlayer addBoundaryTimeObserverForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:CMTimeMake(firstSegment.end*10, 10)]]
                                                                             queue:NULL
                                                                        usingBlock:^{
                                                                            [weakself handleWebMedia2SegmentEnd];
                                                                        }];
        
    }
}
*/
/*
- (void)handlePlayerForWebMedia1{
    
    EmissionDefaultViewController *weakself = self;
    
    NSString *mp3 = [NSString stringWithFormat:@"%@%@",self.richMedia.base,[self.richMedia mediaUri]];
    NSMutableString *mp3bis = [NSMutableString stringWithString:mp3];
    [mp3bis replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [mp3 length])];
    NSURL *url = [NSURL fileURLWithPath:mp3bis]; 
    if(!audioPlayer){
        audioPlayer = [[AVPlayer alloc] initWithPlayerItem:[AVPlayerItem playerItemWithURL:url]];
        NSMutableArray *illustrationsTime = [[NSMutableArray alloc] initWithCapacity:0];
        
        //collect illustration begining time
        for(int i=0, n = [[self.richMedia chapters] count]; i< n; i++){
            Chapter *chap = [[self.richMedia chapters] objectAtIndex:i];
            for(int j=0, m = [chap.segments count]; j< m; j++){
                Segment *seg = [chap.segments objectAtIndex:j];
                for(int k=0,l = [seg.illustrations count]; k<l; k++){
                    Illustration *illustration = [seg.illustrations objectAtIndex:k];
                    [illustrationsTime addObject:[NSValue valueWithCMTime:CMTimeMake(illustration.start*10, 10)]];
                }
            }
        }
        
        self.playerIllustrationTimeObserver= [audioPlayer addBoundaryTimeObserverForTimes:illustrationsTime
                                                                               queue:NULL
                                                                          usingBlock:^{
                                                                              //[weakself updateTimeLine];
                                                                              [weakself updateData:YES];
                                                                          }];
        self.playerSecondObserver = [audioPlayer addPeriodicTimeObserverForInterval:CMTimeMake(1, 1)
                                                                         queue:NULL 
                                                                    usingBlock:^(CMTime time){
                                                                        float t=CMTimeGetSeconds([weakself.audioPlayer currentTime]);
                                                                        
                                                                        if(t>= richMedia.durationInSeconds){
                                                                            [weakself pausePlayer];
                                                                            if(weakself.fullScreenImageView){
                                                                                [weakself.fullScreenImageView showEndMessage];
                                                                            } 
                                                                            [weakself seekPlayerToTime:0];
                                                                            weakself.richMedia.currentChapter = 0;
                                                                            weakself.richMedia.currentSegment = 0;
                                                                            weakself.richMedia.currentSegmentInChapter = 0;
                                                                            
                                                                            [weakself updateData:YES];
                                                                            
                                                                            weakself.activityIndicator.hidden = YES;
                                                                            weakself.messageLabel.hidden=NO;
                                                                            NSInvocation* invoc = [NSInvocation invocationWithMethodSignature:[weakself.messageLabel methodSignatureForSelector:@selector(setHidden:)]];
                                                                            [invoc setTarget:weakself.messageLabel];
                                                                            [invoc setSelector:@selector(setHidden:)];
                                                                            BOOL yes = YES;
                                                                            [invoc setArgument:&yes atIndex:2];
                                                                            [invoc performSelector:@selector(invoke) withObject:nil afterDelay:3];
                                                                        }
                                                                        
                                                                        [weakself updateTimeLineWithTime:time];
                                                                        [weakself updateTimeLineWithTime:time];
                                                                        
                                                                    }];
    }

}
*/
- (void)pausePlayer{
    
        [self.audioPlayer pause];
    
        [self.playPauseButton setImage:[UIImage imageNamed:@"play32"] forState:UIControlStateNormal];
        [self.playPauseButton setNeedsDisplay];
        playing = !playing;
    /*
    if(self.fullScreenImageView){
            self.fullScreenImageView.playing=playing;
            [self.fullScreenImageView.playPauseButton setImage:[UIImage imageNamed:(playing?@"pause32":@"play32")] forState:UIControlStateNormal];
            [self.fullScreenImageView.playPauseButton setNeedsDisplay];
        }
    */
}

/*
-(void) updateFullScreenView{
 	if(fullScreenImageView){
		Illustration *illustration = [[self.richMedia getCurrentSegmentObject] illustrationAtTime:[self getPlayerCurrentTime]];
		fullScreenImageView.image.image =  [UIImage imageWithContentsOfFile:[RichMedia getIllustrationFullPath:illustration forMedia:self.richMedia]] ;
        [fullScreenImageView setTextChapter:[self.richMedia getCurrentChapterObject].title andSegment:[[richMedia getCurrentSegmentObject] title]];
	}
}
*/
- (void)updateIllustrationImage{
    float currentTime = [self getPlayerCurrentTime];
    DLog(@"at currentTime : %f", currentTime);
    
    Chapter *chapter = [[self.richMedia chapters] objectAtIndex:self.richMedia.currentChapter];
	Segment *segment = [chapter.segments objectAtIndex:self.richMedia.currentSegmentInChapter];
	Illustration *illustration = [segment illustrationAtTime:currentTime];
    NSString *imgPath = [RichMedia getIllustrationFullPath:illustration forMedia:self.richMedia];
    UIImage * toImage = imgPath==nil?([UIImage imageNamed:@"noImageAvailable"]):[UIImage imageWithContentsOfFile:imgPath];
    if(!IS_IPAD()){        
        float mediaWidth = self.contentView.bounds.size.width;
        float actionInfoViewHeight = illustration.resource.title ? 58 : 44 ;
        
        if([self isInFullscreenMode]){
            self.actionInfoView.frame = CGRectMake(0, self.playerControlView.frame.origin.y-actionInfoViewHeight, mediaWidth, actionInfoViewHeight);
        }else{
            self.actionInfoView.frame = CGRectMake(0, self.videoView.frame.origin.y + self.videoView.frame.size.height-actionInfoViewHeight, mediaWidth, actionInfoViewHeight);
        }

    }
    
    [UIView transitionWithView:self.illustrationView
                      duration:0.33f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.illustrationImageView.image = toImage;
                    } completion:^(BOOL finished) {
                        self.legendIllustrationLabel.text = illustration.resource.title?:@"";
                        
                    }];
}

- (void)updateFullScreenInformationView:(Segment *)segment{
    
}
-(void)forceUpdate{
    [self updateData:YES];
    if(playerIsExpanded){
        [self setFullPlayerView];
    }else{
        [self setMiniPlayerView];
    }
}
-(void) updateData:(BOOL)andBullet{

	if([[self.richMedia chapters] count]==0 || self.richMedia.currentChapter==-1)return;
		
	Chapter *chapter = [[self.richMedia chapters] objectAtIndex:self.richMedia.currentChapter];
	Segment *segment = [chapter.segments objectAtIndex:self.richMedia.currentSegmentInChapter];
	//Illustration *illustration = [segment illustrationAtTime:[self getPlayerCurrentTime]];
	
	
	NSMutableString *htmlContent= IS_IPAD() ? [NSMutableString stringWithString:self.htmliPadSegmentModel] : [NSMutableString stringWithString:self.htmlContentModel];
    
    NSMutableString *iPadHtmlComplementsString;
    NSMutableString *iPadHtmlReferencesString;
    NSMutableString *iPadHtmlSegmentDescriptionString;

    
    BOOL addNumbers = [[ConfigurationManager sharedInstance] addAutoNumberingToChapitreAndSegmentName];
    //NSString *info ;
    
    NSString *chapterTitle;
    NSString *segmentTitle;
    NSString *tmp = ([segment title] && (![segment.title isEqualToString:[chapter title]]))?[segment title]:@"";
    
    if(addNumbers){
        chapterTitle = [NSString stringWithFormat:@"%d - %@",self.richMedia.currentChapter+1, [chapter title]?[chapter title]:@""];
        segmentTitle = [NSString stringWithFormat:@"%d - %@",self.richMedia.currentSegmentInChapter+1, tmp];
    }else{
        chapterTitle = [chapter title]?[chapter title]:@"";
        segmentTitle = tmp;
    }
	[htmlContent replaceOccurrencesOfString:@"CHAPTER_TITLE"
								 withString:chapterTitle
									options:NSCaseInsensitiveSearch 
									  range:NSMakeRange(0, [htmlContent length]) ];

	[htmlContent replaceOccurrencesOfString:@"SEGMENT_TITLE" 
								 withString:segmentTitle
									options:NSCaseInsensitiveSearch 
									  range:NSMakeRange(0, [htmlContent length]) ];
    
    
    [htmlContent replaceOccurrencesOfString:@"SEGMENT_DESCRIPTION"
								 withString:([segment short_description]?[segment short_description]:@"") 
									options:NSCaseInsensitiveSearch 
									  range:NSMakeRange(0, [htmlContent length]) ];
    
    if(IS_IPAD()){
        iPadHtmlSegmentDescriptionString = [NSMutableString stringWithString:self.htmliPadSegmentDescriptionModel];
        [iPadHtmlSegmentDescriptionString replaceOccurrencesOfString:@"SEGMENT_DESCRIPTION"
                                      withString:([segment short_description]?[segment short_description]:@"")
                                         options:NSCaseInsensitiveSearch
                                           range:NSMakeRange(0, [iPadHtmlSegmentDescriptionString length]) ];
    }
    
	
	self.fullScreenSubtitleLabel.text = segmentTitle;
    self.fullscreenTitleLabel.text = chapterTitle;
    CGRect rframe = self.fullscreenInfoView.frame;
    self.fullscreenInfoView.frame = CGRectMake(rframe.origin.x, rframe.origin.y, rframe.size.width, [segmentTitle isEqualToString:@""]?38:57);
    
    //Set illustration Image
    [self updateIllustrationImage];
    //Set Bullet image
    if (andBullet) {
        NSString *info;
        if(addNumbers)
            info = [NSString stringWithFormat:@"%d - %@",self.richMedia.currentChapter+1, chapter.title];
        else
            info = chapter.title;
        
        if([segment title] && ([chapter.title compare:segment.title] !=NSOrderedSame)){
            if(addNumbers){
                self.bulletLabel.text = [NSString stringWithFormat:@"%@\n%d.%d - %@",info,self.richMedia.currentChapter+1,self.richMedia.currentSegmentInChapter+1,[segment title]];
            }else{
                self.bulletLabel.text = [NSString stringWithFormat:@"%@\n%@",info,[segment title]];
            }
        }else{
            self.bulletLabel.text = [NSString stringWithFormat:@"%@\n",info];
        }
        
        self.bulletImageView.image = [self illustrationImageForTime:[self getPlayerCurrentTime]];
        if(self.bulletImageView.image){
            CGRect rIv = self.bulletImageView.frame;
            self.bulletLabel.frame = IS_IPAD() ? CGRectMake(rIv.origin.x + rIv.size.width + 10,148,self.playerControlView.frame.size.width-2*rIv.origin.x - rIv.size.width ,100) : CGRectMake(128,125,self.playerControlView.frame.size.width-128-40,83);
            self.bulletImageView.hidden = NO;
            self.bulletLabel.textAlignment = UITextAlignmentLeft;
            self.bulletLabel.font = IS_IPAD() ? [UIFont boldSystemFontOfSize:14] : [UIFont systemFontOfSize:12];
        }
        else{
            self.bulletLabel.frame = IS_IPAD() ? CGRectMake(25,148,self.playerControlView.frame.size.width-50,100) : CGRectMake(25,125,self.playerControlView.frame.size.width-50,83);
            self.bulletImageView.hidden = YES;
            self.bulletLabel.textAlignment = UITextAlignmentCenter;
            self.bulletLabel.font = [UIFont boldSystemFontOfSize:IS_IPAD() ? 16 : 14];
        }
	}
    
	[htmlContent replaceOccurrencesOfString:@"IMAGE_VISIBLE"
								 withString:@"none"
									options:NSCaseInsensitiveSearch
									  range:NSMakeRange(0, [htmlContent length]) ];
	
	//complement
	[htmlContent replaceOccurrencesOfString:@"DISPLAY_COMPLEMENT_BLOCK" 
									 withString:(([[segment complements] count]>=1 || [segment.long_description length]>=1)?@"block":@"none") 
										options:NSCaseInsensitiveSearch 
										  range:NSMakeRange(0, [htmlContent length]) ];
	if(IS_IPAD()){
        iPadHtmlComplementsString = [NSMutableString stringWithString:self.htmliPadComplementsModel];
        [iPadHtmlComplementsString replaceOccurrencesOfString:@"DISPLAY_COMPLEMENT_BLOCK"
                                                    withString:(([[segment complements] count]>=1 || [segment.long_description length]>=1)?@"block":@"none")
                                                       options:NSCaseInsensitiveSearch
                                                         range:NSMakeRange(0, [iPadHtmlComplementsString length]) ];
        
        
    }
    NSMutableString *subtitleLandscapeInfo=[[NSMutableString alloc] initWithCapacity:0];
    
	if([[segment complements] count]>=1){
		if([[segment complements] count]==1){
			[htmlContent replaceOccurrencesOfString:@"COMPLEMENT_NUMBER" 
										 withString:[NSString stringWithFormat:@"1 %@",[[[ConfigurationManager sharedInstance] singleComplementString ]lowercaseString]]
											options:NSCaseInsensitiveSearch 
											  range:NSMakeRange(0, [htmlContent length]) ];
            if(IS_IPAD()){
                [iPadHtmlComplementsString replaceOccurrencesOfString:@"COMPLEMENT_NUMBER"
                                             withString:[NSString stringWithFormat:@"1 %@",[[[ConfigurationManager sharedInstance] singleComplementString ]lowercaseString]]
                                                options:NSCaseInsensitiveSearch
                                                  range:NSMakeRange(0, [iPadHtmlComplementsString length]) ];
            }
            [subtitleLandscapeInfo appendFormat:@"(1 %@",[[[ConfigurationManager sharedInstance] singleComplementString] lowercaseString]];
        }
		else{
			[htmlContent replaceOccurrencesOfString:@"COMPLEMENT_NUMBER" 
										 withString:[NSString stringWithFormat:@"%d %@",[[segment complements] count],[[[ConfigurationManager sharedInstance] multipleComplementsString] lowercaseString]]
											options:NSCaseInsensitiveSearch 
											  range:NSMakeRange(0, [htmlContent length]) ];
            if(IS_IPAD()){
                [iPadHtmlComplementsString replaceOccurrencesOfString:@"COMPLEMENT_NUMBER"
                                             withString:[NSString stringWithFormat:@"%d %@",[[segment complements] count],[[[ConfigurationManager sharedInstance] multipleComplementsString] lowercaseString]]
                                                options:NSCaseInsensitiveSearch
                                                  range:NSMakeRange(0, [iPadHtmlComplementsString length]) ];
            }
            [subtitleLandscapeInfo appendFormat:@"(%d %@",[[segment complements] count],[[[ConfigurationManager sharedInstance] shortComplementString] lowercaseString] ];
        }
		
		Complement *firstComplement = [[segment complements] objectAtIndex:0];
		NSMutableString *listComplements = [NSMutableString stringWithFormat:@"<li class=\"first\"><a href=\"showComplement:0\">%@</a></li>",
											[firstComplement title]];
		for (int i=1, n=[[segment complements] count];i<n; i++) {
			Complement *comp = [[segment complements] objectAtIndex:i];
			[listComplements appendString:[NSMutableString stringWithFormat:@"<li><a href=\"showComplement:%d\">%@</a></li>",
										   i,
										   [comp title]]];
		}
		[htmlContent replaceOccurrencesOfString:@"COMPLEMENTS_LIST" 
									 withString:listComplements 
										options:NSCaseInsensitiveSearch 
										  range:NSMakeRange(0, [htmlContent length]) ];
        if(IS_IPAD()){
            [iPadHtmlComplementsString replaceOccurrencesOfString:@"COMPLEMENTS_LIST"
                                         withString:listComplements
                                            options:NSCaseInsensitiveSearch
                                              range:NSMakeRange(0, [iPadHtmlComplementsString length]) ];
        }
		
	}else{
        //self.moviePlayerLandscapeComplementInfoLabel.text =
        //[subtitleLandscapeInfo appendFormat:@"(Aucun %@, ",[[[ConfigurationManager sharedInstance] singleComplementString] lowercaseString]];
        [subtitleLandscapeInfo appendFormat:@"("];

    }
	
    
    if(IS_IPAD()){
        iPadHtmlReferencesString = [NSMutableString stringWithString:self.htmliPadReferencesModel];
    }
    //References
    if([[segment references] count]==0){
        
        if(IS_IPAD()){
            [iPadHtmlReferencesString replaceOccurrencesOfString:@"DISPLAY_REFERENCE_BLOCK"
                                                      withString:@"none"
                                                         options:NSCaseInsensitiveSearch
                                                           range:NSMakeRange(0, [iPadHtmlReferencesString length]) ];

        }

        [htmlContent replaceOccurrencesOfString:@"DISPLAY_REFERENCE_BLOCK"
									 withString:@"none"
										options:NSCaseInsensitiveSearch
										  range:NSMakeRange(0, [htmlContent length]) ];
        //[subtitleLandscapeInfo appendString:@"aucune référence)"];
        [subtitleLandscapeInfo appendString:@")"];
        if([subtitleLandscapeInfo isEqualToString:@"()"]){
            [subtitleLandscapeInfo setString:@""];
        }
    }else{
        if([[segment complements] count]>=1)[subtitleLandscapeInfo appendString:@", "];
        [htmlContent replaceOccurrencesOfString:@"DISPLAY_REFERENCE_BLOCK"
									 withString:@"block"
										options:NSCaseInsensitiveSearch
										  range:NSMakeRange(0, [htmlContent length]) ];
        if(IS_IPAD()){
            [iPadHtmlReferencesString replaceOccurrencesOfString:@"DISPLAY_REFERENCE_BLOCK"
                                         withString:@"block"
                                            options:NSCaseInsensitiveSearch
                                              range:NSMakeRange(0, [iPadHtmlReferencesString length]) ];
        }
        
        
        Reference *firstReference = [[segment references] objectAtIndex:0];
        
        
		NSMutableString *listRefs = [NSMutableString stringWithFormat:@"<li class=\"first\"><a class=\"%@\" href=\"showReference:0\">%@ </a></li>",
									[firstReference type],
                                    [firstReference title]];
        
		for (int i=1, n=[[segment references] count];i<n; i++) {
			Reference *ref = [[segment references] objectAtIndex:i];
            DLog(@"ref:%@",ref.type);

            [listRefs appendString:[NSMutableString stringWithFormat:@"<li><a class=\"%@\" href=\"showReference:%d\">%@ </a></li>",
                                    [ref type],
                                    i,
                                    [ref title]]];
		}
		[htmlContent replaceOccurrencesOfString:@"REFERENCES_LIST"
									 withString:listRefs
										options:NSCaseInsensitiveSearch
										  range:NSMakeRange(0, [htmlContent length]) ];
        if(IS_IPAD()){
            [iPadHtmlReferencesString replaceOccurrencesOfString:@"REFERENCES_LIST"
                                                      withString:listRefs
                                                         options:NSCaseInsensitiveSearch
                                                           range:NSMakeRange(0, [iPadHtmlReferencesString length]) ];

        }
        
        if(firstReference){
            if([[segment references] count]==1){
                [subtitleLandscapeInfo appendString:@"1 référence)"];
            }else{
                [subtitleLandscapeInfo appendFormat:@"%d références)",[[segment references] count]];
            }
        }else{
            
        }
        
    }
    self.fullscreenComplementInfoLabel.text = subtitleLandscapeInfo;
    
	
	[self.abstractView loadHTMLString:htmlContent baseURL:baseURL ];
    if(IS_IPAD()){
        [self.leftWebView loadHTMLString:iPadHtmlReferencesString baseURL:baseURL];
        [self.centerWebView loadHTMLString:iPadHtmlSegmentDescriptionString baseURL:baseURL];
        [self.rightWebView loadHTMLString:iPadHtmlComplementsString baseURL:baseURL];
    }
   [self handleAudioOrVideoViewsConfiguration];
    
    
    
    if([segment hasVideo]){
        DLog(@"<CURRENT SEGMENT HAS VIDEO>");
        if(![self.videoView getPlayer]){
            [self goToSegment:segment atTime:0 continuous:NO];
        }
    }else{
        DLog(@"<CURRENT SEGMENT HASN'T VIDEO>");
    }
    
    [self.view setNeedsDisplay];
	[self.tocView.tableView reloadData];
	//[self updateFullScreenView];
	
}

-(void) updateTimeLineWithTime:(CMTime)time{
    DLog();

    Segment *currentSegment = [self.richMedia getCurrentSegmentObject];
    
    float currentSecond = (float)time.value/(float)time.timescale;
    currentSecond = currentSecond -currentSegment.clipBegin+currentSegment.start;
    currentSecond = lroundf(currentSecond);

    
	//[self.richMedia updateIndexWithTime:currentSecond];
	for(int i=0, n = [[self.richMedia chapters] count]; i< n; i++){
		Chapter *chap = [[self.richMedia chapters] objectAtIndex:i];
		ChapterView *cv = (ChapterView *)[timeLineBkgImageView viewWithTag:100+i];	
		
		if(i<richMedia.currentChapter)[cv setProgression:1.0];
		else if(i>richMedia.currentChapter)[cv setProgression:0];
		else [cv setProgression:(currentSecond-chap.start)/(chap.end-chap.start)];		
	}
	
}

-(void) updateTimeLine{
    DLog();

    if(self.richMedia.durationInSeconds<=0)return;
    
	[self.richMedia updateIndexWithTime:[self getPlayerCurrentTime]];
	for(int i=0, n = [[self.richMedia chapters] count]; i< n; i++){
		Chapter *chap = [[self.richMedia chapters] objectAtIndex:i];
		ChapterView *cv = (ChapterView *)[timeLineBkgImageView viewWithTag:100+i];	
		
		if(i<richMedia.currentChapter)[cv setProgression:1.0];
		else if(i>richMedia.currentChapter)[cv setProgression:0];
		else [cv setProgression:([self getPlayerCurrentTime]-chap.start)/(chap.end-chap.start)];		
	}
}

-(void) updateBullet{
    DLog();
	
	//NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	@autoreleasepool {        
        float scrollTime = [self getVerifiedScrollTimelinePosition];
        
        Segment *segment = [self.richMedia getSegmentAtTime:scrollTime];
        if (segment==nil) {
            //[pool release];
            return;
        }
        
        Illustration *illustration = [segment illustrationAtTime:scrollTime];
        
        if(illustration.start == oldIllustrationBulletStart){
            return;
        }        
        oldIllustrationBulletStart = illustration.start;
        
        int chapIndex = [self.richMedia getChapterIndexAtTime:scrollTime];
        if(chapIndex == -1){
            return;
        } 
        int segIndex = [self.richMedia getSegmentIndexInChapter:chapIndex atTime:scrollTime];
        if(segIndex == -1) {
            return;
        }
        
        
        Chapter *chap = [[richMedia chapters] objectAtIndex:chapIndex];
        
        BOOL addNumbers = [[ConfigurationManager sharedInstance] addAutoNumberingToChapitreAndSegmentName];
        
       // BOOL oneSegOneChap =[self.richMedia isOneSegOneChapMedia];
        NSString *info ;
        if(addNumbers)
            info = [NSString stringWithFormat:@"%d - %@",chapIndex+1, chap.title];
        else
            info = chap.title;
        
        if([segment title] && ([chap.title compare:segment.title] !=NSOrderedSame)){
            if(addNumbers){
                self.bulletLabel.text = [NSString stringWithFormat:@"%@\n%d.%d - %@",info,chapIndex+1,segIndex+1,[segment title]];
            }else{
                self.bulletLabel.text = [NSString stringWithFormat:@"%@\n%@",info,[segment title]];
            }
        }else{
            self.bulletLabel.text = [NSString stringWithFormat:@"%@\n"/*%d.%d*/,info/*,chapIndex+1,segIndex+1*/];
        }
        
        self.bulletImageView.image = [self illustrationImageForTime:scrollTime];
    }
}

-(void) updateTimeLabelsWithTime:(CMTime)time{
    Segment *currentSegment = [self.richMedia getCurrentSegmentObject];

    float currentSecond = (float)time.value/(float)time.timescale;
    currentSecond = MAX(0,currentSecond -currentSegment.clipBegin+currentSegment.start);
    currentSecond = lroundf(currentSecond);
        
	NSUInteger durationInSeconds = MAX(0,lroundf(currentSecond));
	NSUInteger durationInMinutes = durationInSeconds / 60;
	NSUInteger durationInRemainder = durationInSeconds % 60;
	if(durationInRemainder < 10)
		self.advancingTimeLabel.text = [NSString stringWithFormat:@"%i:0%i", durationInMinutes, durationInRemainder];
	else
		self.advancingTimeLabel.text = [NSString stringWithFormat:@"%i:%i", durationInMinutes, durationInRemainder];
	
	durationInSeconds = MAX(0,richMedia.durationInSeconds - durationInSeconds);
	durationInMinutes = durationInSeconds / 60;
	durationInRemainder = durationInSeconds % 60;
	if(durationInRemainder < 10)
		self.remainingTimeLabel.text = [NSString stringWithFormat:@"-%i:0%i", durationInMinutes, durationInRemainder];
	else
		self.remainingTimeLabel.text = [NSString stringWithFormat:@"-%i:%i", durationInMinutes, durationInRemainder];
    
  
    if(durationInSeconds == 0 && [self.richMedia isWebMediaVersion2] ){
        [self handleWebMedia2SegmentEnd];
    }

}

-(void) updateTimeLabels{
    DLog();

	NSUInteger durationInSeconds = [self getPlayerCurrentTime];
	NSUInteger durationInMinutes = durationInSeconds / 60;
	NSUInteger durationInRemainder = durationInSeconds % 60;
	if(durationInRemainder < 10)
		self.advancingTimeLabel.text = [NSString stringWithFormat:@"%i:0%i", durationInMinutes, durationInRemainder];
	else
		self.advancingTimeLabel.text = [NSString stringWithFormat:@"%i:%i", durationInMinutes, durationInRemainder];
	
	durationInSeconds = richMedia.durationInSeconds - [self getPlayerCurrentTime];
	durationInMinutes = durationInSeconds / 60;
	durationInRemainder = durationInSeconds % 60;
	if(durationInRemainder < 10)
		self.remainingTimeLabel.text = [NSString stringWithFormat:@"-%i:0%i", durationInMinutes, durationInRemainder];
	else
		self.remainingTimeLabel.text = [NSString stringWithFormat:@"-%i:%i", durationInMinutes, durationInRemainder];
}





-(float) getPlayerCurrentTime{
    /*OLD*/
    /*
    float t;
    if (self.richMedia.webMediaVersion == WEB_MEDIA_1) {
        t = CMTimeGetSeconds([audioPlayer currentTime]);
    }else{
        Segment *seg = [self.richMedia getCurrentSegmentObject];
        [seg printSegment];
        t = CMTimeGetSeconds([audioPlayer currentTime]);
        t = t - seg.clipBegin + seg.start;
    }*/
    /*END OLD*/
    
    
    
    /*NEW*/
    
    if(self.richMedia.durationInSeconds<=0)return 0;
    
	Segment *seg = [self.richMedia getCurrentSegmentObject];
    float t= CMTimeGetSeconds([audioPlayer currentTime])- seg.clipBegin + seg.start;
    /*END NEW*/
    
        
	if(t<0 || isnan(t)) {
        return 0;
    }
	if(t>=self.richMedia.durationInSeconds){
        return self.richMedia.durationInSeconds;
    }
	return t;
}
-(void) seekPlayerToTime:(float) time{
	/*
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	[formatter setMaximumFractionDigits:2];
	[formatter setRoundingMode: NSNumberFormatterRoundUp];
	*/
	
	float newtime = time;
    	
	if(time<0)newtime=0;
	if(time>self.richMedia.durationInSeconds) newtime = self.richMedia.durationInSeconds;
	
	
    BOOL wasPlaying = playing;
    [self goToSegment:[self.richMedia getCurrentSegmentObject] atTime:time continuous:NO];
    playing = !wasPlaying;
    [self playPauseAction];
    [self updateTimeLine];
    [self updateTimeLabels];
}
  

- (void)viewWillDisappear:(BOOL)animated{
    DLog();
	if(playerIsExpanded){
		[self decreasePlayerView];
	}
	if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
		// back button was pressed.  We know this is true because self is no longer
		// in the navigation stack.  
		
		[self backToRootView];		
		
		self.richMedia.currentChapter = 0;
		self.richMedia.currentSegmentInChapter = 0;
		self.richMedia.currentSegment = 0;
		
		[self.audioPlayer removeTimeObserver:self.playerIllustrationTimeObserver];
		[self.audioPlayer removeTimeObserver:self.playerSecondObserver];
        [self.audioPlayer removeTimeObserver:self.playerSegmentChangeObserver];
        self.playerIllustrationTimeObserver =nil;
        self.playerSecondObserver = nil;
        self.playerSegmentChangeObserver = nil;
    }
 	
	[super viewWillDisappear:animated];
}
- (void)viewWillAppear:(BOOL)animated{
    DLog();
		
	//[self updateTimeLabels];
	//[self updateData:NO];
    
    //UIInterfaceOrientation currentOrientation = [UIApplication sharedApplication].statusBarOrientation;
    
    //[self manageInterfaceOrientation:currentOrientation duration:0];
        
    [super viewWillAppear:animated];
    [self showActionInfo];

}

- (void)viewDidAppear:(BOOL)animated{
    DLog();

	//[self updateTimeLabels];
	//[self updateData:YES];
	[super viewDidAppear:animated];
    [self forceUpdate];
    
    
    if(self.richMedia.durationInSeconds<=0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Le document WebMedia ne semble pas valide et ne peut être lu." delegate:nil cancelButtonTitle:@"Fermer" otherButtonTitles:nil];
        [alert show];
    }
    

}



+ (UIImage*)imageFromResource:(NSString*)name {
	DLog(@"trying to loadImage : %@",name);
    NSMutableString *urlImg = [NSMutableString stringWithString:name];
	
    [urlImg replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [urlImg length] )];
    if(!urlImg)return nil;
    return [UIImage imageWithContentsOfFile:urlImg];
           
}
-(void) loadIllustrationImages{
    DLog();
	
    @autoreleasepool {
       
        BOOL containsIllustration = [self.richMedia hasIllustrationImage];
        self.allIllustrationImageThumbsDictionnary = [[NSMutableDictionary alloc] initWithCapacity:0];
        for(int i=0, n = [[self.richMedia chapters] count]; i< n; i++){
            Chapter *chap = [[self.richMedia chapters] objectAtIndex:i];
            for(int j=0, m = [chap.segments count]; j< m; j++){
                Segment *seg = [chap.segments objectAtIndex:j];
                for(int k=0,l = [seg.illustrations count]; k<l; k++){
                    Illustration *illustration = [seg.illustrations objectAtIndex:k];
                    NSString *key = [NSString stringWithFormat:@"%d-%d-%d",i,j,k] ;
                    UIImage *img;
				                
                    NSString *illustrationPath = [RichMedia getIllustrationFullPath:illustration forMedia:self.richMedia];
                    if(illustrationPath){
                        img=[UIImage imageWithContentsOfFile:illustrationPath];
                        
                    }else{
                        if (containsIllustration || [[ConfigurationManager sharedInstance] showEmptyImageInTocTable]) {
                            img = [UIImage imageNamed:@"blank"];
                        }else{
                            img = nil;
                        }
                        
                    }
                                 
                    CGSize size = img.size;
                    CGSize sizeRef = [UIImage imageNamed:@"blank"].size;
                    float scale = MAX(size.width/sizeRef.width , size.height/sizeRef.height);
				
                    UIImage *newImage = [UIImage imageWithCGImage:img.CGImage scale:scale orientation:UIImageOrientationUp];
				
                    if(img && newImage){
                        [self.allIllustrationImageThumbsDictionnary setValue:newImage forKey:key];
                        DLog(@"illustration key : %@",key);

                    }
				
                }
            }
        }
	
        [self.loadingView setHidden:YES];
        [self.activityIndicator stopAnimating];
    }
	
}

-(UIImage*) illustrationImageForTime:(float) time{
    DLog();
	
    int chapIndex = [self.richMedia getChapterIndexAtTime:time];
	if(chapIndex == -1) return nil;
	int segIndex = [self.richMedia getSegmentIndexInChapter:chapIndex atTime:time];
	if(segIndex ==-1) return nil;
	Chapter *chap = [[self.richMedia chapters] objectAtIndex:chapIndex];
    if(!chap)return nil;
	Segment *seg = [chap.segments objectAtIndex:segIndex];
    
    
    
    if(!seg)return nil;
	int illIndex = [seg illustrationIndexAtTime:time];
	if(illIndex ==-1) return nil;
	return [self.allIllustrationImageThumbsDictionnary valueForKey:[NSString stringWithFormat:@"%d-%d-%d",chapIndex,segIndex,illIndex]];
}

/*
-(UIImage*) illustrationImageForCurrentSegment{
    DLog();
	return [self.allIllustrationImageThumbsDictionnary valueForKey:[NSString stringWithFormat:@"%d-%d-0",richMedia.currentChapter,richMedia.currentSegmentInChapter]];
}
*/

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    DLog();
	
	if ([touch.view isKindOfClass:[UIButton class]]) {
		// we touched our control surface
        return NO; // ignore the touch
    }
    return YES; // handle the touch
}
-(void) handleZoomGesture {
	if(!playerIsExpanded) return;
	float tmpZoom = zoomGestureRecognizerOnTimeLine.scale * zoomFactor;
	if(tmpZoom<=MAX_ZOOM_FACTOR && tmpZoom>=MIN_ZOOM_FACTOR){
		zoomFactor = tmpZoom;
		
		//update timeLine, timeMarker and chapter Views
		timeLineBkgImageView.frame = CGRectMake(0, 0, self.richMedia.durationInSeconds/*timeLineBkgImageView.superview.frame.size.width*/*self.zoomFactor , 54);
		scrollView.frame = CGRectMake(0, 60, scrollView.superview.frame.size.width , 54);
		scrollView.contentSize=timeLineBkgImageView.frame.size;
		
        
        scrollView.contentInset=UIEdgeInsetsMake(0.0,self.view.bounds.size.width/2,0.0,self.view.bounds.size.width/2);
		
		
		float secStep =(self.timeLineBkgImageView.frame.size.width) / self.richMedia.durationInSeconds;
		float startX = 0;
		float startY = 0;
				
		float startSegmentX= 0;
		
		for(int i=0; i<[[self.richMedia chapters] count]; i++){
			
			//Chapter part		
			Chapter *chap = [[self.richMedia chapters] objectAtIndex:i];
			ChapterView *cv = (ChapterView*)[timeLineBkgImageView viewWithTag:100+i];
			cv.frame = CGRectMake(startX, startY+1, secStep*(chap.end-chap.start),self.scrollView.frame.size.height/2-2);
			[cv adapteToFrame:cv.frame showText:YES];
			startSegmentX= 0;
			
			
			//Segment Part
			for (int j=0, n = [chap.segments count];j<n; j++) {
				Segment *seg = [[chap segments] objectAtIndex:j];
				UILabel *lab = (UILabel*)[cv viewWithTag:1000+j];
				if(lab!=nil){
					lab.frame = CGRectMake(startSegmentX, cv.frame.origin.y+cv.frame.size.height+2, secStep*(seg.end-seg.start), cv.frame.size.height-4);
					startSegmentX+=secStep*(seg.end-seg.start);
				}
				lab = nil;
				
			}
			startX+=secStep*(chap.end-chap.start);
		}
		
				
		[self updateData:YES];
		[self updateTimeLine];
		
		timeMarkerView.frame = CGRectMake(0, -10, self.richMedia.durationInSeconds/*timeLineBkgImageView.superview.frame.size.width*/*self.zoomFactor , 10);
		
		
	}
}
-(void) handleSingleTap{
    DLog();
	if(self.richMedia.durationInSeconds<=0)return;
	if(playerIsExpanded){
		CGPoint point = [self.singleTapOnControlsPlayerView locationInView:self.playerControlView];
		//if(CGRectContainsPoint(bulletBackgroundImageView.frame, point)){
		if(point.y>=(self.playerControlTopBarBackgroundImageView.frame.origin.y+self.playerControlTopBarBackgroundImageView.frame.size.height)){
           [self jumpToTimeLineSelection];
		}
		[self decreasePlayerView];
	}else {
		[self expandPlayerView];
	}

}

-(void) handleSlideToNext{
    DLog();
	[self nextAction];
	
}
-(void) handleSlideToPrevious{
    DLog();

	[self previousAction];
}


//iPad OK
-(void) expandPlayerView{
    DLog();
    
    //On retire le slide pour passer d'un segment à l'autre sur la vue des contrôles
    [self.playerControlView removeGestureRecognizer:slideToLeftOnControlsPlayerView];
    [self.playerControlView removeGestureRecognizer:slideToRightOnControlsPlayerView];
    self.playerControlView.userInteractionEnabled=NO;
    CGRect rect = self.playerControlView.frame;
	self.advancingTimeLabel.font = [UIFont systemFontOfSize:14];
	self.remainingTimeLabel.font = [UIFont systemFontOfSize:14];
	
	[UIView beginAnimations:@"expandPlayer" context:nil];
	[UIView setAnimationDelay:0];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(setFullPlayerView)];
	
	self.advancingTimeLabel.frame = CGRectMake(10, 16, 100, 20);
	self.remainingTimeLabel.frame = CGRectMake(self.playerControlView.frame.size.width - (IS_IPAD()?124:110), 16, 100, 20);
    playPauseButton.frame = CGRectMake(scrollView.superview.frame.size.width/2-16, IS_IPAD()?19:9, 32 , 32);
	timeLineBkgImageView.frame = CGRectMake(0, 0, self.richMedia.durationInSeconds*self.zoomFactor , IS_IPAD()? 60 : 54);
	scrollView.frame = IS_IPAD()? CGRectMake(0, 82, scrollView.superview.frame.size.width , 60) : CGRectMake(0, 60, scrollView.superview.frame.size.width , 54);
	self.playerControlView.frame = IS_IPAD()? CGRectMake(rect.origin.x, rect.origin.y - 192, rect.size.width, rect.size.height + 192) : CGRectMake(rect.origin.x, rect.origin.y - 175, rect.size.width, rect.size.height + 175);
    expandIndicatorImageView.transform = CGAffineTransformMakeRotation(M_PI);
	scrollView.contentSize=timeLineBkgImageView.frame.size;

	scrollView.contentInset=UIEdgeInsetsMake(0.0,self.view.bounds.size.width/2,0.0,self.view.bounds.size.width/2);
	self.actionInfoView.hidden = YES;
    
    
    if(IS_IPAD()){
        self.previousButtonFP.frame =CGRectMake(scrollView.superview.frame.size.width/2-16 - 200, 19, 32, 32);
        self.nextButtonFP.frame =CGRectMake(scrollView.superview.frame.size.width/2-16 + 200, 19, 32, 32);
    }
    
    
	[UIView commitAnimations];
		
	playerIsExpanded = YES;
}
- (void)resetInteractionEnabled{
    self.playerControlView.userInteractionEnabled=YES;
}
-(void) setFullPlayerView{
    
    if(self.richMedia.durationInSeconds<=0)return;
    
    DLog();
	
	self.scrollView.userInteractionEnabled = YES;
    self.scrollView.delegate = self;
	
    self.playPauseButton.frame = CGRectMake(scrollView.superview.frame.size.width/2-16, IS_IPAD()?19:9, 32 , 32);
    
    if(IS_IPAD()){
        self.previousButtonFP.frame =CGRectMake(scrollView.superview.frame.size.width/2-16 - 200, 19, 32, 32);
        self.nextButtonFP.frame =CGRectMake(scrollView.superview.frame.size.width/2-16 + 200, 19, 32, 32);
    }
	
    [UIView beginAnimations:@"updatePlayer" context:nil];
	[UIView setAnimationDelay:0];
	[UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(resetInteractionEnabled)];
	float secStep =(self.timeLineBkgImageView.frame.size.width) / self.richMedia.durationInSeconds;
	float startX = 0;
	float startY = 0;

	float startSegmentX= 0;
	
	for(int i=0; i<[[self.richMedia chapters] count]; i++){
		
		//Chapter part		
		Chapter *chap = [[self.richMedia chapters] objectAtIndex:i];
		ChapterView *cv = (ChapterView*)[timeLineBkgImageView viewWithTag:100+i];
		cv.frame = CGRectMake(startX, startY+1, secStep*(chap.end-chap.start)-1,self.scrollView.frame.size.height/2-2);
		[cv adapteToFrame:cv.frame showText:YES];
		startSegmentX= 0;
		
		
		//Segment Part
		for (int j=0, n = [chap.segments count];j<n; j++) {
			Segment *seg = [[chap segments] objectAtIndex:j];
			UILabel *lab = (UILabel*)[cv viewWithTag:1000+j];
			if(lab==nil){
				UILabel *labFirstSegment = [[UILabel alloc] initWithFrame:CGRectMake(startSegmentX, cv.frame.origin.y+cv.frame.size.height+2, secStep*(seg.end-seg.start)-1, cv.frame.size.height-4)];
				labFirstSegment.backgroundColor = [cv.backgroundLabel backgroundColor];
				labFirstSegment.textColor = [UIColor whiteColor];
                
                NSString *segTitle ;
                
                BOOL addNumbers = [[ConfigurationManager sharedInstance] addAutoNumberingToChapitreAndSegmentName];
                
                BOOL oneSegOneChap =[self.richMedia isOneSegOneChapMedia];
                if(oneSegOneChap){
                    if([seg.complements count]>0){
                        segTitle = [NSString stringWithFormat:@"(%d %@)",[[seg complements] count],[[ConfigurationManager sharedInstance] shortComplementString] ];
                    }
                }else{
                    if(addNumbers){
                        segTitle = [NSString stringWithFormat:@"%d. %@",(j+1), seg.title];
                    }else{
                        segTitle = seg.title;
                    }
                }
                
				labFirstSegment.text = segTitle;
				labFirstSegment.font = [UIFont systemFontOfSize:9];
				[cv addSubview:labFirstSegment];
				startSegmentX+=secStep*(seg.end-seg.start);
				[labFirstSegment setTag:1000+j];
				
				[labFirstSegment.layer setMasksToBounds:YES];
				[labFirstSegment.layer setBorderColor:[[[ConfigurationManager sharedInstance] chapterViewBorderColor] CGColor]];
				[labFirstSegment.layer setBorderWidth:1.0];
				
				//[labFirstSegment release];
			}
			lab = nil;
			
		}
		startX+=secStep*(chap.end-chap.start);
	}
	
	
	self.scrollView.contentOffset = CGPointMake(-self.view.frame.size.width/2 + secStep*[self getPlayerCurrentTime], 0.0); 
	[self setCurrentTimeValueForFullPlayer];
	timeMarkerView.hidden=NO;
	
	//playPauseButton.alpha = 1.0f;
	[UIView commitAnimations];
	[self updateData:YES];
	[self updateTimeLine];
		
	nextButtonFP.hidden = NO;
	previousButtonFP.hidden = NO;
	
}

- (float)getVerifiedScrollTimelinePosition{
    float secStep =(self.timeLineBkgImageView.frame.size.width) / self.richMedia.durationInSeconds;
	
	float cPos = (self.scrollView.contentOffset.x + self.view.frame.size.width/2) / secStep;
    
    
	if(cPos<0)cPos=0;
	if(cPos>self.richMedia.durationInSeconds) cPos = self.richMedia.durationInSeconds;
    
    return cPos;
}

-(void) setCurrentTimeValueForFullPlayer{
    DLog();
    
    float cPos = [self getVerifiedScrollTimelinePosition];
    
	int durationInMinutes = cPos / 60;
	int durationInRemainder = (int)(cPos) % 60;
	if(durationInRemainder < 10)
		self.currentTimeFullPlayerLabel.text = [NSString stringWithFormat:@"%i:0%i", durationInMinutes, durationInRemainder];
	else
		self.currentTimeFullPlayerLabel.text = [NSString stringWithFormat:@"%i:%i", durationInMinutes, durationInRemainder];
}

//iPad OK
-(void) setMiniPlayerView{
    DLog();	
	
    //On remet le slide pour passer d'un segment à l'autre sur la vue des contrôles
    [self.playerControlView addGestureRecognizer:slideToLeftOnControlsPlayerView];
    [self.playerControlView addGestureRecognizer:slideToRightOnControlsPlayerView];
	self.scrollView.userInteractionEnabled = NO;
	scrollView.contentSize=CGSizeMake(250 , 18);
	scrollView.contentInset=UIEdgeInsetsZero;
	self.playPauseButton.frame = IS_IPAD() ? CGRectMake(80, 19, 32, 32) :  CGRectMake(9, 9, 32, 32);
	
    if(IS_IPAD()){
        self.previousButtonFP.frame =CGRectMake(80 - 60, 19, 32, 32);
        self.nextButtonFP.frame =CGRectMake(80 + 60, 19, 32, 32);
    }

	[UIView beginAnimations:@"updatePlayer" context:nil];
	[UIView setAnimationDelay:0];
	[UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(resetInteractionEnabled)];
    self.timeLineBkgImageView.frame = CGRectMake(0, 0, self.scrollView.frame.size.width, self.timeLineBkgImageView.frame.size.height);
    float secStep = (self.timeLineBkgImageView.frame.size.width) / self.richMedia.durationInSeconds;
    float startX = 0;
	float startY = 0;
	for(int i=0; i<[[self.richMedia chapters] count]; i++){
		Chapter *chap = [[self.richMedia chapters] objectAtIndex:i];
		ChapterView *cv = (ChapterView*)[timeLineBkgImageView viewWithTag:100+i];
		cv.frame = CGRectMake(startX, startY, ( secStep*(chap.end-chap.start)), self.timeLineBkgImageView.frame.size.height);
		[cv adapteToFrame:cv.frame showText:NO];
		startX+=cv.frame.size.width;
	}
	[UIView commitAnimations];
	timeMarkerView.hidden=YES;
	
	[self updateData:NO];
	[self updateTimeLine];
}

//iPad OK
-(void) decreasePlayerView{
	self.scrollView.delegate = nil;
    self.playerControlView.userInteractionEnabled=NO;
	CGRect rect = self.playerControlView.frame;
	self.advancingTimeLabel.font = [UIFont systemFontOfSize:IS_IPAD()?14:10];
	self.remainingTimeLabel.font = [UIFont systemFontOfSize:IS_IPAD()?14:10];
	nextButtonFP.hidden = !IS_IPAD();
	previousButtonFP.hidden = !IS_IPAD();
	[UIView beginAnimations:@"decreasePlayer" context:nil];
	[UIView setAnimationDelay:0];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(setMiniPlayerView)];
    
    
	self.advancingTimeLabel.frame = IS_IPAD()? CGRectMake(200, 12, 100, 16) : CGRectMake(60, 7, 100, 15);
    float cvw = rect.size.width;
	self.remainingTimeLabel.frame = IS_IPAD()? CGRectMake(cvw-124, 12, 100, 16) : CGRectMake( cvw- 110, 7, 100, 15);
	
	
    self.scrollView.frame = IS_IPAD()?CGRectMake(200, 30, cvw-224 , 24):CGRectMake(60, 20, cvw-70 , 20);
    self.timeLineBkgImageView.frame = IS_IPAD()?CGRectMake(0, 0, cvw-224 , 24):CGRectMake(0, 0, cvw-70 , 20);
	self.playPauseButton.frame = IS_IPAD() ? CGRectMake(80, 19, 32, 32) :  CGRectMake(9, 9, 32, 32);
	
    if(IS_IPAD()){
        self.previousButtonFP.frame =CGRectMake(80 - 60, 19, 32, 32);
        self.nextButtonFP.frame =CGRectMake(80 + 60, 19, 32, 32);
    }
    
	self.playerControlView.frame = CGRectMake(rect.origin.x, rect.origin.y + (IS_IPAD()?192:175), rect.size.width, rect.size.height - (IS_IPAD()?192:175));
	expandIndicatorImageView.transform = CGAffineTransformMakeRotation(0);

	[UIView commitAnimations];	

	playerIsExpanded = NO;
    if([self isInFullscreenMode]){
        //On n'affiche pas la barre de contrôles qd player étendu
        self.actionInfoView.hidden = NO;
        
        [NSObject cancelPreviousPerformRequestsWithTarget: self.actionInfoView];
        [self.actionInfoView performSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES] afterDelay:3];
    }
}

- (void)showExtrasTableView{
    if(!self.extrasView){
        self.extrasView = [[ExtrasTableViewController alloc] initWithStyle:UITableViewStylePlain];
        
		self.extrasView.richMedia = self.richMedia;
        self.extrasView.contentSizeForViewInPopover = CGSizeMake(350, 410);
    }
    if(IS_IPAD()){
        if(self.menu){
            [self.menu dismissPopoverAnimated:NO];
        }
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.extrasView];
        self.menu = nil;
        self.menu = [[UIPopoverController alloc] initWithContentViewController:navController];
        
        
        self.menu.delegate = self;
        self.extrasView.popover = self.menu;
        
        if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
            self.menu.popoverBackgroundViewClass = [SamplePopoverBackgroundView class];
        }else{
            navController.navigationBar.barStyle = UIBarStyleDefault;
            navController.navigationBar.barTintColor = [UIColor whiteColor];
            self.menu.backgroundColor = [UIColor whiteColor];
            
        }
        NSArray *buttons = self.navigationItem.rightBarButtonItems;
        [self.menu presentPopoverFromBarButtonItem:[buttons objectAtIndex:1] permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }else{
        [self.navigationController pushViewController:self.extrasView animated:YES];
    }
}
- (void)showTableView{
    DLog();

    if(tocView==nil){
		tocView = [[TocView alloc] initWithContent:self.richMedia];
		tocView.emissionDefaultViewController = self;
        tocView.contentSizeForViewInPopover = CGSizeMake(350, 410);
    }
    
    if(IS_IPAD()){
        if(self.menu){
            [self.menu dismissPopoverAnimated:NO];
        }
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:tocView];
        self.menu = nil;
        self.menu = [[UIPopoverController alloc] initWithContentViewController:navController];
        tocView.popover = self.menu;
        self.menu.delegate = self;
        if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
            self.menu.popoverBackgroundViewClass = [SamplePopoverBackgroundView class];
        }else{
            navController.navigationBar.barStyle = UIBarStyleDefault;
            navController.navigationBar.barTintColor = [UIColor whiteColor];
            self.menu.backgroundColor = [UIColor whiteColor];
            
        }
        NSArray *buttons = self.navigationItem.rightBarButtonItems;
        if(buttons){
            [self.menu presentPopoverFromBarButtonItem:buttons[0] permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }else{
            [self.menu presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }else{
         [self.navigationController pushViewController:tocView animated:YES];
    }
    			
	//SET the current segment visible in the middle of th view
    //DLog(@"self.richMedia.currentChapter : %d",self.richMedia.currentChapter);
    if(self.richMedia.currentChapter<0){
        self.richMedia.currentChapter = 0;
    }
    if(self.richMedia.currentSegmentInChapter<0){
        self.richMedia.currentSegmentInChapter = 0;
    }
    NSIndexPath *pathToCurrent =  [NSIndexPath indexPathWithIndex:self.richMedia.currentChapter];
    @try {
        [tocView.tableView selectRowAtIndexPath:[pathToCurrent indexPathByAddingIndex:self.richMedia.currentSegmentInChapter] animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}




-(void) resetTimeLine{
    DLog();
	self.richMedia.currentChapter = 0;
	self.richMedia.currentSegmentInChapter = 0;
	self.richMedia.currentSegment = 0;
	
    [self seekPlayerToTime:0.0f];
	[self updateData:NO];
    [self updateTimeLine];
}


-(void) goToSegment:(int)segmentIndex inChapter:(int)chapterIndex{
	DLog(@"segment:%d, in chapter:%d",segmentIndex,chapterIndex);
	
	self.richMedia.currentChapter = chapterIndex;
	self.richMedia.currentSegmentInChapter = segmentIndex;
	[self.richMedia computeCurrentSegment];
	Chapter *chapter = [[self.richMedia chapters] objectAtIndex:self.richMedia.currentChapter];
	Segment *tmp =[chapter.segments objectAtIndex:self.richMedia.currentSegmentInChapter] ;
    [self seekPlayerToTime:tmp.start+0.001] ;
	[self viewWillAppear:NO];
	
    [self updateTimeLine];
	[self updateData:NO];
}
			 
-(void) jumpToTimeLineSelection{
    DLog();
		
	float secStep =(self.timeLineBkgImageView.frame.size.width) / self.richMedia.durationInSeconds;
	
	float scrollTime = (self.scrollView.contentOffset.x + self.view.frame.size.width/2) / secStep; 
	
	if(scrollTime<0)scrollTime=0;
	if(scrollTime>self.richMedia.durationInSeconds) scrollTime = self.richMedia.durationInSeconds;

	int chapterIndex = [self.richMedia getChapterIndexAtTime:scrollTime];
	if(chapterIndex == -1) return;
	int segmentIndex = [self.richMedia getSegmentIndexInChapter:chapterIndex atTime:scrollTime];
	if(segmentIndex == -1) return;
	
	
	self.richMedia.currentChapter = chapterIndex;
	self.richMedia.currentSegmentInChapter = segmentIndex;
	[richMedia computeCurrentSegment];
    
    [self seekPlayerToTime:scrollTime] ;
	[self viewWillAppear:NO];
	
	[self updateTimeLine];
	[self updateData:NO];
}

#pragma mark IBAction
-(IBAction) playPauseAction{
    DLog();
    if(self.richMedia.durationInSeconds<=0)return;
	if (playing) {
        [audioPlayer pause];
		[self.playPauseButton setImage:[UIImage imageNamed:@"play32"] forState:UIControlStateNormal];
		[self.playPauseButton setNeedsDisplay];
#pragma ACCESSIBILITY
        self.playPauseButton.accessibilityLabel = @"Lecture";

	}else {
        [audioPlayer play];
        [self.playPauseButton setImage:[UIImage imageNamed:@"pause32"] forState:UIControlStateNormal];
		[self.playPauseButton setNeedsDisplay];
#pragma ACCESSIBILITY
        self.playPauseButton.accessibilityLabel = @"Pause";
		
	}
	
    
    playing = !playing;
}
-(IBAction) nextAction{
    DLog();

    BOOL wasPlaying = playing;
    if(wasPlaying)[self playPauseAction];
	Chapter *chapter = [[self.richMedia chapters] objectAtIndex:self.richMedia.currentChapter];
	int chapterCount = [[self.richMedia chapters] count];
	int segmentsInChapterCount = [chapter.segments count];
	
    if (self.richMedia.currentSegmentInChapter < (segmentsInChapterCount-1)) {
		DLog(@". Cas n°1");
		self.richMedia.currentSegmentInChapter ++;
		self.richMedia.currentSegment ++;	
    }else if (self.richMedia.currentChapter < (chapterCount-1) ) {
        DLog(@". Cas n°2");
        self.richMedia.currentChapter ++;
		self.richMedia.currentSegmentInChapter = 0;
		self.richMedia.currentSegment ++;
    }else {
        DLog(@". Cas n°3");
		[self resetTimeLine];
	}
	
    DLog(@"\n\n %d / %d / %d",self.richMedia.currentChapter, self.richMedia.currentSegmentInChapter, self.richMedia.currentSegment);
    
    [self goToSegment:richMedia.currentSegmentInChapter inChapter:richMedia.currentChapter];

	if(!playerIsExpanded)[self showActionInfo];
	else {
		float secStep =(self.timeLineBkgImageView.frame.size.width) / self.richMedia.durationInSeconds;
		self.scrollView.contentOffset = CGPointMake(-self.view.frame.size.width/2 + secStep*[self getPlayerCurrentTime], 0.0); 
		[self setCurrentTimeValueForFullPlayer];		
	}
    [self handleAudioOrVideoViewsConfiguration];
	if(wasPlaying)[self playPauseAction];
}
-(IBAction) previousAction{
    DLog();

    BOOL wasPlaying = playing;
    if(wasPlaying)[self playPauseAction];

   	if (self.richMedia.currentSegmentInChapter > 0) {//on reste dans le même chapitre
		DLog(@". Cas n°1");
		self.richMedia.currentSegmentInChapter --;
		self.richMedia.currentSegment --;
    }else if (self.richMedia.currentChapter > 0 ) {//on va dans le dernier segment du chapitre précédent
        DLog(@". Cas n°2");
		self.richMedia.currentChapter -- ;
		Chapter *prevChapter = [[self.richMedia chapters] objectAtIndex:self.richMedia.currentChapter];
		self.richMedia.currentSegmentInChapter = [prevChapter.segments count] - 1;
		self.richMedia.currentSegment --;
    }else {//retour au début du média
        DLog(@". Cas n°3");
	}
    DLog(@"\n\n %d / %d / %d",self.richMedia.currentChapter, self.richMedia.currentSegmentInChapter, self.richMedia.currentSegment);

    [self goToSegment:richMedia.currentSegmentInChapter inChapter:richMedia.currentChapter];
  
	if(!playerIsExpanded)[self showActionInfo];	
	else {
		float secStep =(self.timeLineBkgImageView.frame.size.width) / self.richMedia.durationInSeconds;
		self.scrollView.contentOffset = CGPointMake(-self.view.frame.size.width/2 + secStep*[self getPlayerCurrentTime], 0.0); 
		[self setCurrentTimeValueForFullPlayer];
	}
    [self handleAudioOrVideoViewsConfiguration];
    if(wasPlaying)[self playPauseAction];

}

-(void) showActionInfo{
    DLog();
    
	self.previousButton.enabled = (self.richMedia.currentChapter + self.richMedia.currentSegmentInChapter)>0;
	self.nextButton.enabled = YES;
	
	self.actionInfoView.userInteractionEnabled = YES;
	self.actionInfoView.hidden = NO;
	[NSObject cancelPreviousPerformRequestsWithTarget: self.actionInfoView];
	[self.actionInfoView performSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES] afterDelay:3];
    
}
-(IBAction) showComplement:(id)sender{
    DLog();
	
	SegmentDetailsViewController *sdvc = [[SegmentDetailsViewController alloc] initWithNibName:@"SegmentDetailsViewController" bundle:nil];
	
	UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:self action:nil];
	self.navigationItem.backBarButtonItem = backButton;
	[self.navigationController pushViewController:sdvc animated:YES];
}

-(void) checkForContinueReading{
	if(!self.richMedia.title) return;
	
	NSString *val = [[NSUserDefaults standardUserDefaults] objectForKey:self.richMedia.title];
	
	if(!val) return;
	NSArray *values = [val componentsSeparatedByString:@"_"];
	if([values count]!=4) return;
	float time = [[values objectAtIndex:0] floatValue];
	int cc = [[values objectAtIndex:1] intValue];
	int cs = [[values objectAtIndex:2] intValue];
	int csin = [[values objectAtIndex:3] intValue];
	
	if( (time == 0) && (cc ==0) && (cs ==0) && (csin ==0) ) return ; //no advance
	
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reprendre la lecture ?" message:nil 
												   delegate:self cancelButtonTitle:@"Non" otherButtonTitles:@"Oui",nil];
	[alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
	if(buttonIndex == 1){
		NSString *val = [[NSUserDefaults standardUserDefaults] objectForKey:self.richMedia.title];
		NSArray *values = [val componentsSeparatedByString:@"_"];
						
		self.richMedia.currentChapter = [[values objectAtIndex:1] intValue];
		self.richMedia.currentSegmentInChapter = [[values objectAtIndex:3] intValue];
		self.richMedia.currentSegment = [[values objectAtIndex:2] intValue];
		
		[self seekPlayerToTime:[[values objectAtIndex:0] floatValue]] ;
		[self viewWillAppear:NO];
		
		[self updateTimeLine];
		[self updateData:NO];
		[self playPauseAction];
	}
}

-(void) backToRootView{
    DLog();
	if(self.richMedia.durationInSeconds<=0)return;
	if (playing) {
		[audioPlayer pause];
		playing = NO;
	}
	NSString *str=[NSString stringWithFormat:@"%f_%d_%d_%d",
					   [self getPlayerCurrentTime], 
					   self.richMedia.currentChapter,
					   self.richMedia.currentSegment,
					   self.richMedia.currentSegmentInChapter];
				
	[[NSUserDefaults standardUserDefaults] setObject:str forKey:self.richMedia.title] ;
	DLog(@"done");
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	
    DLog();
	
	NSString *requestString = [[request URL] absoluteString];
	
	
    if(requestString  && [[requestString lowercaseString] hasPrefix:@"showcomplement"]){
		NSArray *components = [requestString componentsSeparatedByString:@":"];
		
		int index = [[components objectAtIndex:1] intValue];
		
		ComplementViewController *cv = [[ComplementViewController alloc] initWithRichMedia:self.richMedia] ;
		
		UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:self action:nil];
		self.navigationItem.backBarButtonItem = backButton;
        [cv showComplement:index];
		
		[self.navigationController pushViewController:cv animated:YES];

		
		
		return NO;
		
	}else if(requestString  && [[requestString lowercaseString] hasPrefix:@"showreference"]){
        NSArray *components = [requestString componentsSeparatedByString:@":"];
		int index = [[components objectAtIndex:1] intValue];
        
		Reference *ref = [[[self.richMedia getCurrentSegmentObject] references] objectAtIndex:index];
        
		self.complementView = nil;
		if(self.complementView ==nil){
			
            
			
            self.complementView = [[ComplementView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            
            
			UIWindow *tmpWindow = [[UIApplication sharedApplication].windows objectAtIndex:0];
			
			[tmpWindow addSubview:self.complementView];
			//[self.view addSubview:complementView];
			
			self.complementView.content.delegate = self;
            
		}
		
		[self.complementView setTitle:ref.title];
		
		      
        NSMutableString *htmlContent= [NSMutableString stringWithString:self.htmlReferenceModel];
        
        [htmlContent replaceOccurrencesOfString:@"REFERENCE_CONTENT"
                                     withString:ref.content
                                        options:NSCaseInsensitiveSearch
                                          range:NSMakeRange(0, [htmlContent length]) ];
        
        [self.complementView setContentString:htmlContent withModelName:self.richMedia.base];
        
		[self.complementView openAction];
        
        return NO;
    }else if ( requestString  && [requestString hasPrefix:@"http://"]){
		if(self.complementView && self.complementView.content==webView){
			[self.complementView closeAction];
		}
        MyBrowser *myBrowser = [[MyBrowser alloc] initWithNibName:(IS_IPAD()?@"MyBrowser_iPad":@"MyBrowser_iPhone") bundle:nil];
		
		UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:nil action:nil];
		self.navigationItem.backBarButtonItem = backButton;
		
		[self.navigationController pushViewController:myBrowser animated:YES];
		[myBrowser showURL:request];
		
		return NO;
	}
	
	
	
	return YES;
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
	if(playerIsExpanded)return;
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
	if(playerIsExpanded)return;
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	if(playerIsExpanded)return;	
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	if(!playerIsExpanded)return;
	
	[self setCurrentTimeValueForFullPlayer];
	
	[NSThread detachNewThreadSelector:@selector(updateBullet) 
							 toTarget:self 
						   withObject:nil];
}

+ (void)attemptRotationToDeviceOrientation{
    DLog();
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    DLog();
    return YES;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    DLog();

    if (self.helpImageView) {
        [self.helpImageView removeFromSuperview];
        self.helpImageView = nil;
    }
}


- (void)viewDidLayoutSubviews{
    DLog();

    [super viewDidLayoutSubviews];
   // if(![self checkIfVideo]) {
        if(playerIsExpanded){
            [self setFullPlayerView];
        }else{
            [self setMiniPlayerView];
        }

        //return;
   // }
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{    
    if(self.complementView){
        [self.complementView updateOrientation: [[UIDevice currentDevice] orientation] duration:0.2];
    }
}
- (void)setPortraitLayout{
    
    float contentheight = self.contentView.bounds.size.height;
        
    float mediaWidth = self.contentView.bounds.size.width;
    
    float mediaHeight = self.view.bounds.size.height>480 ? 260 : 200;
    //iPad ?
    if(IS_IPAD()){
        mediaHeight = 600;
    }
    
    if([[ConfigurationManager sharedInstance] playerDescriptionFirst]){
        if([self isInFullscreenMode]){
            self.videoView.frame = self.illustrationView.frame = self.view.frame;
            self.playerControlView.alpha = 0.8;
        }else{
            if(IS_IPAD()){
                self.topView.frame = CGRectMake(0, 0, mediaWidth, 60);
                self.webViewsContainer.frame = CGRectMake(0, 60, mediaWidth, contentheight-mediaHeight - 60);
                self.videoView.frame = self.illustrationView.frame = CGRectMake(0, contentheight-mediaHeight, mediaWidth, mediaHeight);
            }else{
                self.topView.frame = CGRectMake(0, 0, mediaWidth, contentheight-mediaHeight);
                self.videoView.frame = self.illustrationView.frame = CGRectMake(0, contentheight-mediaHeight, mediaWidth, mediaHeight);
            }
            self.playerControlView.alpha = 1.0;
        }
    }else{
        if([self isInFullscreenMode]){
            self.videoView.frame = self.illustrationView.frame = self.view.frame;
            self.playerControlView.alpha = 0.8;
        }else{
            if(IS_IPAD()){
                self.topView.frame  = CGRectMake(0, 0, mediaWidth, 60);
                self.videoView.frame = self.illustrationView.frame = CGRectMake(0, 60, mediaWidth, mediaHeight);
                self.webViewsContainer.frame = CGRectMake(0, 60+mediaHeight, mediaWidth, contentheight-mediaHeight - 60);
            }else{           
                self.videoView.frame = self.illustrationView.frame = CGRectMake(0, 0, mediaWidth, mediaHeight);
                self.topView.frame  = CGRectMake(0, mediaHeight, mediaWidth, contentheight-mediaHeight);
            }
            self.playerControlView.alpha = 1.0;
            
        }
    }
    
    float actionInfoViewHeight = self.actionInfoView.frame.size.height;
    
    if([self isInFullscreenMode]){
        self.actionInfoView.frame = CGRectMake(0, self.playerControlView.frame.origin.y-actionInfoViewHeight, mediaWidth, actionInfoViewHeight);
        self.webViewsContainer.hidden = YES;
    }else{
        self.actionInfoView.frame = CGRectMake(0, self.videoView.frame.origin.y + self.videoView.frame.size.height-actionInfoViewHeight, mediaWidth, actionInfoViewHeight);
        self.webViewsContainer.hidden = !IS_IPAD();
    }
}

- (void)setLandscapeLayout{
    //Only on iPad
    float contentheight = self.contentView.bounds.size.height;
    
    DLog(@"contentheight:%f", contentheight);
    
    float mediaWidth = self.contentView.bounds.size.width;
    
    float mediaHeight = IS_IPAD()? 390 : 445;
    
    if([[ConfigurationManager sharedInstance] playerDescriptionFirst]){
        if([self isInFullscreenMode]){
            self.videoView.frame = self.illustrationView.frame = self.view.frame;
            self.playerControlView.alpha = 0.8;
        }else{
            if(IS_IPAD()){
                self.topView.frame = CGRectMake(0, 0, mediaWidth, 60);
                self.webViewsContainer.frame = CGRectMake(0, 60, mediaWidth, contentheight-mediaHeight - 60);
                self.videoView.frame = self.illustrationView.frame = CGRectMake(0, contentheight-mediaHeight, mediaWidth, mediaHeight);
                self.scrollingIndicatorView.hidden = YES;
            }else{
                self.topView.frame = CGRectMake(0, 0, mediaWidth, contentheight-mediaHeight);
                self.videoView.frame = self.illustrationView.frame = CGRectMake(0, contentheight-mediaHeight, mediaWidth, mediaHeight);
                self.scrollingIndicatorView.hidden = NO;
            }
            self.playerControlView.alpha = 1.0;
        }
    }else{
        if([self isInFullscreenMode]){
            self.videoView.frame = self.illustrationView.frame = self.view.frame;
            self.playerControlView.alpha = 0.8;
        }else{
            if(IS_IPAD()){
                self.topView.frame  = CGRectMake(0, 0, mediaWidth, 60);
                self.videoView.frame = self.illustrationView.frame = CGRectMake(0, 60, mediaWidth, mediaHeight);
                self.webViewsContainer.frame = CGRectMake(0, 60+mediaHeight, mediaWidth, contentheight-mediaHeight - 60);

            }else{
                self.videoView.frame = self.illustrationView.frame = CGRectMake(0, 0, mediaWidth, mediaHeight);
                self.topView.frame  = CGRectMake(0, mediaHeight, mediaWidth, contentheight-mediaHeight);
            }
            self.playerControlView.alpha = 1.0;
        }
    }
    
    
    
    float actionInfoViewHeight = self.actionInfoView.frame.size.height;
    if([self isInFullscreenMode]){
        self.actionInfoView.frame = CGRectMake(0, self.playerControlView.frame.origin.y-actionInfoViewHeight, mediaWidth, actionInfoViewHeight);
        self.webViewsContainer.hidden = YES;
    }else{
        self.actionInfoView.frame = CGRectMake(0, self.videoView.frame.origin.y+self.videoView.frame.size.height-actionInfoViewHeight, mediaWidth, actionInfoViewHeight);
        self.webViewsContainer.hidden = !IS_IPAD();
    }

}

- (void)viewWillLayoutSubviews{
    DLog();

    [super viewWillLayoutSubviews];
    
    if (self.view.bounds.size.width>self.view.bounds.size.height){//LANDSCAPE
        if(IS_IPAD()){
            self.toggleFullscreenButton.hidden = NO;
            
            [[UIApplication sharedApplication] setStatusBarHidden:[self isInFullscreenMode] withAnimation:UIStatusBarAnimationNone];
            [self.navigationController setNavigationBarHidden:[self isInFullscreenMode] animated:NO];
            self.topView.hidden = YES;
            [UIView animateWithDuration:0.3
                                  delay:0
                                options:UIViewAnimationOptionTransitionNone
                             animations:^{
                                 [self setLandscapeLayout];
                             } completion:^(BOOL finished) {
                                 if(![self isInFullscreenMode]){
                                     self.fullscreenInfoView.hidden= YES;
                                     self.playerControlView.hidden = NO;
                                 }
                                 self.topView.hidden = NO;
                             }];
            
            
            
        }else{
            self.toggleFullscreenButton.hidden = YES;
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            self.topView.hidden = YES;
            self.scrollingIndicatorView.hidden = YES;
            [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionTransitionNone
                         animations:^{
                             CGRect r = CGRectMake(0,0,MAX(self.parentViewController.view.frame.size.width,self.parentViewController.view.frame.size.height),MIN(self.parentViewController.view.frame.size.width,self.parentViewController.view.frame.size.height));
                             self.videoView.frame = r;
                             self.illustrationView.frame = r;
                             float actionInfoViewHeight = self.actionInfoView.frame.size.height;
                             self.actionInfoView.frame = CGRectMake(0, self.playerControlView.frame.origin.y-actionInfoViewHeight, r.size.width, actionInfoViewHeight);
           
                         } completion:^(BOOL finished) {
            
                         }];
        }
        
    

    }
    
    else {
        DLog(@"MANAGE PORTRAIT");
        self.toggleFullscreenButton.hidden = NO;
        
        [[UIApplication sharedApplication] setStatusBarHidden:[self isInFullscreenMode] withAnimation:UIStatusBarAnimationNone];
        [self.navigationController setNavigationBarHidden:[self isInFullscreenMode] animated:NO];
        self.topView.hidden = YES;
        [UIView animateWithDuration:0.3
                         delay:0
                            options:UIViewAnimationOptionTransitionNone
                         animations:^{
            [self setPortraitLayout];
        } completion:^(BOOL finished) {
            if(![self isInFullscreenMode]){
                self.fullscreenInfoView.hidden= YES;
                self.playerControlView.hidden = NO;
            }
            self.topView.hidden = NO;
            self.scrollingIndicatorView.hidden = NO;
        }];
        
    } 
    //if(playerIsExpanded) {
            scrollView.contentInset=UIEdgeInsetsMake(0.0,self.view.bounds.size.width/2,0.0,self.view.bounds.size.width/2);
    //}    
    
}

- (void)didReceiveMemoryWarning {
    DLog();
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    DLog();
    [self setPlayerControlTopBarBackgroundImageView:nil];

    [self setVideoView:nil];
    [self setScrollingIndicatorView:nil];
    [self setFullscreenInfoView:nil];
    [self setFullscreenTitleLabel:nil];
    [self setFullscreenComplementInfoLabel:nil];
   
    [self setHelpImageView:nil];
    [self setIllustrationImageView:nil];
    [self setIllustrationView:nil];
    [self setContentView:nil];
    [self setFullScreenSubtitleLabel:nil];
    [self setToggleFullscreenButton:nil];
    [self setLegendIllustrationLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)showHelpAction:(id)sender {
    DLog(@"self.helpImageView : %@",self.helpImageView );
    self.helpImageView = [[UIImageView alloc] initWithFrame:self.parentViewController.view.frame];
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;

    DLog(@"orientation : %d",orientation);
    
    if (self.view.bounds.size.width>self.view.bounds.size.height){
        

        UIImage * LandscapeImage = IS_IPAD() ? [UIImage imageNamed:@"help_landscape_ipad"] : [UIImage imageNamed: self.view.bounds.size.width>480?@"help_landscape_1_568":@"help_landscape_1"];
        self.helpImageView.image = [[UIImage alloc] initWithCGImage:LandscapeImage.CGImage
                                                              scale: 1.0
                                                        orientation:(orientation == UIInterfaceOrientationLandscapeLeft)?UIImageOrientationLeft:UIImageOrientationRight];
        
    }else{
       UIImage * portraitImage = IS_IPAD() ? [UIImage imageNamed:@"help_portrait_ipad"] :[UIImage imageNamed:self.view.bounds.size.height>480?@"help_portrait_1_568":@"help_portrait_1"];
    
    
        self.helpImageView.image = [[UIImage alloc] initWithCGImage:portraitImage.CGImage
                                                              scale: 1.0
                                                        orientation:(orientation == UIInterfaceOrientationPortrait)?UIImageOrientationUp:UIImageOrientationDown];
        

    
    }
    //self.helpImageView.contentMode = UIViewContentModeScaleAspectFit ;
    self.helpImageView.userInteractionEnabled = YES;
    DLog(@"self.helpImageView : %@",self.helpImageView );
    [[[UIApplication sharedApplication] keyWindow] addSubview:self.helpImageView];
    //[self.navigationController setNavigationBarHidden:YES animated:NO];
   
    UITapGestureRecognizer *tapToHide = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideHelp:)];
    [self.helpImageView addGestureRecognizer:tapToHide];
   
   
}
- (void)hideHelp:(id)sender{
    [self.helpImageView removeFromSuperview];
    self.helpImageView = nil;
}


- (IBAction)toggleFullscreenAction:(id)sender {
    [self toggleFullScreen];
}

- (BOOL)isInFullscreenMode{
    return (!IS_IPAD() && self.view.bounds.size.width>self.view.bounds.size.height) || self.fullscreen;
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    DLog();
    self.menu = nil;
    self.extrasView.popover = nil;
    self.tocView.popover = nil;

}
@end
