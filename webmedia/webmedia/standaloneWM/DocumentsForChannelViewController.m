//
//  DocumentsForChannelViewController.m
//  OptimGenericReader
//
//  Created by Mory Doukouré on 16/02/11.
//  Copyright 2011 Soreha. All rights reserved.
//



#import "DocumentsForChannelViewController.h"
#import "DownlodedDocumentsTableViewController.h"
#import "AvailableDocumentsTableViewController.h"
#import "SearchViewController.h"
#import "AboutViewController.h"
#import "ConfigurationManager.h"



#define AVAILABLE_SEGMENT_INDEX     1
#define DOWNLOADED_SEGMENT_INDEX    0


@implementation DocumentsForChannelViewController

@synthesize segmentData;
@synthesize channel;
@synthesize downlodedDocumentsTableViewController;
@synthesize availableDocumentsTableViewController;
@synthesize toolbar;

-(void) releaseOutlets{
	self.downlodedDocumentsTableViewController = nil;
	self.availableDocumentsTableViewController = nil;
    self.errorView = nil;
	self.toolbar = nil;
}


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/
- (void) viewWillAppear:(BOOL)animated {
    DLog();
    
    if ([[downlodedDocumentsTableViewController.fetchedResultsController fetchedObjects] count] == 0) {
        if (self.segmentData.selectedSegmentIndex == DOWNLOADED_SEGMENT_INDEX) {
            [self.segmentData setSelectedSegmentIndex:AVAILABLE_SEGMENT_INDEX];
            [self switchView];
        }
    }
}

- (void)setChannel:(Channel *)aChannel{
    DLog();

    channel = aChannel;
    [self.downlodedDocumentsTableViewController initWithChannel:self.channel];
	[self.availableDocumentsTableViewController initWithChannel:self.channel];
    self.downlodedDocumentsTableViewController._navigationController = self.navigationController;
	self.availableDocumentsTableViewController._navigationController = self.navigationController;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[CoreDataManager sharedInstance] downloadRssDataForChannel:channel];

}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    DLog();
    
    [super viewDidLoad];
	self.toolbar.hidden = YES;
	[self.downlodedDocumentsTableViewController initWithChannel:self.channel];
	[self.availableDocumentsTableViewController initWithChannel:self.channel];
    self.downlodedDocumentsTableViewController._navigationController = self.navigationController;
	self.availableDocumentsTableViewController._navigationController = self.navigationController;
    
    //use to update segment title number information
    self.downlodedDocumentsTableViewController.contentDelegate = self;
    self.availableDocumentsTableViewController.contentDelegate = self;

    
    //Le bouton de tri
    UIBarButtonItem *openSortButton = [[UIBarButtonItem alloc] initWithTitle:@"Tri" style:UIBarButtonItemStylePlain target:self action:@selector(openSortActionSheet)];
	self.navigationItem.rightBarButtonItem = openSortButton;
    
    //Le titre de la vue (configuration.plist
	self.title = [[ConfigurationManager sharedInstance] documentsListTitle];
    
    //Le nom des segments
    
    [self updateSegmentTitleNumbers:self.downlodedDocumentsTableViewController];
    [self updateSegmentTitleNumbers:self.availableDocumentsTableViewController];

    
    
    //le nom de l'onglet
    self.navigationController.tabBarItem.title = [[ConfigurationManager sharedInstance] libraryTabName];

    [self switchView];
    
    [CoreDataManager sharedInstance].rssDelegate = self;
    UITapGestureRecognizer *tapToHide = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissErrorView)];
    [self.errorView addGestureRecognizer:tapToHide];
}

	
		

-(IBAction) downloadAll {
	//[self.dataManager prepareDownloadWithView:self];
	[[CoreDataManager sharedInstance] downloadAllDocumentsForChannel:self.channel withCategory:nil];
}

-(IBAction) downloadSelected {
	//[self.dataManager prepareDownloadWithView:self];
	if (![[CoreDataManager sharedInstance] downloadSelectedDocumentsForChannel:self.channel withCategory:nil]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Vous n'avez sélectionné aucun élément à télécharger" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
		[alert show];
	}
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}


/* action appelée qd l'utilisateur sélectionne un segment pour passer d'une table à l'autre */
- (void) switchView {

	switch ([self.segmentData selectedSegmentIndex]) {
		case DOWNLOADED_SEGMENT_INDEX:
			[self.view bringSubviewToFront:downlodedDocumentsTableViewController.view];
			self.toolbar.hidden = YES;
			break;
		case AVAILABLE_SEGMENT_INDEX:
			[self.view bringSubviewToFront:self.toolbar];
			[self.view bringSubviewToFront:availableDocumentsTableViewController.view];
			self.toolbar.hidden = NO;
			break;
	};
}

- (void) openSortActionSheet {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Méthode de tri" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Ordre initial",@"Trier par date", @"Trier par titre", nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    [downlodedDocumentsTableViewController reloadFetchedResultControllerSortingBy:buttonIndex];
    [availableDocumentsTableViewController reloadFetchedResultControllerSortingBy:buttonIndex];
}

- (void)actionSheetCancel:(UIActionSheet *)actionSheet {
    [actionSheet dismissWithClickedButtonIndex:2 animated:YES];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [self setErrorView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)showAboutLeftButtonOnNavigationBar{
    UIButton* modalViewButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
	[modalViewButton addTarget:self action:@selector(showAbout) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton];
	self.navigationItem.leftBarButtonItem = modalButton;

}
-(void) showAbout{
	
	AboutViewController *aboutViewController = [[AboutViewController alloc] initWithNibName:UI_USER_INTERFACE_IDIOM()?@"AboutViewController_iPad":@"AboutViewController_iPhone" bundle:nil];
	UINavigationController *modalNav = [[UINavigationController alloc] initWithRootViewController:aboutViewController];
	
	//[[self navigationController] presentModalViewController:modalNav animated:YES];
    //aboutViewController.navigationController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];   ;

    [[self navigationController] presentViewController:modalNav animated:YES completion:^{
        aboutViewController.navigationController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];   ;
    }];

}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (void)updateSegmentTitleNumbers:(id)sender{
    
    if(sender == self.downlodedDocumentsTableViewController){
        [self.segmentData setTitle:[NSString stringWithFormat:@"%@ (%d)",[[ConfigurationManager sharedInstance] downloadedDocumentsButtonTitle], [[self.downlodedDocumentsTableViewController.fetchedResultsController  fetchedObjects] count]]
             forSegmentAtIndex:DOWNLOADED_SEGMENT_INDEX];
    
    }else if(sender == self.availableDocumentsTableViewController){
        [self.segmentData setTitle:[NSString stringWithFormat:@"%@ (%d)",[[ConfigurationManager sharedInstance] availableDocumentsButtonTitle], [[self.availableDocumentsTableViewController.fetchedResultsController  fetchedObjects] count]]
             forSegmentAtIndex:AVAILABLE_SEGMENT_INDEX];
    }
}


- (void)dismissErrorView{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismissErrorView) object:nil];
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         self.errorView.frame = CGRectMake(0, -60, self.view.bounds.size.width, 60);
                     } completion:^(BOOL finished) {
                         
                     }];
    
}
- (void)showErrorView{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismissErrorView) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showErrorView) object:nil];
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         self.errorView.frame = CGRectMake(0, 0, self.view.bounds.size.width, 60);
                     } completion:^(BOOL finished) {
                         [self performSelector:@selector(dismissErrorView) withObject:nil afterDelay:5];
                     }];
    
}


- (void)failToUpdateChannel:(Channel*)aChannel{
    //Attention si chaine non initialisée le titre est : "Récupération des données..."
    DLog(@"Erreur de mise à jour de la chaine : %@ vs current channel %@",aChannel.title, self.channel.title);
    // if(self.currentChannel == channel || ![[ConfigurationManager sharedInstance] multiChannelConfiguration]){
    [self performSelector:@selector(showErrorView) withObject:nil afterDelay:1];
    // }
}
- (void)successToUpdateChannel:(Channel*)aChannel{
    DLog(@"Mise à jour de la chaine OK : %@ vs current channel %@",aChannel.title, self.channel.title);
}



@end
