//
//  HtmlContentType.m
//  PhoneRadio
//
//  Created by soreha on 28/11/10.
//  
//

#import "HtmlContentType.h"


@implementation HtmlContentType

@synthesize refUri;
@synthesize title;
@synthesize galleries;

- (id) init{
	self = [super init];
	if (self != nil) {
		galleries = [[NSMutableArray alloc] initWithCapacity:0];
	}
	return self;
}

@end
