//
//  CoreDataManager.m
//  WebMediaTest
//
//  Created by soreha on 21/01/11.
//  
//

#import "CoreDataManager.h"
#import "Channel.h"
#import "Document.h"
#import "Bookmark.h"

#import "Thumbnail.h"
#import "DocumentRSSImporter.h"
#import "RequestManager.h"
#import "ASINetworkQueue.h"

@implementation CoreDataManager
@synthesize requestManager;
@synthesize managedObjectContext;
@synthesize persistentStoreCoordinator;
@synthesize persistentStorePath;




+ (CoreDataManager *) sharedInstance{
    static dispatch_once_t onceToken;
    static CoreDataManager * __sharedInstance = nil;
    
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[self alloc] init];
    });
    return __sharedInstance;
}


- (id) init {
    DLog();
    
    self = [super init];
    self.requestManager = [[RequestManager alloc] init];
    self.requestManager.coreDataDelegate = self;
    return self;
}



#pragma mark -
#pragma mark Thumbnail managment

- (Thumbnail *)getThumbnailForURL:(NSString *)url{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:@"Thumbnail" inManagedObjectContext:self.managedObjectContext]];
	[request setPredicate: [NSPredicate predicateWithFormat: @"url = %@", url]];
	
	NSError *error = nil;
	NSArray *matching_objects = [self.managedObjectContext executeFetchRequest:request error:&error];
	
	DLog(@"NbImages : %d",[matching_objects count]);
	if ([matching_objects count] > 0) {
		return [matching_objects objectAtIndex:0];
	}
	
	return nil;
}


-(void) downloadThumbnailForDocument : (Document *) document {
	DLog(@" document.title : %@", document.title);
	[self.requestManager addThumbnailRequestForDocument:document];
}

#pragma mark -
#pragma mark Channel managment
- (Channel *)getChannelForRssUrl:(NSString *)rssUrl{
    DLog();
    
    NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                            rssUrl, @"RSS_URL", nil];
    
    NSFetchRequest *fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
                                    fetchRequestFromTemplateWithName:@"ChannelForRssUrl"
                                    substitutionVariables:substitutionDictionary];
    
    NSError *error = nil;
    NSArray *matching_objects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    Channel * channel = nil;
    
    if ([matching_objects count] != 0) {
        channel = [matching_objects objectAtIndex:0];
        DLog(@"Get channel : %@", channel.rssUrl);
    } 
	    
    return channel;
}



- (Channel *)createChannelForRssUrl:(NSString *)rssUrl {
	NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                            rssUrl, @"RSS_URL", nil];
    
    NSFetchRequest *fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
                                    fetchRequestFromTemplateWithName:@"ChannelForRssUrl"
                                    substitutionVariables:substitutionDictionary];
    
    NSError *error = nil;
    NSArray *matching_objects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    Channel * channel = nil;
    
    if ([matching_objects count] != 0) {
        DLog(@"Existing channel :%@", rssUrl);
    } else {
        channel = (Channel *)[NSEntityDescription insertNewObjectForEntityForName:@"Channel" inManagedObjectContext:self.managedObjectContext];
        channel.rssUrl = rssUrl;
        DLog(@"Channel created : %@", rssUrl);
    }
	return channel;
}




- (NSFetchRequest *)fetchRequestAllChannel{
	DLog();
	
    NSSortDescriptor * sortTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    NSArray * sortDescriptors = [NSArray arrayWithObjects: sortTitle, nil];
                          
                          
	NSDictionary *substitutionDictionary = [[NSDictionary alloc] init];
	
	NSFetchRequest *fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
                                    fetchRequestFromTemplateWithName:@"AllChannels"
                                    substitutionVariables:substitutionDictionary];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setIncludesPendingChanges:NO];
	
    
    return fetchRequest;
}


- (void)deleteChannel:(Channel *)channel{
	DLog();
	
	//first remove dowloaded documents from this channel
	for (Document *document in channel.documents) {
		if([document.downloaded boolValue]){
			[[CoreDataManager sharedInstance] removeDownloadedDocument:document];
		}
	}
	
	//next remove the channel
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"%@_deleted",channel.rssUrl]];
    [self.managedObjectContext deleteObject:channel];
    [self saveData];
}


- (NSArray *)categoriesForChannel:(Channel *)channel{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:@"Document" inManagedObjectContext:self.managedObjectContext]];
    
    NSDictionary *entityProperties = [[NSEntityDescription entityForName:@"Document" inManagedObjectContext:self.managedObjectContext] propertiesByName];
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:[entityProperties objectForKey:@"category"]]];
    [fetchRequest setResultType:NSDictionaryResultType];
    [fetchRequest setReturnsDistinctResults:YES];
    
	NSError *error = nil;
	NSArray *matching_objects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    DLog(@"Nb Categ : %d", [matching_objects count]);
    return matching_objects;
}


- (void)downloadAllChannelRssData{	
	NSError *error = nil;
    NSFetchRequest * request = [self fetchRequestAllChannel];
    NSArray * results = [self.managedObjectContext executeFetchRequest:request error:&error];
      
	DLog(@"Nb Channels : %d", [results count]);
    
	if ([results count] > 0) {
		for (Channel * channel in results){
			[self.requestManager addRssRequestForChannel:channel];
		}
	}
}

- (void)downloadRssDataForNewChannel:(Channel *)channel {
	DLog();
    
	if (self.requestManager == nil) {
		self.requestManager = [[RequestManager alloc] init];
		self.requestManager.coreDataDelegate = self;
	}
	
	[self.requestManager addRssRequestForNewChannel:channel];
}

- (void)downloadRssDataForChannel:(Channel *)channel {
	DLog();
    
	if (self.requestManager == nil) {
		self.requestManager = [[RequestManager alloc] init];
		self.requestManager.coreDataDelegate = self;
	}
	
	[self.requestManager addRssRequestForChannel:channel];
}

- (void)downloadThumbnailForChannel:(Channel *)channel {
	DLog();
    
	[self.requestManager addThumbnailRequestForChannel:channel];
}


#pragma mark -
#pragma mark Document managment

- (Document *)newDocument {
    //DLog();
    
	Document *document = (Document *)[NSEntityDescription   insertNewObjectForEntityForName:@"Document" inManagedObjectContext:self.managedObjectContext] ;
	return document;
}

- (void)removeDownloadedDocument:(Document *)document {
    //DLog();
    
	document.downloading = [NSNumber numberWithBool:NO];
	document.downloaded = [NSNumber numberWithBool:NO];
    document.updateAvailable = [NSNumber numberWithBool:NO];
    document.lastOpenedPage=nil;
	
	NSArray *pathComponent;
	if ((pathComponent = [document.localMediaURI pathComponents]) != nil) {
		NSArray *mediaRootPathComponents = [NSArray arrayWithArray:[pathComponent subarrayWithRange: NSMakeRange(0, [pathComponent indexOfObject:@"medias"]+2)]];  
		NSString *mediaRootPath = [NSString pathWithComponents:mediaRootPathComponents];
		[[NSFileManager defaultManager] removeItemAtPath:mediaRootPath error:nil];
	}
    
    if (document.tmpDownloadFilePath != nil) {
		[[NSFileManager defaultManager] removeItemAtPath:document.tmpDownloadFilePath error:nil];
        document.tmpDownloadFilePath = nil;
	}
	
	[self saveData];
}


- (void)copyAttributesFromDocument:(Document *)fromDoc toDocument:(Document *)toDoc{
    //DLog();
    
    NSArray *attributeKeys = [[[fromDoc entity] attributesByName] allKeys];
	NSDictionary  *values = [fromDoc dictionaryWithValuesForKeys:attributeKeys];
	NSMutableDictionary *newValues = [[NSMutableDictionary alloc] initWithDictionary:values];
	
	[newValues removeObjectForKey:@"localMediaURI"];
	[newValues removeObjectForKey:@"tmpDownloadFilePath"];
	[newValues removeObjectForKey:@"downloaded"];
	[newValues removeObjectForKey:@"downloading"];
	[newValues removeObjectForKey:@"updateAvailable"];
    [newValues removeObjectForKey:@"downloadProgress"];
    [newValues removeObjectForKey:@"downloadSuspended"];
    [newValues removeObjectForKey:@"isFavorite"];
    [newValues removeObjectForKey:@"lastOpenedPage"];

	[toDoc setValuesForKeysWithDictionary: newValues];
    
    
   // DLog(@"values:%@",values);
    //DLog(@"newValues:%@",newValues);

}
- (void)addOrModifyDocument:(Document *)document forChannel:(Channel *)channel {
	//DLog();
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
	[outputFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
	
	Document *localDocument  = [self documentForGuid:document.guid];
    
	if (localDocument == nil){
		[channel addDocumentsObject:document];
	} else if (document.lastBuildDate==nil || [localDocument.lastBuildDate isEqualToDate:document.lastBuildDate]) {        
        if ([document.thumbnail_small_URI compare:localDocument.thumbnail_small_URI] != NSOrderedSame) {
            localDocument.thumbnail_small = nil;
        }
        
        if ([document.thumbnail_large_URI compare:localDocument.thumbnail_large_URI] != NSOrderedSame) {
            localDocument.thumbnail_large = nil;
        }
		
        [localDocument copyAttributesFromObject:document];
		[managedObjectContext deleteObject:document];
	} else {

        [self copyAttributesFromDocument:document toDocument:localDocument];
        if ([localDocument.downloaded intValue] == 1) {
            localDocument.updateAvailable = [NSNumber numberWithBool:YES];
        } else {
            localDocument.updateAvailable = [NSNumber numberWithBool:NO];
        }
		[[NSFileManager defaultManager] removeItemAtPath:localDocument.tmpDownloadFilePath error:nil];
		localDocument.tmpDownloadFilePath = nil;
		
		[managedObjectContext deleteObject:document];
	}
}


- (Document *)documentForGuid:(NSString *)guid {
    //DLog();

    NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                            guid, @"GUID", nil];
    
    NSFetchRequest *fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
                                    fetchRequestFromTemplateWithName:@"DocumentsForGuid"
                                    substitutionVariables:substitutionDictionary];
    
    [fetchRequest setIncludesPendingChanges:NO];
    
    NSError *error = nil;
    NSArray *matching_objects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    Document * document = nil;
    
    if ([matching_objects count] != 0) {
        document = [matching_objects objectAtIndex:0];
    } 
    
    return document;
}

- (NSFetchRequest *)fetchRequestAllDocumentsForChannel:(Channel *)channel sortedBy:(int)criteria{
    //DLog();
    
    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;
	NSDictionary *substitutionDictionary;
    
    substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                              channel, @"CHANNEL", nil];
    
    fetchRequest = [[self.persistentStoreCoordinator managedObjectModel]
                    fetchRequestFromTemplateWithName:@"AllDocumentsForChannel"
                    substitutionVariables:substitutionDictionary];

    
    NSSortDescriptor * sortByCategory = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	NSSortDescriptor * sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    
    if (criteria == 0) {
        NSSortDescriptor * sortRssPos = [[NSSortDescriptor alloc] initWithKey:@"rssPos" ascending:YES];
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortRssPos, sortByTitle, nil];
    } else if (criteria == 1) {
        NSSortDescriptor * sortByDate = [[NSSortDescriptor alloc] initWithKey:@"lastBuildDate" ascending:NO];
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByDate, sortByTitle, nil];
    } else {
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByTitle, nil];
    }
	
	[fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setFetchBatchSize:20];
    [fetchRequest setIncludesPendingChanges:YES];
	
    return fetchRequest;
    
    
}

- (NSFetchRequest *) fetchRequestFavoriteDocumentsForChannel : (Channel *) channel withCategory : (NSString *) category sortedBy : (int) criteria{
    //DLog();

    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;
	NSDictionary *substitutionDictionary;
    
    if (category == nil) {
        substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                  channel, @"CHANNEL", nil];
		
        fetchRequest = [[self.persistentStoreCoordinator managedObjectModel]
						fetchRequestFromTemplateWithName:@"FavoriteDocumentsForChannel"
						substitutionVariables:substitutionDictionary];
    } else {
        substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                  channel, @"CHANNEL",category, @"CATEGORY", nil];
        
        fetchRequest = [[self.persistentStoreCoordinator managedObjectModel]
                        fetchRequestFromTemplateWithName:@"FavoriteDocumentsForChannelWithCateg"
                        substitutionVariables:substitutionDictionary];
    }
    
	NSSortDescriptor * sortByCategory = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	NSSortDescriptor * sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    
    if (criteria == 0) {
        NSSortDescriptor * sortRssPos = [[NSSortDescriptor alloc] initWithKey:@"rssPos" ascending:YES];
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortRssPos, sortByTitle, nil];
    } else if (criteria == 1) {
        NSSortDescriptor * sortByDate = [[NSSortDescriptor alloc] initWithKey:@"lastBuildDate" ascending:NO];
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByDate, sortByTitle, nil];
    } else {
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByTitle, nil];
    }
	
	[fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setFetchBatchSize:20];
    [fetchRequest setIncludesPendingChanges:YES];
	
    return fetchRequest;
}



- (NSFetchRequest *) fetchRequestDownloadedDocumentsForChannel : (Channel *) channel withCategory : (NSString *) category sortedBy : (int) criteria{
    //DLog();

    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;
	NSDictionary *substitutionDictionary;
    
    if (category == nil) {
        substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
												channel, @"CHANNEL", nil];
		
        fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
						fetchRequestFromTemplateWithName:@"DownloadedDocumentsForChannel"
						substitutionVariables:substitutionDictionary];
    } else {
        substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                channel, @"CHANNEL",category, @"CATEGORY", nil];
        
        fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
                        fetchRequestFromTemplateWithName:@"DownloadedDocumentsForChannelWithCateg"
                        substitutionVariables:substitutionDictionary];
    }
		
	NSSortDescriptor * sortByCategory = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	NSSortDescriptor * sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    
    if (criteria == 0) {
        NSSortDescriptor * sortRssPos = [[NSSortDescriptor alloc] initWithKey:@"rssPos" ascending:YES];
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortRssPos, sortByTitle, nil];
    } else if (criteria == 1) {
        NSSortDescriptor * sortByDate = [[NSSortDescriptor alloc] initWithKey:@"lastBuildDate" ascending:NO];
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByDate, sortByTitle, nil];
    } else {
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByTitle, nil];
    }
	
	[fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setFetchBatchSize:20];
    [fetchRequest setIncludesPendingChanges:YES];
	
    return fetchRequest;
}


- (NSFetchRequest *) fetchRequestAvailableDocumentsForChannel : (Channel *) channel withCategory : (NSString *) category sortedBy : (int) criteria {
    //DLog();

    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;
	NSDictionary *substitutionDictionary;
    
	if (category == nil) {
        substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
												channel, @"CHANNEL", nil];
		
        fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
						fetchRequestFromTemplateWithName:@"AvailableDocumentsForChannel"
						substitutionVariables:substitutionDictionary];
    } else {
        substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
												channel, @"CHANNEL",category, @"CATEGORY", nil];
		
        fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
						fetchRequestFromTemplateWithName:@"AvailableDocumentsForChannelWithCateg"
						substitutionVariables:substitutionDictionary];
    }
		
	NSSortDescriptor * sortByCategory = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	NSSortDescriptor * sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
	
    if (criteria == 0) {
        NSSortDescriptor * sortRssPos = [[NSSortDescriptor alloc] initWithKey:@"rssPos" ascending:YES];
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortRssPos, sortByTitle, nil];
    } else if (criteria == 1) {
        NSSortDescriptor * sortByDate = [[NSSortDescriptor alloc] initWithKey:@"lastBuildDate" ascending:NO];
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByDate, sortByTitle, nil];
    } else {
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByTitle, nil];
    }
    
	[fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setIncludesPendingChanges:NO];
	[fetchRequest setFetchBatchSize:20];
    
    return fetchRequest;
}


- (NSFetchRequest *) fetchRequestAvailableSelectedDocumentsForChannel : (Channel *) channel withCategory : (NSString *) category {
    //DLog();

    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;
	NSDictionary *substitutionDictionary;
	
    
    if (category == nil) {
        substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                  channel, @"CHANNEL", nil];
        
        fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
                        fetchRequestFromTemplateWithName:@"AvailableSelectedDocumentsForChannel"
                        substitutionVariables:substitutionDictionary];
    } else {
        substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
											channel, @"CHANNEL",category, @"CATEGORY", nil];
	
        fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
					fetchRequestFromTemplateWithName:@"AvailableSelectedDocumentsForChannelWithCateg"
					substitutionVariables:substitutionDictionary];
    }
	
	NSSortDescriptor * sortByCategory = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	NSSortDescriptor * sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
	sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByTitle, nil];
	
	[fetchRequest setSortDescriptors:sortDescriptors];
    [fetchRequest setIncludesPendingChanges:YES];
	[fetchRequest setFetchBatchSize:20];
	
    return fetchRequest;
}


- (NSFetchRequest *) fetchRequestAvailableOrModifiedDocumentsForChannel : (Channel *) channel withCategory : (NSString *) category{
    //DLog();

    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;
	NSDictionary *substitutionDictionary;
	
    if (category == nil) {
        substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                  channel, @"CHANNEL", nil];
        
        fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
                        fetchRequestFromTemplateWithName:@"NewOrModifiedDocumentsForChannel"
                        substitutionVariables:substitutionDictionary];
    } else {
        substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
											channel, @"CHANNEL", category, @"CATEGORY",nil];
	
        fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
					fetchRequestFromTemplateWithName:@"NewOrModifiedDocumentsForChannelAndCategory"
					substitutionVariables:substitutionDictionary];
    }
	
	NSSortDescriptor * sortByCategory = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	NSSortDescriptor * sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
	sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByTitle, nil];
	
	[fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setIncludesPendingChanges:NO];
	    
    return fetchRequest;
}

- (NSFetchRequest *) fetchRequestAvailableOrModifiedDocuments {
   // DLog();

    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;
	NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:nil];
	
	fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
					fetchRequestFromTemplateWithName:@"NewOrModifiedDocuments"
					substitutionVariables:substitutionDictionary];
	
	NSSortDescriptor * sortByCategory = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	NSSortDescriptor * sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
	NSSortDescriptor * sortByChannel = [[NSSortDescriptor alloc] initWithKey:@"channel" ascending:YES];
	sortDescriptors = [NSArray arrayWithObjects:sortByChannel, sortByCategory, sortByTitle, nil];
	
	[fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setIncludesPendingChanges:NO];
    
    return fetchRequest;
}


-(NSFetchRequest *) fetchRequestFavoriteDocuments {
    //DLog();

    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;
	NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:nil];
	
	fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
					fetchRequestFromTemplateWithName:@"AllFavoriteDocuments"
					substitutionVariables:substitutionDictionary];
	
	NSSortDescriptor * sortByChannel = [[NSSortDescriptor alloc] initWithKey:@"channel.title" ascending:YES];
	NSSortDescriptor * sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
	sortDescriptors = [NSArray arrayWithObjects:sortByChannel, sortByTitle, nil];
    
	[fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setFetchBatchSize:20];
    [fetchRequest setIncludesPendingChanges:YES];
	
    return fetchRequest;
}
-(NSFetchRequest *) fetchRequestFavoriteDocumentsWithCategory:(NSString *)category sortedBy:(int)criteria{
    //DLog();

    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;
    NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:nil];
    
    
	NSSortDescriptor * sortByCategory = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	NSSortDescriptor * sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
	
    if (criteria == 0) {
        NSSortDescriptor * sortRssPos = [[NSSortDescriptor alloc] initWithKey:@"rssPos" ascending:YES];
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortRssPos, sortByTitle, nil];
    } else if (criteria == 1) {
        NSSortDescriptor * sortByDate = [[NSSortDescriptor alloc] initWithKey:@"lastBuildDate" ascending:NO];
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByDate, sortByTitle, nil];
    } else {
        sortDescriptors = [NSArray arrayWithObjects:sortByCategory, sortByTitle, nil];
    }
    
	 
	
	fetchRequest = [[self.persistentStoreCoordinator managedObjectModel]
					fetchRequestFromTemplateWithName:@"AllFavoriteDocuments"
					substitutionVariables:substitutionDictionary];
	
	[fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setFetchBatchSize:20];
    [fetchRequest setIncludesPendingChanges:YES];
	
    return fetchRequest;
}

-(NSFetchRequest *) fetchRequestDownloadingDocuments : (BOOL) selected{
    //DLog();

    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;	
	NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:nil];
	
    if (selected) {
        fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
                        fetchRequestFromTemplateWithName:@"AllDownloadingSelectedDocuments"
                        substitutionVariables:substitutionDictionary]; 
    } else {
        fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
					fetchRequestFromTemplateWithName:@"AllDownloadingDocuments"
					substitutionVariables:substitutionDictionary];
	}
    
    NSSortDescriptor * sortByUpdated = [[NSSortDescriptor alloc] initWithKey:@"updateAvailable" ascending:NO];
    NSSortDescriptor * sortByDownloadSuspended = [[NSSortDescriptor alloc] initWithKey:@"downloadSuspended" ascending:YES];
    NSSortDescriptor * sortByDownloadProgress = [[NSSortDescriptor alloc] initWithKey:@"downloadProgress" ascending:NO];
	NSSortDescriptor * sortByChannel = [[NSSortDescriptor alloc] initWithKey:@"channel.title" ascending:YES];
    NSSortDescriptor * sortByCategory = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	NSSortDescriptor * sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
	sortDescriptors = [NSArray arrayWithObjects:sortByUpdated,sortByDownloadSuspended,sortByDownloadProgress,sortByChannel,sortByCategory,sortByTitle, nil];
    
	[fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setFetchBatchSize:20];
    [fetchRequest setIncludesPendingChanges:YES];
	
    return fetchRequest;    
}

-(NSFetchRequest *) fetchRequestAllDocuments{
   //DLog();

    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;	
	NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:nil];
	
	fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
					fetchRequestFromTemplateWithName:@"AllDocuments"
					substitutionVariables:substitutionDictionary];
	
	NSSortDescriptor * sortByChannel = [[NSSortDescriptor alloc] initWithKey:@"channel.title" ascending:YES];
	NSSortDescriptor * sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
	sortDescriptors = [NSArray arrayWithObjects:sortByChannel, sortByTitle, nil];
    
	[fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setFetchBatchSize:20];
    [fetchRequest setIncludesPendingChanges:YES];
	
    return fetchRequest;
}


-(NSFetchRequest *) fetchRequestAllDocumentsForKeyword : (NSString *) keyword{
    //DLog();

    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;	
	NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"*%@*",keyword], @"KEYWORD", nil];
	
    fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
					fetchRequestFromTemplateWithName:@"AllDocumentsWithKeyword"
					substitutionVariables:substitutionDictionary];
	
    
	NSSortDescriptor * sortByChannel = [[NSSortDescriptor alloc] initWithKey:@"channel.title" ascending:YES];
	NSSortDescriptor * sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
	sortDescriptors = [NSArray arrayWithObjects:sortByChannel, sortByTitle, nil];
	
	[fetchRequest setSortDescriptors:sortDescriptors];
    [fetchRequest setIncludesPendingChanges:YES];
	[fetchRequest setFetchBatchSize:20];
	
    return fetchRequest;
}


-(NSFetchRequest *) fetchRequestAllDocumentsWithDistanceLessThan : (NSNumber *) distance{
    //DLog();

    NSFetchRequest *fetchRequest;
	NSArray * sortDescriptors;
	NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:distance, @"DISTANCE", nil];
	
    fetchRequest = [[self.persistentStoreCoordinator managedObjectModel] 
                   fetchRequestFromTemplateWithName:@"AllDocumentsWithDistance"
                   substitutionVariables:substitutionDictionary];
	
	
	NSSortDescriptor * sortByDistance = [[NSSortDescriptor alloc] initWithKey:@"distanceFromLocation" ascending:YES];
	sortDescriptors = [NSArray arrayWithObjects:sortByDistance, nil];
	
	[fetchRequest setSortDescriptors:sortDescriptors];
    [fetchRequest setIncludesPendingChanges:YES];
	[fetchRequest setFetchBatchSize:20];
	
    return fetchRequest;
}


-(void) manageDeletedDocuments: (NSMutableSet *) availableDocs forChannel : (Channel *) channel {
   	//DLog();

    NSMutableSet *toRemove = [[NSMutableSet alloc] initWithCapacity:0];
    for (Document * document in channel.documents){
        if (![availableDocs containsObject:document.guid] && [document.downloaded intValue] == 0){
            [toRemove addObject:document];
        }
    }
    
    for (Document * document in toRemove) {
        [self.managedObjectContext deleteObject:document];
    }
    
    [self saveData];
}
#pragma mark -
#pragma mark Bookmark, Notes and Highlight management

- (Bookmark *) bookmarkForDocument: (Document *)document withPageUri : (NSString *) pageUri {
    NSFetchRequest *fetchRequest;
	NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                            document, @"DOCUMENT", pageUri, @"PAGE_URI", nil];
	
	fetchRequest = [[self.persistentStoreCoordinator managedObjectModel]
					fetchRequestFromTemplateWithName:@"BookmarkForDocument"
					substitutionVariables:substitutionDictionary];
    
    NSError *error = nil;
	NSArray *matching_objects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if ([matching_objects count] > 0) {
        return [matching_objects objectAtIndex:0];
    }
    
    return nil;
}




-(void) addOrRemoveBookmarkForDocument: (Document *) document withPageUri : (NSString *) pageUri pageTitle : (NSString *) pageTitle {
    
    Bookmark *b = [self bookmarkForDocument:document withPageUri:pageUri];
    
    
    
    if (b != nil) {
        [self.managedObjectContext deleteObject: b];
    } else {
        Bookmark *bookmark = (Bookmark *)[NSEntityDescription   insertNewObjectForEntityForName:@"Bookmark" inManagedObjectContext:self.managedObjectContext];
        bookmark.document = document;
        bookmark.pageUri = pageUri;
        bookmark.pageTitle = pageTitle;
        
        [document addBookmarksObject:bookmark];
    }
    
    [self saveData];
}

-(BOOL) isPage: (NSString *) pageUri bookmarkedForDocument :(Document *) document {
    return ([self bookmarkForDocument:document withPageUri:pageUri] != nil);
}


-(NSArray *) bookmarksForDocument: (Document *)document{
    NSFetchRequest *fetchRequest;
	
	NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                            document, @"DOCUMENT", nil];
	
	fetchRequest = [[self.persistentStoreCoordinator managedObjectModel]
					fetchRequestFromTemplateWithName:@"BookmarksForDocument"
					substitutionVariables:substitutionDictionary];
    
    NSError *error = nil;
	return [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
}



#pragma mark -
#pragma mark Download managment

-(void) downloadAllDocumentsForChannel:(Channel *) channel withCategory : (NSString *) category {
	//DLog();

    NSFetchRequest *fetchRequest = [self fetchRequestAvailableOrModifiedDocumentsForChannel:channel withCategory :category];
    NSError *error = nil;
    NSArray *matching_objects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
	
	[self performDownloadsForDocuments:matching_objects];

}

-(BOOL) downloadSelectedDocumentsForChannel:(Channel *) channel withCategory : (NSString *) category {
	//DLog();

	NSFetchRequest * fetchRequest = [self fetchRequestAvailableSelectedDocumentsForChannel:channel withCategory :category];
    NSError *error = nil;
    
    NSArray *matching_objects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if ([matching_objects count] > 0) {
		[self performDownloadsForDocuments:matching_objects];
		return YES;
	} else {
		return NO;
	}
}

-(void) downloadDocument:(Document *)document {
	//DLog();

    if (self.requestManager.documentDownloadQueue == nil) {
		[self.requestManager initAsiQueue];
	}
	[self.requestManager addDocumentRequest:document];
	[self.requestManager.documentDownloadQueue go];
}


-(void) performDownloadsForDocuments: (NSArray *) documents {
	//DLog();

    if (self.requestManager.documentDownloadQueue == nil) {
		[self.requestManager initAsiQueue];
	}
	
	for (Document * document in documents){
		if ([document.downloaded intValue] == 0 || [document.updateAvailable intValue] == 1) {
			[self.requestManager addDocumentRequest:document];
		}
	}

	[self.requestManager.documentDownloadQueue go];
}

#pragma mark -
#pragma mark RequestManagerCoreDataDelegate methods
-(void) rssRequestDone :(ASIHTTPRequest *)request {
	//DLog();
    
    Channel *channel = (Channel *)[request.userInfo objectForKey:@"Channel"];

    if(request.responseStatusCode==404){
        //[self deleteChannel:channel];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Adresse incorrecte" message:[NSString stringWithFormat:@"La source \"%@\" n'est pas accessible.\nVeuillez vérifier vos informations ou supprimer la source créé.",channel.rssUrl] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
	DocumentRSSImporter *importer = [[DocumentRSSImporter alloc] initWithDataManager:self withChannel:channel];
	importer.delegate = self;
	
	NSData *response = [request responseData];
	[importer loadXMLFromData:response];
    if([self.rssDelegate respondsToSelector:@selector(successToUpdateChannel:)]){
        [self.rssDelegate successToUpdateChannel:channel];
    }
}

-(void) rssRequestError :(ASIHTTPRequest *)request {
	if([self.rssDelegate respondsToSelector:@selector(failToUpdateChannel:)]){
        [self.rssDelegate failToUpdateChannel:(Channel *)[request.userInfo objectForKey:@"Channel"]];
    }
}
-(void) rssCreationRequestError :(ASIHTTPRequest *)request{
    NSError *error = [request error];
    
    Channel *channel = (Channel *)[request.userInfo objectForKey:@"Channel"];
    UIAlertView *alert;
    
    /*
	if ([error code] == 1 && [channel.documents count] == 0) {
        alert = [[UIAlertView alloc] initWithTitle:@"Erreur de connexion" message:@"Impossible d'ajouter la source\n\nVeuillez vérifier vos informations et votre connexion réseau." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    } else if (([error code] == 6 || [error code] == 5) && [channel.documents count] == 0 ) {
        alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:[NSString stringWithFormat:@"Impossible d'ajouter la source\n\nL'adresse que vous avez saisie semble incorrecte.\n\n\"%@\"", channel.rssUrl ] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }*/
    
    if (([error code] == 5 || [error code] == 2) && [channel.documents count] == 0) {
        alert = [[UIAlertView alloc] initWithTitle:@"Erreur de connexion" message:@"Impossible d'établir une connexion avec le serveur (Time out).\n\nVeuillez vérifier votre connexion réseau." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    } else if ([error code] == 1 && [channel.documents count] == 0) {
        alert = [[UIAlertView alloc] initWithTitle:@"Erreur de connexion" message:@"Impossible d'ajouter la chaine (Connexion impossible).\n\nVeuillez vérifier vos informations et votre connexion réseau." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }else if ([error code] == 4 && [channel.documents count] == 0) {
        alert = [[UIAlertView alloc] initWithTitle:@"Erreur de connexion" message:@"Impossible d'ajouter la chaine (Connexion annulée).\n\nVeuillez vérifier vos informations et votre connexion réseau." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    } else if ([error code] == 3) {
        alert = [[UIAlertView alloc] initWithTitle:@"Erreur de connexion" message:@"Impossible d'ajouter la chaine.\n\nL'accès à adresse indiquée nécessite une authentification. Vous devez utiliser une url de la forme : [http://login:password@www.site.com/...].\n\nLa source n'a pas été ajoutée" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }else if ([error code] == 6 && [channel.documents count] == 0 ) {
        alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:[NSString stringWithFormat:@"Impossible d'ajouter la chaine\n\nL'adresse que vous avez saisie semble incorrecte.\n\n%@\n\n\n\nLa source n'a pas été ajoutée", channel.rssUrl ] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }else if ([error code] == 9 && [channel.documents count] == 0 ) {
        alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:[NSString stringWithFormat:@"Impossible d'ajouter la chaine (Erreur de redirection)..\n\n%@\n\n\n\nLa source n'a pas été ajoutée", channel.rssUrl ] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }else {
        alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:[NSString stringWithFormat:@"Impossible d'ajouter ou d'actualiser la chaine (Code Erreur=%d).\n\nL'adresse que vous avez saisie semble incorrecte ou n'est pas accessible (Veuillez vérifier votre connexion).", [error code]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
  
    
    if ([channel.documents count] == 0) {
        [managedObjectContext deleteObject:channel];
    }
    
	[self saveData];
}



#pragma mark -
#pragma mark WebMediaRssImporterDelegate methods
/* la réponse est obtenue en entier et est contenu dans data */
-(void) didFinishParsing {
	//DLog();

}
/* il y a eu une erreur qui est définie dans error */
-(void) didFailParsingWithError:(NSError *)error {
	//DLog();
}




#pragma mark -
#pragma mark Core Data stack setup

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    //DLog();

    if (persistentStoreCoordinator == nil) {
        NSURL *storeUrl = [NSURL fileURLWithPath:self.persistentStorePath];
       
		persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[NSManagedObjectModel mergedModelFromBundles:nil]];
       
		//option for automatic model migration
		NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
								 [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
		
		NSError *error = nil;
        [persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error];
        
        //NSPersistentStore *persistentStore = [persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error];
        //NSAssert3(persistentStore != nil, @"Unhandled error adding persistent store in %s at line %d: %@", __FUNCTION__, __LINE__, [error localizedDescription]);
    
        [self addSkipBackupAttributeToItemAtURL:storeUrl];
        
    
    }
    return persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
	//DLog();

    if (managedObjectContext == nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
    }
    return managedObjectContext;
}
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL{
    //assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    //if(!success){
       // DLog(@"Error excluding \"%@\" from backup %@", [URL lastPathComponent], error);
    //}else{
       // DLog(@"Excluding \"%@\" from backup", [URL lastPathComponent]);
    //}
    return success;
}
- (NSString *)persistentStorePath {
   // DLog();

    if (persistentStorePath == nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths lastObject];
        persistentStorePath = [documentsDirectory stringByAppendingPathComponent:@"data.sqlite"];
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:documentsDirectory isDirectory:YES]];
        
    }
    return persistentStorePath;
}

- (void) saveData {
	//DLog();

    NSError *error = nil;
    
    @try {
        if (self.managedObjectContext != nil)
        {
            if ([self.managedObjectContext hasChanges] && ![self.managedObjectContext save:&error])
            {
                ALog(@"Unresolved error %@, %@", error, [error userInfo]);
                // abort();
            } 
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
}

#pragma mark Delegate methods from Document

- (void)document:(Document *)document didLoadThumbnail:(UIImage *)img isSmall : (BOOL) isSmall{
	//DLog();
    
    @try{
        Thumbnail *newThumbnail = [NSEntityDescription insertNewObjectForEntityForName:@"Thumbnail" inManagedObjectContext:managedObjectContext];
        newThumbnail.image = img;
        if (isSmall) {
            newThumbnail.url = document.thumbnail_small_URI;
            document.thumbnail_small = newThumbnail;
        } else {
            newThumbnail.url = document.thumbnail_large_URI;
            document.thumbnail_large = newThumbnail;
        }
    }@catch (NSException *en) {
        //
    }

	[self saveData];
}
-(void) channel:(Channel *)channel didLoadThumbnail:(UIImage *)image{
	//DLog();

	Thumbnail *newThumbnail = [NSEntityDescription insertNewObjectForEntityForName:@"Thumbnail" inManagedObjectContext:managedObjectContext];
	newThumbnail.image = image;
	channel.thumbnail = newThumbnail;
	[self saveData];
}

- (void)document:(Document *)document didLoadMedia:(NSString *)path {
	//DLog(@"Document downloaded : %@",document.title);
	document.localMediaURI = path;
	document.tmpDownloadFilePath = nil;
	document.downloaded = [NSNumber numberWithBool:YES];
	document.updateAvailable = [NSNumber numberWithBool:NO];
	[self saveData];
}

@end
