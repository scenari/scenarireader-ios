//
//  SegmentDetailsViewController.h
//  PhoneRadio
//
//  Created by soreha on 19/01/11.
//  
//

#import <UIKit/UIKit.h>
#import "ComplementView.h"

@class RichMedia;

@interface SegmentDetailsViewController : UIViewController <UIWebViewDelegate, UIGestureRecognizerDelegate> {
	IBOutlet UIWebView *abstractView;
	
	RichMedia *richMedia;
	NSString *htmlContentModel;
	NSURL *baseURL;
		
}
@property (nonatomic, retain)ComplementView *complementView;

@property(nonatomic, strong) IBOutlet UIWebView *abstractView;
@property (nonatomic, strong) RichMedia *richMedia;
@property (nonatomic, strong) NSString *htmlContentModel;
@property (nonatomic, strong) NSURL *baseURL;

-(id) initWithMedia:(RichMedia *)aMedia chapterIndex:(int)cIndex andSegmentIndex:(int)sIndex;


@end
