//
//  OpaleWebReader.m
//  Opale Reader
//
//  Created by soreha on 03/05/11.
//  
//

#import <AVFoundation/AVFoundation.h>

#import "CoreDataManager.h"
#import "Document.h"
#import "MyBrowser.h"
#import "Bookmark.h"
#import "SearchWebView.h"
#import "ConfigurationManager.h"
#import "OpaleWebReader.h"



@interface OpaleWebReader (PrivateMethods)



-(void) showControls;
-(void) hideControls;
-(void) addNote;
-(void) showPopoverMenuExemple;
-(void) handleSingleTap;
-(void) searchForCurrentText;
@end


@implementation OpaleWebReader

@synthesize searchField;
@synthesize resultSearchLabel;
@synthesize searchView;
@synthesize webView;
@synthesize buttonBookMarkOn;
@synthesize buttonBookmarkOff;
@synthesize controlsView;
@synthesize openMenuButton;
@synthesize singleTap;
@synthesize doubleTap;

@synthesize singleTapToTouch;


@synthesize document;


#define KEEP_OPENED_DURATION    10



- (void)configureOnDocumentType{
    NSLog(@"%s", __func__);
    if([[self.document.pubType lowercaseString] isEqualToString:@"topaze"]){
        self.buttonShowBookMarks.enabled = self.buttonSearch.enabled = NO;
        self.buttonBookMarkOn.hidden = self.buttonBookmarkOff.hidden = YES;
    }else{
        self.buttonShowBookMarks.enabled = self.buttonSearch.enabled = YES;
        //self.buttonBookMarkOn.hidden = self.buttonBookmarkOff.hidden = NO;
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    DLog();

    [self.loadingIndicator  performSelector:@selector(startAnimating) withObject:nil afterDelay:1];


}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    DLog();

    [NSObject cancelPreviousPerformRequestsWithTarget:self.loadingIndicator];
    [self.loadingIndicator stopAnimating];

}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    DLog();
    
   // NSString *removeFlashVideo = @"elements = document.getElementsByClassName(\"resVideo \");while(elements.length > 0){elements[0].parentNode.removeChild(elements[0]);}";
   // [self.webView stringByEvaluatingJavaScriptFromString:removeFlashVideo];
    
   // [self.webView stringByEvaluatingJavaScriptFromString:@"elements = document.getElementsByTagName(\"video\");while(elements.length > 0){elements[0].style.max-width=640;}"];
    //DLog("\n\ncontenu : %@\n\n",[self.webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML;"]);
    //NSString *t=[self.webView stringByEvaluatingJavaScriptFromString:@"document.title;"];
    //NSLog(@"Nom de la  page : %@\n",t);
    //webkit-playsinline
    //[self.webView stringByEvaluatingJavaScriptFromString:@"$('video').attr('webkit-playsinline', '');"];
    //[self.webView stringByEvaluatingJavaScriptFromString:@"$('video').attr('max-width', '640');"];

    // [self.webView stringByEvaluatingJavaScriptFromString:@"var e = document.createEvent('Events'); e.initEvent('orientationchange', true, false); document.dispatchEvent(e);"];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self.loadingIndicator];
    

    [self.loadingIndicator stopAnimating];
    

    if(self.searchView.alpha!=0){
        [self searchForCurrentText];
    }
    if (self.webView.isLoading)
        return;
    
}

-(void) showPage:(NSURL*)url{
    if(url){
        [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
        
}

-(void) openDocument:(Document *)aDocument onLastOpenedPage:(BOOL)showLast{
    DLog();
    self.document = aDocument;
    _onlineDocument = NO;

    NSString *urlString;
    DLog(@"showLast=%d",showLast);

    if(showLast){
        DLog(@"showLast is true");
    }
    else{
        DLog(@"showLast is false");
    }
    
    if (showLast && self.document.lastOpenedPage && ![self isTypedDocumentString:self.document.lastOpenedPage] &&
        ![self.document.lastOpenedPage hasPrefix:@"http"]) {
        DLog(@"on doit ouvrir la dernière page lue");
        DLog(@"[document localMediaURI] = %@",[document localMediaURI]);
        urlString = [NSString stringWithFormat:@"%@/%@",[document localMediaURI],document.lastOpenedPage];
    } else {
        urlString = [NSString stringWithFormat:@"%@/index.html",[document localMediaURI]];
    }
    
    NSURL *url=urlString?[NSURL fileURLWithPath:urlString]:nil;
    
    [self configureOnDocumentType];
    
    [self showPage:url];
    
}
-(void) openOnlineDocument:(Document *)aDocument onLastOpenedPage :(BOOL) showLast{
    DLog();

    self.document = aDocument;
    _onlineDocument = YES;
    NSURL *url=[NSURL URLWithString:((showLast && document.lastOpenedPage && ![self.document.lastOpenedPage hasPrefix:@"file://"]) ? document.lastOpenedPage : (self.document.link?self.document.link:@""))];
    [self configureOnDocumentType];

    [self showPage:url];
}


-(void) viewDidAppear:(BOOL)animated{
    //NSString *filePath = [[NSBundle mainBundle] pathForResource:@"co/1" ofType:@"html"];
    //NSURL *url = [NSURL fileURLWithPath:filePath];
    //[self showPage:url];
    [super viewDidAppear:animated];
    
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}





- (void)didReceiveMemoryWarning{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
   //uncomment next line to disable exterior menu tap to hide menu
    //pomVC.discardWithOutsideMenuTap = NO;
    
    //if(IS_IPAD()){
        //[self.homeButton setImage:[UIImage imageNamed:@"shelves-iPad"] forState:UIControlStateNormal];
    //}
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
           // [self setEdgesForExtendedLayout:UIRectEdgeNone];   // iOS 7 specific
            CGRect oldFrame = self.webView.frame;
            self.webView.frame = CGRectMake(oldFrame.origin.x, oldFrame.origin.y + 20.0f, oldFrame.size.width, oldFrame.size.height-20);
            oldFrame= self.searchView.frame;
            self.searchView.frame =CGRectMake(oldFrame.origin.x, oldFrame.origin.y + 20.0f, oldFrame.size.width, oldFrame.size.height);
            oldFrame= self.buttonBookmarkOff.frame;
            self.buttonBookmarkOff.frame =CGRectMake(oldFrame.origin.x, oldFrame.origin.y + 20.0f, oldFrame.size.width, oldFrame.size.height);
            oldFrame= self.buttonBookMarkOn.frame;

            self.buttonBookMarkOn.frame =CGRectMake(oldFrame.origin.x, oldFrame.origin.y + 20.0f, oldFrame.size.width, oldFrame.size.height);

        
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0,0, oldFrame.size.width, 20)];
        [UIColor colorWithRed:(float)(0xD4)/255.0 green:(float)(0xD4)/255.0 blue:(float)(0xD4)/255.0 alpha:(float)(0xFF)/255.0];
        [self.view addSubview:v];
    }
    
    //Gesture Recognizer
	//singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap)];
	//[singleTap setNumberOfTapsRequired:1];
	//[singleTap setDelegate:self];
	//[self.controlsView addGestureRecognizer:singleTap];
   
    //singleTapToTouch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPopoverMenuExemple)];
	//[singleTapToTouch setNumberOfTapsRequired:1];
    //[singleTapToTouch setNumberOfTouchesRequired:2];
	//[singleTapToTouch setDelegate:self];
	//[self.webView addGestureRecognizer:singleTapToTouch];

    //doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addNote)];
    //[singleTap requireGestureRecognizerToFail:doubleTap];
      
	//[doubleTap setNumberOfTapsRequired:2];
	//[doubleTap setDelegate:self];
	//[self.webView addGestureRecognizer:doubleTap];
    
    
    UITapGestureRecognizer *webViewTapped = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    webViewTapped.numberOfTapsRequired = 1;
    webViewTapped.delegate = self;
    [self.webView addGestureRecognizer:webViewTapped];
    
    UITapGestureRecognizer *leaveGesture= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leaveScreen:)] ;
    leaveGesture.numberOfTapsRequired = 3;
    [self.webView addGestureRecognizer:leaveGesture];
    
    [self.controlsView.layer setMasksToBounds:YES];
	//Set the corner radius
	[self.controlsView.layer setCornerRadius:10.0];
	//Set the border color
	[self.controlsView.layer setBorderColor:[[UIColor colorWithRed:0.0f green:0.478f blue:1.0f alpha:0.7f] CGColor]];//[UIColor grayColor]
	//Set the image border
	[self.controlsView.layer setBorderWidth:1.0];
	self.controlsView.userInteractionEnabled = YES;
    
    
    self.menuIsOpened = NO;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchForCurrentText) name:@"UITextFieldTextDidChangeNotification" object:searchField];

    [self.searchView.layer setMasksToBounds:YES];
	//Set the border color
	[self.searchView.layer setBorderColor:[[UIColor blackColor] CGColor]];
	//Set the image border
	[self.searchView.layer setBorderWidth:1.0];
	self.searchView.userInteractionEnabled = YES;
    
    self.openMenuButton.accessibilityLabel = [NSString stringWithFormat:@"Appuyer pour afficher le menu. Le menu se masque automatiquement après %d secondes.",KEEP_OPENED_DURATION];


}
- (void)leaveScreen:(UISwipeGestureRecognizer *)sender{
    DLog(@"leaveScreen");

    [self homeAction:nil];
}
- (void)tapAction:(UITapGestureRecognizer *)sender{
    DLog(@"touched");
    // Get the specific point that was touched
    self.point = [sender locationInView:self.view];
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
	//NSLog(@" %s", __FUNCTION__);	
    //NSLog(@"%@",[touch description]);
	
	if ([touch.view isKindOfClass:[UIButton class]] || touch.view == self.controlsView) {
		DLog(@"bouton ou controlsview");
        return NO; // ignore the touch
    }
    return YES; // handle the touch
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return NO;
}

- (void)viewDidUnload
{
 
    self.webView.delegate = nil;
    [self setWebView:nil];
    [self setButtonBookMarkOn:nil];
    [self setButtonBookmarkOff:nil];
    [self setControlsView:nil];
    
    [self setOpenMenuButton:nil];
    [self setSearchView:nil];
    [self setSearchField:nil];
    [self setResultSearchLabel:nil];
    [self setLoadingIndicator:nil];
    [self setHomeButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (IBAction)closeSearchModeAction:(id)sender {
    [self.searchField resignFirstResponder];
    [self.webView removeAllHighlights];
    self.searchView.alpha = 0.0;
    self.webView.frame= CGRectMake(0,+self.searchView.frame.origin.y,self.webView.frame.size.width,self.view.frame.size.height-self.searchView.frame.origin.y);
}

- (IBAction)addToBookMark:(id)sender {
    [UIView beginAnimations:@"addToBookMark" context:nil];
	[UIView setAnimationDelay:0];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelegate:self];
	//[UIView setAnimationDidStopSelector:@selector()];
	
    
    
	self.buttonBookMarkOn.frame = self.buttonBookmarkOff.frame;
	
	[UIView commitAnimations];	
    
    [[CoreDataManager sharedInstance] addOrRemoveBookmarkForDocument:document withPageUri:document.lastOpenedPage pageTitle:[self.webView stringByEvaluatingJavaScriptFromString:@"document.title"]];
}

- (IBAction)removeToBookMark:(id)sender {
    [UIView beginAnimations:@"removeToBookMark" context:nil];
	[UIView setAnimationDelay:0];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelegate:self];
	//[UIView setAnimationDidStopSelector:@selector()];
	
    CGRect rectOff = self.buttonBookmarkOff.frame;
    
	self.buttonBookMarkOn.frame = CGRectMake(rectOff.origin.x + rectOff.size.width, rectOff.origin.y, rectOff.size.width, rectOff.size.height);
	
	[UIView commitAnimations];
	[[CoreDataManager sharedInstance] addOrRemoveBookmarkForDocument:document withPageUri:document.lastOpenedPage pageTitle:[self.webView stringByEvaluatingJavaScriptFromString:@"document.title"]];
}

- (IBAction)homeAction:(id)sender {
    DLog();
    if (![self isBeingDismissed]) {
        [self.webView stopLoading];
        [self dismissViewControllerAnimated:YES completion:^{}];
    }
    
}

- (IBAction)searchAction:(id)sender {
    
    if(self.searchView.alpha == 0){
        //adding
        
        self.searchView.alpha = 1.0;
        [self.searchField becomeFirstResponder];
        
        CGRect r = self.webView.frame;
        
        self.webView.frame = CGRectMake(0, self.searchView.frame.size.height+r.origin.y, self.webView.frame.size.width, self.view.frame.size.height - self.searchView.frame.size.height-r.origin.y);
        
    }else{
        [self closeSearchModeAction:nil];    
    }
}

- (IBAction)showBookMarksAction:(id)sender {
    [self switchControlsVisibility:nil];
    BookmarksList *bookmarksList = [[BookmarksList alloc] initWithNibName:@"BookmarksList" bundle:nil];
    
    bookmarksList.bookMarkListManager = self;
    
    bookmarksList.bookmarks = [NSMutableArray arrayWithArray:[[CoreDataManager sharedInstance] bookmarksForDocument:document]];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:bookmarksList];

    [nav setModalPresentationStyle:UIModalPresentationFormSheet];
    [nav setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
     if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
         nav.navigationBar.barStyle = UIBarStyleBlack;
     }
    
    //[self presentModalViewController:nav animated:YES];
    
    [self presentViewController:nav animated:YES completion:NULL];
}
-(void) selectBookMark:(Bookmark *) aBookmark{
    
    DLog( @"trying to open URL %@", aBookmark.pageUri);
    
    if(_onlineDocument){
       
        NSURL *url=[NSURL URLWithString:aBookmark.pageUri];
        [self showPage:url];

    }else{
        NSString *docURL = [NSString stringWithFormat:@"%@/%@",[document localMediaURI],aBookmark.pageUri];
        [self showPage:[NSURL fileURLWithPath:docURL]];
    }
}
-(void) hasRemoveBookmark:(Bookmark *) aBookmark{
    [[CoreDataManager sharedInstance] addOrRemoveBookmarkForDocument:aBookmark.document withPageUri:aBookmark.document.lastOpenedPage pageTitle:aBookmark.pageTitle];
    
    [self chekBookmarking];
}


-(void) showControls{
    float advance = 183;
    if(![[self.document.pubType lowercaseString] isEqualToString:@"topaze"]){
        self.buttonBookmarkOff.hidden = NO;
    }else{
        advance = 75;
    }
    CGRect rectOff = self.controlsView.frame;
    [UIView beginAnimations:@"showControls" context:nil];
	[UIView setAnimationDelay:0];
	[UIView setAnimationDuration:0.2];
	[UIView setAnimationDelegate:self];
	//[UIView setAnimationDidStopSelector:@selector()];
	
    self.openMenuButton.transform = CGAffineTransformMakeRotation(M_PI);
    
    
    
    
	self.controlsView.frame = CGRectMake(rectOff.origin.x - advance, rectOff.origin.y, rectOff.size.width, rectOff.size.height);
	
	[UIView commitAnimations];

    
    self.openMenuButton.accessibilityLabel = @"Appuyer pour masquer le menu";
    UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, self.openMenuButton);
    
    self.menuIsOpened = YES;
    [self performSelector:@selector(switchControlsVisibility:) withObject:nil afterDelay:KEEP_OPENED_DURATION];
    
    
}

-(void) hideControls{
    float advance = 183;
    if([[self.document.pubType lowercaseString] isEqualToString:@"topaze"]){
        advance = 75;
    }
    self.buttonBookmarkOff.hidden = YES;
    CGRect rectOff = self.controlsView.frame;
    [UIView beginAnimations:@"hideControls" context:nil];
	[UIView setAnimationDelay:0];
	[UIView setAnimationDuration:0.2];
	[UIView setAnimationDelegate:self];
	//[UIView setAnimationDidStopSelector:@selector()];
    
	self.controlsView.frame = CGRectMake(rectOff.origin.x + advance, rectOff.origin.y, rectOff.size.width, rectOff.size.height);
	
	[UIView commitAnimations];
	self.openMenuButton.transform = CGAffineTransformIdentity;
    
    UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, self.openMenuButton);
    self.openMenuButton.accessibilityLabel = [NSString stringWithFormat:@"Appuyer pour afficher le menu. Le menu se masque automatiquement après %d secondes.",KEEP_OPENED_DURATION];
    self.menuIsOpened = NO;

}


-(void) handleSingleTap{
    
    printf("handle single tap\n");
    
    //[self performSelector:@selector(switchControlsVisibility) withObject:nil afterDelay:0.0];
   }

-(IBAction) switchControlsVisibility:(id)sender{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(switchControlsVisibility:) object:nil];
    //BOOL visible = ! self.buttonBookmarkOff.hidden;
    //BOOL visible = (self.view.frame.size.width - self.controlsView.frame.origin.x ) > 50;
    if( self.menuIsOpened )[self hideControls];
    else [self showControls];
    
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    printf("willRotateToInterfaceOrientation\n");
   // [pomVC.view removeFromSuperview];
   
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self.webView setNeedsDisplay];
    //[self.webView reload];
}

-(void) showEmailModalView:(NSString *)to subject:(NSString *)subject body:(NSString *)body {
	if (![MFMailComposeViewController canSendMail]) {
		return;
	}
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;// &lt;- very important step if you want feedbacks on what the user did with your email sheet
	picker.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
	[picker setSubject:subject];
	[picker setToRecipients:[NSArray arrayWithObjects:to,nil]];
	
	// Fill out the email body text
	
	
	[picker setMessageBody:body isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
	
	//picker.navigationBar.barStyle = UIBarStyleBlack; // choose your style, unfortunately, Translucent colors behave quirky.
	
	//[self presentModalViewController:picker animated:YES];
    [self presentViewController:picker animated:YES completion:NULL];

}
-(void) chekBookmarking{
    if (document.lastOpenedPage && [[CoreDataManager sharedInstance] isPage:document.lastOpenedPage bookmarkedForDocument:document]) {
        self.buttonBookMarkOn.frame = self.buttonBookmarkOff.frame;
    } else {
        CGRect rectOff = self.buttonBookmarkOff.frame;
        
        self.buttonBookMarkOn.frame = CGRectMake(rectOff.origin.x + rectOff.size.width, rectOff.origin.y, rectOff.size.width, rectOff.size.height);
        
    }
}

-(void) dismissBrowser{
    DLog();
    if (![self isBeingDismissed]) {
        [self.webView stopLoading];
        [self dismissViewControllerAnimated:YES completion:^{}];
    }
}

-(BOOL)isTypedDocumentString:(NSString *)requestString{
    return ([[requestString lowercaseString] hasSuffix:@".pdf"]
			|| [[requestString lowercaseString] hasSuffix:@".ppt"]
			|| [[requestString lowercaseString] hasSuffix:@".pptx"]
			|| [[requestString lowercaseString] hasSuffix:@".key"]
			|| [[requestString lowercaseString] hasSuffix:@".knt"]
            || [[requestString lowercaseString] hasSuffix:@".doc"]
            || [[requestString lowercaseString] hasSuffix:@".txt"]
            || [[requestString lowercaseString] hasSuffix:@".xls"]
            || [[requestString lowercaseString] hasSuffix:@".csv"]
            || [[requestString lowercaseString] hasSuffix:@".odt"]
            || [[requestString lowercaseString] hasSuffix:@".odg"]
            || [[requestString lowercaseString] hasSuffix:@".zip"]);
}
- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}

-(BOOL)isClassicalPage:(NSString *)requestString{
    return ([[requestString lowercaseString] hasSuffix:@".htm"]
            || [[requestString lowercaseString] hasSuffix:@".html"]);
}


-(BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType) navigationType{
    //[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(switchControlsVisibility) object:nil];
    
    NSString *hostDocument = nil;
    if(self.document.link){
        hostDocument = [[NSURL URLWithString:self.document.link] host];
    }
    
    
    NSString *absoluteString = [[request URL] absoluteString];
    NSURL *standardizedURL = [[request URL] standardizedURL];
    NSString *host = [[request URL] host];
    NSString *scheme = [[request URL] scheme];
    NSString *pathExtension = [[request URL] pathExtension];
    NSString *lastPathComponent = [[request URL] lastPathComponent];
    
    DLog(@"\n***** absoluteString : %@\n***** standardizedURL : %@\n***** host : %@\n***** hostDocument : %@\n***** scheme : %@\n***** pathExtension : %@\n***** lastPathComponent : %@",absoluteString, standardizedURL,host,hostDocument,scheme,pathExtension,lastPathComponent);
    
    // A - Ecrire un mail
    if( absoluteString  && [absoluteString hasPrefix:@"mailto:"]){
		NSArray *components = [absoluteString componentsSeparatedByString:@":"];
		[self showEmailModalView:[components objectAtIndex:1] subject:@"A propos de Scenari Reader" body:@"Depuis l'application Scenari Reader"];
		return NO;
	}
    //Youtube embeded
    if([absoluteString hasPrefix:@"http://www.youtube.com/embed/"] || [absoluteString hasPrefix:@"www.youtube.com/embed/"]){
        return YES;
    }
    //Youtbe embeded local link (must add http: as prefix of the url)
    if(!_onlineDocument && [absoluteString hasPrefix:@"//www.youtube."]){
        NSURL *nurl = [NSURL URLWithString:[NSString stringWithFormat:@"%@http:",absoluteString]];
        NSURLRequest *nRequest = [NSURLRequest requestWithURL:nurl];
        return [self webView:webView shouldStartLoadWithRequest:nRequest navigationType:navigationType];
    }
    
    BOOL typedDoc = [self isTypedDocumentString:absoluteString];
    BOOL canOpen  = [[UIApplication sharedApplication] canOpenURL:[request URL]];
    
    //clicked link
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        // Catch links
        //On ouvre un lien externe ou un document
        if(_onlineDocument){
            DLog(@" *** ONLINE DOCUMENT ***");
            //on doit vérifier si le lien sort du document Opale
            
            if([lastPathComponent hasPrefix:@"www."]){
                DLog(@"Last path component hasPrefix:www ");
                NSURL *externalLinkURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/",lastPathComponent]];
                MyBrowser *myBrowser = [[MyBrowser alloc] initWithNibName:(IS_IPAD()?@"MyBrowser_iPad":@"MyBrowser_iPhone") bundle:nil];
                UINavigationController *nController = [[UINavigationController alloc] initWithRootViewController:myBrowser];
                UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Fermer" style:UIBarButtonItemStylePlain target:self action:@selector(dismissBrowser)];
                myBrowser.navigationItem.leftBarButtonItem = backButton;
                
                [nController setModalPresentationStyle:UIModalPresentationPageSheet];
                [nController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                
                if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
                    nController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
                }
                
                [self presentViewController:nController animated:YES completion:^{
                    [myBrowser showURL:[NSURLRequest requestWithURL:externalLinkURL]];
                }];
                
                return NO;
            }
            
            
            if(host && ([host isEqualToString:hostDocument] || [[NSString stringWithFormat:@"www.%@",host] isEqualToString:hostDocument] || [[NSString stringWithFormat:@"www.%@",hostDocument] isEqualToString:host]) &&![self isTypedDocumentString:absoluteString]){
                DLog(@"on navigue sur le même site que le document, on reste sur le lecteur Opale");
                if([self isClassicalPage:absoluteString]){
                    self.document.lastOpenedPage = absoluteString;
                }
                
                [[CoreDataManager sharedInstance] saveData];
                //DLog("document.lastOpenedPage = %@",document.lastOpenedPage);
                DLog("[request URL] = %@",[request URL]);
                [self chekBookmarking];
                return YES;
            }else if (typedDoc ){
                DLog(@"*** DOCUMENT IS TYPED ***");
                if(canOpen){
                    DLog(@"*** DOCUMENT CAN BE OPENED ***");
                    [[UIApplication sharedApplication] openURL:[request URL]];
                    //     DLog(@"[[UIApplication sharedApplication] openURL:[request URL]]");
                    
                    return NO;
                }else{
                    self.docController = [UIDocumentInteractionController interactionControllerWithURL:[request URL]];
                    [self.docController setDelegate:self];
                    [self.docController presentPreviewAnimated:YES];
                    //present a drop down list of the apps that support the file type, click an item in the list will open that app while passing in the file.
                    //[docController presentOpenInMenuFromRect:self.view.frame inView:self.view animated:YES];
                    return NO;
                }
                
            }else {
                //on ouvre un autre site via le navigateur
                MyBrowser *myBrowser = [[MyBrowser alloc] initWithNibName:(IS_IPAD()?@"MyBrowser_iPad":@"MyBrowser_iPhone") bundle:nil];
                
                
                UINavigationController *nController = [[UINavigationController alloc] initWithRootViewController:myBrowser];
                UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Fermer" style:UIBarButtonItemStylePlain target:self action:@selector(dismissBrowser)];
                myBrowser.navigationItem.leftBarButtonItem = backButton;
                
                
                [nController setModalPresentationStyle:UIModalPresentationPageSheet];
                [nController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                
                if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
                    nController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
                }
                [self presentViewController:nController animated:YES completion:^{
                    [myBrowser showURL:request];
                }];
                
                
                
                
                if([self isTypedDocumentString:absoluteString]){
                    // myBrowser.navigationItem.rightBarButtonItem=nil;
                    //use the UIDocInteractionController API to get list of devices that support the file type
                    
                    // UIDocumentInteractionController *docController = [UIDocumentInteractionController interactionControllerWithURL:[request URL]];
                    
                    //present a drop down list of the apps that support the file type, click an item in the list will open that app while passing in the file.
                    //[docController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
                    if([[UIApplication sharedApplication] canOpenURL:[request URL]]){
                        // [[UIApplication sharedApplication] openURL:[request URL]];
                    }
                }
                return NO;
            }
        }else{//On navigue dans un document téléchargé
            
           DLog("*** DOCUMENT LOCAL *** ");
            
            
           if (typedDoc ){
               DLog(@"*** DOCUMENT IS TYPED ***");
               if(canOpen){
                   DLog(@"*** DOCUMENT CAN BE OPENED ***");
                   [[UIApplication sharedApplication] openURL:[request URL]];
               //     DLog(@"[[UIApplication sharedApplication] openURL:[request URL]]");

                    return NO;
               }else if([[absoluteString lowercaseString] hasSuffix:@".zip"] ||[[absoluteString lowercaseString] hasSuffix:@".bin"]){
                   self.docController = [UIDocumentInteractionController interactionControllerWithURL:[request URL]];
                   [ self.docController presentOpenInMenuFromRect:CGRectMake(self.point.x, self.point.y, 1, 1) inView: self.view animated: YES ];
                    return NO;
               }else{
                   self.docController = [UIDocumentInteractionController interactionControllerWithURL:[request URL]];
                   [self.docController setDelegate:self];
                   [self.docController presentPreviewAnimated:YES];
                //present a drop down list of the apps that support the file type, click an item in the list will open that app while passing in the file.
                //[docController presentOpenInMenuFromRect:self.view.frame inView:self.view animated:YES];
                return NO;
               }

            }
            
            if([lastPathComponent hasPrefix:@"www."]){
                NSURL *externalLinkURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/",lastPathComponent]];
                MyBrowser *myBrowser = [[MyBrowser alloc] initWithNibName:(IS_IPAD()?@"MyBrowser_iPad":@"MyBrowser_iPhone") bundle:nil];
                UINavigationController *nController = [[UINavigationController alloc] initWithRootViewController:myBrowser];
                UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Fermer" style:UIBarButtonItemStylePlain target:self action:@selector(dismissBrowser)];
                myBrowser.navigationItem.leftBarButtonItem = backButton;
                
                [nController setModalPresentationStyle:UIModalPresentationPageSheet];
                [nController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                
                if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
                    nController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
                }
                
                [self presentViewController:nController animated:YES completion:^{
                    [myBrowser showURL:[NSURLRequest requestWithURL:externalLinkURL]];
                }];
                
                return NO;
            }
            if ([self isTypedDocumentString:absoluteString] || [absoluteString hasPrefix:@"http"] || [absoluteString hasPrefix:@"www."]){
                MyBrowser *myBrowser = [[MyBrowser alloc] initWithNibName:(IS_IPAD()?@"MyBrowser_iPad":@"MyBrowser_iPhone") bundle:nil];
                UINavigationController *nController = [[UINavigationController alloc] initWithRootViewController:myBrowser];
                UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Fermer" style:UIBarButtonItemStylePlain target:self action:@selector(dismissBrowser)];
                myBrowser.navigationItem.leftBarButtonItem = backButton;
                
                [nController setModalPresentationStyle:UIModalPresentationPageSheet];
                [nController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                
                if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
                    nController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
                }
                
                [self presentViewController:nController animated:YES completion:^{
                    [myBrowser showURL:request];
                }];
                
                
                
                return NO;
            }

        }
        
    }
   
    //NSArray *components = [[[request URL] path] componentsSeparatedByString:@"co/"];
    if([self isClassicalPage:absoluteString]){
        if(_onlineDocument){
            document.lastOpenedPage = absoluteString;
        }else{
            document.lastOpenedPage = [NSString stringWithFormat:@"co/%@", lastPathComponent];
        }
    }
    
    
    [[CoreDataManager sharedInstance] saveData];
    [self chekBookmarking];
    return YES;
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
	// Notifies users about errors associated with the interface
	switch (result){
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
		default:{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Erreur lors de l'envoi du mail"
														   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
		}
			
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

- (void)searchForCurrentText{
    
    
    NSString *txt = searchField.text;
    
    DLog(@"texte = %@",txt);
    
    if(!txt || [txt length]==0){
        [self.webView removeAllHighlights];
        self.resultSearchLabel.text = @"";
        return ;
    }
    
    NSInteger n = [self.webView highlightAllOccurencesOfString:txt];
    
    
    
    if (n==0){
        self.resultSearchLabel.text = @"Aucun résultat";
    }else if(n==1){
        self.resultSearchLabel.text = @"1 résultat";
    }else{
        self.resultSearchLabel.text =
        [NSString stringWithFormat:@"%i résultats",n];
    }
    
}

@end
