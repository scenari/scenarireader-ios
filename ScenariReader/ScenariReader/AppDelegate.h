//
//  AppDelegate.h
//  ScenariReader
//
//  Created by soreha on 02/04/13.
//  
//

#import <UIKit/UIKit.h>
#import "MediaHandlerDelegate.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, MediaHandlerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIImageView *splashView;

@end
