//
//  WebMediaRssImporterDelegate.h
//  WebMediaTest
//
//  Created by soreha on 25/01/11.
//  
//

#import <Foundation/Foundation.h>


@protocol DocumentRssImporterDelegate <NSObject> 

@required
//Méthodes obligatoires
/* la réponse est obtenue en entier et est contenu dans data */
-(void) didFinishParsing;
/* il y a eu une erreur qui est définie dans error */
-(void) didFailParsingWithError:(NSError *)error;

@end
