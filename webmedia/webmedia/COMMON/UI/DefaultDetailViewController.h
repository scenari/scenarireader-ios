//
//  DefaultDetailViewController.h
//  OptimGenericReader
//
//  Created by soreha on 24/02/11.
//  
//
#import <MessageUI/MessageUI.h>
#import <UIKit/UIKit.h>


@interface DefaultDetailViewController : UIViewController <UIWebViewDelegate, MFMailComposeViewControllerDelegate>{
	IBOutlet UIWebView *content;
	IBOutlet UIButton *actionButton;
	
	UIColor *defaultTintColor;
	
	int currentItem;
	NSArray *itemsList;
	
}

@property (nonatomic, strong) IBOutlet UIWebView *content;
@property (nonatomic, strong) IBOutlet UIButton *actionButton;
@property (nonatomic, strong) NSArray *itemsList;
@property (nonatomic) int currentItem;

-(IBAction) deleteDocument;

- (void)setItems:(NSArray *)items withCurrent:(int)current;
- (NSString *)number2digit:(int)number;
- (NSString *)encodeBase64WithData:(NSData *)objData;

@end
