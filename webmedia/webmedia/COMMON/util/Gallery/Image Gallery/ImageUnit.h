//
//  ImageUnit.h
//  WebMedia
//
//  Created by soreha on 22/02/11.
//  
//

#import <Foundation/Foundation.h>


@interface ImageUnit : NSObject {
	NSString *identifiant;
	NSString *name;
	NSString *description;
	NSString *imageTitle;
	NSString *refUri;
}
@property (nonatomic, strong) NSString *identifiant;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *imageTitle;
@property (nonatomic, strong) NSString *refUri;

- (id) initWithUri:(NSString*)refUri title:(NSString*)aTitle;
@end
