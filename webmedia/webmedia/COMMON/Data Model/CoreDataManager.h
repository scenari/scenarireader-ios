//
//  CoreDataManager.h
//  WebMediaTest
//
//  Created by soreha on 21/01/11.
//  
//

#import <Foundation/Foundation.h>
#import "RequestManagerCoreDataDelegate.h"
#import "DocumentRssImporterDelegate.h"

@class Channel;
@protocol RssDelegate <NSObject>

- (void)failToUpdateChannel:(Channel*)channel;
- (void)successToUpdateChannel:(Channel*)channel;
@end

@class Document, Thumbnail, Channel, RequestManager;

@interface CoreDataManager : NSObject <NSFetchedResultsControllerDelegate, RequestManagerCoreDataDelegate, DocumentRssImporterDelegate> 

@property (nonatomic, strong) RequestManager *requestManager;


// Properties for the Core Data stack.
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSString *persistentStorePath;
@property (nonatomic, strong) id<RssDelegate> rssDelegate;

+ (CoreDataManager *)sharedInstance;

//Data management
- (void)saveData;


//Thumbnail management
- (Thumbnail *)getThumbnailForURL:(NSString *)url;
- (void)addOrModifyDocument:(Document *)document  forChannel:(Channel *)channel;
- (void)downloadThumbnailForDocument:(Document *)document;

// Channel management
- (Channel *) getChannelForRssUrl:(NSString *) rssUrl;
- (Channel *) createChannelForRssUrl:(NSString *) rssUrl;
- (void) deleteChannel:(Channel *)channel;
- (NSFetchRequest *)fetchRequestAllChannel;
- (NSArray *)categoriesForChannel:(Channel *) channel;
- (void)downloadThumbnailForChannel:(Channel *) channel;


// Documents management
- (Document *) documentForGuid : (NSString *) guid;
- (Document *) newDocument;
- (void) removeDownloadedDocument :(Document *) document;

- (NSFetchRequest *) fetchRequestAvailableDocumentsForChannel : (Channel *) channel withCategory : (NSString *) category sortedBy : (int) criteria;
- (NSFetchRequest *) fetchRequestDownloadedDocumentsForChannel : (Channel *) channel withCategory : (NSString *) category sortedBy : (int) criteria;
- (NSFetchRequest *) fetchRequestAllDocumentsForChannel: (Channel *) channel sortedBy: (int) criteria;;

- (NSFetchRequest *) fetchRequestAvailableSelectedDocumentsForChannel : (Channel *) channel withCategory : (NSString *) category;

- (NSFetchRequest *) fetchRequestAvailableOrModifiedDocuments;
- (NSFetchRequest *) fetchRequestAvailableOrModifiedDocumentsForChannel : (Channel *) channel withCategory : (NSString *) category;

- (NSFetchRequest *) fetchRequestFavoriteDocuments;
- (NSFetchRequest *) fetchRequestFavoriteDocumentsWithCategory:(NSString *)category sortedBy:(int)criteria;
- (NSFetchRequest *) fetchRequestFavoriteDocumentsForChannel:(Channel *)channel withCategory:(NSString *)category sortedBy:(int)criteria;
- (NSFetchRequest *) fetchRequestDownloadingDocuments : (BOOL) selected;

- (NSFetchRequest *) fetchRequestAllDocuments;
- (NSFetchRequest *) fetchRequestAllDocumentsForKeyword : (NSString *) keyword;

- (NSFetchRequest *) fetchRequestAllDocumentsWithDistanceLessThan : (NSNumber *) distance;

- (void) manageDeletedDocuments: (NSMutableSet *) availableDocs forChannel : (Channel *) channel;

-(void) addOrRemoveBookmarkForDocument: (Document *) document withPageUri : (NSString *) pageUri pageTitle : (NSString *) pageTitle;
-(BOOL) isPage: (NSString *) pageUri bookmarkedForDocument :(Document *) document;
-(Bookmark *) bookmarkForDocument: (Document *)document withPageUri : (NSString *) pageUri;
-(NSArray *) bookmarksForDocument: (Document *)document;

// Download management
- (void)downloadAllChannelRssData;
- (void)downloadRssDataForNewChannel:(Channel *)channel;
- (void)downloadRssDataForChannel:(Channel *)channel;
- (void)downloadAllDocumentsForChannel:(Channel *) channel withCategory:(NSString *)category;
- (BOOL)downloadSelectedDocumentsForChannel:(Channel *)channel withCategory:(NSString *)category;
- (void)downloadDocument:(Document *)document;
- (void)performDownloadsForDocuments:(NSArray *)documents;


@end
