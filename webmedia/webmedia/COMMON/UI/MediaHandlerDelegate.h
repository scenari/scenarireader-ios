//
//  MediaHandlerDelegate.h
//  WebMedia Reader
//
//  Created by soreha on 07/04/11.
//  
//

#import <Foundation/Foundation.h>

@class Document;

@protocol MediaHandlerDelegate <NSObject>

@required 
-(void)openDocument:(Document *)document;


@optional
-(void)openDocument:(Document *) document onLastOpenedPage:(BOOL) lastOpenPage;
-(void)openOnlineDocument:(Document *) document onLastOpenedPage:(BOOL) lastOpenPage;
@end
