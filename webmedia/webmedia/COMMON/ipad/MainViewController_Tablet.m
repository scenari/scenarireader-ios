//
//  MainViewController_Tablet.m
//  webmedia
//
//  Created by soreha on 27/11/12.
//  
//

#import "MainViewController_Tablet.h"
#import "ConfigurationManager.h"
#import "Thumbnail.h"
#import "BroadcastView.h"
#import "RequestManager.h"
#import "ASINetworkQueue.h"
#import "ChannelDetailViewController.h"

#import "Document.h"
#import "SamplePopoverBackgroundView.h"
#import "DownloadManagerViewController.h"
#import "SearchViewController.h"
#import "AboutViewController.h"
#import "AddChannelViewController.h"

#import "Reachability.h"
#import "DocumentMenuTableViewController.h"
#import "MediaHandlerManager.h"

#define START_X 10
#define START_Y 10
#define HORIZONTAL_GAP 20
#define VERTICAL_GAP 0
#define BROADCAST_VIEW_WIDTH 236
#define BROADCAST_VIEW_HEIGHT 240

@interface MainViewController_Tablet ()

@property (nonatomic) BroadcastView *selectedBV;
@property (nonatomic) ChannelView *selectedCV;

@end

@implementation MainViewController_Tablet
BOOL favModeOn = NO;


- (void)dealloc{
   // self.channelFetchedResultsController.delegate = nil;
   // self.channelFetchedResultsController = nil;
   // self.broadcastFetchedResultsController.delegate = nil;
   // self.broadcastFetchedResultsController = nil;
}


- (NSFetchedResultsController *)channelFetchedResultsController{
    DLog( );
    if(!_channelFetchedResultsController){
        _channelFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllChannel]
                                                                                   managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:nil
                                                                                              cacheName:@"AllChannel"];
        _channelFetchedResultsController.delegate = self;
        
    }
    return _channelFetchedResultsController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)configureForSingleChannelConfiguration{
    DLog();

    self.scrollChannels.hidden = YES;
    self.controlsView.frame = CGRectMake(0, 0, self.scrollChannels.bounds.size.width, self.controlsView.bounds.size.height);
    CGRect emmissionScrollFrame = CGRectMake(self.scrollChannels.bounds.origin.x,
                                             self.controlsView.bounds.size.height,
                                             self.scrollChannels.bounds.size.width,
                                             self.view.bounds.size.height-self.scrollChannels.bounds.origin.x - self.controlsView.bounds.size.height);
    self.scrollEmissions.frame = emmissionScrollFrame;
    if(!self.currentChannel)self.currentChannel = ([[self.channelFetchedResultsController fetchedObjects] count]>0) ? [[self.channelFetchedResultsController fetchedObjects] objectAtIndex:0] : nil;
    self.errorView.frame = CGRectMake(0, self.controlsView.frame.origin.y-4, self.scrollChannels.bounds.size.width, 57);
}

- (void)setChannelObject:(Channel *)channel toChannelView:(ChannelView *)channelView{
    UIImage *defaultImage = [UIImage imageNamed:@"chanelViewDefault"];

    [channelView.infoButton addTarget:self action:@selector(showInfoForChannel:) forControlEvents:UIControlEventTouchUpInside];
    [channelView.deleteButton addTarget:self action:@selector(askToConfirmChannelDelete:) forControlEvents:UIControlEventTouchUpInside];
    
    channelView.titleLabel.text = channel.title;
    
    [self.scrollChannels addSubview:channelView];
    if (channel.thumbnail == nil) {//L'image de la chaîne est nulle on tente de la télécharger
        channelView.imageView.image = defaultImage;
        
        if (channel.thumbnailURI != nil || [channel.thumbnailURI compare:@""] != NSOrderedSame) {
            [[CoreDataManager sharedInstance] downloadThumbnailForChannel:channel];
        }else{
            channelView.imageView.image = defaultImage;
        }
    } else {
        channelView.imageView.image = channel.thumbnail.image;
    }
    channelView.descriptionInfoTextView.text = channel.summary;
    channelView.selected = NO;
    channelView.delegate = self;
    channelView.channelObject = channel;
}
- (BOOL)connected{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    DLog(@"networkStatus=%d", networkStatus);
    return !(networkStatus == NotReachable);
}

- (void)configureForMultiChannelConfiguration:(BOOL)update{
    DLog();
    
    for (UIView *view in self.scrollChannels.subviews) {
        [view removeFromSuperview];
    }
    int n = [[self.channelFetchedResultsController fetchedObjects] count];
    
    self.noSourceLabel.hidden = (n>0);
    
    DLog(@"Nombre de chaines à ajouter : %d",n);
    
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    float startX = 15;
    
    for(int i=0;i<n;i++){
        Channel *channel = [[self.channelFetchedResultsController fetchedObjects] objectAtIndex:i];
        DLog(@"Channel add to scrollChannels : %@",channel.title);
        ChannelView *channelView = [[ChannelView alloc] initWithFrame:CGRectMake(startX+i*337, 4, 322, 180)];
        [self setChannelObject:channel toChannelView:channelView];
        if(channel==self.currentChannel)channelView.selected = YES;
        [array addObject:channelView];
    }
    [self.scrollChannels setContentSize:CGSizeMake(n*337+2*startX, 180)];
    self.channelViews = [NSMutableArray arrayWithArray:array];
    
    if(update || !self.currentChannel){
        [self didSelectChannelView:[self.channelViews count]>0?[self.channelViews objectAtIndex:0]:nil];
    }
    self.errorView.frame = CGRectMake(0, self.controlsView.frame.origin.y-4, self.scrollChannels.bounds.size.width, 57);
}
- (void)unselectAllChannelViews{
    DLog();
    
    for (ChannelView *cv in self.channelViews) {
        if(cv.selected){
            cv.selected = NO;
        }
    }
}
- (void)unselectAllChannelViewsExcepted:(ChannelView *)channelView{
    DLog();
    
    for (ChannelView *cv in self.channelViews) {
        if(cv!=channelView && cv.selected){
            cv.selected = NO;
        }
    }
}
- (void)didSelectChannelView:(ChannelView *)channelView{
    DLog();
    
    [self unselectAllChannelViewsExcepted:channelView];
    [channelView setSelected:YES];

    //Updtae content;
    self.currentChannel = channelView.channelObject;

}

- (void)viewDidLoad{
    DLog();
        
    [super viewDidLoad];
    
    //1 - Apparence
    self.title = [[ConfigurationManager sharedInstance] iPadMainViewTitle];
    
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        [self navigationController].navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
        self.scrollChannels.backgroundColor = [UIColor blackColor];
    }else{
        self.scrollChannels.backgroundColor = [[ConfigurationManager sharedInstance] mainViewTabletteChannelScrollBackgroundColor];
    }
    
    self.scrollEmissions.backgroundColor = [UIColor colorWithRed:0.392 green:0.392 blue:0.400 alpha:1.000];

    //boutons
    //left
    UIButton *bt2=[UIButton buttonWithType:UIButtonTypeCustom];
    bt2.accessibilityLabel = @"Rechercher un document";
    [bt2 setFrame:CGRectMake(0, 0, 48, 32)];
    [bt2 setImage:[UIImage imageNamed:SYSTEM_VERSION_LESS_THAN(@"7.0")?@"search20-inverted":@"search20"] forState:UIControlStateNormal];
    [bt2 addTarget:self action:@selector(searchAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchButton=[[UIBarButtonItem alloc] initWithCustomView:bt2];
    UIButton *bt3=[UIButton buttonWithType:UIButtonTypeCustom];
    bt3.accessibilityLabel = @"Afficher les téléchargments";
    [bt3 setFrame:CGRectMake(0, 0, 48, 32)];
    [bt3 setImage:[UIImage imageNamed:SYSTEM_VERSION_LESS_THAN(@"7.0")?@"downloadManager-inverted":@"downloadManager"] forState:UIControlStateNormal];
    [bt3 addTarget:self action:@selector(openDownloadManagerAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *downloadButton=[[UIBarButtonItem alloc] initWithCustomView:bt3];
    UIButton *bt4=[UIButton buttonWithType:UIButtonTypeCustom];
    bt4.accessibilityLabel = @"Ajouter une source de documents";
    [bt4 setFrame:CGRectMake(0, 0, 48, 32)];
    [bt4 setImage:[UIImage imageNamed:SYSTEM_VERSION_LESS_THAN(@"7.0")?@"addSource-inverted":@"addSource"] forState:UIControlStateNormal];
    [bt4 addTarget:self action:@selector(openAddChannelViewAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *addChannelButton = [[UIBarButtonItem alloc] initWithCustomView:bt4];
    
    if([[ConfigurationManager sharedInstance] canAddSource]){
         self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:/*favButton,*/searchButton,downloadButton,addChannelButton,nil];
    }else{
        self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:/*favButton,*/searchButton,downloadButton,nil];

    }
    //right
    
    UIButton *bt5 = [UIButton buttonWithType:UIButtonTypeInfoLight];
    bt5.accessibilityLabel = @"Information sur l'application Scenari Reader";
    [bt5 addTarget:self action:@selector(showInfoAction:) forControlEvents:UIControlEventTouchUpInside];    
    UIBarButtonItem *infoButton = [[UIBarButtonItem alloc] initWithCustomView:bt5];
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:infoButton,nil];
    
    
    [CoreDataManager sharedInstance].rssDelegate = self;
    
    UITapGestureRecognizer *tapToHide = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissErrorView)];
        
    [self.errorView addGestureRecognizer:tapToHide];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        addChannelButton.tintColor = downloadButton.tintColor = searchButton.tintColor =
        bt2.tintColor = bt3.tintColor = bt4.tintColor =[UIColor blueColor];
    }
    
}


- (void)viewDidAppear:(BOOL)animated{
    DLog();
    [super viewDidAppear:animated];
    
    
    //2 - Configuration chaine(s)
   
    NSArray *sources = [[ConfigurationManager sharedInstance] sourcesList] ;
    
    if([sources count]<1){
        ALog(@"ERROR MISSING SOURCE URL IN CONFIGURATION FILE");
        return;
    }
    
    NSString *channelURL = [sources objectAtIndex:0];
    Channel *channel = [[CoreDataManager sharedInstance] getChannelForRssUrl:channelURL];
    if(!channel && !([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@_deleted",channelURL]])){
        DLog(@"channel doesn't exist %@",channelURL);
        channel =[[CoreDataManager sharedInstance] createChannelForRssUrl:channelURL];
        [[CoreDataManager sharedInstance] saveData];
    }else{
        DLog(@"channel  exists or was deleted%@",channelURL);
    }
    NSError *error = nil;
	if (![self.channelFetchedResultsController performFetch:&error]){
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	}
    
    if([[ConfigurationManager sharedInstance] multiChannelConfiguration]){
        [self configureForMultiChannelConfiguration:NO];
    }else{
        [self configureForSingleChannelConfiguration];
    }
    //[self setCurrentChannel:_currentChannel? : firstChannel];
    
   
}


- (void)setCurrentChannel:(Channel *)currentChannel{
    DLog(@"currentChannel:%@",currentChannel);
    
    _currentChannel = currentChannel;
    if(currentChannel)[[CoreDataManager sharedInstance] downloadRssDataForChannel:currentChannel];
        
    self.broadcastFetchedResultsController.delegate = nil;
    self.broadcastFetchedResultsController = nil;
    for (UIView *view in self.scrollEmissions.subviews) {
        [view removeFromSuperview];
    }
    if(!currentChannel){
        self.broadcastViews = nil;
        self.noDocumentLabel.hidden = ([self.broadcastViews count] != 0);

        [self viewDidLayoutSubviews];
        return;
    }
    
    if(self.segmentType.selectedSegmentIndex == 0){//ALL
        self.broadcastFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllDocumentsForChannel:_currentChannel sortedBy:self.segmentOrder.selectedSegmentIndex]
                                                                        managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"category"
                                                                                                                        cacheName:nil];
    }else if(self.segmentType.selectedSegmentIndex == 1){//DOWNLOADED
        self.broadcastFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestDownloadedDocumentsForChannel:_currentChannel withCategory:nil sortedBy:self.segmentOrder.selectedSegmentIndex]
                                                                                     managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"category"
                                                                                                cacheName:nil];
    }else if(self.segmentType.selectedSegmentIndex == 2){//AVAILABLE
        self.broadcastFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAvailableDocumentsForChannel:_currentChannel withCategory:nil sortedBy:self.segmentOrder.selectedSegmentIndex]
                                                                                     managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"category"
                                                                                                cacheName:nil];
    }else if(self.segmentType.selectedSegmentIndex == 3){//FAVORITES
        self.broadcastFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestFavoriteDocumentsForChannel:_currentChannel withCategory:nil sortedBy:self.segmentOrder.selectedSegmentIndex]
                                                                                         managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"category"
                                                                                                    cacheName:nil];
    }
    self.broadcastFetchedResultsController.delegate = self;
	
    NSError *error = nil;
	if (![self.broadcastFetchedResultsController performFetch:&error])
    {
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	}
    
    
    //for (Document *doc in self.broadcastFetchedResultsController.fetchedObjects) {
    //}
    
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    
    for(int i=0,n=[self.broadcastFetchedResultsController.fetchedObjects count];i<n;i++){
        BroadcastView *bv = [[BroadcastView alloc] initWithFrame:CGRectMake(START_X, START_Y, BROADCAST_VIEW_WIDTH, BROADCAST_VIEW_HEIGHT)];
        [bv.buttonDownload addTarget:self action:@selector(downloadDocumentAction:) forControlEvents:UIControlEventTouchUpInside];
        [bv.buttonCancelDownload addTarget:self action:@selector(cancelDownloadDocumentAction:) forControlEvents:UIControlEventTouchUpInside];
        [bv.buttonResumeDownload addTarget:self action:@selector(resumeDownloadDocumentAction:) forControlEvents:UIControlEventTouchUpInside];
        [bv.buttonSuspendDownload addTarget:self action:@selector(suspendDownloadDocumentAction:) forControlEvents:UIControlEventTouchUpInside];
        [bv.infoButton addTarget:self action:@selector(showInfoForBroadcast:) forControlEvents:UIControlEventTouchUpInside];
        [bv.deleteButton addTarget:self action:@selector(askToConfirmBroadcastDelete:) forControlEvents:UIControlEventTouchUpInside];

        Document  *document = [self.broadcastFetchedResultsController.fetchedObjects objectAtIndex:i];
        bv.document = document;
        [bv updateDownloadProgress];

        if (document.thumbnail_large == nil) {
            bv.imageView.image = [UIImage imageNamed:@"noImageLarge"];
            if (document.thumbnail_small_URI != nil || [document.thumbnail_small_URI compare:@""] != NSOrderedSame) {
                [[CoreDataManager sharedInstance] downloadThumbnailForDocument:document];
            }
            
        } else {
            bv.imageView.image = document.thumbnail_large.image?:document.thumbnail_small.image;
        }
        
        [self.scrollEmissions addSubview:bv];
        [array addObject:bv];
    }
    
    self.broadcastViews = [NSArray arrayWithArray:array];
    [self viewDidLayoutSubviews];

    self.noDocumentLabel.hidden = ([self.broadcastViews count] != 0);

    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setScrollChannels:nil];
    [self setScrollEmissions:nil];
    [self setSegmentType:nil];
    [self setSegmentOrder:nil];
    [self setControlsView:nil];
    [self setErrorView:nil];
    [super viewDidUnload];
}
- (IBAction)showInfoAction:(id)sender {
    [self.menu dismissPopoverAnimated:NO];
    self.menu = nil;
    AboutViewController *aboutViewController = [[AboutViewController alloc] initWithNibName:UI_USER_INTERFACE_IDIOM()?@"AboutViewController_iPad":@"AboutViewController_iPhone" bundle:nil];
	UINavigationController *modalNav = [[UINavigationController alloc] initWithRootViewController:aboutViewController];
	modalNav.modalPresentationStyle = UIModalPresentationFormSheet;
	//[[self navigationController] presentModalViewController:modalNav animated:YES];
    //aboutViewController.navigationController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];   ;
    
    if(SYSTEM_VERSION_LESS_THAN(@"7.0)")){
        aboutViewController.navigationController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
    } else{
        aboutViewController.navigationController.navigationBar.tintColor = [UIColor blueColor];
    }
    
    [[self navigationController] presentViewController:modalNav animated:YES completion:^{
    }];
}
- (void)switchFavMode:(id)sender{
}
- (void)searchAction:(id)sender{
    UIButton *button = (UIButton *) sender;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    
    SearchViewController *searchController = [storyboard instantiateViewControllerWithIdentifier:@"searchViewController"];//[[DownloadManagerViewController alloc] initWithNibName:@"DownloadManager" bundle:nil];
	
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:searchController];
	searchController.contentSizeForViewInPopover = CGSizeMake(310, 350);
    navController.contentSizeForViewInPopover = CGSizeMake(310, 350);
    [self.menu dismissPopoverAnimated:NO];
    self.menu = nil;
    self.menu = [[UIPopoverController alloc] initWithContentViewController:navController];
    searchController.popover = self.menu;
    
    
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        self.menu.popoverBackgroundViewClass = [SamplePopoverBackgroundView class];
    }else{
        navController.navigationBar.barStyle = UIBarStyleDefault;
        navController.navigationBar.barTintColor = [UIColor whiteColor];
        self.menu.backgroundColor = [UIColor whiteColor];
        
    }
    [self.menu presentPopoverFromBarButtonItem:[self.navigationItem.rightBarButtonItems objectAtIndex:0] permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES] ;
                                                                                                                                   //]FromRect:button.frame inView:self.navigationController.navigationBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}
- (void)openAddChannelViewAction:(id)sender{
    DLog();
    [self.menu dismissPopoverAnimated:NO];
    self.menu = nil;
	self.addChannelView = nil;
    
    
    // if the class doesn't exist, myClass will be Nil and myVar will be nil
   
    
	self.addChannelView = [[AddChannelViewController alloc] init];
    UINavigationController *control = [[UINavigationController alloc] initWithRootViewController:self.addChannelView];
	control.navigationBar.barStyle = self.navigationController.navigationBar.barStyle;
    control.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
    self.addChannelView.addChannelViewDelegate = self;

    control.modalPresentationStyle = UIModalPresentationFormSheet;
    control.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	//[[self navigationController] presentModalViewController:control animated:YES];
    //control.view.superview.bounds = CGRectMake(0, 0, 420, 320);
    [[self navigationController] presentViewController:control animated:YES completion:^{
    }];
    control.view.superview.bounds = CGRectMake(0, 0, 420, 320);
}
#pragma mark -
#pragma mark AddChannelViewDelegate methods
- (void)addChannelForUrl : (NSString *) channelUrl{
    Channel * channel = nil;
	if ((channel = [[CoreDataManager sharedInstance] createChannelForRssUrl:channelUrl]) != nil) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:[NSString stringWithFormat:@"%@_deleted",channel.rssUrl]];
		[[CoreDataManager sharedInstance] downloadRssDataForNewChannel:channel];
	} else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Cette Chaine a déjà été ajoutée" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
		[alert show];
	}
}
- (void)cancelAddChanel:(UIViewController *)source{
    [self dismissModalViewControllerAnimated:YES];
}


- (void)openDownloadManagerAction:(id)sender{
    UIButton *button = (UIButton *) sender;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    
    DownloadManagerViewController *downloadManager = [storyboard instantiateViewControllerWithIdentifier:@"downloadManagerViewController"];//[[DownloadManagerViewController alloc] initWithNibName:@"DownloadManager" bundle:nil];
	
	downloadManager.contentSizeForViewInPopover = CGSizeMake(310, 350);
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:downloadManager];
    [self.menu dismissPopoverAnimated:NO];
    self.menu = nil;
    self.menu = [[UIPopoverController alloc] initWithContentViewController:navController];
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        self.menu.popoverBackgroundViewClass = [SamplePopoverBackgroundView class];
    }else{
        navController.navigationBar.barStyle = UIBarStyleDefault;
        navController.navigationBar.barTintColor = [UIColor whiteColor];
        self.menu.backgroundColor = [UIColor whiteColor];
        
    }
    [self.menu presentPopoverFromBarButtonItem:[self.navigationItem.rightBarButtonItems objectAtIndex:1] permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES] ;

    //[self.menu presentPopoverFromRect:button.frame inView:self.navigationController.navigationBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath{

    //Modification des résultats de la requete pour obtenir les émissions
    if(controller == self.broadcastFetchedResultsController){

        
        if([anObject isKindOfClass:[Document class]]){
             if(type == NSFetchedResultsChangeUpdate){

                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     dispatch_async(dispatch_get_main_queue(), ^{
                         //BroadcastView *bv = [self.broadcastViews objectAtIndex:indexPath.row];
                         Document  *document = [self.broadcastFetchedResultsController objectAtIndexPath:indexPath];
                         for (BroadcastView *bv in self.broadcastViews) {
                             if([bv.document.guid isEqualToString:document.guid]){
                                 bv.document = document;
                                 [bv updateDownloadProgress];
                                 bv.imageView.image = document.thumbnail_large.image?:document.thumbnail_small.image;
                                 [bv updateDownloadProgress];
                                 break;
                             }
                         }
                         
                     });
                 });
             }
             else{
                 [self setCurrentChannel:self.currentChannel];
                /*
                 switch(type){
                        case NSFetchedResultsChangeInsert:
                            [self setCurrentChannel:self.currentChannel];
                            break;
                        case NSFetchedResultsChangeDelete:
                            //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                            ;
                            break;
                        case NSFetchedResultsChangeMove:
                            //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                            //[tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
                            break;
                    }
                */
             }
        }
    }else if(controller == self.channelFetchedResultsController){
        //Modification des résultats de la requete pour obtenir les Chaines
        
        switch(type){
            case NSFetchedResultsChangeUpdate:

                //int n = indexPath.row;
                /*
                if(n<[self.channelViews count]){
                    ChannelView *cv = [self.channelViews objectAtIndex:n];
                    [cv setChannelObject:[self.channelFetchedResultsController objectAtIndexPath:indexPath]];
                }*/
                if([[ConfigurationManager sharedInstance] multiChannelConfiguration]){
                    [self configureForMultiChannelConfiguration:NO];
                }else{
                    [self configureForSingleChannelConfiguration];
                }
                break;
            case NSFetchedResultsChangeInsert:
                if([[ConfigurationManager sharedInstance] multiChannelConfiguration]){
                    [self configureForMultiChannelConfiguration:YES];
                }else{
                    [self configureForSingleChannelConfiguration];
                }
                break;
            case NSFetchedResultsChangeDelete:
                if([[ConfigurationManager sharedInstance] multiChannelConfiguration]){
                    [self configureForMultiChannelConfiguration:YES];
                }else{
                    [self configureForSingleChannelConfiguration];
                }
                break;

                break;
            case NSFetchedResultsChangeMove:
                if([[ConfigurationManager sharedInstance] multiChannelConfiguration]){
                    [self configureForMultiChannelConfiguration:YES];
                }else{
                    [self configureForSingleChannelConfiguration];
                }
                break;                break;
        }
        
        /*
        
        if(type == NSFetchedResultsChangeDelete || type == NSFetchedResultsChangeInsert){

         //[self setCurrentChannel:self.currentChannel];
           
            if([[ConfigurationManager sharedInstance] multiChannelConfiguration]){
                [self configureForMultiChannelConfiguration];
            }else{
                [self configureForSingleChannelConfiguration];
            }
       //
        }
            //else{

         
       // }
         */
    }
    
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    
    /*
    if(controller == self.channelFetchedResultsController){//mise à jour des chaînes
        
        
        if([[ConfigurationManager sharedInstance] multiChannelConfiguration]){
            [self configureForMultiChannelConfiguration:YES];
        }else{
            [self configureForSingleChannelConfiguration];
        }
        
        
    }
    */
}
- (void)cancelDownloadDocumentAction:(id)sender{
    BroadcastView *bv = (BroadcastView *)[[((UIButton *)sender) superview] superview];
    [bv.document.request clearDelegatesAndCancel];
    [bv hideDownloadingOptionButtons];
    [bv hideSuspendedOptionButtons];
/*
    bv.document.downloadProgress = 0;
    bv.document.downloading = [NSNumber numberWithBool:NO];
    bv.document.downloadSuspended = [NSNumber numberWithBool:NO];
    bv.document.isSelected = [NSNumber numberWithBool:NO];
    bv.document.request = nil;
*/
    [[CoreDataManager sharedInstance] removeDownloadedDocument:bv.document];
    [bv updateDownloadProgress];
}
- (void)resumeDownloadDocumentAction:(id)sender{
    BroadcastView *bv = (BroadcastView *)[[((UIButton *)sender) superview] superview];
    [bv hideSuspendedOptionButtons];

    if ([CoreDataManager sharedInstance].requestManager.documentDownloadQueue == nil) {
        [[CoreDataManager sharedInstance].requestManager initAsiQueue];
    }
    
    
    bv.document.isSelected = [NSNumber numberWithBool:NO];
    if (bv.document.request == nil) {
        [[CoreDataManager sharedInstance].requestManager addDocumentRequest:bv.document];
    }
    //Document suspended between updates
    NSString *tmpDirectory = [[[CoreDataManager sharedInstance] requestManager] getTmpDownloadFolder];
    
    if(bv.document.tmpDownloadFilePath && ![bv.document.tmpDownloadFilePath hasPrefix:tmpDirectory]){
        //Cancel previous tmp download file
        ALog(@"new app version number -> previous tmp file canceled document:%@",bv.document.title);

        bv.document.tmpDownloadFilePath = nil;
    }
    
    [[CoreDataManager sharedInstance].requestManager.documentDownloadQueue go];
    [[CoreDataManager sharedInstance] saveData];
    [bv updateDownloadProgress];

    
}
- (void)suspendDownloadDocumentAction:(id)sender{
    BroadcastView *bv = (BroadcastView *)[[((UIButton *)sender) superview] superview];
    [bv hideDownloadingOptionButtons];

        [bv.document.request clearDelegatesAndCancel];
        bv.document.downloadProgress = 0;
        bv.document.downloadSuspended = [NSNumber numberWithBool:YES];
        bv.document.isSelected = [NSNumber numberWithBool:NO];
        bv.document.request = nil;
    
    [[CoreDataManager sharedInstance] saveData];
    [bv updateDownloadProgress];

}
- (void)downloadDocumentAction:(id)sender{
    if ([CoreDataManager sharedInstance].requestManager.documentDownloadQueue == nil) {
        [[CoreDataManager sharedInstance].requestManager initAsiQueue];
    }
    BroadcastView *bv = (BroadcastView *)[[((UIButton *)sender) superview] superview];
    [bv.document setIsSelected:[NSNumber numberWithInt:1]];
    [[CoreDataManager sharedInstance] downloadSelectedDocumentsForChannel:self.currentChannel withCategory:nil];
    [bv updateDownloadProgress];
   
}
- (void)askToConfirmBroadcastDelete:(id)sender{
    UIButton *button = (UIButton *) sender;
    if(![[button superview] isMemberOfClass:[BroadcastView class]]){
        return;
    }
    
    BroadcastView *bv = ( BroadcastView *) [button superview];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Suppression"
                                                    message:[NSString stringWithFormat:@"Confirmer la suppression de \"%@\"",bv.document.title] delegate:self cancelButtonTitle:@"Annuler" otherButtonTitles:@"Supprimer", nil];
    alert.tag = 100;
    alert.delegate = self;
    self.selectedBV = bv;
    [NSObject cancelPreviousPerformRequestsWithTarget: bv.deleteButton];

    [alert show];
}
- (void)askToConfirmChannelDelete:(id)sender{
    UIButton *button = (UIButton *) sender;
    if(![[button superview] isMemberOfClass:[ChannelView class]]){
        return;
    }
    
    ChannelView *cv = ( ChannelView *) [button superview];
    Channel *channel= (Channel *)(cv.channelObject);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Suppression"
                                                    message:[NSString stringWithFormat:@"Confirmer la suppression de \"%@\"",channel.title] delegate:self cancelButtonTitle:@"Annuler" otherButtonTitles:@"Supprimer", nil];
    alert.tag = 200;
    alert.delegate = self;
    self.selectedCV = cv;
    [NSObject cancelPreviousPerformRequestsWithTarget: cv.deleteButton];

    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    if(alertView.tag == 100){
        if(buttonIndex == 1){
            [self.selectedBV.document.request clearDelegatesAndCancel];
            [self.selectedBV hideDownloadingOptionButtons];
            [self.selectedBV hideSuspendedOptionButtons];
            [[CoreDataManager sharedInstance] removeDownloadedDocument:self.selectedBV.document];
            [self.selectedBV updateDownloadProgress];
        }
        self.selectedBV.deleteButton.hidden = YES;
        self.selectedBV = nil;

    }else if(alertView.tag == 200){
        self.selectedCV.deleteButton.hidden = YES;
        if(buttonIndex == 1){
            Channel *channel= (Channel *)(self.selectedCV.channelObject);
            [[CoreDataManager sharedInstance] deleteChannel:channel];
        }
    }
}
- (void)showInfoForBroadcast:(id)sender{

    UIButton *button = (UIButton *) sender;
    if(![[button superview] isMemberOfClass:[BroadcastView class]]){
        return;
    }
    BroadcastView *bv = ( BroadcastView *) [button superview];
    //DocumentDetailViewController *detailViewController = [[DocumentDetailViewController alloc] initWithNibName:@"DefaultDetailViewController" bundle:nil];
	//detailViewController.favChangeDelegate = self;
	self.menuContent = nil;
    self.menuContent = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"documentMenuTableViewController"];//[[DownloadManagerViewController alloc] initWithNibName:@"DownloadManager" bundle:nil];
    self.menuContent.menuDelegate = self;
    self.menuContent.userInfo = bv;
    Document * document = bv.document;
	
	//[detailViewController setItems:[NSArray arrayWithObject:document] withCurrent:0];
	self.menuContent.contentSizeForViewInPopover = CGSizeMake(310, 350);
	
	//ios 7>
	if([self.menuContent respondsToSelector:@selector(preferredContentSize:)]){
		self.menuContent.preferredContentSize = CGSizeMake(310, 350);
	}

	
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.menuContent];
    self.menu = nil;
    self.menu = [[UIPopoverController alloc] initWithContentViewController:navController];
    
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        self.menu.popoverBackgroundViewClass = [SamplePopoverBackgroundView class];
    }else{
        navController.navigationBar.barStyle = UIBarStyleDefault;
        navController.navigationBar.barTintColor = [UIColor whiteColor];
        self.menu.backgroundColor = [UIColor whiteColor];
        
    }
	[self.menuContent setDocument:document];
    [self.menu presentPopoverFromRect:button.frame inView:bv permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}

- (void)showInfoForChannel:(id)sender{
    UIButton *button = (UIButton *) sender;
    if(![[button superview] isMemberOfClass:[ChannelView class]]){
        return;
    }
    ChannelView *cv = ( ChannelView *) [button superview];
	
	Channel *channel = cv.channelObject;
    
    ChannelDetailViewController *detailViewController = [[ChannelDetailViewController alloc] initWithNibName:@"DefaultDetailViewController" bundle:nil];
	[detailViewController setItems:[NSArray arrayWithObject:channel] withCurrent:0];
	
	detailViewController.contentSizeForViewInPopover = CGSizeMake(310, 350);
	
	//ios 7>
	if([detailViewController respondsToSelector:@selector(preferredContentSize:)]){
		detailViewController.preferredContentSize = CGSizeMake(310, 350);
	}
	
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:detailViewController];
    self.menu = nil;
    self.menu = [[UIPopoverController alloc] initWithContentViewController:navController];
    
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        self.menu.popoverBackgroundViewClass = [SamplePopoverBackgroundView class];
    }else{
        navController.navigationBar.barStyle = UIBarStyleDefault;
        navController.navigationBar.barTintColor = [UIColor whiteColor];
        self.menu.backgroundColor = [UIColor whiteColor];
        
    }
    [self.menu presentPopoverFromRect:button.frame inView:cv permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}




- (void)favoriteAttributeForDocumentHasChanged:(Document *)document{
    [self.menuContent setDocument:document];

    if (self.menu && self.segmentType.selectedSegmentIndex==3) {
        [self.menu dismissPopoverAnimated:YES];
    }
    //[self setCurrentChannel:self.currentChannel];
}


- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    int itemPerLine=UIDeviceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])?4:3;
    for(int i=0,n=[self.broadcastViews count];i<n;i++){
        BroadcastView *bv = [self.broadcastViews objectAtIndex:i];
        bv.frame = CGRectMake(START_X+(i%itemPerLine)*(BROADCAST_VIEW_WIDTH + HORIZONTAL_GAP), START_Y+(i/itemPerLine)*(BROADCAST_VIEW_HEIGHT + VERTICAL_GAP), BROADCAST_VIEW_WIDTH, BROADCAST_VIEW_HEIGHT);
    }
    int lineNumber = ([self.broadcastViews count]%itemPerLine)==0 ? ([self.broadcastViews count]/itemPerLine) : ([self.broadcastViews count]/itemPerLine)+1;

    self.scrollEmissions.contentSize = CGSizeMake(self.scrollEmissions.contentSize.width,(lineNumber)*(BROADCAST_VIEW_HEIGHT + VERTICAL_GAP) );

}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}
- (IBAction)segmentOrderValueChange:(id)sender {
    [self setCurrentChannel:self.currentChannel];
}

- (IBAction)segmentTypeValueChange:(id)sender {
    [self setCurrentChannel:self.currentChannel];
}


- (void)dismissErrorView{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismissErrorView) object:nil];
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         self.errorView.frame = CGRectMake(0, self.controlsView.frame.origin.y-4, self.scrollChannels.bounds.size.width, 57);
                     } completion:^(BOOL finished) {
                         
                     }];
    
}
- (void)showErrorView{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismissErrorView) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showErrorView) object:nil];

    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         self.errorView.frame = CGRectMake(0, self.controlsView.frame.origin.y+self.controlsView.frame.size.height, self.scrollChannels.bounds.size.width, 57);
                     } completion:^(BOOL finished) {
                         [self performSelector:@selector(dismissErrorView) withObject:nil afterDelay:5];
                     }];
    
}
- (void)showInformationsForDocument:(Document *)document sender:(id)sender{
	DocumentDetailViewController *detailViewController = [[DocumentDetailViewController alloc] initWithNibName:@"DefaultDetailViewController" bundle:nil];
	detailViewController.favChangeDelegate = self;
	
	[detailViewController setItems:[NSArray arrayWithObject:document] withCurrent:0];
	detailViewController.contentSizeForViewInPopover = CGSizeMake(310, 350);
	
	//ios 7>
	if([detailViewController respondsToSelector:@selector(preferredContentSize:)]){
		detailViewController.preferredContentSize = CGSizeMake(310, 350);
	}
	
	DocumentMenuTableViewController *menu = (DocumentMenuTableViewController *)sender;
	[menu.navigationController pushViewController:detailViewController animated:YES];
}

- (void)failToUpdateChannel:(Channel*)channel{
    //Attention si chaine non initialisée le titre est : "Récupération des données..."
    DLog(@"Erreur de mise à jour de la chaine : %@ vs current channel %@",channel.title, self.currentChannel.title);
   // if(self.currentChannel == channel || ![[ConfigurationManager sharedInstance] multiChannelConfiguration]){
    [self performSelector:@selector(showErrorView) withObject:nil afterDelay:1];
   // }
}
- (void)successToUpdateChannel:(Channel*)channel{
    DLog(@"Mise à jour de la chaine OK : %@ vs current channel %@",channel.title, self.currentChannel.title);
}

- (void)addToFavs:(Document *)document sender:(id)sender{
    document.isFavorite = [NSNumber numberWithInt:1];
    [[CoreDataManager sharedInstance] saveData];
    [self favoriteAttributeForDocumentHasChanged:document];
}
- (void)removeFromFavs:(Document *)document sender:(id)sender{
    document.isFavorite = [NSNumber numberWithInt:0];
    [[CoreDataManager sharedInstance] saveData];
    [self favoriteAttributeForDocumentHasChanged:document];
}

- (void)openDocument:(Document *)document onLastOpenedPage:(BOOL)lastOpened sender:(id)sender{
    if (self.menu) {
        [self.menu dismissPopoverAnimated:YES];
    }
    [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document onLastOpenedPage:lastOpened];
}
- (void)openOnlineDocument:(Document *)document onLastOpenedPage:(BOOL)lastOpened sender:(id)sender{
    if (self.menu) {
        [self.menu dismissPopoverAnimated:YES];
    }
    [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openOnlineDocument:document onLastOpenedPage:lastOpened];
}

- (void)download:(Document *)document sender:(id)sender{
    if (self.menu) {
        [self.menu dismissPopoverAnimated:YES];
    }
    if([sender isKindOfClass:[DocumentMenuTableViewController class] ]){
        DocumentMenuTableViewController *tmp = (DocumentMenuTableViewController *)sender;
        BroadcastView *bv = (BroadcastView *)(tmp.userInfo);
        [bv.document setIsSelected:[NSNumber numberWithInt:1]];
        [[CoreDataManager sharedInstance] downloadSelectedDocumentsForChannel:self.currentChannel withCategory:nil];
        [bv updateDownloadProgress];
    }
}

- (void)resumeDownloadForDocument:(Document *)document sender:(id)sender{
    if (self.menu) {
        [self.menu dismissPopoverAnimated:YES];
    }
    if([sender isKindOfClass:[DocumentMenuTableViewController class] ]){
        DocumentMenuTableViewController *tmp = (DocumentMenuTableViewController *)sender;
        BroadcastView *bv = (BroadcastView *)(tmp.userInfo);
        [bv hideSuspendedOptionButtons];
    
        if ([CoreDataManager sharedInstance].requestManager.documentDownloadQueue == nil) {
            [[CoreDataManager sharedInstance].requestManager initAsiQueue];
        }
    
        bv.document.isSelected = [NSNumber numberWithBool:NO];
        if (bv.document.request == nil) {
            [[CoreDataManager sharedInstance].requestManager addDocumentRequest:bv.document];
        }
        //Document suspended between updates
        NSString *tmpDirectory = [[[CoreDataManager sharedInstance] requestManager] getTmpDownloadFolder];
    
        if(bv.document.tmpDownloadFilePath && ![bv.document.tmpDownloadFilePath hasPrefix:tmpDirectory]){
            //Cancel previous tmp download file
            DLog(@"new app version number -> previous tmp file canceled document:%@",bv.document.title);
        
            bv.document.tmpDownloadFilePath = nil;
        }
    
        [[CoreDataManager sharedInstance].requestManager.documentDownloadQueue go];
        [[CoreDataManager sharedInstance] saveData];
        [bv updateDownloadProgress];
    }

    
}
- (void)cancelDownloadForDocument:(Document *)document sender:(id)sender{
    if (self.menu) {
        [self.menu dismissPopoverAnimated:YES];
    }
    if([sender isKindOfClass:[DocumentMenuTableViewController class] ]){
        DocumentMenuTableViewController *tmp = (DocumentMenuTableViewController *)sender;
        BroadcastView *bv = (BroadcastView *)(tmp.userInfo);
        [bv.document.request clearDelegatesAndCancel];
        [bv hideDownloadingOptionButtons];
        [bv hideSuspendedOptionButtons];
    
        [[CoreDataManager sharedInstance] removeDownloadedDocument:bv.document];
        [bv updateDownloadProgress];
    }
    
}
- (void)suspendDownloadForDocument:(Document *)document sender:(id)sender{
    if (self.menu) {
        [self.menu dismissPopoverAnimated:YES];
    }
    if([sender isKindOfClass:[DocumentMenuTableViewController class] ]){
        DocumentMenuTableViewController *tmp = (DocumentMenuTableViewController *)sender;
        BroadcastView *bv = (BroadcastView *)(tmp.userInfo);

        [bv hideDownloadingOptionButtons];
    
        [bv.document.request clearDelegatesAndCancel];
        bv.document.downloadProgress = 0;
        bv.document.downloadSuspended = [NSNumber numberWithBool:YES];
        bv.document.isSelected = [NSNumber numberWithBool:NO];
        bv.document.request = nil;
    
        [[CoreDataManager sharedInstance] saveData];
        [bv updateDownloadProgress];
    }
}

- (void)deleteDocument:(Document *)document sender:(id)sender{
    if (self.menu) {
        [self.menu dismissPopoverAnimated:YES];
    }
    if([sender isKindOfClass:[DocumentMenuTableViewController class] ]){
        DocumentMenuTableViewController *tmp = (DocumentMenuTableViewController *)sender;
        BroadcastView *bv = (BroadcastView *)(tmp.userInfo);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Suppression"
                                                    message:[NSString stringWithFormat:@"Confirmer la suppression de \"%@\"",bv.document.title] delegate:self cancelButtonTitle:@"Annuler" otherButtonTitles:@"Supprimer", nil];
    alert.tag = 100;
    alert.delegate = self;
    self.selectedBV = bv;
    [NSObject cancelPreviousPerformRequestsWithTarget: bv.deleteButton];
    
    [alert show];
    }
}


@end
