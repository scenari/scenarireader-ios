//
//  OpaleWebReader.h
//  Opale Reader
//
//  Created by soreha on 03/05/11.
//  
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import "BookmarksList.h"

@class Document;



@interface OpaleWebReader : UIViewController <UIDocumentInteractionControllerDelegate, UIWebViewDelegate, UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate, BookMarkListManagement, UITextFieldDelegate>{
    UIWebView *webView;
    UIButton *buttonBookMarkOn;
    UIButton *buttonBookmarkOff;
    UIView *controlsView;
    UIButton *openMenuButton;
    
    Document *document;
    
    
    UITapGestureRecognizer *singleTap;
    UITapGestureRecognizer *doubleTap;
    UITapGestureRecognizer *singleTapToTouch;
    
    UIView *searchView;
    UITextField *searchField;
    UILabel *resultSearchLabel;
}


@property (nonatomic, strong) UIDocumentInteractionController *docController;
@property (nonatomic) CGPoint point;
@property (nonatomic) BOOL onlineDocument;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

@property (nonatomic, strong) IBOutlet UITextField *searchField;
@property (nonatomic, strong) IBOutlet UILabel *resultSearchLabel;

@property (nonatomic, strong) IBOutlet UIView *searchView;
@property (nonatomic, strong) IBOutlet UIWebView *webView;

@property (nonatomic, strong) IBOutlet UIButton *buttonBookMarkOn;
@property (nonatomic, strong) IBOutlet UIButton *buttonBookmarkOff;
@property (weak, nonatomic) IBOutlet UIButton *buttonShowBookMarks;
@property (weak, nonatomic) IBOutlet UIButton *buttonSearch;

@property (nonatomic, strong) IBOutlet UIView *controlsView;
@property (nonatomic, strong) IBOutlet UIButton *openMenuButton;


@property (nonatomic, strong) UITapGestureRecognizer *singleTap;
@property (nonatomic, strong) UITapGestureRecognizer *doubleTap;
@property (nonatomic, strong) UITapGestureRecognizer *singleTapToTouch;

@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (nonatomic) BOOL menuIsOpened;

@property (nonatomic, strong) Document *document;

- (IBAction)closeSearchModeAction:(id)sender;

- (IBAction)addToBookMark:(id)sender;
- (IBAction)removeToBookMark:(id)sender;

- (IBAction)homeAction:(id)sender;
- (IBAction)searchAction:(id)sender;
- (IBAction)showBookMarksAction:(id)sender;
- (IBAction)switchControlsVisibility:(id)sender;

- (void)showPage:(NSURL*)url;
- (void)openDocument:(Document *)aDocument onLastOpenedPage:(BOOL)showLast;
-(void) openOnlineDocument:(Document *)aDocument onLastOpenedPage :(BOOL) showLast;

- (void)chekBookmarking;

@end
