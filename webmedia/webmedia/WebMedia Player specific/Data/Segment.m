//
//  Segment.m
//  PhoneRadio
//
//  Created by soreha on 08/07/10.
//  
//
#import <AVFoundation/AVFoundation.h>

#import "Segment.h"
#import "Illustration.h"

@implementation Segment



NSMutableArray *illustrations;
NSMutableArray *complements;

@synthesize segmentId;
@synthesize title;
@synthesize short_description;
@synthesize long_description;

@synthesize start;
@synthesize end;

@synthesize illustrations;
@synthesize complements;

@synthesize mediaUri;
@synthesize mediaType;
@synthesize clipBegin;
@synthesize clipEnd;

-(id) init {
	if((self = [super init])){
		self.complements = [[NSMutableArray alloc] initWithCapacity:0];
		self.illustrations = [[NSMutableArray alloc] initWithCapacity:0];
        self.references = [[NSMutableArray alloc] initWithCapacity:0];
	}
	return self;
}

- (BOOL)hasIllustration{
    return [self.illustrations count]>0;
}
- (BOOL)hasIllustrationImage{
    if([self.illustrations count]==0){
        return NO;
    }
    for(int i=0, n=[self.illustrations count]; i<n;i++){
		Illustration *illu =[self.illustrations objectAtIndex:i];
        if([illu isImage])return YES;
    }
    return NO;
}
-(Illustration *) firstIllustration{
	if([self.illustrations count]>0)
		return [self.illustrations objectAtIndex:0];
	return nil;
}
-(int) illustrationIndexAtTime:(float) second{
	for(int i=0, n=[self.illustrations count]; i<n;i++){
		Illustration *illu =[self.illustrations objectAtIndex:i];
		if(illu.end>second){
			return i;
		}
	}
	return -1;
}
- (Illustration *)nextIllustration:(int)currentIllustrationIndex{
    if((currentIllustrationIndex+1)<[illustrations count]){
        return [illustrations objectAtIndex:currentIllustrationIndex+1];
    }
    return nil;
}
- (Illustration *)previousIllustration:(int)currentIllustrationIndex{
    if(currentIllustrationIndex>0 && currentIllustrationIndex<=[illustrations count]){
        return [illustrations objectAtIndex:currentIllustrationIndex-1];
    }
    return nil;
}

- (Illustration*)lastIllustration{
    return [illustrations lastObject];
}
-(Illustration *) illustrationAtTime:(float) second{
	DLog(@"with time = %f", second);
    [self printSegment];
    for(Illustration *illu in self.illustrations){
		DLog(@"%f : looking in start=%f and end=%f\n",second,illu.start,illu.end);
		if(second<=illu.end){
			DLog(@"found illustration :start=%f and end=%f \nURI=%@",illu.start,illu.end,illu.resource.refUri );
			return illu;
		}
	}
	return nil;
}
-(BOOL) hasComplement{
	return [self.complements count]!=0;
}
-(BOOL) hasReferences{
    return [self.references count]!=0;
}
- (void)printSegment{
     DLog(@"\nSegment  : \nsegmentId=%@\ntitle=%@\nstart=%f\nend=%f\nclipBegin=%f\nclipEnd=%f\nmediaUri=%@",segmentId,title,start,end,clipBegin,clipEnd,mediaUri);
}
-(BOOL) hasVideo{
    return [[self.mediaType lowercaseString] isEqualToString:@"video"];
}

- (NSArray *)illustrationBeginTimeValues{
    NSMutableArray *illustrationsTime = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (Illustration *illustration in self.illustrations) {
        DLog(@"illustration begin time : %@", [NSValue valueWithCMTime:CMTimeMake(illustration.start*1000.0f, 1000.0f)]);
        [illustrationsTime addObject:[NSValue valueWithCMTime:CMTimeMake(illustration.start*1000.0f, 1000.0f)]];
    }
    
    return [NSArray arrayWithArray:illustrationsTime];
}


@end
