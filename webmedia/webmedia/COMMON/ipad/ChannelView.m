//
//  ChannelView.m
//  webmedia
//
//  Created by soreha on 27/11/12.
//  
//

#import <QuartzCore/QuartzCore.h>


#import "ChannelView.h"
#import "ConfigurationManager.h"

@implementation ChannelView

- (id)initWithFrame:(CGRect)frame{
    DLog(@"frame : %@",NSStringFromCGRect(frame));
    self = [super initWithFrame:frame];
    if (self) {
        UIColor *bkgColor = SYSTEM_VERSION_LESS_THAN(@"7.0")?[UIColor blackColor]:[[ConfigurationManager sharedInstance] mainViewTabletteChannelScrollBackgroundColor];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 2,frame.size.width-4, 24)];
        _titleLabel.textAlignment = UITextAlignmentCenter;
        _titleLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        _titleLabel.textColor = [UIColor whiteColor];
        //_titleLabel.shadowColor = [UIColor blackColor];
        _titleLabel.opaque = YES;
        _titleLabel.font = [UIFont boldSystemFontOfSize:17];
        _titleLabel.adjustsFontSizeToFitWidth = YES;
        _titleLabel.minimumFontSize = 12;
        _titleLabel.isAccessibilityElement = YES;

        DLog(@"img height : %f",frame.size.height-4);
    
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2,100, frame.size.height-4)];
        _imageView.backgroundColor = bkgColor;
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_imageView];
        [self addSubview:_titleLabel];
 
        _descriptionInfoTextView = [[UITextView alloc] initWithFrame:CGRectMake(112, 26,frame.size.width-122, frame.size.height-60)];
        _descriptionInfoTextView.textColor = [UIColor whiteColor];
        _descriptionInfoTextView.backgroundColor = [UIColor clearColor];
        _descriptionInfoTextView.font = [UIFont systemFontOfSize:12];
        _descriptionInfoTextView.editable = NO;
        _descriptionInfoTextView.userInteractionEnabled = NO;
        [self addSubview:_descriptionInfoTextView];
        
        

        _backgroundFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height-24, frame.size.width, 24)];
        _backgroundFooterView.opaque = YES;
        _backgroundFooterView.backgroundColor = [UIColor colorWithRed:0.35 green:0.12 blue:0.3 alpha:0.5];
        //[self addSubview:_backgroundFooterView];
        
        self.selectedImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, frame.size.height-4, frame.size.width, 24)];
        self.selectedImage.image = [UIImage imageNamed:@"selectedChannel"];
        self.selectedImage.clipsToBounds = YES;
        //self.selectedImage.backgroundColor = [UIColor blackColor];
        [self addSubview:self.selectedImage];
        
        self.infoButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-32, frame.size.height-32, 24, 24)];
        [self.infoButton setImage:[UIImage imageNamed:@"infoWhite24"] forState:UIControlStateNormal];
        self.infoButton.hidden = YES;
        [self addSubview:self.infoButton];
        
        
        self.deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(0 , 0, 32, 32)];
        [self.deleteButton setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        self.deleteButton.hidden = YES;
        [self addSubview:self.deleteButton];
        
        self.backgroundColor = bkgColor;//[UIColor colorWithWhite:0 alpha:1];
        self.layer.borderColor = [[UIColor colorWithRed:0.439 green:0.443 blue:0.451 alpha:1] CGColor];
        self.layer.borderWidth = 1;
        
        
        self.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        [self addGestureRecognizer:tapGesture];
        tapGesture.delegate = self;
        
        if([[ConfigurationManager sharedInstance] canDeleteSource]){
            UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
            [self addGestureRecognizer:longPress];
            longPress.minimumPressDuration = 2;
            longPress.delegate = self;
            [longPress requireGestureRecognizerToFail:tapGesture];
        }
        
        self.opaque = YES;
        // Initialization code
    }
    return self;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return !([touch.view isKindOfClass:[UIButton class]] || [touch.view isKindOfClass:[UIPageControl class]]);
}

- (void)setSelected:(BOOL)selected{
    DLog(@"selected:%d ",selected);
    _selected = selected;
    self.titleLabel.alpha = _selected?1.0:0.5;
    self.descriptionInfoTextView.alpha = _selected?1.0:0.5;

    self.layer.borderColor = _selected?[[[ConfigurationManager sharedInstance] selectedChannelBorderColor] CGColor]:
                                        [[[ConfigurationManager sharedInstance] unselectedChannelBorderColor] CGColor];//[[UIColor colorWithRed:0.439 green:0.443 blue:0.451 alpha:0.33] CGColor];
   // self.backgroundFooterView.backgroundColor = _selected?[UIColor colorWithRed:0.85 green:0.00 blue:0.49 alpha:1]:[UIColor colorWithRed:0.35 green:0.12 blue:0.3 alpha:0.5];
    self.selectedImage.hidden = YES;//!selected;
    self.infoButton.hidden = !selected;
    [self setNeedsLayout];
}

- (void)handleSingleTap:(id)sender{
    if([self.delegate respondsToSelector:@selector(didSelectChannelView:)]){
        [self.delegate didSelectChannelView:self];
    }
}
- (void)handleLongPress:(id)sender{
    //[self setSelected:YES];
    //[self handleSingleTap:nil];
    self.deleteButton.hidden = NO;
    [NSObject cancelPreviousPerformRequestsWithTarget: self.deleteButton];
    [self.deleteButton performSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES] afterDelay:2.5];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)infoAction:(id)sender {
}




#pragma ACCESSIBILTY
- (NSArray *)accessibleElements{
    if ( _accessibleElements != nil )
    {
        return _accessibleElements;
    }
    _accessibleElements = [[NSMutableArray alloc] init];
    
    /* Create an accessibility element to represent the first contained element and initialize it as a component of MultiFacetedView. */
    /* Set attributes of the first contained element here. */
    [_accessibleElements addObject:self.titleLabel];
    [_accessibleElements addObject:self.descriptionInfoTextView];
    /* Perform similar steps for the second contained element. */
    
    /* Set attributes of the second contained element here. */
    [_accessibleElements addObject:self.infoButton];
    
    return _accessibleElements;
}

/* The container itself is not accessible, so MultiFacetedView should return NO in isAccessiblityElement. */
- (BOOL)isAccessibilityElement
{
    return NO;
}

/* The following methods are implementations of UIAccessibilityContainer protocol methods. */
- (NSInteger)accessibilityElementCount{
    return [[self accessibleElements] count];
}

- (id)accessibilityElementAtIndex:(NSInteger)index
{
    return [[self accessibleElements] objectAtIndex:index];
}

- (NSInteger)indexOfAccessibilityElement:(id)element
{
    return [[self accessibleElements] indexOfObject:element];
}



@end
