//
//  SearchWebView.h
//  OpaleReader
//
//  Created by soreha on 04/07/11.
//  
//

#import <Foundation/Foundation.h>

@interface UIWebView (SearchWebView)

- (NSInteger)highlightAllOccurencesOfString:(NSString*)str;
- (void)removeAllHighlights;

@end
