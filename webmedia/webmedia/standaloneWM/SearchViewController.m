//
//  SearchViewController.m
//  OptimGenericReader
//
//  Created by Mory Doukouré on 26/02/11.
//  Copyright 2011 Soreha. All rights reserved.
//

#import "SearchViewController.h"
#import "CoreDataManager.h"
#import "RequestManager.h"

#import "Document.h"
#import "Thumbnail.h"
#import "DocumentDetailViewController.h"
#import "ConfigurationManager.h"
#import "MediaHandlerManager.h"
#import "CoreDataManager.h"
#import "SearchViewControllerCell.h"


int sliderPreviousValue;

@implementation SearchViewController

@synthesize aChannel;
@synthesize aTableView;
@synthesize searchBar;
@synthesize searchView;
@synthesize defaultFetchedResultsController;
@synthesize textFilteredFetchedResultsController;
@synthesize distanceFilteredFetchedResultsController;
@synthesize searchModeInfo;
@synthesize distanceLabel;
@synthesize distanceSlider;
@synthesize searchModeSegment;
@synthesize locationManager;
@synthesize searchIsActive;
@synthesize searchDisplayController;

-(void) releaseOutlets{
	searchDisplayController = nil;
	aTableView = nil;
	searchBar = nil;
	searchModeInfo = nil;
	distanceLabel = nil;
	distanceSlider = nil;
	searchModeSegment = nil;
}

- (void)dealloc{
    [self releaseOutlets];
    self.defaultFetchedResultsController.delegate = nil;
    self.defaultFetchedResultsController = nil;
    self.textFilteredFetchedResultsController.delegate = nil;
    self.textFilteredFetchedResultsController = nil;
    self.distanceFilteredFetchedResultsController.delegate = nil;
    self.distanceFilteredFetchedResultsController = nil;
}



// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	self.title = @"Recherche";
    [self navigationController].navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];   ;   

    
    
    if(![[ConfigurationManager sharedInstance] useUserLocation]){
        //hide options search view
        CGRect placeToFill=self.searchView.bounds;
        [searchView removeFromSuperview];
        self.searchBar.frame = CGRectMake(0, 0, self.searchBar.bounds.size.width, self.searchBar.bounds.size.height);
       // [self.searchBar setNeedsLayout];
         self.aTableView.frame = CGRectMake(self.aTableView.frame.origin.x, self.searchBar.frame.size.height, self.aTableView.frame.size.width, self.aTableView.frame.size.height+placeToFill.size.height);
    }
    else {
       [[self locationManager] startUpdatingLocation];
    }
    [self updateSearchMode];
  }



- (void)closeView {
	[[self locationManager] setDelegate:nil];
	[self.navigationController dismissModalViewControllerAnimated:YES];
}


 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
 }



/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

-(void) initDataManager : (id) dManager {
    
    
    searchIsActive = NO;
    [self updateDistances];
	[self initDefaultFetchedResultController];
}

#pragma mark -
#pragma mark fetchedResultsController management

- (NSFetchedResultsController *) getCurrentFetchedResultController {
    NSFetchedResultsController * fetchedResultsController;
    if (searchModeSegment.selectedSegmentIndex==1) {
        fetchedResultsController = self.distanceFilteredFetchedResultsController;
    } else {
        if (self.searchIsActive) {
            fetchedResultsController = textFilteredFetchedResultsController;
        } else {
            fetchedResultsController = defaultFetchedResultsController;
        }
    }
    return fetchedResultsController;
}

-(void) initDefaultFetchedResultController {
    self.defaultFetchedResultsController.delegate = nil;
	self.defaultFetchedResultsController = nil;
	self.defaultFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllDocuments]
																		managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"channel.title" 
																				   cacheName:nil];
	self.defaultFetchedResultsController.delegate = self;
	
    NSError *error = nil;
	if (![self.defaultFetchedResultsController performFetch:&error]){
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
}

-(void) initDistanceFetchedResultController {
	NSNumber * d ;
	
	if (((int)self.distanceSlider.value) == 1){
		d = [[NSNumber alloc] initWithInt : 500];
	}else if (((int)self.distanceSlider.value) == 2){
		d = [[NSNumber alloc] initWithInt : 1000];
	}else if (((int)self.distanceSlider.value) == 3){
		d =  [[NSNumber alloc] initWithInt : 5000];
	}else if (((int)self.distanceSlider.value) == 4){
		d = [[NSNumber alloc] initWithInt : 10000];
	}else if (((int)self.distanceSlider.value) == 5){
		d = [[NSNumber alloc] initWithInt : 50000];
	}else if (((int)self.distanceSlider.value) == 6){
		d = [[NSNumber alloc] initWithInt : 50000000];
	}else{
       d = [[NSNumber alloc] initWithInt : 100]; 
    }
	
	
	DLog(@"distance min : %f m", [d floatValue]);
	self.distanceFilteredFetchedResultsController.delegate = nil;
	self.distanceFilteredFetchedResultsController = nil;
    
	self.distanceFilteredFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllDocumentsWithDistanceLessThan:d] 
																		managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:nil 
																				   cacheName:nil];
	
    self.distanceFilteredFetchedResultsController.delegate = self;
	
	
	NSError *error = nil;
	if (![self.distanceFilteredFetchedResultsController performFetch:&error]){
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    
	DLog(@"Nb doc filtrés : %d\n",[[self.distanceFilteredFetchedResultsController fetchedObjects] count]);
}

- (IBAction) updateSearchMode{
	distanceLabel.hidden = distanceSlider.hidden = (searchModeSegment.selectedSegmentIndex==0);
	searchModeInfo.hidden = searchBar.hidden = (searchModeSegment.selectedSegmentIndex==1);
	
	if(searchModeSegment.selectedSegmentIndex==1){
		// If it's not possible to get a location, then return.
		//CLLocation *location = [self.locationManager location];
		[self initDistanceFetchedResultController];
	} else { 
        [self.view bringSubviewToFront:searchBar];
        [self initDefaultFetchedResultController];
    }
    [self.aTableView reloadData];
}

- (IBAction) sliderValueChanged:(id)sender{
	
	int step = (int)[distanceSlider value];
	
	if(sliderPreviousValue == step) return;
	
	sliderPreviousValue = step;
	
    if(!self.distanceValues){
        self.distanceValues = [[NSArray alloc] initWithObjects:@"Distance inférieure à 100 m", @"Distance inférieure à 500 m", @"Distance inférieure à 1 km", @"Distance inférieure à 5 km", @"Distance inférieure à 10 km", @"Distance inférieure à 50 km", @"Distance illimitée", nil];
    }
    
	distanceLabel.text = [self.distanceValues objectAtIndex:step];
    distanceLabel.hidden=NO;
	
	[self initDistanceFetchedResultController];
    
	[self.aTableView reloadData];
}


#pragma mark -
#pragma mark Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
     if (searchModeSegment.selectedSegmentIndex==0) {
         NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
         DLog(@"Nb Section : %d ", [fetchedResultsController.sections count]);
         return [fetchedResultsController.sections count];
     } else {
         return 1;
     }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
    
    if (searchModeSegment.selectedSegmentIndex==0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else {
        return [[fetchedResultsController fetchedObjects] count];
    }
        
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (searchModeSegment.selectedSegmentIndex==0) {
        NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
        id <NSFetchedResultsSectionInfo> sectionInfo = [[fetchedResultsController sections] objectAtIndex:section];
        
        NSString *unnamedLabel = [[ConfigurationManager sharedInstance] getNoNamedCategoryHeaderLabel];
        
        return [[[sectionInfo name] lowercaseString] isEqualToString:@"aucune"]?(unnamedLabel?unnamedLabel:@"Aucune"):[sectionInfo name];
        
    } else {
        return @"Documents géo-localisés";
    }
}
//Custom title view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	
	
	// create the parent view that will hold header Label
	UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 24)];
	customView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	customView.backgroundColor = [[ConfigurationManager sharedInstance] tableViewSectionBackgroundColor];
	
	// create the button object
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
	headerLabel.textColor = [[ConfigurationManager sharedInstance] tableViewSectionForegroundColor];
	headerLabel.highlightedTextColor = [UIColor whiteColor];
	headerLabel.font = [UIFont boldSystemFontOfSize:14];
	headerLabel.frame = CGRectMake(10.0, 0.0, 310.0, 24);
	headerLabel.adjustsFontSizeToFitWidth = YES;
	headerLabel.minimumFontSize = 12.0;
	
	headerLabel.text = [self tableView:tableView titleForHeaderInSection:section];
	
	[customView addSubview:headerLabel];
	
	return customView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return 24.0;
}




// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *MyIdentifier = @"MySearchViewCell";
	
    SearchViewControllerCell *cell = (SearchViewControllerCell *)[self.aTableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (!cell) {
		
        cell = [[SearchViewControllerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
    }
    DLog(@"cell : %@",[cell description]);
	// Configure the cell...
	[self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(SearchViewControllerCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    DLog();
    
    NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	Document  *document = [fetchedResultsController objectAtIndexPath:indexPath];
   	
    DLog(@"document : %@",[document.title description]);
	
	cell.titleLabel.text = [NSString stringWithFormat:@"%@", document.title];
	int duration = [document.duration intValue];
    if(duration==0) cell.titleLabel.numberOfLines = 2;
    else cell.titleLabel.numberOfLines = 1;
    
     DLog(@"cell.titleLabel : %@",[cell.titleLabel description]);
	
	if (document.thumbnail_small == nil) {
		cell.imageView.image = [UIImage imageNamed:@"blank.png"];
        if (document.thumbnail_small_URI != nil || [document.thumbnail_small_URI compare:@""] != NSOrderedSame) {
            [[CoreDataManager sharedInstance] downloadThumbnailForDocument:document];
        }
	} else {
		cell.imageView.image = document.thumbnail_small.image;
	}
	
    
    NSString *cat = [[ConfigurationManager sharedInstance] getNoNamedCategorySearchCellLabel];
    

	cell.categoryLabel.text = [NSString stringWithFormat:@"Catégorie : %@",[[document.category lowercaseString] isEqualToString:@"aucune"]?cat:document.category];

	
	cell.subTitleLabel.textColor = [UIColor grayColor];
		
    if(duration==0){
        cell.subTitleLabel.text = @"";
    }else{
        
        cell.subTitleLabel.text = [NSString stringWithFormat:@"%@:%@",
				  [self number2digit:(int)(duration/60.0)], 
				  [self number2digit:((int)(duration))%60]];
	}
	if ([document.downloading intValue] == 1) {
		if ([document.downloadProgress floatValue] > 0 ) {
			cell.downloadLabel.text = [NSString stringWithFormat:@"Téléchargement en cours : %d %%",(int)([document.downloadProgress floatValue]*100)];
            cell.downloadLabel.textColor = [[ConfigurationManager sharedInstance] downloadProgressInfoColor];
		} else if ([document.downloadSuspended intValue]== 0){
			cell.downloadLabel.text = @"Téléchargement en attente";
            cell.downloadLabel.textColor = [[ConfigurationManager sharedInstance] downloadProgressInfoColor];
		} else {
            cell.downloadLabel.text = @"Téléchargement suspendu";
            cell.downloadLabel.textColor = [[ConfigurationManager sharedInstance] downloadErrorInfoColor];
        }
    } else if ([document.updateAvailable intValue] == 1) {
		cell.downloadLabel.text = @"Mise à jour disponible";
		cell.downloadLabel.textColor = [[ConfigurationManager sharedInstance] downloadErrorInfoColor];
	} else {
		cell.downloadLabel.text = @"";
	}
	
	
	[cell setAccessoryView:[self makeDetailDisclosureButtonWithFrame:CGRectMake(0, 0, 30, 30) andImage:[[ConfigurationManager sharedInstance] getAccessoryImage]]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	Document  *document = [fetchedResultsController objectAtIndexPath:indexPath];
	
	if (([document.downloaded intValue] == 1 || [document.downloading intValue] == 0) && [document.updateAvailable intValue] == 0) {
        return 66.0;
	} else {
		return 86.0;
	}
}


- (NSString*) number2digit:(int) number{
	if (number<10) {
		return [NSString stringWithFormat:@"0%d",number];
	}
	return [NSString stringWithFormat:@"%d",number];
}

- (UIButton *) makeDetailDisclosureButtonWithFrame:(CGRect)rect andImage:(UIImage *)img{
	UIButton *button = [[UIButton alloc] initWithFrame:rect];
	[button setImage:img forState:UIControlStateNormal];
	
	
	[button addTarget: self
			   action: @selector(accessoryButtonTapped:withEvent:)
	 forControlEvents: UIControlEventTouchUpInside];
	
	return ( button );
}

- (void) accessoryButtonTapped: (UIControl *) button withEvent: (UIEvent *) event	{
    
    UITableView * tableView;
    
    if (!self.searchIsActive || searchModeSegment.selectedSegmentIndex == 1 ) {
        tableView = self.aTableView;
    } else {
        tableView = self.searchDisplayController.searchResultsTableView;
    }
        
    
    NSIndexPath * indexPath = [tableView indexPathForRowAtPoint: [[[event touchesForView: button] anyObject] locationInView: tableView]];
    
    if ( indexPath == nil )
		return;
		
	if([tableView.delegate respondsToSelector:@selector(tableView:accessoryButtonTappedForRowWithIndexPath:)])
		[tableView.delegate tableView:tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    DLog();
    
	// Navigation logic may go here. Create and push another view controller.
	
	DocumentDetailViewController *detailViewController = [[DocumentDetailViewController alloc] initWithNibName:@"DefaultDetailViewController" bundle:nil];
	
    NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
		
	Document * document = [fetchedResultsController objectAtIndexPath:indexPath];
	
	[detailViewController setItems:[fetchedResultsController fetchedObjects] withCurrent:[[fetchedResultsController fetchedObjects] indexOfObject:document]];
	
	// Pass the selected object to the new view controller.
	[self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark -
#pragma mark Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	
    self.lastSelectedPath = indexPath;
    
    Document  *document = [[self getCurrentFetchedResultController] objectAtIndexPath:indexPath];
	
    if ([document.updateAvailable intValue] == 1 && [document.downloading intValue] == 0){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Mise à jour" message:@"Une mise à jour est disponible, souhaitez-vous la télécharger maintenant ?" delegate:self cancelButtonTitle:@"Oui" otherButtonTitles:@"Non", nil];
        [alert show];
	} else if ([document.downloading intValue] == 1){
        UIAlertView *alert;
		if ([document.downloadSuspended intValue] == 0) {
            alert = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Document en cours de téléchargement, merci de patienter" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        }
        else {
            alert = [[UIAlertView alloc] initWithTitle:@"Téléchargement" message:@"Téléchargement suspendu, souhaitez-vous le reprendre ?" delegate:self cancelButtonTitle:@"Oui" otherButtonTitles:@"Non", nil];
        }
        
		[alert show];
	} else if ([document.downloaded intValue] == 0 ){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Téléchargement" message:@"Le document n'a pas été téléchargé, souhaitez-vous le télécharger maintenant ?" delegate:self cancelButtonTitle:@"Oui" otherButtonTitles:@"Non", nil];
        [alert show];
	} else {
        [self openMedia:document];
    }
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
    if ([actionSheet.title compare:@"Mise à jour"] == NSOrderedSame || [actionSheet.title compare:@"Téléchargement"] == NSOrderedSame) {
		Document  *document;
        
        document = [fetchedResultsController objectAtIndexPath:self.lastSelectedPath];
        
        
        if (buttonIndex == 0){
            NSString *tmpDirectory = [[[CoreDataManager sharedInstance] requestManager] getTmpDownloadFolder];
            if(document.tmpDownloadFilePath && ![document.tmpDownloadFilePath hasPrefix:tmpDirectory]){
                //Cancel previous tmp download file
                ALog(@"new app version number -> previous tmp file canceled document:%@",document.title);
                document.tmpDownloadFilePath = nil;
            }
            [[CoreDataManager sharedInstance] downloadDocument:document];
		} else if ([actionSheet.title compare:@"Mise à jour"] == NSOrderedSame) {
            [self openMedia:document];
        }
	}
}


- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
    if ([actionSheet.title compare:@"Mise à jour"] == NSOrderedSame || [actionSheet.title compare:@"Téléchargement"] == NSOrderedSame) {
		Document  *document;
        if (searchIsActive) {
            document = [fetchedResultsController objectAtIndexPath:self.lastSelectedPath];
        } else {
            document = [fetchedResultsController objectAtIndexPath:self.lastSelectedPath];
        }
        
        if (buttonIndex == 0){
            NSString *tmpDirectory = [[[CoreDataManager sharedInstance] requestManager] getTmpDownloadFolder];            
            if(document.tmpDownloadFilePath && ![document.tmpDownloadFilePath hasPrefix:tmpDirectory]){
                //Cancel previous tmp download file
                ALog(@"new app version number -> previous tmp file canceled document:%@",document.title);
                document.tmpDownloadFilePath = nil;
            }
            [[CoreDataManager sharedInstance] downloadDocument:document];
		} else if ([actionSheet.title compare:@"Mise à jour"] == NSOrderedSame) {
            [self openMedia:document];
        }
	} 
}


-(void) openMedia:(Document *) document {
    
    [[MediaHandlerManager sharedInstance].mediaHandlerDelegate openDocument:document];
    if(IS_IPAD()){
        DLog();
        
        [self.popover dismissPopoverAnimated:YES];
    }
}

#pragma mark -
#pragma mark ToolBar buttons actions

-(IBAction) downloadAll {
	//[self.dataManager prepareDownloadWithView:self];
	[[CoreDataManager sharedInstance] downloadAllDocumentsForChannel:self.aChannel withCategory:nil];
}

-(IBAction) downloadSelected {
	//[self.dataManager prepareDownloadWithView:self];
	if (![[CoreDataManager sharedInstance] downloadSelectedDocumentsForChannel:self.aChannel withCategory:nil]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Vous n'avez sélectionné aucun élément à télécharger" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
		[alert show];
	}
}




#pragma mark -
#pragma mark Location manager

/**
 Return a location manager -- create one if necessary.
 */
- (CLLocationManager *)locationManager {
    DLog();

    if (locationManager != nil) {
		return locationManager;
	}
	
	locationManager = [[CLLocationManager alloc] init];
	[locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
	[locationManager setDelegate:self];
	
	return locationManager;
}


/**
 Conditionally enable the Add button:
 If the location manager is generating updates, then enable the button;
 If the location manager is failing, then disable the button.
 */
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
	DLog(@"didUpdateToLocation\n");
	[self updateDistances];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
	DLog(@"Update location didFailWithError\n");
}


- (void) updateDistances {
	
	
	CLLocation *location = [self.locationManager location];
	
	if (location) {
        DLog();
        	
		NSFetchedResultsController * allDocumentsFetchedResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllDocuments] 
																						  managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:nil 
																									 cacheName:nil];
	
		NSError *error = nil;
		if (![allDocumentsFetchedResultController performFetch:&error]){
			ALog(@"Unresolved error %@, %@", error, [error userInfo]);
			//abort();
		}
	
		for (Document * document in [allDocumentsFetchedResultController fetchedObjects]) {
			NSNumber *d;
			if (document.location != nil &&  [document.location compare:@""] != NSOrderedSame) {
				
				NSArray * locationComponent = [document.location componentsSeparatedByString:@","]; 
				
				DLog(@"docLocation lat : %f long : %f",[[locationComponent objectAtIndex:0] floatValue],[[locationComponent objectAtIndex:1] floatValue]);
			
				CLLocation *docLoc = [[CLLocation alloc] initWithLatitude:[[locationComponent objectAtIndex:0] floatValue] longitude: [[locationComponent objectAtIndex:1] floatValue]];
			
				CLLocationDistance dist =  [location distanceFromLocation:docLoc];//meters
						
				d = [[NSNumber alloc] initWithDouble:dist];
				document.distanceFromLocation = d;
			
			}
			else {
				d = [[NSNumber alloc] initWithInt:-1];
				document.distanceFromLocation = d;
			}
		}
		
	}
}


#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    self.textFilteredFetchedResultsController.delegate = nil;
	self.textFilteredFetchedResultsController = nil;
    self.textFilteredFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[[CoreDataManager sharedInstance] fetchRequestAllDocumentsForKeyword:searchText]
																		managedObjectContext:[CoreDataManager sharedInstance].managedObjectContext sectionNameKeyPath:@"channel.title" 
																				   cacheName:nil];
	

    self.textFilteredFetchedResultsController.delegate = self;
	
    NSError *error = nil;
	if (![self.textFilteredFetchedResultsController performFetch:&error]){
	    ALog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    searchIsActive = YES;
}



#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:
	 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:
	  [self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
	
    return YES;
}


- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
	 [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
	
    return YES;
}

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller {
	[self.searchModeSegment setEnabled:NO];
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller {
    searchIsActive = NO;
    [self.aTableView reloadData];
	[self.searchModeSegment setEnabled:YES];
}


#pragma mark -
#pragma mark - Fetched results controller delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	if (controller == fetchedResultsController && !searchIsActive) {
        DLog();
        
		[self.aTableView beginUpdates];
	} 
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    
	NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	UITableView *tableView;
	if (controller == fetchedResultsController && !searchIsActive) {
		tableView = self.aTableView;
        DLog();
        
		switch(type)
		{
			case NSFetchedResultsChangeInsert:
				[tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
				break;
            
			case NSFetchedResultsChangeDelete:
				[tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
				break;
		}
	}
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    
	NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	UITableView *tableView;
	if (controller == fetchedResultsController && !searchIsActive) {
		tableView = self.aTableView;
		switch(type)
		{
            
			case NSFetchedResultsChangeInsert:
				[tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
				break;
				
			case NSFetchedResultsChangeDelete:
				[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
				break;
            
			case NSFetchedResultsChangeUpdate:
				[self configureCell:(SearchViewControllerCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
				break;
            
			case NSFetchedResultsChangeMove:
				[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
				[tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
				break;
		}
	}
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    NSFetchedResultsController * fetchedResultsController = [self getCurrentFetchedResultController];
	UITableView *tableView;
	if (controller == fetchedResultsController && !searchIsActive) {
		tableView = self.aTableView;
        DLog();
        
		[tableView endUpdates];
	} else if (searchIsActive) {
		[self.searchDisplayController.searchResultsTableView reloadData];
	}
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    searchView = nil;
    [self setSearchView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end
