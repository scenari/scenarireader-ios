//
//  SearchViewController.h
//  OptimGenericReader
//
//  Created by soreha on 26/02/11.
//  
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MediaHandlerDelegate.h"
#import "DefaultDocumentsTableViewController.h"

@class Channel, Document, SearchViewControllerCell;

@interface SearchViewController : UIViewController <UIActionSheetDelegate,NSFetchedResultsControllerDelegate, UISearchDisplayDelegate, UISearchBarDelegate, CLLocationManagerDelegate>{

    
	
	CLLocationManager *locationManager;
}
@property (nonatomic, strong) IBOutlet UITableView * aTableView;

@property (nonatomic, strong) NSIndexPath *lastSelectedPath;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (nonatomic, strong) NSFetchedResultsController *defaultFetchedResultsController;
@property (nonatomic, strong) NSFetchedResultsController *textFilteredFetchedResultsController;
@property (nonatomic, strong) NSFetchedResultsController *distanceFilteredFetchedResultsController;
@property (nonatomic, strong) Channel * aChannel;
@property (nonatomic, strong) IBOutlet UILabel *searchModeInfo;
@property (nonatomic, strong) IBOutlet UILabel *distanceLabel;
@property (nonatomic, strong) IBOutlet UISlider *distanceSlider;
@property (nonatomic, strong) IBOutlet UISegmentedControl *searchModeSegment;
@property (nonatomic, strong) IBOutlet UISearchDisplayController *searchDisplayController;
@property (nonatomic, strong) UITableViewCell *myCell;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic) BOOL searchIsActive;

@property (nonatomic, strong) NSArray *distanceValues;
@property (nonatomic, strong) UIPopoverController *popover;

- (void) initDataManager : (id) dManager;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (NSString*) number2digit:(int) number;
- (UIButton *) makeDetailDisclosureButtonWithFrame:(CGRect)rect andImage:(UIImage *)img;
- (IBAction) updateSearchMode;
- (IBAction) sliderValueChanged:(id)sender;
- (void) updateDistances;
- (void) initDefaultFetchedResultController;
- (void) initDistanceFetchedResultController;
- (NSFetchedResultsController *) getCurrentFetchedResultController;
- (void) openMedia:(Document *) document;

-(IBAction) downloadAll;
-(IBAction) downloadSelected;

@end
