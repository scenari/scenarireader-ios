//
//  ExtrasTableViewController.h
//  webmedia
//
//  Created by soreha on 07/01/13.
//  
//

#import <UIKit/UIKit.h>

@class RichMedia;

@interface ExtrasTableViewController : UITableViewController

@property(nonatomic, retain) RichMedia *richMedia;
@property(nonatomic, strong) UIPopoverController *popover;

@end
