//
//  Thumbnail.m
//  OptimGenericReader
//
//  Created by soreha on 14/02/11.
//  
//

#import "Thumbnail.h"


@implementation Thumbnail
@dynamic image;
@dynamic url;
@dynamic format;

@end
