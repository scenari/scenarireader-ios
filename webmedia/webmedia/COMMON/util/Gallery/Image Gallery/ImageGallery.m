//
//  ImageGallery.m
//  WebMedia
//
//  Created by soreha on 22/02/11.
//  
//

#import "ImageGallery.h"

#import "ImageUnit.h"


@implementation ImageGallery

@synthesize images, name, identifiant, urlRelativeTo;


-(id) init {
	
    self = [super init];
    if(self){
		self.images = [[NSMutableArray alloc] initWithCapacity:0];
	}
	return self;
}

-(id) initWithSingleImage:(ImageUnit*)img {
	self = [super init];
    if(self){
		self.images = [[NSMutableArray alloc] initWithCapacity:1];
	}
	[self.images addObject:img];
	return self;
}

-(ImageUnit*) imageAt:(int)index{
	return [images objectAtIndex:index];
}
-(int) imagesCount{
	return [images count];
}


@end
