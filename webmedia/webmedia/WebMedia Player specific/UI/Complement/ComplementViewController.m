//
//  ComplementViewController.m
//  WebMedia
//
//  Created by soreha on 20/12/10.
//  
//

#import "ComplementViewController.h"
#import "RichMedia.h"
#import "Segment.h"
#import "Extras.h"

#import "Chapter.h"
#import "Complement.h"
#import "ImageUnit.h"
#import "MyBrowser.h"
#import "ComplementView.h"

#import "ConfigurationManager.h"
#import "Resource.h"

#define kCustomButtonHeight		30.0


@interface ComplementViewController (PrivateMethods)
-(void) loadCurrentComplement;
-(void) segmentAction;
-(void) showEmailModalView:(NSString *)to subject:(NSString *)subject body:(NSString *)body;

@end


@implementation ComplementViewController
@synthesize content;
//@synthesize complementTitleLabel;
@synthesize richMedia;

int currentComplement = 0;
int complementsCount=1;
int indexForChapter=0;
int indexForSegment=0;

BOOL inExtra = NO;
int currentExtra = 0;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (void)initContent{
    DLog();
    
    
    content.delegate = self;
	UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRightAction:)];
    
	swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
	swipeRight.delegate = self;
	[content addGestureRecognizer:swipeRight];
	
	UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftAction:)];
	swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
	swipeLeft.delegate = self;
	[content addGestureRecognizer:swipeLeft];
	//currentComplement = 0;
	
	
	//[self loadCurrentComplement];
	
	
	UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:
											[NSArray arrayWithObjects:
											 [UIImage imageNamed:@"up"],
											 [UIImage imageNamed:@"down"],
											 nil]];
	
	[segmentedControl addTarget:self action:@selector(segmentAction) forControlEvents:UIControlEventValueChanged];
	segmentedControl.frame = CGRectMake(0, 0, 80, kCustomButtonHeight);
	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    
	segmentedControl.momentary = YES;
	
	
	
	UIBarButtonItem *segmentBarItem = [[UIBarButtonItem alloc] initWithCustomView:segmentedControl];
    
	self.navigationItem.rightBarButtonItem = segmentBarItem;
    
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    DLog();
	[super viewDidLoad];
	if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        self.navigationItem.rightBarButtonItem.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
    }else{
        self.topShadowView.hidden = YES;
    }
    self.contentSizeForViewInPopover = CGSizeMake(350, 410);
}

//current segment is used to load complements



- (void)handleComplementCount:(int)index{
    if (complementsCount>1) {
		self.title = [NSString stringWithFormat:@"%@ (%d/%d)",[[ConfigurationManager sharedInstance] singleComplementString],index, complementsCount];
	}else {
		self.title = [NSString stringWithFormat:@"%@",[[ConfigurationManager sharedInstance] singleComplementString]] ;
        self.navigationItem.rightBarButtonItem = nil;
    }
}
-(id) initForExtrasWithRichMedia:(RichMedia*)media{
    DLog();
    self = [super initWithNibName:@"ComplementView" bundle:nil];
    if (self) {
        // Custom initialization.
    }
    [self initContent];
	self.richMedia = media;
    self.content.delegate = self;
	complementsCount = 0;
    inExtra = YES;
    self.title = @"Extra" ;
    self.navigationItem.rightBarButtonItem = nil;
    
	return self;

}

-(id) initWithRichMedia:(RichMedia*)media{
	
	self = [super initWithNibName:@"ComplementView" bundle:nil];
    if (self) {
        // Custom initialization.
    }
    [self initContent];
	self.richMedia = media;
    inExtra = NO;
	indexForChapter=self.richMedia.currentChapter;
	indexForSegment=self.richMedia.currentSegmentInChapter;
	Chapter *chap = [self.richMedia.chapters objectAtIndex:indexForChapter];
	Segment *segment = [chap.segments objectAtIndex:indexForSegment];
	
	complementsCount = [segment.complements count];
	
    [self handleComplementCount:1];

	return self;
}
-(id) initWithRichMedia:(RichMedia*)media chapterIndex:(int)cindex andSegmentIndex:(int)sindex{
	self = [super initWithNibName:@"ComplementView" bundle:nil];
    if (self) {
        // Custom initialization.
    }
    [self initContent];
	self.richMedia = media;
    inExtra = NO;

	indexForChapter=cindex;
	indexForSegment=sindex;
	
	Chapter *chap = [self.richMedia.chapters objectAtIndex:indexForChapter];
	Segment *segment = [chap.segments objectAtIndex:indexForSegment];
	
	complementsCount = [segment.complements count];
    [self handleComplementCount:1];

	
	return self;
	
}


-(void) showExtra:(int)index{
    DLog(@"index=%d",index);
    if([self.richMedia.extras count]==0 || index>=[self.richMedia.extras count]){
        
        DLog(@"[self.richMedia.extras count]==0 || index>=[self.richMedia.extras count]");
        
        return;
    }
    currentExtra = index;
    Extras *extra = [self.richMedia.extras objectAtIndex:index];
	NSMutableString *path = [NSMutableString stringWithString:[self.richMedia.base stringByAppendingPathComponent:extra.refUri]];
	
	[path replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [path length])];
	
	//DLog(@"%@",[path description]);
	
    DLog(@"path=%@",path);
    
	NSURLRequest *rq = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]];
    self.content.delegate = self;
	[self.content loadRequest:rq];
	
}


-(void) loadCurrentComplement{
	
    Chapter *chap = [self.richMedia.chapters objectAtIndex:indexForChapter];
	Segment *segment = [chap.segments objectAtIndex:indexForSegment];
	if(currentComplement>=[segment.complements count]) return;
    Complement *comp = [segment.complements objectAtIndex:currentComplement];
	//complementTitleLabel.text = comp.title; 
	NSMutableString *path = [NSMutableString stringWithString:[self.richMedia.base stringByAppendingPathComponent:comp.refUri]];
	
	[path replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [path length])];
	
	//DLog(@"%@",[path description]);
	
	NSURLRequest *rq = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]];
	[content loadRequest:rq];
	
}


- (void)swipeRightAction:(id)ignored{
	//DLog(@"Swipe Right");
	if (complementsCount==1) {
		return;
	}
	currentComplement--;
	if (currentComplement<0) {
		currentComplement=complementsCount-1;
	}
    
    [self handleComplementCount:currentComplement+1];
    
	[self loadCurrentComplement];
	
	
}

-(void) showComplement:(int)index{
	if (index<0 || index>=complementsCount) {
		return;
	}
	currentComplement = index;
    
    [self handleComplementCount:currentComplement+1];
    
	[self loadCurrentComplement];

}

- (void)swipeLeftAction:(id)ignored{
	//DLog(@"Swipe Left");
	if (complementsCount==1) {
		return;
	}
	currentComplement++;
	if (currentComplement>=complementsCount) {
		currentComplement=0;
	}
    
    [self handleComplementCount:currentComplement+1];
    
	[self loadCurrentComplement];
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self.content reload];

    if(self.complementView){
        [self.complementView updateOrientation: [[UIDevice currentDevice] orientation] duration:0.2];
    }
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated{
    DLog();
    [super viewWillAppear:animated];
    self.content.delegate = self;
    if(inExtra){
        [self showExtra:currentExtra];
    }else{
        [self showComplement:currentComplement];
        
    }
    UISegmentedControl *segmentedControl = (UISegmentedControl *)self.navigationItem.rightBarButtonItem.customView;
	
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        // Before we show this view make sure the segmentedControl matches the nav bar style
        if (self.navigationController.navigationBar.barStyle == UIBarStyleBlackTranslucent ||
            self.navigationController.navigationBar.barStyle == UIBarStyleBlackOpaque)
            segmentedControl.tintColor = [UIColor darkGrayColor];
        else
            segmentedControl.tintColor = self.navigationController.navigationBar.tintColor;
	}
		
	[segmentedControl setEnabled:currentComplement>0 forSegmentAtIndex:0];
	
		
	[segmentedControl setEnabled:(currentComplement+1<complementsCount) forSegmentAtIndex:1];
    if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
        segmentedControl.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
    }
	[self.view setNeedsLayout];
    
    
   
    
	
}


- (int)numberOfPhotosForPhotoGallery:(FGalleryViewController*)gallery{
    //Chapter *chap = [self.richMedia.chapters objectAtIndex:indexForChapter];
    //Segment *segment = [chap.segments objectAtIndex:indexForSegment];
    //Complement *comp = [segment.complements objectAtIndex:currentComplement];
    
    //DLog(@"nb images: %d",[[self.gallery images] count]);
    
    return [[self.gallery images] count];
}
- (FGalleryPhotoSourceType)photoGallery:(FGalleryViewController*)gallery sourceTypeForPhotoAtIndex:(NSUInteger)index{
    return FGalleryPhotoSourceTypeLocal;
}


//file:///Users/emmanuelbeaufort/Library/Application Support/iPhone Simulator/6.1/Applications/A3E8D188-F1F1-49D9-957B-DB77BD284A51/Documents/medias/519019601/res/InaGrm00361.jpg
//Users/emmanuelbeaufort/Library/Application Support/iPhone Simulator/6.1/Applications/A3E8D188-F1F1-49D9-957B-DB77BD284A51/Documents/medias/1865468389//res/aria-strumentisti.jpg
- (NSString*)photoGallery:(FGalleryViewController*)gallery filePathForPhotoSize:(FGalleryPhotoSize)size atIndex:(NSUInteger)index {
   
    NSMutableString *imageName;
    if(self.gallery.urlRelativeTo){
        imageName =  [NSMutableString stringWithFormat:@"%@/%@", self.gallery.urlRelativeTo, [[self.gallery imageAt:index] refUri] ];
    }else{
        imageName =  [NSMutableString stringWithFormat:@"%@", [[self.gallery imageAt:index] refUri] ];
        if([imageName hasPrefix:@"file://"]){
            imageName = [NSMutableString stringWithString:[imageName substringFromIndex:7]];
        }
    }
    [imageName replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [imageName length])];
    
    //DLog(@"index: %d, imageName: %@ ", index, imageName);
    
    return imageName;
}
- (NSString*)photoGallery:(FGalleryViewController *)gallery captionForPhotoAtIndex:(NSUInteger)index
{
    //DLog(@"index: %d, caption: %@ ", index, [[self.gallery imageAt:index] imageTitle]);
    id image = [self.gallery imageAt:index];
    DLog(@"image : %@",[image description]);
    if([image respondsToSelector:@selector(imageTitle)]){
        return [image imageTitle];
    }
    return [image title];
}

- (NSString *)stringByDecodingURLFormat:(NSString *)aString{
    NSString *result = [aString stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    result = [result stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return result;
}

- (BOOL)isLocalImageLink:(NSString *)requestString{
    NSString *req = [requestString lowercaseString];
    return [req hasSuffix:@".jpg"] || [req hasSuffix:@".jpeg"] || [req hasSuffix:@".gif"] || [req hasSuffix:@".png"];
}

- (BOOL)presentedInPopoverController{
    for (UIView *v = self.view; v.superview != nil; v=v.superview) {
        if ([v isKindOfClass:[NSClassFromString(@"_UIPopoverView") class]]) {
            DLog(@"\n\n\nIM IN A POPOVER!\n\n\n\n");
            return YES;
        }
    }
    return NO;
}
- (void)printParentController{
    for (UIViewController *v = self; v.presentingViewController != nil; v=v.presentingViewController) {
            DLog(@"parent : %@",v);
    }
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	
	NSString *requestString = [[request URL] absoluteString];
    DLog(@"requestString:%@",requestString);
		
	//Gestion des gleries d'images
	if ( requestString  && 
		([[requestString lowercaseString] hasPrefix:@"opengallery:"] || [[requestString lowercaseString] hasPrefix:@"openimage:"]) ){
		
		NSArray *components = [requestString componentsSeparatedByString:@":"];
		
		Chapter *chap = [self.richMedia.chapters objectAtIndex:indexForChapter];
		Segment *segment = [chap.segments objectAtIndex:indexForSegment];
		
        Complement *comp = nil;
        Extras *extra = nil;
        if(inExtra){
            extra = [self.richMedia.extras objectAtIndex:currentExtra];
        }else{
            comp = [segment.complements objectAtIndex:currentComplement];
        }
                
		
		//get the relative path
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentDirectory = [paths objectAtIndex:0];
		
        NSString *bookId = self.richMedia.base;
		NSString *bookDirectory=bookId;
        
        DLog(@"documentDirectory:[[%@]]",documentDirectory);

        DLog(@"bookId:[[%@]]",bookId);

        DLog(@"bookDirectory:[[%@]]",bookDirectory);
		
		//Gallery
		if([[requestString lowercaseString]  hasPrefix:@"opengallery:"]){
			int index = [[components objectAtIndex:1] intValue]; 
			
			self.gallery = inExtra?[[extra galleries] objectAtIndex:index]:[[comp galleries] objectAtIndex:index];
			NSMutableString *path = [NSMutableString stringWithString:self.richMedia.base];
			[path replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [path length])];
			if([path hasSuffix:@"/"])
				self.gallery .urlRelativeTo= [path substringToIndex:[path length]-1];
			else {
				self.gallery .urlRelativeTo= path ;
			}
			
			if ([[self.gallery images] count]!=0) {
				
                if(!self.localGallery)
                    self.localGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
                [self.localGallery setUseThumbnailView:[[self.gallery images] count]>1];

                if(IS_IPAD()){
                    UINavigationController *tmpController = [[UINavigationController alloc] initWithRootViewController:self.localGallery];
                    tmpController.navigationBar.barStyle = UIBarStyleBlack;
                    tmpController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
                    
                    UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closeGallery:)];
                    self.localGallery.navigationItem.leftBarButtonItem = close;
                    tmpController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
                    [tmpController setModalPresentationStyle:UIModalPresentationFullScreen];
                    
                    if([self presentedInPopoverController] &&  ([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending)){
                        //[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
                    }
                    
                    [self presentViewController:tmpController animated:YES completion:NULL];
                }else{
                    [self.navigationController pushViewController:self.localGallery animated:YES];
                }
                return NO;
                
			}else {// LA GALERIE EST VIDE
				UIAlertView *alert = [[UIAlertView alloc]
									  initWithTitle:@"La galerie est vide" 
									  message:@"Son contenu sera disponible lors d'une prochaine mise à jour."
									  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
				
				[alert show];
			}
			
		}else {//image
			NSString *imgUri =  [[components objectAtIndex:1] substringFromIndex:3];
            
            DLog(@"imgUri:%@",imgUri);
			
            NSMutableString *imgT ;
			if([components count]==3){
				imgT=[NSMutableString stringWithString:[self stringByDecodingURLFormat:[components objectAtIndex:2]]];
				//[imgT replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [imgT length])];
                //[imgT replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [imgT length])];
			} 
			else imgT=[NSMutableString stringWithString:@""];
			
			if(imgUri){
				ImageUnit *unit = [[ImageUnit alloc] initWithUri:imgUri title:imgT];
				
				self.gallery = [[ImageGallery alloc] initWithSingleImage:unit];
				self.gallery.urlRelativeTo = bookDirectory;
				
                FGalleryViewController *localGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
                [localGallery setUseThumbnailView:NO];
                if(IS_IPAD()){
                    UINavigationController *tmpController = [[UINavigationController alloc] initWithRootViewController:localGallery];
                    tmpController.navigationBar.barStyle = UIBarStyleBlack;
                    tmpController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
                    UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closeGallery:)];
                    localGallery.navigationItem.leftBarButtonItem = close;
                    tmpController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
                    [tmpController setModalPresentationStyle:UIModalPresentationFullScreen];
                    
                    if([self presentedInPopoverController] &&  ([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending)){
                        //[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
                    }
                    
                    [self presentViewController:tmpController animated:YES completion:NULL];
                }else{
                    
                    [self.navigationController pushViewController:localGallery animated:YES];
                }
				return NO;
			}else {// L'image HD n'existe pas
				UIAlertView *alert = [[UIAlertView alloc]
									  initWithTitle:@"L'image haute définition n'a pu être trouvée" 
									  message:@"Elle sera disponible lors d'une prochaine mise à jour."
									  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
				
				[alert show];
			}
			
		}
		
		
	}else if ( requestString  && [[requestString lowercaseString] hasPrefix:@"openextra:"]){
		
        
		NSArray *components = [requestString componentsSeparatedByString:@":"];
		
		if([components count]<3)return NO;
        
		NSString *uriBlocString = [components objectAtIndex:1] ;
		
		NSMutableString *complementName = [NSMutableString stringWithString:[components objectAtIndex:2]];
		for (int i=3, n=[components count]; i<n; i++) {
			[complementName appendString:[NSMutableString stringWithFormat:@":%@",[components objectAtIndex:i]]];
		}
		self.complementView = nil;
		if(self.complementView ==nil){
			
            
			self.complementView = [[ComplementView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
			UIWindow *tmpWindow = [[UIApplication sharedApplication].windows objectAtIndex:0];
			
			[tmpWindow addSubview:self.complementView];
			
			self.complementView.content.delegate = self;
            
		}
		
		[self.complementView setTitle:complementName];
		
		NSURLRequest *rq = [NSURLRequest requestWithURL:
							[NSURL URLWithString:uriBlocString relativeToURL:[self.content.request URL]]];
		
		[self.complementView urlRequestBlocString:rq];
		[self.complementView openAction];
        
		return NO;
	}
	//GESTION LIEN EXTERNES
	else if ( requestString  && [requestString hasPrefix:@"http://"]){
		if(self.complementView && self.complementView.content==webView){
			[self.complementView closeAction];
		}
        MyBrowser *myBrowser = [[MyBrowser alloc] initWithNibName:(IS_IPAD()?@"MyBrowser_iPad":@"MyBrowser_iPhone") bundle:nil];
		
		UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:nil action:nil];
		self.navigationItem.backBarButtonItem = backButton;
		
		[self.navigationController pushViewController:myBrowser animated:YES];
		[myBrowser showURL:request];
		
        
		
		return NO;
	}
	else if( requestString  && [requestString hasPrefix:@"mailto:"]){
		if(self.complementView && self.complementView.content==webView){
			[self.complementView closeAction];
		}
		NSArray *components = [requestString componentsSeparatedByString:@":"];
		
		[self showEmailModalView:[components objectAtIndex:1] subject:kEmailSubject body:kEmailBody];		
		return NO;
		
	}
	else if([self isLocalImageLink:requestString]){
        //Gestion d'un lien vers une image locale
        
            ImageUnit *unit = [[ImageUnit alloc] initWithUri:requestString title:@""];
            
            self.gallery = [[ImageGallery alloc] initWithSingleImage:unit];
            self.gallery.urlRelativeTo = nil;
            
            FGalleryViewController *localGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
            [localGallery setUseThumbnailView:NO];
            if(IS_IPAD()){
                UINavigationController *tmpController = [[UINavigationController alloc] initWithRootViewController:localGallery];
                tmpController.navigationBar.barStyle = UIBarStyleBlack;
                tmpController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
                UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closeGallery:)];
                localGallery.navigationItem.leftBarButtonItem = close;
                tmpController.navigationBar.tintColor = [[ConfigurationManager sharedInstance] navigationBarTintColor];
                [tmpController setModalPresentationStyle:UIModalPresentationFullScreen];
                
                if([self presentedInPopoverController] &&  ([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending)){
                    //[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
                }
                
                [self presentViewController:tmpController animated:YES completion:NULL];
                
                
            }else{
                
                [self.navigationController pushViewController:localGallery animated:YES];
            }
            return NO;
                
        
        return NO;
    }
	
	return YES;
	
	
	//return YES; // Return YES to make sure regular navigation works as expected.
}

- (void)closeGallery:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

-(void) showEmailModalView:(NSString *)to subject:(NSString *)subject body:(NSString *)body {
	if (![MFMailComposeViewController canSendMail]) {
		return;
	}
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;// &lt;- very important step if you want feedbacks on what the user did with your email sheet
	picker.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
	[picker setSubject:subject];
	[picker setToRecipients:[NSArray arrayWithObjects:to,nil]];
	
	// Fill out the email body text
	
	
	[picker setMessageBody:body isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
	
	//picker.navigationBar.barStyle = UIBarStyleBlack; // choose your style, unfortunately, Translucent colors behave quirky.
	
	//[self presentModalViewController:picker animated:YES];
    [self presentViewController:picker animated:YES completion:NULL];

}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{ 
	// Notifies users about errors associated with the interface
	switch (result){
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
		default:{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Erreur lors de l'envoi du mail"
														   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
		}
			
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

-(void) segmentAction{
	UISegmentedControl *segmentedControl = (UISegmentedControl *)self.navigationItem.rightBarButtonItem.customView;
	if(segmentedControl.selectedSegmentIndex == 0){
		[self swipeRightAction:nil];
	}
	else {
		[self swipeLeftAction:nil];
	}
	[self viewWillAppear:NO];
	
}


@end
