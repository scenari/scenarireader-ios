//
//  ChannelDetailViewController.m
//  OptimGenericReader
//
//  Created by Mory Doukouré on 24/02/11.
//  Copyright 2011 Soreha. All rights reserved.
//

#import "ChannelDetailViewController.h"
#import "Channel.h"

@implementation ChannelDetailViewController


- (void)viewWillAppear:(BOOL)animated{
	UISegmentedControl *segmentedControl = (UISegmentedControl *)self.navigationItem.rightBarButtonItem.customView;

    
	// Before we show this view make sure the segmentedControl matches the nav bar style
	if (self.navigationController.navigationBar.barStyle == UIBarStyleBlackTranslucent ||
		self.navigationController.navigationBar.barStyle == UIBarStyleBlackOpaque)
		segmentedControl.tintColor = [UIColor darkGrayColor];
	else
		segmentedControl.tintColor = defaultTintColor;
	
    if(IS_IPAD()){
        self.navigationItem.rightBarButtonItem = nil;
    }
#pragma ACCESSIBILITY
   	self.navigationItem.leftBarButtonItem.accessibilityLabel = @"Retour à la liste des sources.";


    
	Channel *mediaToShow = [self.itemsList objectAtIndex:currentItem];
	
	
	NSString  * modelForDescriptionPage = [NSString 
								   stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource :@"ChannelDetailTemplate" ofType:@"html"] 
								   encoding:NSUTF8StringEncoding 
								   error:nil];

	
	
	
	NSMutableString *desc=[NSMutableString stringWithString:modelForDescriptionPage];
	
	[desc replaceOccurrencesOfString:@"CHANNEL_TITLE" 
						  withString:([mediaToShow title]?[mediaToShow title]:@"-") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
    /*
    NSData *imageData = UIImagePNGRepresentation(currentDocument.thumbnail_large.image);
    NSString *encodedString = [self encodeBase64WithData:imageData];
    
    [desc replaceOccurrencesOfString:@"QRCODE"
						  withString:encodedString
							 options:NSCaseInsensitiveSearch
							   range:NSMakeRange(0, [desc length]) ];
     */
	[desc replaceOccurrencesOfString:@"CHANNEL_LINK" 
						  withString:([mediaToShow link]?[mediaToShow link]:@"") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
	
	[desc replaceOccurrencesOfString:@"CHANNEL_SUMMARY" 
						  withString:([mediaToShow summary]?[mediaToShow summary]:@"-") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
	[desc replaceOccurrencesOfString:@"CHANNEL_COPYRIGHT" 
						  withString:([mediaToShow copyright]?[mediaToShow copyright]:@"-") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
	
	[desc replaceOccurrencesOfString:@"CHANNEL_PUBDATE" 
						  withString:([mediaToShow pubDate]?[formatter stringFromDate:[mediaToShow pubDate]]:@"-") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
	[desc replaceOccurrencesOfString:@"CHANNEL_LASTBUIDDATE" 
						  withString:([mediaToShow lastBuildDate]?[formatter stringFromDate:[mediaToShow lastBuildDate]]:@"-") 
							 options:NSCaseInsensitiveSearch 
							   range:NSMakeRange(0, [desc length]) ];
	
	
	
	
	NSString *path = [[NSBundle mainBundle] pathForResource :@"ChannelDetailTemplate" ofType:@"html"];
	NSURL *baseURL = [NSURL fileURLWithPath:path];
	
	
	[self.content loadHTMLString:desc baseURL:baseURL ];
	
	
	[segmentedControl setEnabled:(self.currentItem!=0) forSegmentAtIndex:0];
	[segmentedControl setEnabled:(self.currentItem<([itemsList count]-1)) forSegmentAtIndex:1];
	segmentedControl.tintColor = self.navigationController.navigationBar.tintColor;
    


}



@end
